﻿using PostSharp.Aspects;
using ProjectX.General;
using ProjectX.Infrastructure.DependencyInjection;
using ProjectX.Infrastructure.ExceptionHandlers;
using ProjectX.Infrastructure.LogAttributes.Helpers;
using ProjectX.Infrastructure.LogService;
using System;

#region Source
//Source: http://doc.postsharp.net/method-decorator
#endregion

namespace ProjectX.Infrastructure.LogAttributes
{
    /// <summary>
    /// Method decorator: Intercepting attribute for an individual method.
    /// <para>No need to implement a try/catch, only if customized exception handling is needed within the method.</para>
    /// <para>ATTENTION!</para>
    /// <para>Rethrow exception when a try/catch is implemented, to ensure that the exception is logged properly!</para>
    /// <para>Extended with the possibility to exit the application on a fatal error by throwing a custom fatal exception.</para>
    /// </summary>
    [Serializable]
    [LinesOfCodeAvoided(10)]
    public class LogAttribute : OnMethodBoundaryAspect
    {
        #region Properties
        public bool LogOnEntry { get; }
        public bool LogOnSuccess { get; }
        public bool LogOnExit { get; }
        public bool ShowMessage { get; }
        #endregion

        #region Constructors
        public LogAttribute(bool logOnEntry = false, bool logOnSuccess = false, bool logOnExit = false, bool showMessage = true)
        {
            this.LogOnEntry = logOnEntry;
            this.LogOnSuccess = logOnSuccess;
            this.LogOnExit = logOnExit;
            this.ShowMessage = showMessage;
        }
        #endregion

        #region Methods
        public override void OnEntry(MethodExecutionArgs args)
        {
            if (this.LogOnEntry) { this.LogData(args, LogLevel.OnEntry); }
        }

        public override void OnSuccess(MethodExecutionArgs args)
        {
            if (this.LogOnSuccess) { this.LogData(args, LogLevel.OnSuccess); }
            //TODO: is this correct??? return boolean true when the method returnvalue is not a boolean?
            // -> CHECK it OUT! + TEST on all [Log]
            //args.ReturnValue = true;
            args.ReturnValue = args.ReturnValue ?? true;
        }

        public override void OnException(MethodExecutionArgs args)
        {
            this.HandleException(args, args.Exception);
            args.ReturnValue = false;
            var type = args.Exception.GetType();
            //if (type == typeof(System.Data.SqlClient.SqlException))
            //{
            //    return;
            //}
            //TODO: ????? + System.Data.SqlClient.SqlException????
            //TODO: reset?
            //if (type == typeof(FatalException) || type == typeof(FatalSqlException))
            //{
            //    args.FlowBehavior = FlowBehavior.RethrowException;
            //}
            //if (type == typeof(System.Data.SqlClient.SqlException))
            //{
            //    args.ReturnValue = true;
            //    args.FlowBehavior = FlowBehavior.Return;
            //}
            //else
            //{
            args.FlowBehavior = FlowBehavior.Continue;
            //}
        }

        public override void OnExit(MethodExecutionArgs args)
        {
            if (this.LogOnExit) { this.LogData(args, LogLevel.OnExit); }
        }
        #endregion

        #region Methods: Helpers
        private void LogData(MethodExecutionArgs args, LogLevel logLevel)
        {
            using (var dependency = new DI<ILogger>(ApplicationDependencies.LoggerTypes))
            {
                var logger = dependency.Instance;
                if (logger != null)
                {
                    var information = args.GetLogInformation();
                    logger.Log(logLevel, information);
                }
            }
        }

        private void HandleException(MethodExecutionArgs args, Exception exception)
        {
            using (var dependency = new DI<IExceptionHandler>(ApplicationDependencies.ExceptionHandlerTypes))
            {
                var handler = dependency.Instance;
                if (handler != null)
                {
                    var information = args.GetLogInformation();
                    handler.Handle(information, exception, this.ShowMessage);
                }
            }
        }
        #endregion
    }
}
