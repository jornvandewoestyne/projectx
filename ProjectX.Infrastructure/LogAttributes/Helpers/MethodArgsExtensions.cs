﻿using PostSharp.Aspects;
using ProjectX.Infrastructure.LogService;
using System.Linq;
using System.Reflection;

namespace ProjectX.Infrastructure.LogAttributes.Helpers
{
    public static class MethodArgsExtensions
    {
        #region Methods
        public static T GetArgumentValue<T>(this MethodInterceptionArgs args) //where T : struct
        {
            var parameters = args.Method.GetParameters();
            var arguments = args.Arguments;
            for (int i = 0; i < arguments.Count(); i++)
            {
                var param = parameters[i];
                var arg = arguments[i];
                if (param.ParameterType == typeof(T))
                {
                    return (T)arg;
                }
            }
            return default(T);
        }

        internal static LogInformation GetLogInformation(this MethodInterceptionArgs args)
        {
            var obj = new MethodArgs(args.Instance, args.Method, args.Arguments);
            return obj.GetLogInformation();
        }

        internal static LogInformation GetLogInformation(this MethodExecutionArgs args)
        {
            var obj = new MethodArgs(args.Instance, args.Method, args.Arguments);
            return obj.GetLogInformation();
        }
        #endregion

        #region Methods: Helpers
        private static LogInformation GetLogInformation(this MethodArgs args)
        {
            var assembly = args.GetAssembly();
            var qualifiedName = args.GetAssemblyQualifiedName();
            var methodInfo = args.Method as MethodInfo;
            var arguments = args.GetArguments();
            //TODO: interceptor
            return new LogInformation("LogAttribute", assembly, qualifiedName, methodInfo, arguments);
        }

        private static Assembly GetAssembly(this MethodArgs args)
        {
            Assembly result = null;
            if (args.Instance == null) { result = args.Method.ReflectedType.Assembly; }
            else { result = args.Instance.GetType().Assembly; }
            return result;
        }

        private static string GetAssemblyQualifiedName(this MethodArgs args)
        {
            string result = null;
            if (args.Instance == null) { result = args.Method.ReflectedType.AssemblyQualifiedName; }
            else { result = args.Instance.GetType().AssemblyQualifiedName; }
            return result;
        }

        private static object[] GetArguments(this MethodArgs args)
        {
            var arguments = new object[args.Arguments.Count];
            for (int i = 0; i < args.Arguments.Count; i++)
            {
                arguments[i] = args.Arguments[i];
            }
            return arguments;
        }
        #endregion
    }
}
