﻿using PostSharp.Aspects;
using System.Reflection;

namespace ProjectX.Infrastructure.LogAttributes.Helpers
{
    class MethodArgs
    {
        #region Properties
        public object Instance { get; protected set; }
        public Arguments Arguments { get; protected set; }
        public MethodBase Method { get; protected set; }
        #endregion

        #region Constructors
        public MethodArgs(object instance, MethodBase method, Arguments arguments)
        {
            this.Instance = instance;
            this.Method = method;
            this.Arguments = arguments;
        }
        #endregion
    }
}
