﻿using PostSharp.Aspects;
using ProjectX.General;
using ProjectX.Infrastructure.DependencyInjection;
using ProjectX.Infrastructure.ExceptionHandlers;
using ProjectX.Infrastructure.LogAttributes.Helpers;
using System;
using System.Threading;
using System.Threading.Tasks;

#region Source
//Source: http://doc.postsharp.net/method-interception
#endregion

namespace ProjectX.Infrastructure.LogAttributes
{
    /// <summary>
    /// Method interceptor: Intercepting attribute for an individual method.
    /// <para>No need to implement a try/catch, only if customized exception handling is needed within the method.</para>
    /// <para>ATTENTION!</para>
    /// <para>Rethrow exception when a try/catch is implemented, to ensure that the exception is logged properly!</para>
    /// </summary>
    [Serializable]
    public abstract class InterceptExceptionAspect : MethodInterceptionAspect
    {
        #region Properties
        public uint MaxRetries { get; set; } = (uint)NumberConstant.RetryLimit;
        public bool ShowMessage { get; set; } = true;
        public ExceptionStrategy Strategy { get; set; } = ExceptionStrategy.Default;
        public IExceptionInfo ExceptionInfo { get; protected set; }
        public bool IsHandled { get; protected set; } = false;
        public bool DoExit { get; protected set; } = false;
        public bool Retry { get; protected set; } = false;
        #endregion

        #region Methods: -> MethodInterceptionAspect
        public override void OnInvoke(MethodInterceptionArgs args)
        {
            var initialLoad = true;
            var retriesCounter = XLib.MagicNumber.IntZero;
            while (true)
            {
                try
                {
                    args.Proceed();
                    return;
                }
                catch (Exception exception)
                {
                    if (this.DoExit) { return; }
                    if (!this.Retry)
                    {
                        retriesCounter++;
                        if (!initialLoad && retriesCounter > this.MaxRetries)
                        {
                            this.HandleException(args, new RetriesExceededException(retriesCounter), ExceptionStrategy.Default, true);
                            throw;
                        }
                        this.HandleException(args, exception, this.Strategy, this.ShowMessage);
                    }
                    this.Retry = false;
                    initialLoad = false;
                }
            }
        }

        public override async Task OnInvokeAsync(MethodInterceptionArgs args)
        {
            var initialLoad = true;
            var retriesCounter = XLib.MagicNumber.IntZero;
            while (true)
            {
                try
                {
                    await args.ProceedAsync();
                    return;
                }
                catch (Exception exception)
                {
                    if (this.DoExit) { return; }
                    if (!this.Retry)
                    {
                        retriesCounter++;
                        var token = args.GetArgumentValue<CancellationToken>();
                        if (!initialLoad && retriesCounter > this.MaxRetries)
                        {
                            await this.HandleExceptionAsync(args, new RetriesExceededException(retriesCounter), ExceptionStrategy.Default, true, token);
                            throw;
                        }
                        token.ThrowIfCancellationRequested();
                        await this.HandleExceptionAsync(args, exception, this.Strategy, this.ShowMessage, token);
                    }
                    this.Retry = false;
                    initialLoad = false;
                }
            }
        }
        #endregion

        #region Methods
        public virtual IExceptionInfo HandleException(MethodInterceptionArgs args, Exception exception, ExceptionStrategy strategy, bool showMessage)
        {
            this.IsHandled = false;
            switch (strategy)
            {
                case ExceptionStrategy.Default:
                    return this.HandleDefaultStrategy(args, exception, showMessage);
                case ExceptionStrategy.Exit:
                    return this.HandleExitStrategy(args, exception, showMessage, true);
                case ExceptionStrategy.Data:
                    return this.HandleDataStrategy(args, exception, showMessage);
            }
            return this.ExceptionInfo;
        }

        public virtual async Task<IExceptionInfo> HandleExceptionAsync(MethodInterceptionArgs args, Exception exception, ExceptionStrategy strategy, bool showMessage, CancellationToken cancellationToken)
        {
            this.IsHandled = false;
            switch (strategy)
            {
                case ExceptionStrategy.Default:
                    return await this.HandleDefaultStrategyAsync(args, exception, showMessage, cancellationToken);
                case ExceptionStrategy.Exit:
                    return await this.HandleExitStrategyAsync(args, exception, showMessage, true, cancellationToken);
                case ExceptionStrategy.Data:
                    return await this.HandleDataStrategyAsync(args, exception, showMessage, cancellationToken);
            }
            return this.ExceptionInfo;
        }
        #endregion

        #region Virtual methods
        public virtual IExceptionInfo HandleDefaultStrategy(MethodInterceptionArgs args, Exception exception, bool showMessage)
        {
            return AsyncHelper.RunSync<IExceptionInfo>
                (() => this.HandleDefaultStrategyAsync(args, exception, showMessage, CancellationToken.None));
        }

        public virtual async Task<IExceptionInfo> HandleDefaultStrategyAsync(MethodInterceptionArgs args, Exception exception, bool showMessage, CancellationToken cancellationToken)
        {
            using (var dependency = new DI<IExceptionHandler>(ApplicationDependencies.ExceptionHandlerTypes))
            {
                var handler = dependency.Instance;
                if (handler != null)
                {
                    var information = args.GetLogInformation();
                    return await Task.Run(() => handler.Handle(information, exception, showMessage));
                }
                return this.ExceptionInfo;
            }
        }
        #endregion

        #region Abstract methods
        public abstract IExceptionInfo HandleExitStrategy(MethodInterceptionArgs args, Exception exception, bool showMessage, bool isFatal);
        public abstract Task<IExceptionInfo> HandleExitStrategyAsync(MethodInterceptionArgs args, Exception exception, bool showMessage, bool isFatal, CancellationToken cancellationToken);

        public abstract IExceptionInfo HandleDataStrategy(MethodInterceptionArgs args, Exception exception, bool showMessage);
        public abstract Task<IExceptionInfo> HandleDataStrategyAsync(MethodInterceptionArgs args, Exception exception, bool showMessage, CancellationToken cancellationToken);
        #endregion
    }
}
