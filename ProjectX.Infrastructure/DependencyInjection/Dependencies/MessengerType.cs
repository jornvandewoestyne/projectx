﻿using ProjectX.General;
using ProjectX.Infrastructure.MessageService;
using System;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class MessengerType : DependencyType
    {
        #region Properties
        public MessageBoxType MessageBoxType { get; }
        #endregion

        #region Constructors
        internal MessengerType(Type type, MessageBoxType messageBoxType) : base(type, typeof(IMessenger))
        {
            this.MessageBoxType = messageBoxType;
        }
        #endregion

        #region Methods
        public new IMessenger CreateInstance()
        {
            return this.Type.CreateInstance<IMessenger>();
        }
        #endregion
    }
}
