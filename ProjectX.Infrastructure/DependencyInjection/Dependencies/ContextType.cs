﻿using ProjectX.Data;
using ProjectX.General;
using System;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class ContextType : DependencyType, IBackEndModule
    {
        #region Constructors
        internal ContextType(Type type) : base(type, typeof(IContext))
        { }
        #endregion

        #region Methods
        public new IContext CreateInstance()
        {
            return this.Type.CreateInstance<IContext>();
        }
        #endregion
    }
}
