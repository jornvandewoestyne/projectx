﻿using ProjectX.Data;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class StoredProcedureType<T> : BaseDependencyType<T> where T : IStoredProcedure
    {
        #region Properties
        public new StoredProcedureType DependencyType { get; }
        #endregion

        #region Constructors
        public StoredProcedureType()
        {
            this.DependencyType = new StoredProcedureType(typeof(T));
        }
        #endregion
    }
}
