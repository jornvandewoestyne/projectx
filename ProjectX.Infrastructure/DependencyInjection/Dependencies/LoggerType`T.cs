﻿using ProjectX.Infrastructure.LogService;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class LoggerType<T> : BaseDependencyType<T> where T : ILogger
    {
        #region Properties
        public new LoggerType DependencyType { get; }
        #endregion

        #region Constructors
        public LoggerType()
        {
            this.DependencyType = new LoggerType(typeof(T));
        }
        #endregion
    }
}
