﻿using ProjectX.General;
using ProjectX.Infrastructure.MessageService;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class ApplicationDependencies
    {
        #region Fields
        private static CompositionRootType _rootType;
        private static ModuleType _moduleType;

        private static ContextType _contextType;
        private static ExceptionHandlerType _exceptionHandlerType;
        private static InterceptorType _interceptorType;
        private static LoggerType _loggerType;
        private static MessengerType _messengerType;
        private static ServiceType _serviceType;
        private static StoredProcedureType _storedProcedureType;
        private static TranslatorType _translatorType;
        private static UnitOfWorkType _unitOfWorkType;
        #endregion

        #region Properties
        public static CompositionRootType RootType => GetRootType();
        public static ModuleType ModuleType => GetModuleType();
        public static ContextType ContextType => GetContextType();
        public static ExceptionHandlerType ExceptionHandlerType => GetExceptionHandlerType();
        public static InterceptorType InterceptorType => GetInterceptorType();
        public static LoggerType LoggerType => GetLoggerType();
        public static MessengerType MessengerType => GetMessengerType();
        public static ServiceType ServiceType => GetServiceType();
        public static StoredProcedureType StoredProcedureType => GetStoredProcedureType();
        public static TranslatorType TranslatorType => GetTranslatorType();
        public static UnitOfWorkType UnitOfWorkType => GetUnitOfWorkType();
        #endregion

        #region Properties: Collections
        public static DependencyModuleTypeCollection ContextTypes => new DependencyModuleTypeCollection(ContextType, StoredProcedureType, UnitOfWorkType, ServiceType);
        public static DependencyModuleTypeCollection DefaultTypes => new DependencyModuleTypeCollection(ArrayHelper.Combine(ContextTypes.DependencyTypes, ExceptionHandlerTypes.DependencyTypes));
        public static DependencyModuleTypeCollection ExceptionHandlerTypes => new DependencyModuleTypeCollection(LoggerType, ExceptionHandlerType, MessengerType);
        public static DependencyModuleTypeCollection LoggerTypes => ExceptionHandlerTypes;
        public static DependencyModuleTypeCollection MessengerTypes => ExceptionHandlerTypes;
        public static DependencyModuleTypeCollection TranslatorTypes => new DependencyModuleTypeCollection(ArrayHelper.Combine(new DependencyType[] { TranslatorType }, MessengerTypes.DependencyTypes));
        #endregion

        #region Constructors
        public ApplicationDependencies(
            CompositionRootType root,
            ModuleType module,
            ContextType context,
            ExceptionHandlerType handler,
            InterceptorType interceptor,
            LoggerType logger,
            MessengerType messenger,
            ServiceType service,
            StoredProcedureType storedProcedure,
            TranslatorType translator,
            UnitOfWorkType unitOfWork)
        {
            SetRootType(root);
            SetModuleType(module);
            SetContextType(context);
            SetExceptionHandlerType(handler);
            SetInterceptorType(interceptor);
            SetLoggerType(logger);
            SetMessengerType(messenger);
            SetServiceType(service);
            SetStoredProcedureType(storedProcedure);
            SetTranslatorType(translator);
            SetUnitOfWorkType(unitOfWork);
        }
        #endregion

        #region Methods: Getters
        private static CompositionRootType GetRootType()
        {
            var result = _rootType;
            if (result == null)
            {
                throw new ArgumentNullPropertyException(typeof(DependencyType), nameof(RootType));
            }
            return result;
        }

        private static ModuleType GetModuleType()
        {
            var result = _moduleType;
            if (result == null)
            {
                throw new ArgumentNullPropertyException(typeof(DependencyType), nameof(ModuleType));
            }
            return result;
        }

        private static ContextType GetContextType()
        {
            var result = _contextType;
            if (result == null)
            {
                throw new ArgumentNullPropertyException(typeof(DependencyType), nameof(ContextType));
            }
            return result;
        }

        private static ExceptionHandlerType GetExceptionHandlerType()
        {
            var result = _exceptionHandlerType;
            if (result == null)
            {
                throw new ArgumentNullPropertyException(typeof(DependencyType), nameof(ExceptionHandlerType));
            }
            return result;
        }

        private static InterceptorType GetInterceptorType()
        {
            var result = _interceptorType;
            if (result == null)
            {
                throw new ArgumentNullPropertyException(typeof(DependencyType), nameof(InterceptorType));
            }
            return result;
        }

        private static LoggerType GetLoggerType()
        {
            var result = _loggerType;
            if (result == null)
            {
                throw new ArgumentNullPropertyException(typeof(DependencyType), nameof(LoggerType));
            }
            return result;
        }

        private static MessengerType GetMessengerType()
        {
            var result = _messengerType;
            if (result == null)
            {
                throw new ArgumentNullPropertyException(typeof(DependencyType), nameof(MessengerType));
            }
            return result;
        }

        private static ServiceType GetServiceType()
        {
            var result = _serviceType;
            if (result == null)
            {
                throw new ArgumentNullPropertyException(typeof(DependencyType), nameof(ServiceType));
            }
            return result;
        }

        private static StoredProcedureType GetStoredProcedureType()
        {
            var result = _storedProcedureType;
            if (result == null)
            {
                throw new ArgumentNullPropertyException(typeof(DependencyType), nameof(StoredProcedureType));
            }
            return result;
        }

        private static TranslatorType GetTranslatorType()
        {
            var result = _translatorType;
            if (result == null)
            {
                throw new ArgumentNullPropertyException(typeof(DependencyType), nameof(TranslatorType));
            }
            return result;
        }

        private static UnitOfWorkType GetUnitOfWorkType()
        {
            var result = _unitOfWorkType;
            if (result == null)
            {
                throw new ArgumentNullPropertyException(typeof(DependencyType), nameof(UnitOfWorkType));
            }
            return result;
        }
        #endregion

        #region Methods: Setters
        private void SetRootType(CompositionRootType type)
        {
            _rootType = type;
        }

        private void SetModuleType(ModuleType type)
        {
            _moduleType = type;
        }

        private void SetContextType(ContextType type)
        {
            _contextType = type;
        }

        private void SetExceptionHandlerType(ExceptionHandlerType type)
        {
            _exceptionHandlerType = type;
        }

        private void SetInterceptorType(InterceptorType type)
        {
            _interceptorType = type;
        }

        private void SetLoggerType(LoggerType type)
        {
            _loggerType = type;
        }

        private void SetMessengerType(MessengerType type)
        {
            _messengerType = type;
        }

        private void SetServiceType(ServiceType type)
        {
            _serviceType = type;
        }

        private void SetStoredProcedureType(StoredProcedureType type)
        {
            _storedProcedureType = type;
        }

        private void SetTranslatorType(TranslatorType type)
        {
            _translatorType = type;
        }

        private void SetUnitOfWorkType(UnitOfWorkType type)
        {
            _unitOfWorkType = type;
        }
        #endregion
    }
}