﻿
namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class DependencyModuleTypeCollection
    {
        #region Properties
        public DependencyType[] DependencyTypes { get; }
        #endregion

        #region Constructors
        public DependencyModuleTypeCollection(params DependencyType[] dependencyTypes)
        {
            this.DependencyTypes = dependencyTypes;
        }
        #endregion

        #region Methods
        public T[] CreateInstances<T>() where T : IDependencyModule
        {
            var dependencyTypes = this.DependencyTypes;
            var length = dependencyTypes.Length;
            var modules = new T[length];
            for (int i = 0; i < length; i++)
            {
                var instance = dependencyTypes[i].CreateInstance();
                if (instance is T module)
                {
                    modules[i] = module;
                }               
            }
            return modules;
        }
        #endregion
    }
}
