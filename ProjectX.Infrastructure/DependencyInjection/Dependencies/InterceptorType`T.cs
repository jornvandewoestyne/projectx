﻿using Ninject.Extensions.Interception;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class InterceptorType<T> : BaseDependencyType<T> where T : IInterceptor
    {
        #region Properties
        public new InterceptorType DependencyType { get; }
        #endregion

        #region Constructors
        public InterceptorType()
        {
            this.DependencyType = new InterceptorType(typeof(T));
        }
        #endregion
    }
}
