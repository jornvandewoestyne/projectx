﻿using ProjectX.General;
using System;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class CompositionRootType : DependencyType
    {
        #region Constructors
        internal CompositionRootType(Type type) : base(type, typeof(ICompositionRoot))
        { }
        #endregion

        #region Methods
        public new ICompositionRoot CreateInstance()
        {
            return this.Type.CreateInstance<ICompositionRoot>();
        }
        #endregion
    }
}