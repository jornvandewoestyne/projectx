﻿using ProjectX.General;
using System;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class TranslatorType : DependencyType
    {
        #region Constructors
        internal TranslatorType(Type type) : base(type, typeof(ITranslator))
        { }
        #endregion

        #region Methods
        public new ITranslator CreateInstance()
        {
            return this.Type.CreateInstance<ITranslator>();
        }
        #endregion
    }
}
