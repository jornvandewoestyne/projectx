﻿
namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class CompositionRootType<T> : BaseDependencyType<T> where T : ICompositionRoot
    {
        #region Properties
        public new CompositionRootType DependencyType { get; }
        #endregion

        #region Constructors
        public CompositionRootType()
        {
            this.DependencyType = new CompositionRootType(typeof(T));
        }
        #endregion
    }
}