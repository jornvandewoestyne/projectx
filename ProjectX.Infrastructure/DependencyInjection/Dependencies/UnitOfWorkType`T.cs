﻿using ProjectX.Data;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class UnitOfWorkType<T> : BaseDependencyType<T> where T : IUnitOfWork
    {
        #region Properties
        public new UnitOfWorkType DependencyType { get; }
        #endregion

        #region Constructors
        public UnitOfWorkType()
        {
            this.DependencyType = new UnitOfWorkType(typeof(T));
        }
        #endregion
    }
}
