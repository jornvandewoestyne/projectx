﻿using ProjectX.Infrastructure.ExceptionHandlers;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class ExceptionHandlerType<T> : BaseDependencyType<T> where T : IExceptionHandler
    {
        #region Properties
        public new ExceptionHandlerType DependencyType { get; }
        #endregion

        #region Constructors
        public ExceptionHandlerType()
        {
            this.DependencyType = new ExceptionHandlerType(typeof(T));
        }
        #endregion
    }
}
