﻿using ProjectX.Data;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class ContextType<T> : BaseDependencyType<T> where T : IContext
    {
        #region Properties
        public new ContextType DependencyType { get; }
        #endregion

        #region Constructors
        public ContextType()
        {
            this.DependencyType = new ContextType(typeof(T));
        }
        #endregion
    }
}
