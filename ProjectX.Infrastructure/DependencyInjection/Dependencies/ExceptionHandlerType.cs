﻿using ProjectX.General;
using ProjectX.Infrastructure.ExceptionHandlers;
using System;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class ExceptionHandlerType : DependencyType
    {
        #region Constructors
        internal ExceptionHandlerType(Type type) : base(type, typeof(IExceptionHandler))
        { }
        #endregion

        #region Methods
        public new IExceptionHandler CreateInstance()
        {
            return this.Type.CreateInstance<IExceptionHandler>();
        }
        #endregion
    }
}
