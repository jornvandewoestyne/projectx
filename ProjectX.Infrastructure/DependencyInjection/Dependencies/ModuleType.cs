﻿using ProjectX.General;
using System;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class ModuleType : DependencyType
    {
        #region Constructors
        internal ModuleType(Type type) : base(type, typeof(IDependencyModule))
        { }
        #endregion

        #region Methods
        [Obsolete("This method is not supported in this class.", true)]
        public new IDependencyModule CreateInstance()
        {
            throw new NotImplementedException();
        }

        public IDependencyModule CreateInstance(DependencyModuleTypeCollection dependencyModuleTypeCollection)
        {
            return this.CreateInstance<IDependencyModule>(dependencyModuleTypeCollection);
        }

        public T CreateInstance<T>(DependencyModuleTypeCollection dependencyModuleTypeCollection)
        {
            return this.Type.CreateInstance<T>(dependencyModuleTypeCollection);
        }
        #endregion
    }
}
