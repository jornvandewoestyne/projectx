﻿using ProjectX.General;
using ProjectX.Infrastructure.LogService;
using System;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class LoggerType : DependencyType
    {
        #region Constructors
        internal LoggerType(Type type) : base(type, typeof(ILogger))
        { }
        #endregion

        #region Methods
        public new ILogger CreateInstance()
        {
            return this.Type.CreateInstance<ILogger>();
        }
        #endregion
    }
}
