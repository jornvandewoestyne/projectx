﻿
namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class ModuleType<T> : BaseDependencyType<T> where T : IDependencyModule
    {
        #region Properties
        public new ModuleType DependencyType { get; }
        #endregion

        #region Constructors
        public ModuleType()
        {
            this.DependencyType = new ModuleType(typeof(T));
        }
        #endregion
    }
}
