﻿
namespace ProjectX.Infrastructure.DependencyInjection
{
    public abstract class BaseDependencyType<T>
    {
        #region Properties
        public DependencyType DependencyType { get; }
        #endregion

        #region Constructors
        public BaseDependencyType()
        {
            this.DependencyType = new DependencyType(typeof(T), null);
        }
        #endregion
    }
}
