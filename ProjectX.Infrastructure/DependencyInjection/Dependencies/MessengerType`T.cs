﻿using ProjectX.Infrastructure.MessageService;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class MessengerType<T> : BaseDependencyType<T> where T : IMessenger
    {
        #region Properties
        public new MessengerType DependencyType { get; }
        #endregion

        #region Constructors
        public MessengerType(MessageBoxType messageBoxType)
        {
            this.DependencyType = new MessengerType(typeof(T), messageBoxType);
        }
        #endregion
    }
}
