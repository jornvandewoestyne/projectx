﻿using ProjectX.Data;
using ProjectX.General;
using System;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class UnitOfWorkType : DependencyType, IBackEndModule
    {
        #region Constructors
        internal UnitOfWorkType(Type type) : base(type, typeof(IUnitOfWork))
        { }
        #endregion

        #region Methods
        public new IUnitOfWork CreateInstance()
        {
            return this.Type.CreateInstance<IUnitOfWork>();
        }
        #endregion
    }
}
