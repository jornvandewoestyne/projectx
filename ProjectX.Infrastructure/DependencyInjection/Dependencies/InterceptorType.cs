﻿using Ninject.Extensions.Interception;
using ProjectX.General;
using System;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class InterceptorType : DependencyType
    {
        #region Constructors
        internal InterceptorType(Type type) : base(type, typeof(IInterceptor))
        { }
        #endregion

        #region Methods
        public new IInterceptor CreateInstance()
        {
            return this.Type.CreateInstance<IInterceptor>();
        }
        #endregion
    }
}
