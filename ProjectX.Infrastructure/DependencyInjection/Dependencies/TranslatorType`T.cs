﻿using ProjectX.General;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class TranslatorType<T> : BaseDependencyType<T> where T : ITranslator
    {
        #region Properties
        public new TranslatorType DependencyType { get; }
        #endregion

        #region Constructors
        public TranslatorType()
        {
            this.DependencyType = new TranslatorType(typeof(T));
        }
        #endregion
    }
}
