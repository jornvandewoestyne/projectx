﻿using ProjectX.General;
using System;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public class DependencyType
    {
        #region Properties
        public Type Type { get; }
        public Type GenericType { get; }
        #endregion

        #region Constructors
        public DependencyType(Type type, Type genericType)
        {
            this.Type = type;
            this.GenericType = genericType;
        }
        #endregion

        #region Methods
        public IDependencyModule CreateInstance()
        {
            return this.Type.CreateInstance<IDependencyModule>();
        }
        #endregion
    }
}
