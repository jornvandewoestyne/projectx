﻿using ProjectX.Data;
using ProjectX.General;
using System;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class StoredProcedureType : DependencyType, IBackEndModule
    {
        #region Constructors
        internal StoredProcedureType(Type type) : base(type, typeof(IStoredProcedure))
        { }
        #endregion

        #region Methods
        public new IStoredProcedure CreateInstance()
        {
            return this.Type.CreateInstance<IStoredProcedure>();
        }
        #endregion
    }
}
