﻿using ProjectX.Business;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class ServiceType<T> : BaseDependencyType<T> where T : IService
    {
        #region Properties
        public new ServiceType DependencyType { get; }
        #endregion

        #region Constructors
        public ServiceType()
        {
            this.DependencyType = new ServiceType(typeof(T));
        }
        #endregion
    }
}
