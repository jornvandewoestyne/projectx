﻿using ProjectX.Business;
using ProjectX.General;
using System;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class ServiceType : DependencyType
    {
        #region Constructors
        internal ServiceType(Type type) : base(type, typeof(IService))
        { }
        #endregion

        #region Methods
        public new IService CreateInstance()
        {
            return this.Type.CreateInstance<IService>();
        }
        #endregion
    }
}
