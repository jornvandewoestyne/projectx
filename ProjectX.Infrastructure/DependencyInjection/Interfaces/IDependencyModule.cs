﻿
namespace ProjectX.Infrastructure.DependencyInjection
{
    public interface IDependencyModule
    {
        DependencyModuleTypeCollection DependencyModuleTypeCollection { get; }
    }
}
