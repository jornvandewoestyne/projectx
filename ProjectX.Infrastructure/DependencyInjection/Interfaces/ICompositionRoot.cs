﻿using System;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public interface ICompositionRoot : IDisposable
    {
        #region Methods
        ICompositionRoot GetInstance(DependencyModuleTypeCollection types);
        T Get<T>();
        #endregion
    }
}
