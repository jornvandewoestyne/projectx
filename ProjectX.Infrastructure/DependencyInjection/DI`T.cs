﻿using ProjectX.General;
using System;

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class DI<T> : DisposableObject, IDisposable
    {
        #region Properties
        private ICompositionRoot Root { get; set; }
        public T Instance { get; private set; }
        #endregion

        #region Constructors
        public DI() : this(ApplicationDependencies.DefaultTypes)
        { }

        public DI(DependencyModuleTypeCollection types)
        {
            if (types == null)
            {
                throw new LogBugException(new NotImplementedException(this.ToString()));
            }
            this.Set(types);
        }
        #endregion

        #region Methods
        private void Set(DependencyModuleTypeCollection types)
        {
            this.Root = ApplicationDependencies.RootType?.CreateInstance()?.GetInstance(types);
            this.Instance = this.Root.Get<T>();
        }
        #endregion

        #region Methods: DisposableObject
        public override void Dispose(bool disposing)
        {
            if (disposing && !IsDisposed)
            {
                if (this.Instance != null && this.Instance is IDisposable disposable)
                {
                    disposable.Dispose();
                }
                else
                {
                    //throw new NotImplementedException(this.Instance.GetType().ToString());
                }
                this.Root.Dispose();
                this.Root = null;
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
