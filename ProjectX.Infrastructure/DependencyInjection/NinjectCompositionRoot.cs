﻿using Ninject;
using Ninject.Modules;
using System;
using ProjectX.General;

#region Source
//http://stackoverflow.com/questions/14127763/dependency-injection-in-winforms-using-ninject-and-entity-framework
//https://github.com/ninject/Ninject
#endregion

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class NinjectCompositionRoot : DisposableObject, IDisposable, ICompositionRoot
    {
        #region Properties
        private IKernel Kernel { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Required! Sets the CompositionRoot.
        /// </summary>
        public ICompositionRoot GetInstance(DependencyModuleTypeCollection types)
        {
            var module = ApplicationDependencies.ModuleType;
            this.Kernel = new StandardKernel(module.CreateInstance<INinjectModule>(types));
            return this;
        }

        /// <summary>
        /// Get an instance of a specific interface.
        /// First set the CompositionRoot before usage to avoid exceptions!
        /// </summary>
        public T Get<T>()
        {
            if (this.Kernel != null)
            {
                return this.Kernel.Get<T>();
            }
            //TODO: test!
            return default(T);
        }
        #endregion

        #region Methods: Helpers
        private INinjectModule[] GetModules(DependencyModuleTypeCollection types)
        {
            return Array.ConvertAll(types.CreateInstances<IDependencyModule>(), item => (INinjectModule)item);
        }
        #endregion

        #region Methods: IDisposable
        public override void Dispose(bool disposing)
        {
            if (disposing && !IsDisposed)
            {
                this.Kernel.Dispose();
                this.Kernel = null;
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
