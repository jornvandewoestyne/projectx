﻿using Ninject.Extensions.Interception.Infrastructure.Language;
using Ninject.Modules;
using ProjectX.Infrastructure.MessageService;

//CHECK: https://mono.software/2016/04/21/Ninject-ambient-scope-and-deterministic-dispose/
//TODO: Interception doesn't work for generic modules: FIND A SOLUTION!
//potential solution: ???? https://github.com/ninject/Ninject.Extensions.Interception/blob/master/src/Ninject.Extensions.Interception/AsyncInterceptor.cs

namespace ProjectX.Infrastructure.DependencyInjection
{
    public sealed class NinjectUiWindowsFormsModule : NinjectModule, IDependencyModule
    {
        #region Properties
        public DependencyModuleTypeCollection DependencyModuleTypeCollection { get; }
        #endregion

        #region Constructors
        public NinjectUiWindowsFormsModule(DependencyModuleTypeCollection dependencyModuleTypeCollection)
        {
            this.DependencyModuleTypeCollection = dependencyModuleTypeCollection;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Sets all bindings necessary for the dependency injection.
        /// </summary>
        public override void Load()
        {
            foreach (var dependencyType in this.DependencyModuleTypeCollection.DependencyTypes)
            {
                if (dependencyType is IBackEndModule)
                {
                    this.Bind(dependencyType.GenericType)
                        .To(dependencyType.Type)
                        .InTransientScope();
                }
                else
                {
                    this.Bind(dependencyType.GenericType)
                        .To(dependencyType.Type)
                        .InTransientScope()
                        .Intercept()
                        .With(ApplicationDependencies.InterceptorType.Type);
                }
            }
        }
        #endregion
    }
}
