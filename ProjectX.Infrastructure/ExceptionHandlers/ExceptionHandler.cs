﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using ProjectX.Infrastructure.LogService;
using ProjectX.Infrastructure.MessageService;
using System;
using System.Data.Entity.Core;
using System.Data.SqlClient;
using System.Text;

//TODO: add 'System.ArgumentException'?
//TODO: add 'System.Data.Entity.Core.EntityCommandExecutionException'? (CustomerKey)

namespace ProjectX.Infrastructure.ExceptionHandlers
{
    public class ExceptionHandler : ExceptionHandlerBase
    {
        #region Properties
        protected ILogger Logger { get; }
        protected IMessenger Messenger { get; }
        //protected Exception Exception { get; set; }
        protected bool ShowMessage { get; set; }
        #endregion

        #region Properties: -> IExceptionInfo
        public override LogEntry LogEntry => new LogEntry(this.LogLevel, this.LogInformation, this.Exception);
        #endregion

        #region Constructors
        public ExceptionHandler(ILogger logger, IMessenger messenger)
        {
            this.Logger = logger;
            this.Messenger = messenger;
        }
        #endregion

        #region Methods: -> IExceptionHandler
        public override IExceptionInfo Handle(LogInformation information, Exception exception, bool showMessage = true)
        {
            //Set necessary properties:
            this.LogInformation = information;
            this.Exception = exception;
            this.ShowMessage = showMessage;
            //Handle the exception:
            this.EvaluateException(exception);
            //Set the LogInformation:
            //TODO: interceptor
            this.LogInformation =
                new LogInformation(this.LogInformation, this.ErrorInformation, this.FriendlyMessage);
            //Log the exception:
            if (this.Logger != null)
            {
                this.Logger.Log(this.LogLevel, this.LogInformation, exception);
            }
            //Inform the user:
            if (this.Messenger != null)
            {
                this.Messenger.Show(this.FriendlyMessage, this.LogLevel, this.ShowMessage);
            }
            return this;
        }
        #endregion

        //REMINDER: add known issues accordingly...
        #region Helpers
        private IExceptionInfo EvaluateException(Exception exception)
        {
            switch (exception)
            {
                #region Custom Exceptions
                case ArgumentNullPropertyException ex:
                    return HandleNullPropertyException(ex);
                case CloseApplicationException ex:
                    return HandleCloseApplicationException(ex);
                case FatalException ex:
                    return HandleFatalException(ex);
                case FatalSqlException ex:
                    return HandleFatalSqlException(ex);
                case LoadingResourcesException ex:
                    return HandleLoadingResourcesException(ex);
                case LogBugException ex:
                    return HandleLogBugException(ex);
                case LogSuccessException ex:
                    return HandleLogSuccessException(ex);
                case MissingKeyValueException ex:
                    return HandleMissingKeyValueException(ex);
                case MissingMethodParameterException ex:
                    return HandleMissingMethodParameterException(ex);
                case NullEntityException ex:
                    return HandleNullEntityException(ex);
                case RetriesExceededException ex:
                    return HandleRetriesExceededException(ex);
                case StartApplicationException ex:
                    return HandleStartApplicationException(ex);
                #endregion
                #region System.Data.SqlClient
                case SqlException ex:
                    return HandleSqlException(ex);
                #endregion
                #region System.Data.Entity.Core Exceptions:
                case EntityException ex:
                    return HandleEntityException(ex);
                #endregion
                #region System Exceptions:
                case ArgumentNullException ex:
                    return HandleArgumentNullException(ex);
                case FormatException ex:
                    return HandleFormatException(ex);
                case InvalidCastException ex:
                    return HandleInvalidCastException(ex);
                case NotImplementedException ex:
                    return HandleNotImplementedException(ex);
                case NullReferenceException ex:
                    return HandleNullReferenceException(ex);
                case OperationCanceledException ex:
                    return HandleOperationCanceledException(ex);
                #endregion
                default:
                    return HandleException(exception);
            }
        }
        #endregion

        #region Helpers: RetriesExceededException
        private IExceptionInfo HandleRetriesExceededException(RetriesExceededException exception)
        {
            this.LogLevel = LogLevel.Warning;
            this.FriendlyErrorInformation = this.GetFriendlyErrorInformation(exception);
            this.FriendlyMessage = ApplicationMessage.RetriesExceededInfo(exception);
            return this;
        }
        #endregion

        #region Helpers: LogSuccessExceptions
        private IExceptionInfo HandleLogSuccessException(LogSuccessException exception)
        {
            this.ShowMessage = false;
            HandleException((Exception)exception, LogLevel.Info);
            return this;
        }
        #endregion

        #region Helpers: LoadingResourcesExceptions
        private IExceptionInfo HandleLoadingResourcesException(LoadingResourcesException exception)
        {
            this.LogLevel = LogLevel.Error;
            this.FriendlyErrorInformation = this.GetFriendlyErrorInformation(exception);
            var info = new FriendlyInfo(exception.Message, this.FriendlyErrorInformation, ApplicationMessage.ContactAdministratorAdvice().Message);
            this.FriendlyMessage = ApplicationMessage.GenericMessage(StringHelper.Join("\n", info.MainMessage, info.Advice, info.ExtraMessage)); // ApplicationMessage.UnhandledBugInfo(info);
            return this;
        }
        #endregion

        #region Helpers: LogBugExceptions
        private IExceptionInfo HandleLogBugException(LogBugException exception)
        {
            this.LogLevel = LogLevel.Bug;
            this.FriendlyErrorInformation = this.GetFriendlyErrorInformation(exception);
            var info = new FriendlyInfo(exception.Message, this.FriendlyErrorInformation, ApplicationMessage.ContactAdministratorAdvice().Message);
            this.FriendlyMessage = ApplicationMessage.UnhandledBugInfo(info);
            return this;
        }
        #endregion

        #region Helpers: NotImplementedExceptions
        private IExceptionInfo HandleNotImplementedException(NotImplementedException exception)
        {
            return HandleException((Exception)exception, LogLevel.Warning);
        }
        #endregion

        #region Helpers: NullReferenceExceptions
        private IExceptionInfo HandleNullReferenceException(NullReferenceException exception)
        {
            return HandleException((Exception)exception);
        }
        #endregion

        #region Helpers: ArgumentNullException
        private IExceptionInfo HandleArgumentNullException(ArgumentNullException exception)
        {
            return HandleException((Exception)exception);
        }
        #endregion

        #region Helpers: NullEntityExceptions
        private IExceptionInfo HandleNullEntityException(NullEntityException exception)
        {
            this.LogLevel = LogLevel.Warning;
            this.FriendlyErrorInformation = this.GetFriendlyErrorInformation(exception);
            this.FriendlyMessage = ApplicationMessage.EntityNotFoundInfo();
            return this;
        }
        #endregion

        #region Helpers: NullPropertyExceptions
        private IExceptionInfo HandleNullPropertyException(ArgumentNullPropertyException exception)
        {
            //this.ShowMessage = false;
            this.LogLevel = LogLevel.Info;
            this.FriendlyErrorInformation = this.GetFriendlyErrorInformation(exception);
            var info = new FriendlyInfo();
            switch (exception.Command)
            {
                case MethodCommand.OrderBy:
                case MethodCommand.OrderByDescending:
                    info.MainMessage = ApplicationMessage.SortingFailedAlert().Message;
                    info.Advice = ApplicationMessage.SortingFailedAdvice().Message;
                    break;
                default:
                    info.MainMessage = ApplicationMessage.UnknownErrorAlert().Message;
                    info.Advice = ApplicationMessage.RetryReloadAdvice().Message;
                    break;
            }
            info.ExtraMessage = ApplicationMessage.PropertyNotFoundAlert(exception.PropertyName).Message;
            this.FriendlyMessage = ApplicationMessage.PropertyNotFoundInfo(info);
            return this;
        }
        #endregion

        #region Helpers: MissingKeyValueExceptions
        private IExceptionInfo HandleMissingKeyValueException(MissingKeyValueException exception)
        {
            this.LogLevel = LogLevel.Bug;
            this.FriendlyErrorInformation = this.GetFriendlyErrorInformation(exception);
            this.FriendlyMessage = ApplicationMessage.MissingKeyValueInfo();
            return this;
        }
        #endregion

        #region Helpers: MissingMethodParameterException
        private IExceptionInfo HandleMissingMethodParameterException(MissingMethodParameterException exception)
        {
            this.LogLevel = LogLevel.Bug;
            this.FriendlyErrorInformation = this.GetFriendlyErrorInformation(exception);
            this.FriendlyMessage = ApplicationMessage.MissingMethodParameterAlert(exception.PropertyName);
            return this;
        }
        #endregion

        #region Helpers: FormatException
        private IExceptionInfo HandleFormatException(FormatException exception)
        {
            //TODO: custom message?
            //TODO: do not show this message??? -> PostSharp BUG???
            //this.ShowMessage = false;
            return HandleException(exception, LogLevel.Bug);
        }
        #endregion

        #region Helpers: InvalidCastException
        private IExceptionInfo HandleInvalidCastException(InvalidCastException exception)
        {
            //TODO: custom message?
            //TODO: do not show this message??? -> PostSharp BUG???
            //this.ShowMessage = false;
            return HandleException(exception, LogLevel.Bug);
        }
        #endregion

        #region Helpers: SqlExceptions
        private IExceptionInfo HandleSqlException(SqlException exception)
        {
            this.FriendlyErrorInformation = this.GetFriendlyErrorInformation(exception);
            this.ExtraErrorInformation = this.GetSqlErrorInformation(exception).ToString();
            switch (exception.Number)
            {
                case -1:
                case 2:
                case 258:
                case 1225:
                    // The server is not found or is unavailable.
                    this.LogLevel = LogLevel.Error;
                    this.FriendlyMessage = ApplicationMessage.DatabaseUnavailableInfo();
                    break;
                case 4060:
                case 18456:
                    // Login failed!
                    this.FriendlyMessage = ApplicationMessage.DatabaseLoginFailedInfo();
                    break;
                case -2:
                    // time-out
                    this.FriendlyMessage = ApplicationMessage.TimeOutAlert();
                    break;
                default:
                    HandleException((Exception)exception);
                    break;
            }
            return this;
        }

        private StringBuilder GetSqlErrorInformation(SqlException exception)
        {
            StringBuilder message = new StringBuilder();
            //TODO: add array to Preference or add new custom class to avoid strings?
            message.Append(exception.ToFriendlyString("Class", "Number", "ErrorCode", "LineNumber", "Server"));
            return message;
        }
        #endregion

        #region Helpers: EntityException
        private IExceptionInfo HandleEntityException(EntityException exception)
        {
            //TODO: show custom message?
            this.LogLevel = LogLevel.Error;
            this.FriendlyErrorInformation = this.GetFriendlyErrorInformation(exception);
            this.FriendlyMessage = ApplicationMessage.DatabaseUnavailableInfo();
            //HandleException((Exception)exception);
            return this;
        }
        #endregion

        #region Helpers: OperationCanceledException
        private IExceptionInfo HandleOperationCanceledException(OperationCanceledException exception)
        {
            //TODO: show custom message?
            //HandleException((Exception)exception);
            //System.Windows.Forms.MessageBox.Show(this.ShowMessage.ToString());
            this.LogLevel = LogLevel.Info;
            this.FriendlyMessage = ApplicationMessage.GenericMessage(exception.Message);
            return this;
        }
        #endregion

        #region Helpers: FatalSqlExceptions
        private IExceptionInfo HandleFatalSqlException(FatalSqlException exception)
        {
            HandleSqlException((SqlException)exception.InnerException);
            this.LogLevel = LogLevel.Fatal;
            return this;
        }
        #endregion

        #region Helpers: FatalExceptions
        private IExceptionInfo HandleFatalException(FatalException exception)
        {
            this.ShowMessage = true;
            this.LogLevel = LogLevel.Fatal;
            this.FriendlyErrorInformation = this.GetFriendlyErrorInformation(exception);
            var error = new FriendlyMessage(FriendlyMessageType.FatalErrorAlert, XLib.Preference.TranslateArguments);
            var info = new FriendlyInfo(exception.Message, this.FriendlyErrorInformation, ApplicationMessage.ContactAdministratorAdvice().Message);
            this.FriendlyMessage = ApplicationMessage.GenericMessage(StringHelper.Join("\n\n", error.Message, info.MainMessage, info.Advice, info.ExtraMessage));
            return this;
        }
        #endregion

        #region Helpers: StartApplicationExceptions
        private IExceptionInfo HandleStartApplicationException(StartApplicationException exception)
        {
            this.ShowMessage = false;
            return HandleException((Exception)exception, LogLevel.Info);
        }
        #endregion

        #region Helpers: CloseApplicationExceptions
        private IExceptionInfo HandleCloseApplicationException(CloseApplicationException exception)
        {
            this.ShowMessage = false;
            return HandleException((Exception)exception, LogLevel.Info);
        }
        #endregion

        #region Helpers: Exceptions
        private IExceptionInfo HandleException(Exception exception, LogLevel logLevel = LogLevel.Error)
        {
            this.LogLevel = logLevel;
            this.FriendlyMessage = new FriendlyMessage(FriendlyMessageType.UnhandledErrorAlert, XLib.Preference.TranslateArguments, exception.Message);
            return this;
        }
        #endregion

        #region Helpers: Methods
        private string GetFriendlyErrorInformation(Exception exception)
        {
            //TODO: check first if (needed on sqlexception (overflow)
            if (exception.InnerException == null)
            {
                return string.Empty;
            }
            //if (exception.InnerException != null)
            //{
            exception = exception.InnerException;
            //}
            EvaluateException(exception);
            return this?.FriendlyMessage?.Message.RemoveWhiteSpace();
        }
        #endregion
    }
}
