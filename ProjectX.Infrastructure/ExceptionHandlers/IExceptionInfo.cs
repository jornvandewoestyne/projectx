﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using ProjectX.Infrastructure.LogService;
using System;

//TODO: add summary...

namespace ProjectX.Infrastructure.ExceptionHandlers
{
    /// <summary>
    /// ...
    /// </summary>
    public interface IExceptionInfo
    {
        #region Properties
        //bool IsHandled { get; }
        LogEntry LogEntry { get; }
        LogLevel LogLevel { get; }
        Exception Exception { get; }
        FriendlyMessage FriendlyMessage { get; }
        #endregion
    }
}
