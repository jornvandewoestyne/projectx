﻿using ProjectX.Infrastructure.LogService;
using System;

//TODO: add summary...

namespace ProjectX.Infrastructure.ExceptionHandlers
{
    /// <summary>
    /// ...
    /// </summary>
    public interface IExceptionHandler
    {
        #region Methods
        /// <summary>
        /// Handles and logs a thrown exception, with optional messaging service.
        /// </summary>
        IExceptionInfo Handle(LogInformation information, Exception exception, bool showMessage = true);
        #endregion
    }
}
