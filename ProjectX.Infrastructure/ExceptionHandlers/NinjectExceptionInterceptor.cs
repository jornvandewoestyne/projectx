﻿using Ninject.Extensions.Interception;
using ProjectX.General;
using ProjectX.Infrastructure.LogService;
using System;

namespace ProjectX.Infrastructure.ExceptionHandlers
{
    /// <summary>
    /// Global exceptions interceptor implemented via dependency injection (inject via the Intercept() method).
    /// <para>No need to implement a try/catch, only if customized exception handling is needed within the method.</para>
    /// <para>ATTENTION!</para>
    /// <para>Not applicable in the UI layer.</para>
    /// <para>-> Use a method interceptor attribute for individual methods to enable logging!</para>
    /// <para>-> An exception is thrown when no interception attribute is added to the UI method!!</para>
    /// </summary>
    public class NinjectExceptionInterceptor : XAsyncInterceptor
    {
        #region Properties
        public ILogger Logger { get; }
        public IExceptionHandler Handler { get; }
        #endregion

        #region Constructor
        public NinjectExceptionInterceptor(ILogger logger, IExceptionHandler handler)
        {
            this.Logger = logger;
            this.Handler = handler;
        }
        #endregion

        #region Methods: Overrides
        protected override bool HandleException(IInvocation invocation, Exception exception)
        {
            //TODO: LogBugException correct?
            //If a BUG occured!                      
            if (exception is LogBugException || exception is LoadingResourcesException)
            {
                if (this.Handler != null)
                {
                    this.Handler.Handle(GetInformation(this, invocation), exception);
                    return true;
                }
            }
            //All other cases:
            if (this.Logger != null)
            {
                this.Logger.Log(LogLevel.Warning, GetInformation(this, invocation), exception);
            }
            return false;
        }
        #endregion

        #region Methods: Helpers
        private static LogInformation GetInformation(object interceptor, IInvocation invocation)
        {
            var request = invocation.Request;
            var assembly = request.Target.GetType().Assembly;
            var qualifiedName = request.Target.GetType().AssemblyQualifiedName;
            var methodInfo = request.Method;
            var arguments = request.Arguments;
            //TODO: interceptor
            return new LogInformation(interceptor, assembly, qualifiedName, methodInfo, arguments);
        }
        #endregion
    }
}
