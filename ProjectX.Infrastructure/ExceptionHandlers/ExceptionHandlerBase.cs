﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using ProjectX.Infrastructure.LogService;
using System;

namespace ProjectX.Infrastructure.ExceptionHandlers
{
    public abstract class ExceptionHandlerBase : IExceptionHandler, IExceptionInfo
    {
        //TODO: FriendlyErrorInformation, ExtraErrorInformation, ErrorInformation => make class???

        #region Properties
        protected LogInformation LogInformation { get; set; }
        protected string FriendlyErrorInformation { get; set; }
        protected string ExtraErrorInformation { get; set; }
        protected string ErrorInformation => StringHelper.ArrowStyle(this.ExtraErrorInformation, this.FriendlyErrorInformation);
        //protected FriendlyMessage FriendlyMessage { get; set; }
        //protected LogLevel LogLevel { get; set; } = LogLevel.Warning;
        #endregion

        #region Properties= -> IExceptionInfo
        //public bool IsHandled { get; private set; }
        public abstract LogEntry LogEntry { get; }
        public Exception Exception { get; protected set; }
        public LogLevel LogLevel { get; protected set; } = LogLevel.Warning;
        public FriendlyMessage FriendlyMessage { get; protected set; }
        #endregion

        #region Methods: -> IExceptionHandler
        public abstract IExceptionInfo Handle(LogInformation information, Exception exception, bool showMessage = true);
        #endregion
    }
}
