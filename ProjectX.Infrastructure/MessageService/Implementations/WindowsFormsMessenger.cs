﻿using ProjectX.General;
using ProjectX.Infrastructure.DependencyInjection;
using ProjectX.Infrastructure.Localization;
using System.Reflection;
using System.Windows.Forms;

//TODO: case when IsNotification -> don't show MessageBoxPlus! 
//=> change return type (void -> bool/INotifier?): public void Show(FriendlyMessage message, LogLevel logLevel, bool showMessage)

namespace ProjectX.Infrastructure.MessageService
{
    public class WindowsFormsMessenger : DisposableObject, IMessenger
    {
        #region Properties
        public bool IsReady { get; set; } = false;

        public AppTitle AppTitle => AppTitle.Instance;

        public MessageBoxType MessageBoxType => ApplicationDependencies.MessengerType.MessageBoxType;
        #endregion

        #region Methods: -> IMessenger
        public void Show(FriendlyMessage message, LogLevel logLevel, bool showMessage)
        {
            using (var messageBox = this.GetMessageBox())
            {
                if (showMessage && !message.Message.IsEmptyString())
                {
                    var title = GetMessageBoxTitle(logLevel);
                    messageBox.Show(message, title, this.GetMessageBoxIcon(logLevel));
                }
            }
        }

        public MessengerResult Show(FriendlyMessage message, LogLevel logLevel, MessengerButton buttons)
        {
            using (var messageBox = this.GetMessageBox())
            {
                if (!message.Message.IsEmptyString())
                {
                    var title = StringHelper.Title(this.AppTitle, XLib.Text.Confirm);
                    var icon = this.GetMessageBoxIcon(logLevel);
                    var defaultButton = this.GetMessageBoxDefaultButton(buttons, logLevel);
                    return (MessengerResult)messageBox.Show(message, title, (MessageBoxButtons)buttons, icon, defaultButton);
                }
                return (MessengerResult)DialogResult.None;
            }
        }
        #endregion

        #region Methods: Helpers
        private IMessageBox GetMessageBox()
        {
            var messageBoxType = this.MessageBoxType.Type;
            var assembly = Assembly.Load(messageBoxType.Namespace);
            var type = assembly.GetType(messageBoxType.FullName, true);
            return type.CreateInstance<IMessageBox>();
        }

        private string GetMessageBoxTitle(LogLevel logLevel)
        {
            //TODO: 
            string title;
            switch (logLevel)
            {
                case LogLevel.Bug:
                    title = XLib.Text.Bug.ToUpper();
                    break;
                case LogLevel.Error:
                    title = XLib.Text.Error.ToUpper();
                    break;
                case LogLevel.Fatal:
                    title = XLib.Text.FatalError.ToUpper();
                    break;
                default:
                    title = XLib.Text.Info;
                    break;
            }
            return StringHelper.Title(this.AppTitle, title);
        }

        private MessageBoxIcon GetMessageBoxIcon(LogLevel logLevel)
        {
            MessageBoxIcon icon;
            switch (logLevel)
            {
                case LogLevel.Warning:
                    icon = MessageBoxIcon.Warning;
                    break;
                case LogLevel.Bug:
                case LogLevel.Error:
                    icon = MessageBoxIcon.Error;
                    break;
                case LogLevel.Fatal:
                    icon = MessageBoxIcon.Stop;
                    break;
                case LogLevel.Question:
                    icon = MessageBoxIcon.Question;
                    break;
                default:
                    icon = MessageBoxIcon.Information;
                    break;
            }
            return icon;
        }

        private MessageBoxDefaultButton GetMessageBoxDefaultButton(MessengerButton buttons, LogLevel logLevel)
        {
            MessageBoxDefaultButton defaultButton;
            switch (logLevel)
            {
                case LogLevel.Bug:
                case LogLevel.Warning:
                case LogLevel.Error:
                case LogLevel.Fatal:
                    switch (buttons)
                    {
                        case MessengerButton.YesNo:
                        case MessengerButton.OKCancel:
                        case MessengerButton.RetryCancel:
                            defaultButton = MessageBoxDefaultButton.Button2;
                            break;
                        case MessengerButton.YesNoCancel:
                            defaultButton = MessageBoxDefaultButton.Button3;
                            break;
                        default:
                            defaultButton = MessageBoxDefaultButton.Button1;
                            break;
                    }
                    break;
                default:
                    defaultButton = MessageBoxDefaultButton.Button1;
                    break;
            }
            return defaultButton;
        }
        #endregion
    }
}
