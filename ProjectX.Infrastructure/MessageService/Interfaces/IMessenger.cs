﻿using ProjectX.General;
using ProjectX.Infrastructure.DependencyInjection;
using ProjectX.Infrastructure.Localization;
using System;

namespace ProjectX.Infrastructure.MessageService
{
    /// <summary>
    /// Messenger service.
    /// </summary>
    public interface IMessenger : IApplicationContainer, IDisposable
    {
        #region Properties
        MessageBoxType MessageBoxType { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Inform the user.
        /// </summary>
        void Show(FriendlyMessage message, LogLevel logLevel, bool showMessage);

        ///// <summary>
        ///// Inform the user.
        ///// </summary>
        ///// <param name="information"></param>
        ///// <param name="message"></param>
        ///// <param name="logLevel"></param>
        ///// <param name="showMessage"></param>
        //void Show(LogInformation information, FriendlyMessage message, LogLevel logLevel, bool showMessage);

        /// <summary>
        /// Get confirmation from the user.
        /// <para>ATTENTION!</para>
        /// <para>No confirmation is asked when the message is null or empty.</para>
        /// <para>Returns MessengerResult.None in that case.</para>
        /// <para>Keep in mind to handle the MessengerResult.None correctly.</para>
        /// </summary>
        MessengerResult Show(FriendlyMessage message, LogLevel logLevel, MessengerButton buttons);
        #endregion
    }
}
