﻿using ProjectX.Infrastructure.Localization;
using System;
using System.Windows.Forms;

namespace ProjectX.Infrastructure.MessageService
{
    public interface IMessageBox : IDisposable
    {
        DialogResult Show(FriendlyMessage message, string title, MessageBoxIcon icon);
        DialogResult Show(FriendlyMessage message, string title, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton);
    }
}
