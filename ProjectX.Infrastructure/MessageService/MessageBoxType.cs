﻿using System;

namespace ProjectX.Infrastructure.MessageService
{
    public sealed class MessageBoxType
    {
        #region Properties
        public Type Type { get; }
        #endregion

        #region Constructors
        internal MessageBoxType(Type type)
        {
            this.Type = type;
        }
        #endregion
    }
}
