﻿using ProjectX.Infrastructure.MessageService;

namespace ProjectX.Infrastructure.MessageService
{
    public sealed class MessageBoxType<T> where T : IMessageBox
    {
        #region Properties
        public MessageBoxType DependencyType { get; }
        #endregion

        #region Constructors
        public MessageBoxType()
        {
            this.DependencyType = new MessageBoxType(typeof(T));
        }
        #endregion
    }
}
