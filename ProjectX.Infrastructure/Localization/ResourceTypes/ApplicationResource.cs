﻿using ProjectX.General;
using System.Collections.Concurrent;

namespace ProjectX.Infrastructure.Localization.ResourceTypes
{
    public sealed class ApplicationResource
    {
        #region Properties
        public SupportedLanguage Language { get; }
        private readonly ConcurrentDictionary<string, string> Resources = new ConcurrentDictionary<string, string>();
        #endregion

        #region Constructors
        public ApplicationResource(SupportedLanguage language)
        {
            this.Language = language;
        }
        #endregion

        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public bool TryAdd(string key, string value)
        {
            var resources = this.Resources;
            if (!key.IsEmptyString() && !resources.ContainsKey(key))
            {
                return resources.TryAdd(key, value);
            }
            return false;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetValue(string key)
        {
            this.TryGetValue(key, out var value);
            return value;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetValue(string key, out string value)
        {
            value = key;
            var success = false;
            if (this.Resources != null && !key.IsEmptyString())
            {
                success = this.Resources.TryGetValue(key, out var translation);
                if (success && translation != null)
                {
                    value = translation;
                }
            }
            return success;
        }
        #endregion
    }
}
