﻿using ProjectX.General;
using ProjectX.Infrastructure.DependencyInjection;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.Infrastructure.Localization
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class TranslationCollection
    {
        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="reset"></param>
        /// <returns></returns>
        public ITranslator Get(bool reset = true)
        {
            return AsyncHelper.RunSync(() => this.GetAsync(CancellationToken.None, reset));
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <param name="reset"></param>
        /// <returns></returns>
        public async Task<ITranslator> GetAsync(CancellationToken cancellationToken, bool reset = true)
        {
            if (AppLibrary.Translator == null || reset)
            {
                //IMPORTANT: Don't dispose the DI<T> instance! (to be avoided, required only in this situation)
                AppLibrary.Translator = new DI<ITranslator>(ApplicationDependencies.TranslatorTypes).Instance;
                await AppLibrary.Translator?.GetResourcesAsync(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), cancellationToken);
            }
            return AppLibrary.Translator;
        }
        #endregion
    }
}
