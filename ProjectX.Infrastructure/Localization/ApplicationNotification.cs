﻿using ProjectX.General;

//TODO: Register when resource not found!!!
//TODO: id -> case sentive!!!!
//TODO: summary

namespace ProjectX.Infrastructure.Localization
{
    /// <summary>
    /// ...
    /// </summary>
    public static class ApplicationNotification
    {
        #region Methods
        //TODO: Notification, not FriendlyMessageBase?
        //TODO: to be removed!!!!????
        public static FriendlyMessage GetMessage(this Notification notification)
        {
            return GetMessage(notification, XLib.Preference.TranslateArguments);
        }

        public static FriendlyMessage GetMessage(this Notification notification, bool translateArguments)
        {
            return new FriendlyMessage(notification, translateArguments);
        }
        #endregion
    }
}
