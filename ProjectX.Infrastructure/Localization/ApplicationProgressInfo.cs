﻿using ProjectX.General;

//TODO: Register when resource not found!!!
//TODO: id -> case sentive!!!!
//TODO: summary

namespace ProjectX.Infrastructure.Localization
{
    /// <summary>
    /// ...
    /// </summary>
    public static class ApplicationProgressInfo
    {
        #region Methods: Helpers
        private static FriendlyMessage GetMessage(this FriendlyProgressType value, params object[] args)
        {
            return GetMessage(value, XLib.Preference.TranslateArguments, args);
        }

        private static FriendlyMessage GetMessage(this FriendlyProgressType value, bool translateArguments, params object[] args)
        {
            return new FriendlyMessage(value, translateArguments, args);
        }
        #endregion

        #region FriendlyProgress
        public static FriendlyMessage CompletedTask()
        {
            return FriendlyProgressType.CompletedTask.GetMessage();
        }

        public static FriendlyMessage ConnectingToDataBaseTask()
        {
            return FriendlyProgressType.ConnectingToDatabaseTask.GetMessage();
        }

        public static FriendlyMessage InitializingTask()
        {
            return FriendlyProgressType.InitializingTask.GetMessage();
        }

        public static FriendlyMessage InitializingModuleTask(string module)
        {
            return FriendlyProgressType.InitializingModuleTask.GetMessage(module);
        }

        public static FriendlyMessage LoadingTask()
        {
            return FriendlyProgressType.LoadingTask.GetMessage();
        }

        public static FriendlyMessage LoadingModuleTask()
        {
            return FriendlyProgressType.LoadingModuleTask.GetMessage();
        }

        public static FriendlyMessage LoadingResourceTask()
        {
            return FriendlyProgressType.LoadingResourceTask.GetMessage();
        }

        public static FriendlyMessage OpeningTask()
        {
            return FriendlyProgressType.OpeningTask.GetMessage();
        }

        public static FriendlyMessage SearchingTask()
        {
            return FriendlyProgressType.SearchingTask.GetMessage();
        }

        public static FriendlyMessage StartUpTask()
        {
            return FriendlyProgressType.StartUpTask.GetMessage();
        }
        #endregion
    }
}