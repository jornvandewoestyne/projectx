﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization.ResourceTypes;
using System.Threading;
using System.Threading.Tasks;

//TODO: summary

namespace ProjectX.Infrastructure.Localization
{
    /// <summary>
    /// ...
    /// </summary>
    internal interface IResoureceBuilder
    {
        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="language"></param>
        /// <param name="resource"></param>
        /// <param name="translationResource"></param>
        void GetResources(SupportedLanguage language, CustomResource resource, ApplicationResource translationResource);

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="language"></param>
        /// <param name="resource"></param>
        /// <param name="translationResource"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task GetResourcesAsync(SupportedLanguage language, CustomResource resource, ApplicationResource translationResource, CancellationToken cancellationToken);
        #endregion
    }
}
