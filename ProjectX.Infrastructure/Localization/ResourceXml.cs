﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization.ResourceTypes;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

//TODO: summary

namespace ProjectX.Infrastructure.Localization
{
    public sealed class ResourceXml : BaseResource
    {
        #region Methods -> IResoureceBuilder
        public override void GetResources(SupportedLanguage language, CustomResource resource, ApplicationResource translationResource)
        {
            try
            {
                var file = this.GetFullFileName(this.Location, language, resource);
                if (File.Exists(file))
                {
                    using (var reader = XmlReader.Create(file))
                    {
                        reader.MoveToContent();
                        while (reader.Read())
                        {
                            this.GetResource(reader, translationResource);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                //TODO: handle exception!!!
                //TODO: try get other resources
                throw new LoadingResourcesException(exception);
            }
        }

        public override async Task GetResourcesAsync(SupportedLanguage language, CustomResource resource, ApplicationResource translationResource, CancellationToken cancellationToken)
        {
            try
            {
                var file = this.GetFullFileName(this.Location, language, resource);
                if (File.Exists(file))
                {
                    var settings = new XmlReaderSettings() { Async = true };
                    using (var reader = XmlReader.Create(file, settings))
                    {
                        await reader.MoveToContentAsync().ConfigureAwait(false);
                        while (await reader.ReadAsync())
                        {
                            cancellationToken.ThrowIfCancellationRequested();
                            this.GetResource(reader, translationResource);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                //TODO: handle exception!!!
                //TODO: try get other resources
                throw new LoadingResourcesException(exception);
            }
        }
        #endregion

        #region Helpers
        private void GetResource(XmlReader reader, ApplicationResource resource)
        {
            if (reader.HasAttributes)
            {
                reader.MoveToAttribute(KeyWord.id.ToString());
                var key = reader.ReadContentAsString();
                reader.MoveToContent();
                var value = reader.ReadInnerXml();
                resource.TryAdd(key, value);
            }
        }

        private string GetFullFileName(string location, SupportedLanguage language, CustomResource resource)
        {
            var directory = resource.ToString();
            var extension = StringHelper.Concat(XLib.Token.Dot, SupportedExtension.resxml);
            var fileGroup = StringHelper.GetPluralOrSingular(directory);
            var file = StringHelper.Concat(fileGroup, language, extension);
            return Path.Combine(location, directory, file);
        }
        #endregion
    }
}
