﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization.ResourceTypes;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.Infrastructure.Localization
{
    internal static class ApplicationResourceManager
    {
        #region Properties
        private static readonly ConcurrentDictionary<SupportedLanguage, CustomResource[]> Library
            = new ConcurrentDictionary<SupportedLanguage, CustomResource[]>();
        private static readonly ConcurrentDictionary<SupportedLanguage, ApplicationResource> Resources
            = new ConcurrentDictionary<SupportedLanguage, ApplicationResource>();
        #endregion

        #region Methods
        /// <summary>
        /// Get value of node (key) in Resources.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="language"></param>
        /// <returns></returns>        
        public static string GetValue(string key, SupportedLanguage language)
        {
            TryGetValue(key, language, out var value);
            return value;
        }

        /// <summary>
        /// Try get value of node (key) in Resources.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="language"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool TryGetValue(string key, SupportedLanguage language, out string value)
        {
            value = key;
            var success = false;
            if (Resources != null)
            {
                success = Resources.TryGetValue(language, out var resource);
                if (success)
                {
                    success = resource.TryGetValue(key, out var translation);
                    value = translation;
                }
            }
            return success;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="resources"></param>
        public static void GetResources(IResoureceBuilder builder, params CustomResource[] resources)
        {
            Initialize(resources);
            foreach (var item in Library)
            {
                var language = item.Key;
                var translations = new ApplicationResource(language);
                foreach (var resource in item.Value)
                {
                    builder.GetResources(language, resource, translations);
                }
                Resources.TryAdd(language, translations);
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="resources"></param>
        /// <returns></returns>
        public static async Task GetResourcesAsync(IResoureceBuilder builder, CancellationToken cancellationToken, params CustomResource[] resources)
        {
            Initialize(resources);
            foreach (var item in Library)
            {
                var language = item.Key;
                var translations = new ApplicationResource(language);
                foreach (var resource in item.Value)
                {
                    await builder.GetResourcesAsync(language, resource, translations, cancellationToken);
                }
                Resources.TryAdd(language, translations);
            }
        }
        #endregion

        #region Helpers
        private static void Initialize(params CustomResource[] resources)
        {
            Resources.Clear();
            Library.Clear();
            var languages = EnumHelper.GetValues<SupportedLanguage>();
            if (resources?.Length == XLib.MagicNumber.IntZero) { resources = EnumHelper.GetValues<CustomResource>(); }
            foreach (var language in languages)
            {
                Library.TryAdd(language, resources);
            }
        }
        #endregion
    }
}
