﻿using ProjectX.General;

//TODO: Register when resource not found!!!
//TODO: id -> case sentive!!!!
//TODO: summary

namespace ProjectX.Infrastructure.Localization
{
    /// <summary>
    /// ...
    /// </summary>
    public static class ApplicationMessage
    {
        #region Methods: Helpers
        private static FriendlyMessage GetMessage(this FriendlyMessageType value, params object[] args)
        {
            return GetMessage(value, XLib.Preference.TranslateArguments, args);
        }

        private static FriendlyMessage GetMessage(this FriendlyMessageType value, bool translateArguments, params object[] args)
        {
            return new FriendlyMessage(value, translateArguments, args);
        }
        #endregion

        #region FriendlyMessages
        public static FriendlyMessage CloseApplicationAlert()
        {
            return FriendlyMessageType.CloseApplicationAlert.GetMessage();
        }

        public static FriendlyMessage CloseApplicationQuestion()
        {
            return FriendlyMessageType.CloseApplicationQuestion.GetMessage(AppTitle.FullName);
        }

        public static FriendlyMessage CloseCurrentFormQuestion(string formName)
        {
            return FriendlyMessageType.CloseCurrentFormQuestion.GetMessage(formName);
        }

        public static FriendlyMessage ContactAdministratorAdvice()
        {
            return FriendlyMessageType.ContactAdministratorAdvice.GetMessage();
        }

        public static FriendlyMessage ContactNetworkAdministratorAdvice()
        {
            return FriendlyMessageType.ContactNetworkAdministratorAdvice.GetMessage();
        }

        public static FriendlyMessage DatabaseConnectionAlert()
        {
            return FriendlyMessageType.DatabaseConnectionAlert.GetMessage();
        }

        public static FriendlyMessage DatabaseConnectionRestoreAdvice()
        {
            return FriendlyMessageType.DatabaseConnectionRestoreAdvice.GetMessage();
        }

        public static FriendlyMessage DatabaseLoginFailedInfo()
        {
            return FriendlyMessageType.DatabaseLoginFailedInfo.GetMessage(
                ApplicationMessage.DatabaseConnectionAlert().Message,
                ApplicationMessage.DatabaseConnectionRestoreAdvice().Message);
        }

        public static FriendlyMessage DatabaseUnavailableAlert()
        {
            return FriendlyMessageType.DatabaseUnavailableAlert.GetMessage();
        }

        public static FriendlyMessage DatabaseUnavailableInfo()
        {
            return FriendlyMessageType.DatabaseUnavailableInfo.GetMessage(
                ApplicationMessage.DatabaseConnectionAlert().Message,
                ApplicationMessage.DatabaseUnavailableAlert().Message,
                ApplicationMessage.ContactNetworkAdministratorAdvice().Message);
        }

        //TODO: use this one?? or more generic?
        public static FriendlyMessage DataGridViewWithNoSelectionAdvice()
        {
            return FriendlyMessageType.DataGridViewWithNoSelectionAdvice.GetMessage();
        }

        public static FriendlyMessage EntityNotFoundAlert()
        {
            return FriendlyMessageType.EntityNotFoundAlert.GetMessage();
        }

        public static FriendlyMessage EntityNotFoundCause()
        {
            return FriendlyMessageType.EntityNotFoundCause.GetMessage();
        }

        public static FriendlyMessage EntityNotFoundInfo()
        {
            return FriendlyMessageType.EntityNotFoundInfo.GetMessage(
                ApplicationMessage.EntityNotFoundAlert().Message,
                ApplicationMessage.EntityNotFoundCause().Message,
                ApplicationMessage.RetryReloadAdvice().Message);
        }

        public static FriendlyMessage FatalErrorAlert()
        {
            return FriendlyMessageType.FatalErrorAlert.GetMessage();
        }

        public static FriendlyMessage FormAlreadyOpenedAlert()
        {
            return FriendlyMessageType.FormAlreadyOpenedAlert.GetMessage();
        }

        public static FriendlyMessage GeneralAlert()
        {
            return FriendlyMessageType.GeneralAlert.GetMessage();
        }

        public static FriendlyMessage GenericMessage(string info)
        {
            return FriendlyMessageType.GenericMessage.GetMessage(info);
        }

        public static FriendlyMessage ItemNotAvailableAlert()
        {
            return FriendlyMessageType.ItemNotAvailableAlert.GetMessage();
        }

        public static FriendlyMessage MissingKeyValueAlert()
        {
            return FriendlyMessageType.MissingKeyValueAlert.GetMessage();
        }

        public static FriendlyMessage MissingKeyValueCause()
        {
            return FriendlyMessageType.MissingKeyValueCause.GetMessage();
        }

        public static FriendlyMessage MissingKeyValueInfo()
        {
            return FriendlyMessageType.MissingKeyValueInfo.GetMessage(
                ApplicationMessage.MissingKeyValueAlert().Message,
                ApplicationMessage.MissingKeyValueCause().Message,
                ApplicationMessage.ContactAdministratorAdvice().Message);
        }

        public static FriendlyMessage MissingMethodParameterAlert(string parameterName)
        {
            return FriendlyMessageType.MissingMethodParameterAlert.GetMessage(parameterName);
        }

        public static FriendlyMessage LoadDataAdvice()
        {
            return FriendlyMessageType.LoadDataAdvice.GetMessage();
        }

        public static FriendlyMessage PatiencePleaseInfo()
        {
            return FriendlyMessageType.PatiencePleaseInfo.GetMessage();
        }

        public static FriendlyMessage PropertyNotFoundAlert(string property)
        {
            return FriendlyMessageType.PropertyNotFoundAlert.GetMessage(property);
        }

        public static FriendlyMessage PropertyNotFoundInfo(FriendlyInfo info)
        {
            return FriendlyMessageType.PropertyNotFoundInfo.GetMessage(info.MainMessage, info.ExtraMessage, info.Advice);
        }

        public static FriendlyMessage RetriesExceededInfo(RetriesExceededException exception)
        {
            return FriendlyMessageType.RetriesExceededInfo.GetMessage(exception.Attempts);
        }

        public static FriendlyMessage RetryAdvice()
        {
            return FriendlyMessageType.RetryAdvice.GetMessage();
        }

        public static FriendlyMessage RetryQuestion()
        {
            return FriendlyMessageType.RetryQuestion.GetMessage();
        }

        public static FriendlyMessage RetryAlert()
        {
            return FriendlyMessageType.RetryAlert.GetMessage();
        }

        public static FriendlyMessage RetryReloadAdvice()
        {
            return FriendlyMessageType.RetryReloadAdvice.GetMessage();
        }

        public static FriendlyMessage RetryReloadQuestion()
        {
            return FriendlyMessageType.RetryReloadQuestion.GetMessage();
        }

        //TODO: make class for the parameters?
        public static FriendlyMessage SelectItemAdvice(string item, string action)
        {
            return FriendlyMessageType.SelectItemAdvice.GetMessage(item, action);
        }

        public static FriendlyMessage SettingsAlert()
        {
            return FriendlyMessageType.SettingsAlert.GetMessage();
        }

        public static FriendlyMessage SortingFailedAdvice()
        {
            return FriendlyMessageType.SortingFailedAdvice.GetMessage();
        }

        public static FriendlyMessage SortingFailedAlert()
        {
            return FriendlyMessageType.SortingFailedAlert.GetMessage();
        }

        public static FriendlyMessage TimeOutAlert()
        {
            return FriendlyMessageType.TimeOutAlert.GetMessage();
        }

        public static FriendlyMessage TimeOutInfo()
        {
            var sentence = StringHelper.Sentence(ApplicationMessage.TimeOutAlert().Message, ApplicationMessage.PatiencePleaseInfo().Message);
            return ApplicationMessage.GenericMessage(sentence);
        }

        public static FriendlyMessage UnhandledBugInfo(FriendlyInfo info)
        {
            return FriendlyMessageType.UnhandledBugInfo.GetMessage(info.MainMessage, info.ExtraMessage, info.Advice);
        }

        public static FriendlyMessage UnknownErrorAlert()
        {
            return FriendlyMessageType.UnknownErrorAlert.GetMessage();
        }
        #endregion
    }
}