﻿using ProjectX.General;

//TODO: summary
//TODO: bool translateArguments

namespace ProjectX.Infrastructure.Localization
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class FriendlyMessage : FriendlyMessageBase
    {
        #region Constructors
        internal FriendlyMessage(Notification notification, bool translateArguments)
            : base(notification.EnumType, notification.IsNotification, notification.Arguments)
        {
            this.SetMessage(translateArguments, notification.Arguments);
        }

        internal FriendlyMessage(FriendlyMessageType enumType, bool translateArguments, params object[] args)
            : base(enumType, args)
        {
            this.SetMessage(translateArguments, args);
        }

        internal FriendlyMessage(FriendlyProgressType enumType, bool translateArguments, params object[] args)
            : base(enumType, args)
        {
            this.SetMessage(translateArguments, args);
        }

        internal FriendlyMessage(FriendlyToolTipType enumType, bool translateArguments, params object[] args)
            : base(enumType, args)
        {
            this.SetMessage(translateArguments, args);
        }
        #endregion

        #region Methods
        protected sealed override void SetMessage(bool translateArguments, object[] args)
        {
            this.Message = StringHelper.Translation(this.Name, XLib.Preference.CurrentLanguage, translateArguments, args);
            if (this.EnumType is FriendlyProgressType)
            {
                this.Message = StringHelper.Concat(this.Message, XLib.Text.DefaultInfo);
            }
            if (this.Name == FriendlyMessageType.GenericMessage.ToString())
            {
                this.Message = args.TryArrayToString();
            }
            //TODO: make other FriendlyMessageType
            if (this.Name == FriendlyMessageType.UnhandledErrorAlert.ToString())
            {
                this.Message = StringHelper.Join("\n\n", this.Message, XLib.Text.Error, args.TryArrayToString());
            }
        }
        #endregion
    }
}