﻿using ProjectX.General;
using System;

namespace ProjectX.Infrastructure.Localization
{
    public static class ApplicationError
    {
        public static FatalException FatalException(Exception exception)
        {
            var message = ApplicationMessage.CloseApplicationAlert().Message;
            return new FatalException(message, exception);
        }
    }
}
