﻿using ProjectX.General;

//TODO: Register when resource not found!!!
//TODO: id -> case sentive!!!!
//TODO: summary

namespace ProjectX.Infrastructure.Localization
{
    public static class ApplicationToolTipMessage
    {
        //TODO: keep????
        #region Methods: Helpers
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="value"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static FriendlyMessage GetMessage(this FriendlyToolTipType value, params object[] args)
        {
            return GetMessage(value, XLib.Preference.TranslateArguments, args);
        }

        public static FriendlyMessage GetMessage(this FriendlyToolTipType value, bool translateArguments, params object[] args)
        {
            return new FriendlyMessage(value, translateArguments, args);
        }
        #endregion
    }
}
