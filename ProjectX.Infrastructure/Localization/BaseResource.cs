﻿using ProjectX.General;
using ProjectX.General.Localization;
using ProjectX.Infrastructure.Localization.ResourceTypes;
using System.Threading;
using System.Threading.Tasks;

//TODO: summary

namespace ProjectX.Infrastructure.Localization
{
    public abstract class BaseResource : IResoureceBuilder, ITranslator
    {
        #region Properties
        protected string Location { get; private set; }
        #endregion

        #region Methods -> IResoureceBuilder
        public abstract void GetResources(SupportedLanguage language, CustomResource resource, ApplicationResource translationResource);
        public abstract Task GetResourcesAsync(SupportedLanguage language, CustomResource resource, ApplicationResource translationResource, CancellationToken cancellationToken);
        #endregion

        #region Methods -> ITranslator
        public virtual void GetResources(string location, params CustomResource[] resources)
        {
            this.SetProperties(location);
            ApplicationResourceManager.GetResources(this, resources);
        }

        public virtual async Task GetResourcesAsync(string location, CancellationToken cancellationToken, params CustomResource[] resources)
        {
            this.SetProperties(location);
            await ApplicationResourceManager.GetResourcesAsync(this, cancellationToken, resources);
        }

        public virtual Translation TryTanslate(string key, SupportedLanguage language, bool translateArguments, params object[] args)
        {
            var success = ApplicationResourceManager.TryGetValue(key, language, out var value);
            //Translate args:
            if (args != null && args.Length > XLib.MagicNumber.IntZero)
            {
                var length = args.Length;
                var newArgs = new string[length];
                string newKey = null;
                for (int i = 0; i < length; i++)
                {
                    if (args[i] != null)
                    {
                        newKey = args[i].ToString();
                    }
                    if (translateArguments || newKey.GetType().IsEnum)
                    {
                        newArgs[i] = ApplicationResourceManager.GetValue(newKey, language);
                    }
                    else
                    {
                        newArgs[i] = newKey;
                    }
                }
                value = value.TryFormat(newArgs);
            }
            return new Translation(key, value, language, success);
        }
        #endregion

        #region Helpers
        private void SetProperties(string location)
        {
            this.Location = location;
        }
        #endregion
    }
}

