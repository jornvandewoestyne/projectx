﻿using ProjectX.General;
using System.Drawing;

namespace ProjectX.Infrastructure
{
    public static class ImageExtensions
    {
        public static Bitmap ToBitmap(this string image, Size size)
        {
            var result = default(Bitmap);
            if (!image.IsEmptyString())
            {
                var obj = ProjectX.Infrastructure.Properties.Resources.ResourceManager.GetObject(image);
                if (obj != null)
                {
                    var type = obj.GetType();
                    if (type == typeof(Icon))
                    {
                        var icon = (Icon)obj;
                        result = icon.ToBitmap(size);
                    }
                    else if (type == typeof(Bitmap))
                    {
                        result = new Bitmap((Bitmap)obj, size);
                    }
                }
            }
            return result;
        }

        public static Bitmap ToBitmap(this Icon icon, Size size)
        {
            return icon != null ? new Bitmap(icon.ToBitmap(), size) : default(Bitmap);
        }
    }
}
