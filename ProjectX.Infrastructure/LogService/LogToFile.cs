﻿using ProjectX.General;
using System;
using System.IO;

//TODO: name of logfile to settings!

namespace ProjectX.Infrastructure.LogService
{
    public class LogToFile : ILogger
    {
        #region Methods: -> ILogger
        public void Log(LogEntry entry)
        {
            Stream stream = null;
            try
            {
                //TODO: clean up!
                //var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                //TODO: XString
                //TODO: DefaultApplicationName when EMPTY!!!!!

                var file = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), AppTitle.FolderName, "Log", "LogFile.log");
                Directory.CreateDirectory(Path.GetDirectoryName(file));
                stream = new FileStream(file, FileMode.Append);
                using (var writer = new StreamWriter(stream))
                {
                    //stream = null;
                    //var value = StringHelper.Concat(new SurroundFormat(XLib.Text.VerticalEnumSeparator, null), entry);
                    var value = entry.ToString();
                    writer.WriteLine(value);
                }
            }
            //catch (Exception exception)
            //{
            //    //System.Windows.Forms.MessageBox.Show(exception.Message);
            //    //TODO: do some stuff...?
            //}
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        #endregion
    }
}
