﻿using ProjectX.General;
using System;

namespace ProjectX.Infrastructure.LogService
{
    internal static class LogExtensions
    {
        #region Extension Methods
        /// <summary>
        /// Sets the LogEntry.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="logLevel"></param>
        /// <param name="information"></param>
        public static void Log(this ILogger logger, LogLevel logLevel, LogInformation information)
        {
            if (logger != null)
            {
                logger.Log(logLevel, information, null);
            }
        }

        /// <summary>
        /// Sets the LogEntry.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="logLevel"></param>
        /// <param name="exception"></param>
        public static void Log(this ILogger logger, LogLevel logLevel, Exception exception)
        {
            if (logger != null)
            {
                logger.Log(logLevel, null, exception);
            }
        }

        /// <summary>
        /// Sets the LogEntry.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="logLevel"></param>
        /// <param name="information"></param>
        /// <param name="exception"></param>
        public static void Log(this ILogger logger, LogLevel logLevel, LogInformation information, Exception exception)
        {
            if (logger != null)
            {
                logger.Log(new LogEntry(logLevel, information, exception));
            }
        }
        #endregion
    }
}
