﻿using ProjectX.Data;
using ProjectX.General;
using ProjectX.Infrastructure.DependencyInjection;
using ProjectX.Infrastructure.Localization;
using System.Reflection;

namespace ProjectX.Infrastructure.LogService
{
    public sealed class LogInformation : LogInformationBase
    {
        #region Properties
        public override string ConnectionString => this.GetConnectionString();
        #endregion

        #region Constructor
        public LogInformation(LogInformation information, string errorInformation, FriendlyMessage friendlyMessage)
            : this(information.Interceptor, information.Assembly, information.AssemblyQualifiedName, information.FriendlyMethodInfo.MethodInfo, information.FriendlyMethodInfo.Arguments, errorInformation, friendlyMessage)
        { }

        public LogInformation(object interceptor, Assembly assembly, string assemblyQualifiedName, MethodInfo methodInfo, object[] arguments, string errorInformation = null, FriendlyMessage friendlyMessage = null)
            : base(interceptor, assembly, assemblyQualifiedName, methodInfo, arguments, errorInformation, friendlyMessage)
        { }
        #endregion

        #region Methods: Helpers
        private string GetConnectionString()
        {
            using (var dependency = new DI<IContext>(ApplicationDependencies.ContextTypes))
            {
                var context = dependency.Instance;
                return context != null ? context.ToString() : new DbConnection().ConnectionString.RemoveLineBreaks();
            }
        }
        #endregion
    }
}