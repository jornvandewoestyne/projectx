﻿using ProjectX.General;
using System;

namespace ProjectX.Infrastructure.LogService
{
    public sealed class LogEntry : LogEntryBase
    {
        #region Constructor
        public LogEntry(LogLevel logLevel, LogInformation information, Exception exception)
            : base(logLevel, information, exception)
        { }
        #endregion
    }
}
