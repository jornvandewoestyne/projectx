﻿
namespace ProjectX.Infrastructure.LogService
{
    /// <summary>
    /// Service which enables logging.
    /// </summary>
    public interface ILogger
    {
        #region Methods
        /// <summary>
        /// Run the log method.
        /// </summary>
        void Log(LogEntry entry);
        #endregion
    }
}
