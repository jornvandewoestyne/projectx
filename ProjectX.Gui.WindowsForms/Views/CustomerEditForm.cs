﻿using ProjectX.Domain.Models;
using ProjectX.General;
using ProjectX.Infrastructure.LogAttributes;
using ProjectX.UiControls.Windows.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Windows.Forms;

//TODO: Not fully implemented...yet! (commented lines!)

namespace ProjectX.Gui.WindowsForms.Views
{
    public partial class CustomerEditForm : XChildForm
    {
        #region Fields
        private Partner _entity = null;
        #endregion

        #region Property Overrides
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                if (!DesignMode)
                {
                    //WS_EX_COMPOSITED. Prevents flickering, with BUG on ObjectListview
                    cp.ExStyle |= 0x02000000;
                }
                return cp;
            }
        }
        #endregion

        #region Properties
        public Partner Entity
        {
            get => this._entity;
            set => this._entity = value;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Add a new entity.
        /// </summary>
        public CustomerEditForm(XMdiChildDetail detail)
            : this(detail, null)
        { }

        /// <summary>
        /// Edit a existing entity.
        /// </summary>
        public CustomerEditForm(XMdiChildDetail detail, Partner entity)
            : base(detail)
        {
            InitializeComponent();
            this.Entity = entity;
            //TODO -> PictureBox
            //this.mainPictureBox.Image = this.Picture
            //    = ProjectX.Infrastructure.Properties.Resources.Customer.ToBitmap(new System.Drawing.Size(64, 64));
        }
        #endregion

        #region DisposableForm
        public override void Subscribe()
        {
            this.Subscribe(Event.Load, this.LoadForm, this);
            this.Subscribe(Event.Click, this.Edit, this.editButton, this.editToolStripButton);
            this.Subscribe(Event.Click, this.Save, this.saveButton, this.saveToolStripButton);
            this.Subscribe(Event.Click, this.Delete, this.deleteButton, this.deleteToolStripButton);
            this.Subscribe(Event.Click, this.Switch, this.collapsePictureBox);
            this.Subscribe(Event.Click, this.ShowErrors, this.errorsListView);
            this.Subscribe(ListViewEvent.ItemSelectionChanged, this.CheckAndShowErrors, this.detailsListView);
            base.Subscribe();
        }

        protected override void DisposeComponents()
        {
            this.components?.Dispose();
            base.DisposeComponents();
        }

        protected override void Unsubscribe()
        {
            base.Unsubscribe();
        }

        protected override void Unbind()
        {
            base.Unbind();
        }
        #endregion

        #region Events
        [Log]
        protected override void LoadForm(object sender, EventArgs e)
        {
            base.LoadForm(sender, e);
            LoadForm();
        }

        private void Edit(object sender, EventArgs e)
        {
            SetEditControlsNew();
        }

        private void Delete(object sender, EventArgs e)
        {
            Delete();
        }

        private void Save(object sender, EventArgs e)
        {
            //Save();
        }

        private void Switch(object sender, EventArgs e)
        {
            SwitchPictureBox();
        }

        private void CheckAndShowErrors(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            CheckAndShowErrors(e);
        }

        private void ShowErrors(object sender, EventArgs e)
        {
            ShowErrors();
        }
        #endregion

        #region Methods: Initialize
        //private async void LoadForm()
        private void LoadForm()
        {
            if (this.Entity != null)
            {
                //string prefix = "_";
                SetEditControlsEdit();

                //TODO: test -> show loading...
                //await Task.Run(()=> FillDetailsListview());
                FillDetailsListview(this.detailsListView); //, voedingsMiddel.GetGroupValues<VoedingsMiddel>());

                //FillWarningListview(this.listView_Errors, voedingsMiddel.GetGroupValues<VoedingsMiddel>(), this.groupBox_Data, prefix, "Warning");
                //voedingsMiddel.SetResultValues<VoedingsMiddel>(this, this.groupBox_Data, prefix);
            }
            else
            {
                SetEditControlsInitial();
            }
            //SetUserControlValues(this.Text, true, true);
        }
        #endregion

        #region Methods: -> BaseChildForm Members
        protected override EntityKey GetEntityKey()
        {
            if (this.Entity != null)
            {
                using (var dependency = new XDI().Get<XKeyPresenter>())
                {
                    return dependency.Instance.GetEntityKey(this.Entity);
                }
            }
            return null;
        }

        protected override string GetEntityFullName()
        {
            if (this.Entity != null)
            {
                using (var dependency = new XDI().Get<XValuePresenter>())
                {
                    return dependency.Instance.GetFullName(this.Entity);
                }
            }
            return null;
        }
        #endregion

        #region Methods: Enable/Disable Controls
        private void SetEditControlsInitial()
        {
            ShowPictureBox(this.collapsePictureBox, false);
            EnableEditButtons(this.editButtonsPanel, this.editButton);
            EnableEditToolStripButtons(this.mainToolStrip, this.editToolStripButton);
            CollapsePanel1(this.dataSplitContainer, false);
            CollapsePanel1(this.inputSplitContainer, false);
            CollapsePanel2(this.mainSplitContainer, this.collapsePictureBox, true);
        }

        private void SetEditControlsEdit()
        {
            ShowPictureBox(this.collapsePictureBox, true);
            EnableEditButtons(this.editButtonsPanel, null);
            EnableEditToolStripButtons(this.mainToolStrip, null);
            CollapsePanel1(this.dataSplitContainer, false);
            CollapsePanel1(this.inputSplitContainer, false);
            CollapsePanel2(this.mainSplitContainer, this.collapsePictureBox, true);
        }

        private void SetEditControlsNew()
        {
            ShowPictureBox(this.collapsePictureBox, true);
            EnableEditButtons(this.editButtonsPanel, this.saveButton);
            EnableEditToolStripButtons(this.mainToolStrip, this.saveToolStripButton);
            CollapsePanel1(this.dataSplitContainer, false);
            CollapsePanel1(this.inputSplitContainer, false);
            CollapsePanel2(this.mainSplitContainer, this.collapsePictureBox, false);
        }

        private void SetEditControlsSave()
        {
            SetEditControlsNew();
        }

        private void SetEditControlsDelete()
        {
            ShowPictureBox(this.collapsePictureBox, false);
            EnableEditButtons(this.editButtonsPanel, null);
            EnableEditToolStripButtons(this.mainToolStrip, null);
            CollapsePanel2(this.mainSplitContainer, this.collapsePictureBox, true);
            this.detailsListView.ForeColor = Color.Red;
            this.summaryLabel.ForeColor = Color.Red;
            //TODO: hard-coded string
            this.summaryLabel.Text = StringHelper.Title(this.summaryLabel.Text, "Deleted!");
        }

        private void ShowErrors()
        {
            if (this.inputSplitContainer.Panel1Collapsed == false)
            {
                if (this.errorsListView.Items.Count > XLib.MagicNumber.IntZero)
                {
                    CollapsePanel1Input(this.dataSplitContainer);
                }
            }
        }

        private void CheckAndShowErrors(ListViewItemSelectionChangedEventArgs e)
        {
            if (e.Item.ImageKey.ToLower() == "warning" && this.Entity != null)
            {
                CollapsePanel1(this.dataSplitContainer, true);
                CollapsePanel1(this.inputSplitContainer, true);
                CollapsePanel2(this.mainSplitContainer, this.collapsePictureBox, false);
            }
        }

        private void EnableEditButtons(Panel panel, params Button[] buttonsToEnable)
        {
            //panel.EnableButtons(this.Entity, this.MdiChildDetail, buttonsToEnable);
        }

        private void EnableEditToolStripButtons(ToolStrip toolStrip, params ToolStripButton[] buttonsToEnable)
        {
            //toolStrip.EnableToolStripButtons(this.Entity, this.MdiChildDetail, buttonsToEnable);
        }

        private void ShowPictureBox(PictureBox pictureBox, bool bShow)
        {
            pictureBox.Visible = bShow;
        }

        private void SwitchPictureBox()
        {
            CollapsePanel2(this.mainSplitContainer, this.collapsePictureBox, !this.mainSplitContainer.Panel2Collapsed);
        }

        private void CollapsePanel2(SplitContainer splitContainer, PictureBox pictureBox, bool bCollapse)
        {
            splitContainer.CollapsePanel2(pictureBox, bCollapse);
        }

        private void CollapsePanel1Input(SplitContainer splitContainer)
        {
            splitContainer.Panel1Collapsed = !splitContainer.Panel1Collapsed;
        }

        private void CollapsePanel1(SplitContainer splitContainer, bool bCollapse)
        {
            splitContainer.Panel1Collapsed = bCollapse;
        }
        #endregion

        #region Methods: Actions
        //private Product Save()
        //{
        //    // Declare some variables:
        //    string message = null;
        //    string prefix = "_";
        //    bool bResult = false;
        //    Color backColor = Color.White;
        //    // Reset labels:
        //    ResetValidationErrors(this.groupBox_Data, prefix, Color.Black);
        //    // Save input:
        //    VoedingsMiddel voedingsMiddel = SetInstance();
        //    string[] action = GetActionStringEdit();
        //    if (voedingsMiddel.InputDataWithWinForm<VoedingsMiddel>(this, this.groupBox_Data, this.groupBox_Result, prefix))
        //    {
        //        VoedingsMiddelenBestand voedingsMiddelen = ((MainForm)this.MdiParent).VoedingsMiddelen;
        //        if (this.ActionIdentifier == MenuGroupAction.Nieuw) 
        //        {
        //            bResult = voedingsMiddelen.Toevoegen(voedingsMiddel);
        //            this.CurrentId = voedingsMiddel;
        //        }
        //        else if (this.ActionIdentifier == MenuGroupAction.Bewerken) 
        //        {
        //            bResult = voedingsMiddelen.Wijzigen(voedingsMiddel);
        //        }
        //    }            
        //    if (bResult)
        //    {
        //        message = String.Format("{0} is {1}!", this.FormIdentifier, action[XLib.Number.Index0]);                       
        //        backColor = Color.LightGreen;
        //        string newTitle = String.Format("{0} - {1}", this.FormIdentifier, voedingsMiddel.Naam);               
        //        ResetOpenChild(newTitle, MenuGroupAction.Bewerken);
        //        SetUserControlValues(this.Text, true);
        //        FillDetailsListview(this.listView_Details, voedingsMiddel.GetGroupValues<VoedingsMiddel>());
        //    }
        //    else
        //    {
        //        backColor = Color.Red;
        //        if (voedingsMiddel.IsDubbel)
        //        {
        //            message = String.Format("{0} is reeds toegevoegd!", this.FormIdentifier);
        //        }
        //        else
        //        {
        //            message = String.Format("Het {0} is mislukt!", action[XLib.Number.Index1]);
        //            ICollection<ValidationResult> validationErrors = voedingsMiddel.GetValidationResult();
        //            ShowValidationErrors(this.groupBox_Data, this.listView_Errors, validationErrors, prefix, backColor);         
        //        }              
        //    }
        //    SetLabelInfo(this.label_Info, message, backColor, Color.White);
        //    FillWarningListview(this.listView_Errors, voedingsMiddel.GetGroupValues<VoedingsMiddel>(), this.groupBox_Data, prefix, "Warning");
        //    SetEditControlsSave();
        //    return voedingsMiddel;
        //}

        private bool Delete()
        {
            bool bResult = false;
            //    string[] action = GetActionStringDelete();
            //    string message = String.Format("Wilt u de {0} {1} (J/N)?", this.FormIdentifier, action[XLib.Number.Index1]);
            //    DialogResult msg =
            //            MessageBoxPlus.Show(message, this.ParentForm.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            //    switch (msg)
            //    {
            //        case DialogResult.Yes:
            //            VoedingsMiddelenBestand voedingsMiddelen = ((MainForm)this.MdiParent).VoedingsMiddelen;
            //            bResult = voedingsMiddelen.Verwijderen(this.CurrentId);
            //            if (bResult) 
            //            {
            //                message = String.Format("De {0} is {1}!", this.FormIdentifier, action[XLib.Number.Index0]);
            //                string newTitle = String.Format("{0} ({1}) - {2}", this.FormIdentifier, action[XLib.Number.Index0], this.CurrentId.Naam);
            //                this.CurrentId = null;
            //                ResetOpenChild(newTitle, MenuGroupAction.Bewerken);
            //                SetEditControlsDelete();

            //            }
            //            else 
            //            {
            //                //message = String.Format("Het {0} van de {1} is mislukt!", action[XLib.Number.Index1], this.FormIdentifier);
            //                return bResult;
            //            }
            //            break;
            //        default:
            //            message = String.Format("Het {0} van de {1} is geannuleerd!", action[XLib.Number.Index1], this.FormIdentifier);
            //            break;
            //    }
            //    MessageBoxPlus.Show(message, this.ParentForm.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            return bResult;
        }

        private Partner SetInstance()
        {
            if (this.MdiChildDetail.ActionIdentifier == MenuGroupAction.New)
            {
                return new Partner();
            }
            else if (this.MdiChildDetail.ActionIdentifier == MenuGroupAction.Edit)
            {
                return this.Entity;
            }
            return null;
        }

        private string[] GetActionStringEdit()
        {
            string[] action = new string[2];
            if (this.MdiChildDetail.ActionIdentifier == MenuGroupAction.New)
            {
                //TODO: add these fields to the database?
                action[XLib.MagicNumber.Index0] = "toegevoegd";
                action[XLib.MagicNumber.Index1] = "toevoegen";
            }
            else if (this.MdiChildDetail.ActionIdentifier == MenuGroupAction.Edit)
            {
                action[XLib.MagicNumber.Index0] = "gewijzigd";
                action[XLib.MagicNumber.Index1] = "wijzigen";
            }
            return action;
        }

        private string[] GetActionStringDelete()
        {
            string[] action = new string[2];
            action[XLib.MagicNumber.Index0] = "verwijderd";
            action[XLib.MagicNumber.Index1] = "verwijderen";
            return action;
        }
        #endregion

        #region Methods: ValidationErrors
        private void ShowValidationErrors(GroupBox groupBox, ListView listview, ICollection<ValidationResult> validationErrors
            , string prefix, Color color, string imageKey = "Error")
        {
            foreach (ValidationResult error in validationErrors)
            {
                foreach (var member in error.MemberNames)
                {
                    foreach (Control ctrl in groupBox.Controls)
                    {
                        if (ctrl is Label)
                        {
                            if (ctrl.Name.Contains(prefix + member))
                            {
                                ctrl.ForeColor = color;
                                SetImageKey((Label)ctrl, imageKey);
                                break;
                            }
                        }
                    }
                }
            }
            FillErrorListview(listview, validationErrors, imageKey);
        }

        private void ResetValidationErrors(GroupBox groupBox, string prefix, Color color)
        {
            foreach (Control ctrl in groupBox.Controls)
            {
                if (ctrl is Label)
                {
                    ctrl.ForeColor = color;
                    SetImageKey((Label)ctrl, null);
                }
            }
            ResetErrorListview(this.errorsListView);
        }

        private void SetImageKey(GroupBox groupBox, string name, string prefix, string imageKey)
        {
            foreach (Control ctrl in groupBox.Controls)
            {
                if (ctrl is Label)
                {
                    if (ctrl.Name.ToLower().Contains(prefix + name.ToLower()))
                    {
                        Label label = (Label)ctrl;
                        label.ImageKey = imageKey;
                        break;
                    }
                }
            }
        }

        private void SetLabelInfo(Label labelInfo, string message, Color backColor, Color foreColor)
        {
            labelInfo.Text = message;
            labelInfo.BackColor = backColor;
            labelInfo.ForeColor = foreColor;
        }

        private void SetImageKey(Label label, string imageKey)
        {
            label.ImageKey = imageKey;
        }
        #endregion

        #region Methods: Reset OpenChilds
        private void ResetOpenChild(string newTitle, XMdiChildDetail detail)
        {
            WinFormXMenuUserControlHelper.ResetOpenChild<XMenuUserControl>(this, this.MdiChildDetail, newTitle);
        }
        #endregion

        #region Methods: Listiew
        private void ResetErrorListview(ListView listview)
        {
            listview.Items.Clear();
        }

        private void FillErrorListview(ListView listview, ICollection<ValidationResult> errors, string imageKey)
        {
            ResetErrorListview(listview);
            ListViewGroup group = new ListViewGroup("Foutmelding");
            listview.Groups.Add(group);
            foreach (ValidationResult error in errors)
            {
                ListViewItem listItem = new ListViewItem(error.ToString(), (ListViewGroup)group);
                listItem.ImageKey = imageKey;
                listview.Items.Add(listItem);
            }
            listview.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        }

        //private void FillWarningListview(ListView listview, List<GroupValue> groups, GroupBox groupBox
        //    , string prefix, string imageKey)
        //{
        //    ListViewGroup group = new ListViewGroup("Waarschuwing");
        //    listview.Groups.Add(group);
        //    foreach (GroupValue groupValue in groups)
        //    {
        //        if (groupValue.Warning != null)
        //        {
        //            ListViewItem listItem = new ListViewItem(groupValue.Warning.ToString(), (ListViewGroup)group);
        //            listItem.ImageKey = imageKey;
        //            listview.Items.Add(listItem);
        //            SetImageKey(groupBox, groupValue.Title, prefix, imageKey);
        //        }
        //    }
        //    listview.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        //}

        [Log]
        private void FillDetailsListview(ListView listview) //, List<GroupValue> groups = null)
        //private void FillDetailsListview() //, List<GroupValue> groups = null)
        {
            //var listview = this.detailsListView;
            listview.Items.Clear();

            var service = this.Service;
            var groups = service.GetEntityValues(this.Entity);
            groups.AddRange(service.GetEntityValues(this.Entity.MasterContact));
            groups.AddRange(service.GetEntityValues(this.Entity.MasterContact.VatNumber));
            groups.AddRange(service.GetEntityValues(this.Entity.MasterContact.Person));
            groups.AddRange(service.GetEntityValues(this.Entity.MasterContact.Company));
            groups.AddRange(service.GetEntityValues(this.Entity.MasterContact.Address));

            foreach (GroupValue groupValue in groups)
            {
                bool bFound = false;
                ListViewGroup group = null;
                foreach (ListViewGroup newGroup in listview.Groups)
                {
                    if (newGroup.Header.Equals(groupValue.Group))
                    {
                        group = newGroup;
                        bFound = true;
                        break;
                    }
                }
                if (!bFound)
                {
                    group = new ListViewGroup(groupValue.Group);
                    listview.Groups.Add(group);
                }
                ListViewItem listItem = new ListViewItem(groupValue.Title.ToString(), (ListViewGroup)group);
                listItem.SubItems.Add(groupValue.Data.ToString());
                if (groupValue.Warning != null) { listItem.ImageKey = "Warning"; }
                listview.Items.Add(listItem);
            }

            //TODO: this is just for testing -> move to BLL!!!
            ////Partner entity = this.Service.GetPartnerByID(this.Entity.PartnerID);
            //Partner entity = this.Entity;
            //ListViewItem listItem;
            //if (entity.Contact.Company != null)
            //{
            //    listItem = new ListViewItem(entity.Contact.Company.Name);
            //    listview.Items.Add(listItem);
            //}
            //if (entity.Contact.Person != null)
            //{
            //    listItem = new ListViewItem(entity.Contact.Person.FirstName);
            //    listview.Items.Add(listItem);
            //    listItem = new ListViewItem(entity.Contact.Person.LastName);
            //    listview.Items.Add(listItem);
            //    //TODO: remove/add test line! (raise an error for demo!)
            //    //listview.Items.Add(listItem);
            //    if (entity.Contact.Person.PersonGender != null)
            //    {
            //        listItem = new ListViewItem(entity.Contact.Person.PersonGender.LocalName);
            //        listview.Items.Add(listItem);
            //    }
            //}
            //if (entity.Contact.Address != null)
            //{
            //    listItem = new ListViewItem(entity.Contact.Address.Street);
            //    listview.Items.Add(listItem);
            //    listItem = new ListViewItem(entity.Contact.Address.HouseNumber);
            //    listview.Items.Add(listItem);
            //    listItem = new ListViewItem(entity.Contact.Address.City.ZipCode);
            //    listview.Items.Add(listItem);
            //    listItem = new ListViewItem(entity.Contact.Address.City.Name);
            //    listview.Items.Add(listItem);
            //    listItem = new ListViewItem(entity.Contact.Address.City.Country.Name);
            //    listview.Items.Add(listItem);
            //}
            listview.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        }
        #endregion        

        #region Methods: Service/Entity Helpers

        #endregion

        #region Methods: BackgroundWorker

        #endregion
    }
}