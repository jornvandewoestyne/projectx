﻿using ProjectX.General;
using ProjectX.Infrastructure;
using ProjectX.UiControls.Windows.Forms;

namespace ProjectX.Gui.WindowsForms.Views
{
    public partial class CustomerSearchForm : XChildSearchForm
    {
        #region Constructors
        public CustomerSearchForm(XMdiChildDetail detail)
            : base(detail, new XDI().Get<XCustomerCollectionPresenter>().Instance)
        {
            InitializeComponent();

            //this.Presenter.Container = this;
            this.Paging = new Paging(1, XLib.Limit.MaxItemsPerPage);
            this.Ordering = null; //Ordering.None; // this.Presenter.GetOrdering(new OrderingSetting("FullName", OrderingDirection.Descending)); //new Ordering(this.Presenter.DataType, new OrderingSetting("FullName"));

            //TODO:
            this.PictureBox.Image = this.Picture
                = ProjectX.Infrastructure.Properties.Resources.Customer.ToBitmap(new System.Drawing.Size(48, 48));
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            //TODO: correct?
            //this.Presenter?.Container?.Dispose();
            this.Presenter?.Dispose();

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region DisposableForm
        protected override void DisposeComponents()
        {
            this.components?.Dispose();
            base.DisposeComponents();
        }

        protected override void Unsubscribe()
        {
            base.Unsubscribe();
        }

        protected override void Unbind()
        {
            base.Unbind();
        }
        #endregion
    }
}
