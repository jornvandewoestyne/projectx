﻿namespace ProjectX.Gui.WindowsForms.Views
{
    using ProjectX.UiControls.Windows.Forms;

    partial class CustomerEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomerEditForm));
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("Nieuwe klant...");
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("Producten...");
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("Offertes...");
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("Orders...");
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("Facturen...");
            System.Progress<string> progress_11 = new System.Progress<string>();
            this.editToolStripButton = new ProjectX.UiControls.Windows.Forms.XToolStripButtonEdit();
            this.deleteToolStripButton = new ProjectX.UiControls.Windows.Forms.XToolStripButtonDelete();
            this.saveToolStripButton = new ProjectX.UiControls.Windows.Forms.XToolStripButtonSave();
            this.infoGroupBox = new ProjectX.UiControls.Windows.Forms.XGroupBox();
            this.infoLabel = new ProjectX.UiControls.Windows.Forms.XLabel();
            this.infoPictureBox = new ProjectX.UiControls.Windows.Forms.XPictureBox();
            this.inputSplitContainer = new System.Windows.Forms.SplitContainer();
            this.dataSplitContainer = new System.Windows.Forms.SplitContainer();
            this.dataGroupBox = new ProjectX.UiControls.Windows.Forms.XGroupBox();
            this.errorsListView = new ProjectX.UiControls.Windows.Forms.XListView();
            this.errorsListViewValueColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.validationImageList = new System.Windows.Forms.ImageList(this.components);
            this.errorsPanel = new ProjectX.UiControls.Windows.Forms.XPanel();
            this.mainToolStrip = new ProjectX.UiControls.Windows.Forms.XToolStrip();
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.resultGroupBox = new ProjectX.UiControls.Windows.Forms.XGroupBox();
            this.summaryLabel = new ProjectX.UiControls.Windows.Forms.XLabel();
            this.detailsListView = new ProjectX.UiControls.Windows.Forms.XListView();
            this.detailsListViewLabelColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.detailsListViewValueColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.collapsePictureBox = new ProjectX.UiControls.Windows.Forms.XPictureBox();
            this.editButtonsPanel = new ProjectX.UiControls.Windows.Forms.XPanel();
            this.saveButton = new ProjectX.UiControls.Windows.Forms.XButtonSave();
            this.deleteButton = new ProjectX.UiControls.Windows.Forms.XButtonDelete();
            this.editButton = new ProjectX.UiControls.Windows.Forms.XButtonEdit();
            this.linksListView = new ProjectX.UiControls.Windows.Forms.XListView();
            this.mainPictureBox = new ProjectX.UiControls.Windows.Forms.XPictureBox();
            this.searchPanel = new ProjectX.UiControls.Windows.Forms.XPanel();
            this.infoGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.infoPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputSplitContainer)).BeginInit();
            this.inputSplitContainer.Panel1.SuspendLayout();
            this.inputSplitContainer.Panel2.SuspendLayout();
            this.inputSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSplitContainer)).BeginInit();
            this.dataSplitContainer.Panel1.SuspendLayout();
            this.dataSplitContainer.Panel2.SuspendLayout();
            this.dataSplitContainer.SuspendLayout();
            this.mainToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.resultGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.collapsePictureBox)).BeginInit();
            this.editButtonsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainPictureBox)).BeginInit();
            this.searchPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // childHeaderUserControl
            // 
            this.childHeaderUserControl.Size = new System.Drawing.Size(1000, 25);
            this.childHeaderUserControl.Title = "";
            // 
            // editToolStripButton
            // 
            this.editToolStripButton.AutoSize = false;
            this.editToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editToolStripButton.Enabled = false;
            this.editToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("editToolStripButton.Image")));
            this.editToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.editToolStripButton.Name = "editToolStripButton";
            this.editToolStripButton.Size = new System.Drawing.Size(30, 30);
            this.editToolStripButton.Text = "Edit";
            this.editToolStripButton.Work = ProjectX.General.Work.Edit;
            this.editToolStripButton.XControlText = ProjectX.General.FriendlyTextType.Edit;
            this.editToolStripButton.XToolTipText = ProjectX.General.FriendlyToolTipType.Edit;
            // 
            // deleteToolStripButton
            // 
            this.deleteToolStripButton.AutoSize = false;
            this.deleteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteToolStripButton.Enabled = false;
            this.deleteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("deleteToolStripButton.Image")));
            this.deleteToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.deleteToolStripButton.Name = "deleteToolStripButton";
            this.deleteToolStripButton.Size = new System.Drawing.Size(30, 30);
            this.deleteToolStripButton.Text = "Delete";
            this.deleteToolStripButton.Work = ProjectX.General.Work.Delete;
            this.deleteToolStripButton.XControlText = ProjectX.General.FriendlyTextType.Delete;
            this.deleteToolStripButton.XToolTipText = ProjectX.General.FriendlyToolTipType.Delete;
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.AutoSize = false;
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Enabled = false;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(30, 30);
            this.saveToolStripButton.Text = "Save";
            this.saveToolStripButton.Work = ProjectX.General.Work.Save;
            this.saveToolStripButton.XControlText = ProjectX.General.FriendlyTextType.Save;
            this.saveToolStripButton.XToolTipText = ProjectX.General.FriendlyToolTipType.Save;
            // 
            // infoGroupBox
            // 
            this.infoGroupBox.Controls.Add(this.infoLabel);
            this.infoGroupBox.Controls.Add(this.infoPictureBox);
            this.infoGroupBox.Location = new System.Drawing.Point(18, 10);
            this.infoGroupBox.Name = "infoGroupBox";
            this.infoGroupBox.Size = new System.Drawing.Size(370, 85);
            this.infoGroupBox.TabIndex = 0;
            this.infoGroupBox.TabStop = false;
            // 
            // infoLabel
            // 
            this.infoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.infoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infoLabel.Image = null;
            this.infoLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.infoLabel.Location = new System.Drawing.Point(85, 23);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(267, 45);
            this.infoLabel.TabIndex = 2;
            this.infoLabel.Text = "Vul de gegevens in voor de klant...";
            this.infoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // infoPictureBox
            // 
            this.infoPictureBox.Location = new System.Drawing.Point(15, 20);
            this.infoPictureBox.Name = "infoPictureBox";
            this.infoPictureBox.Size = new System.Drawing.Size(50, 50);
            this.infoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.infoPictureBox.TabIndex = 0;
            this.infoPictureBox.TabStop = false;
            // 
            // inputSplitContainer
            // 
            this.inputSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.inputSplitContainer.IsSplitterFixed = true;
            this.inputSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.inputSplitContainer.Name = "inputSplitContainer";
            this.inputSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // inputSplitContainer.Panel1
            // 
            this.inputSplitContainer.Panel1.BackColor = System.Drawing.Color.White;
            this.inputSplitContainer.Panel1.Controls.Add(this.infoGroupBox);
            this.inputSplitContainer.Panel1MinSize = 105;
            // 
            // inputSplitContainer.Panel2
            // 
            this.inputSplitContainer.Panel2.Controls.Add(this.dataSplitContainer);
            this.inputSplitContainer.Size = new System.Drawing.Size(496, 445);
            this.inputSplitContainer.SplitterDistance = 105;
            this.inputSplitContainer.TabIndex = 2;
            this.inputSplitContainer.TabStop = false;
            // 
            // dataSplitContainer
            // 
            this.dataSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.dataSplitContainer.IsSplitterFixed = true;
            this.dataSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.dataSplitContainer.Name = "dataSplitContainer";
            // 
            // dataSplitContainer.Panel1
            // 
            this.dataSplitContainer.Panel1.BackColor = System.Drawing.Color.White;
            this.dataSplitContainer.Panel1.Controls.Add(this.dataGroupBox);
            this.dataSplitContainer.Panel1MinSize = 400;
            // 
            // dataSplitContainer.Panel2
            // 
            this.dataSplitContainer.Panel2.Controls.Add(this.errorsListView);
            this.dataSplitContainer.Panel2.Controls.Add(this.errorsPanel);
            this.dataSplitContainer.Panel2MinSize = 30;
            this.dataSplitContainer.Size = new System.Drawing.Size(496, 336);
            this.dataSplitContainer.SplitterDistance = 400;
            this.dataSplitContainer.TabIndex = 1;
            // 
            // dataGroupBox
            // 
            this.dataGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGroupBox.Location = new System.Drawing.Point(18, 10);
            this.dataGroupBox.Name = "dataGroupBox";
            this.dataGroupBox.Size = new System.Drawing.Size(370, 343);
            this.dataGroupBox.TabIndex = 0;
            this.dataGroupBox.TabStop = false;
            this.dataGroupBox.Text = "Gegevens";
            // 
            // errorsListView
            // 
            this.errorsListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.errorsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.errorsListViewValueColumnHeader});
            this.errorsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.errorsListView.FullRowSelect = true;
            this.errorsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.errorsListView.HideSelection = false;
            this.errorsListView.Location = new System.Drawing.Point(0, 22);
            this.errorsListView.MultiSelect = false;
            this.errorsListView.Name = "errorsListView";
            this.errorsListView.ShowItemToolTips = true;
            this.errorsListView.Size = new System.Drawing.Size(92, 314);
            this.errorsListView.SmallImageList = this.validationImageList;
            this.errorsListView.TabIndex = 1;
            this.errorsListView.UseCompatibleStateImageBehavior = false;
            this.errorsListView.View = System.Windows.Forms.View.Details;
            this.errorsListView.Work = ProjectX.General.Work.None;
            // 
            // errorsListViewValueColumnHeader
            // 
            this.errorsListViewValueColumnHeader.Text = "Item";
            this.errorsListViewValueColumnHeader.Width = 100;
            // 
            // validationImageList
            // 
            this.validationImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("validationImageList.ImageStream")));
            this.validationImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.validationImageList.Images.SetKeyName(0, "Validated");
            this.validationImageList.Images.SetKeyName(1, "Warning");
            this.validationImageList.Images.SetKeyName(2, "Error");
            // 
            // errorsPanel
            // 
            this.errorsPanel.BackColor = System.Drawing.Color.White;
            this.errorsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.errorsPanel.Location = new System.Drawing.Point(0, 0);
            this.errorsPanel.Name = "errorsPanel";
            this.errorsPanel.Size = new System.Drawing.Size(92, 22);
            this.errorsPanel.TabIndex = 2;
            this.errorsPanel.Work = ProjectX.General.Work.None;
            this.errorsPanel.XControlText = ProjectX.General.FriendlyTextType.None;
            this.errorsPanel.XToolTipText = ProjectX.General.FriendlyToolTipType.None;
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.AutoSize = false;
            this.mainToolStrip.BackColor = System.Drawing.Color.White;
            this.mainToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.mainToolStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripButton,
            this.saveToolStripButton,
            this.deleteToolStripButton});
            this.mainToolStrip.Location = new System.Drawing.Point(0, 25);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Padding = new System.Windows.Forms.Padding(5, 0, 1, 0);
            this.mainToolStrip.Size = new System.Drawing.Size(1000, 32);
            this.mainToolStrip.TabIndex = 1;
            this.mainToolStrip.Work = ProjectX.General.Work.None;
            this.mainToolStrip.XControlText = ProjectX.General.FriendlyTextType.None;
            this.mainToolStrip.XToolTipText = ProjectX.General.FriendlyToolTipType.None;
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 57);
            this.mainSplitContainer.Name = "mainSplitContainer";
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.BackColor = System.Drawing.Color.White;
            this.mainSplitContainer.Panel1.Controls.Add(this.resultGroupBox);
            this.mainSplitContainer.Panel1.Controls.Add(this.editButtonsPanel);
            this.mainSplitContainer.Panel1.Controls.Add(this.linksListView);
            this.mainSplitContainer.Panel1.Controls.Add(this.mainPictureBox);
            this.mainSplitContainer.Panel1MinSize = 500;
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.searchPanel);
            this.mainSplitContainer.Size = new System.Drawing.Size(1000, 445);
            this.mainSplitContainer.SplitterDistance = 500;
            this.mainSplitContainer.TabIndex = 1;
            // 
            // resultGroupBox
            // 
            this.resultGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.resultGroupBox.Controls.Add(this.summaryLabel);
            this.resultGroupBox.Controls.Add(this.detailsListView);
            this.resultGroupBox.Controls.Add(this.collapsePictureBox);
            this.resultGroupBox.Location = new System.Drawing.Point(135, 0);
            this.resultGroupBox.Name = "resultGroupBox";
            this.resultGroupBox.Size = new System.Drawing.Size(361, 436);
            this.resultGroupBox.TabIndex = 5;
            this.resultGroupBox.TabStop = false;
            // 
            // summaryLabel
            // 
            this.summaryLabel.AutoSize = true;
            this.summaryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.summaryLabel.Image = null;
            this.summaryLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.summaryLabel.Location = new System.Drawing.Point(10, 20);
            this.summaryLabel.Name = "summaryLabel";
            this.summaryLabel.Size = new System.Drawing.Size(92, 16);
            this.summaryLabel.TabIndex = 13;
            this.summaryLabel.Text = "Klantenfiche";
            // 
            // detailsListView
            // 
            this.detailsListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.detailsListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.detailsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.detailsListViewLabelColumnHeader,
            this.detailsListViewValueColumnHeader});
            this.detailsListView.FullRowSelect = true;
            this.detailsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.detailsListView.HideSelection = false;
            this.detailsListView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
            this.detailsListView.Location = new System.Drawing.Point(7, 41);
            this.detailsListView.MultiSelect = false;
            this.detailsListView.Name = "detailsListView";
            this.detailsListView.ShowItemToolTips = true;
            this.detailsListView.Size = new System.Drawing.Size(348, 372);
            this.detailsListView.SmallImageList = this.validationImageList;
            this.detailsListView.TabIndex = 12;
            this.detailsListView.TabStop = false;
            this.detailsListView.UseCompatibleStateImageBehavior = false;
            this.detailsListView.View = System.Windows.Forms.View.Details;
            this.detailsListView.Work = ProjectX.General.Work.None;
            // 
            // detailsListViewLabelColumnHeader
            // 
            this.detailsListViewLabelColumnHeader.Text = "Item";
            this.detailsListViewLabelColumnHeader.Width = 120;
            // 
            // detailsListViewValueColumnHeader
            // 
            this.detailsListViewValueColumnHeader.Text = "Waarde";
            // 
            // collapsePictureBox
            // 
            this.collapsePictureBox.Image = ((System.Drawing.Image)(resources.GetObject("collapsePictureBox.Image")));
            this.collapsePictureBox.Location = new System.Drawing.Point(335, 16);
            this.collapsePictureBox.Name = "collapsePictureBox";
            this.collapsePictureBox.Size = new System.Drawing.Size(20, 20);
            this.collapsePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.collapsePictureBox.TabIndex = 11;
            this.collapsePictureBox.TabStop = false;
            // 
            // editButtonsPanel
            // 
            this.editButtonsPanel.BackColor = System.Drawing.Color.Transparent;
            this.editButtonsPanel.Controls.Add(this.saveButton);
            this.editButtonsPanel.Controls.Add(this.deleteButton);
            this.editButtonsPanel.Controls.Add(this.editButton);
            this.editButtonsPanel.Location = new System.Drawing.Point(18, 108);
            this.editButtonsPanel.Name = "editButtonsPanel";
            this.editButtonsPanel.Size = new System.Drawing.Size(80, 82);
            this.editButtonsPanel.TabIndex = 9;
            this.editButtonsPanel.Work = ProjectX.General.Work.None;
            this.editButtonsPanel.XControlText = ProjectX.General.FriendlyTextType.None;
            this.editButtonsPanel.XToolTipText = ProjectX.General.FriendlyToolTipType.None;
            // 
            // saveButton
            // 
            this.saveButton.Enabled = false;
            this.saveButton.Location = new System.Drawing.Point(0, 58);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(80, 23);
            this.saveButton.TabIndex = 3;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            // 
            // deleteButton
            // 
            this.deleteButton.Enabled = false;
            this.deleteButton.Location = new System.Drawing.Point(0, 29);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(80, 23);
            this.deleteButton.TabIndex = 2;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            // 
            // editButton
            // 
            this.editButton.Enabled = false;
            this.editButton.Location = new System.Drawing.Point(0, 0);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(80, 23);
            this.editButton.TabIndex = 1;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = true;
            // 
            // linksListView
            // 
            this.linksListView.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.linksListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.linksListView.Cursor = System.Windows.Forms.Cursors.Hand;
            this.linksListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linksListView.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.linksListView.HideSelection = false;
            this.linksListView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5});
            this.linksListView.Location = new System.Drawing.Point(18, 214);
            this.linksListView.MultiSelect = false;
            this.linksListView.Name = "linksListView";
            this.linksListView.Size = new System.Drawing.Size(80, 192);
            this.linksListView.TabIndex = 6;
            this.linksListView.TabStop = false;
            this.linksListView.UseCompatibleStateImageBehavior = false;
            this.linksListView.View = System.Windows.Forms.View.List;
            this.linksListView.Work = ProjectX.General.Work.None;
            // 
            // mainPictureBox
            // 
            this.mainPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.mainPictureBox.Location = new System.Drawing.Point(18, 18);
            this.mainPictureBox.Name = "mainPictureBox";
            this.mainPictureBox.Size = new System.Drawing.Size(80, 80);
            this.mainPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.mainPictureBox.TabIndex = 1;
            this.mainPictureBox.TabStop = false;
            // 
            // searchPanel
            // 
            this.searchPanel.Controls.Add(this.inputSplitContainer);
            this.searchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchPanel.Location = new System.Drawing.Point(0, 0);
            this.searchPanel.Name = "searchPanel";
            this.searchPanel.Size = new System.Drawing.Size(496, 445);
            this.searchPanel.TabIndex = 2;
            this.searchPanel.Tag = "";
            this.searchPanel.Work = ProjectX.General.Work.None;
            this.searchPanel.XControlText = ProjectX.General.FriendlyTextType.None;
            this.searchPanel.XToolTipText = ProjectX.General.FriendlyToolTipType.None;
            // 
            // CustomerEditForm
            // 
            this.AcceptButton = this.saveButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1000, 525);
            this.Controls.Add(this.mainSplitContainer);
            this.Controls.Add(this.mainToolStrip);
            this.Name = "CustomerEditForm";
            this.ProgressManager = progress_11;
            this.Tag = "";
            this.Controls.SetChildIndex(this.childHeaderUserControl, 0);
            this.Controls.SetChildIndex(this.mainToolStrip, 0);
            this.Controls.SetChildIndex(this.mainSplitContainer, 0);
            this.infoGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.infoPictureBox)).EndInit();
            this.inputSplitContainer.Panel1.ResumeLayout(false);
            this.inputSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.inputSplitContainer)).EndInit();
            this.inputSplitContainer.ResumeLayout(false);
            this.dataSplitContainer.Panel1.ResumeLayout(false);
            this.dataSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataSplitContainer)).EndInit();
            this.dataSplitContainer.ResumeLayout(false);
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.resultGroupBox.ResumeLayout(false);
            this.resultGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.collapsePictureBox)).EndInit();
            this.editButtonsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainPictureBox)).EndInit();
            this.searchPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private XPictureBox infoPictureBox;
        private XGroupBox infoGroupBox;
        private System.Windows.Forms.SplitContainer inputSplitContainer;
        private XPanel searchPanel;
        private XToolStrip mainToolStrip;
        private XToolStripButtonSave saveToolStripButton;
        private XGroupBox dataGroupBox;
        private XLabel infoLabel;
        private System.Windows.Forms.SplitContainer dataSplitContainer;
        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private XPictureBox mainPictureBox;
        private XButtonEdit editButton;
        private XGroupBox resultGroupBox;
        private XListView linksListView;
        private XButtonSave saveButton;
        private XButtonDelete deleteButton;
        private XPanel editButtonsPanel;
        private XPictureBox collapsePictureBox;
        private XToolStripButtonEdit editToolStripButton;
        private XToolStripButtonDelete deleteToolStripButton;
        private XListView detailsListView;
        private System.Windows.Forms.ColumnHeader detailsListViewLabelColumnHeader;
        private System.Windows.Forms.ColumnHeader detailsListViewValueColumnHeader;
        private XLabel summaryLabel;
        private System.Windows.Forms.ImageList validationImageList;
        private XListView errorsListView;
        private System.Windows.Forms.ColumnHeader errorsListViewValueColumnHeader;
        private XPanel errorsPanel;
    }
}