﻿using ProjectX.Business;
using ProjectX.Gui.WindowsForms.Controls;
using ProjectX.UiControls.Windows.Forms;

namespace ProjectX.Gui.WindowsForms.Views
{
    public partial class MainForm : XMainForm //XMainFormOld
    {
        #region Constructor
        public MainForm(IService service)
            : base(service)
        {
            //InitializeComponent();
        }
        #endregion

        #region Overrides
        public override void Quit()
        {
            base.Quit();
            Program.Quit();
        }

        protected override XMenuUserControl CreateInstanceMenuUserControl(Domain.Models.Menu menu)
        {
            return new MenuUserControl(menu);
        }
        #endregion
    }
}
