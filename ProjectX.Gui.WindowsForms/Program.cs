﻿using ProjectX.General;
using ProjectX.Gui.WindowsForms.Views;
using ProjectX.UiControls.Windows.Forms;
using System;
using System.Threading;
using System.Windows.Forms;

//TODO: Global: Clean References of all Classes???
//TODO: Entity Validation -> @back-end + @front-end
//TODO: static Translator

#region Source
//https://stackoverflow.com/questions/49902641/async-task-main-in-winforms-application-having-async-initialization
#endregion

namespace ProjectX.Gui.WindowsForms
{
    static class Program
    {
        #region Methods: Entry point
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        [XIntercept(Strategy = ExceptionStrategy.Exit, MaxRetries = (uint)NumberConstant.None)]
        static void Main()
        {
            //Local function which implements the asynchronous startup:
            async void StartUpHandlerAsync(object startUp)
            {
                await XApplicationStartUp.RunAsync();
            }

            //Run the application with the given Mainform:
            XApplicationSettings.SetDefaults<MainForm>();
            using (var context = new WindowsFormsSynchronizationContext())
            {
                SynchronizationContext.SetSynchronizationContext(context);
                try
                {
                    context.Post(StartUpHandlerAsync, null);
                    Application.Run(XApplication.ApplicationContext);
                }
                finally
                {
                    SynchronizationContext.SetSynchronizationContext(null);
                }
            }
        }
        #endregion

        #region Methods:
        [Obsolete]
        public static void Quit()
        {
            Application.ExitThread();
        }
        #endregion
    }
}