﻿Fonetisch (soundex)
https://blogs.u2u.be/diederik/post/2011/02/10/Fuzzy-lookup-of-Names-with-a-Dutch-Metaphone-implementation.aspx

//CHECK: 
//https://riptutorial.com/csharp/example/28712/disposing-of-the-singleton-instance-when-it-is-no-longer-needed
//https://thomasfreudenberg.com/archive/2019/01/28/async-await-in-desktop-applications/
//https://thomaslevesque.com/2015/11/11/explicitly-switch-to-the-ui-thread-in-an-async-method/
//https://stephenhaunts.com/2014/10/14/using-async-and-await-to-update-the-ui-thread/comment-page-1/
//https://www.tek-tips.com/viewthread.cfm?qid=1747045
//http://blog.nethouse.se/2018/11/29/asynchronously-wait-task-complete-timeout/
//https://stackoverflow.com/questions/4238345/asynchronously-wait-for-taskt-to-complete-with-timeout
// https://devblogs.microsoft.com/pfxteam/crafting-a-task-timeoutafter-method/
//https://stackoverflow.com/questions/57337189/sqlconnection-openasync-blocks-ui-when-sql-server-is-shutdown

Caching:
https://ntotten.com/2011/04/29/caching-data-in-asp-net-applications/
https://dotnetcodr.com/2014/09/04/externalising-dependencies-with-dependency-injection-in-net-part-2-caching/
https://www.codeproject.com/Articles/51409/Exploring-Caching-Using-Caching-Application-Enterp
https://stackoverflow.com/questions/35359399/should-i-store-localization-data-for-desktop-application-in-database/35360096

Factory: CHECK!
https://dotnetcodr.com/2015/09/29/design-patterns-and-practices-in-net-the-factory-patterns-concrete-static-abstract/


http://stackoverflow.com/questions/102913/caching-data-objects-when-using-repository-service-pattern-and-mvc

Formatting
https://msdn.microsoft.com/en-us/library/26etazsy(v=vs.110).aspx

Ideas from the internet:

https://www.simple-talk.com/dotnet/net-framework/catching-bad-data-in-entity-framework/

http://www.thereformedprogrammer.net/architecture-of-business-layer-working-with-entity-framework/
https://buildplease.com/pages/repositories-dto/
https://www.simple-talk.com/dotnet/net-tools/entity-framework-performance-and-what-you-can-do-about-it/

String.Format:
//http://www.codeproject.com/Tips/988589/Nameof-Operator-A-New-Feature-of-Csharp
//http://geekswithblogs.net/BlackRabbitCoder/archive/2015/04/02/c.net-little-wonders-getting-the-name-of-an-identifier.aspx
//http://stackoverflow.com/questions/72121/finding-the-variable-name-passed-to-a-function-in-c-sharp
//count format items in as string c#
//http://stackoverflow.com/questions/4989106/string-format-count-the-number-of-expected-args
//http://stackoverflow.com/questions/948303/is-there-a-better-way-to-count-string-format-placeholders-in-a-string-in-c
//https://gist.github.com/Virtlink/7532225
//http://haacked.com/archive/2009/01/04/fun-with-named-formats-string-parsing-and-edge-cases.aspx/
//http://www.simosh.com/article/bfjabh-named-string-formatting-in-c-sharp.html

Resource files:
https://msdn.microsoft.com/en-us/library/7k989cfy(v=vs.90).aspx
http://www.codeproject.com/Articles/29923/Assigning-an-application-s-icon-to-all-forms-in-th

https://msdn.microsoft.com/en-us/library/ms973839.aspx -> tips on performance
https://lowrymedia.com/2014/06/30/understanding-entity-framework-and-sql-indexes/
http://stackoverflow.com/questions/22618237/how-to-create-index-in-entity-framework-6-2-with-code-first
https://msdn.microsoft.com/en-us/data/jj556606 -> ef interceptor config
https://cmatskas.com/logging-and-tracing-with-entity-framework-6/
http://www.sidarok.com/web/blog/content/2008/05/02/10-tips-to-improve-your-linq-to-sql-application-performance.html
http://www.mindscapehq.com/documentation/lightspeed/Basic-Operations/Querying-the-Database-Using-LINQ
https://www.talksharp.com/entity-framework-paging
http://www.mikesdotnetting.com/article/252/mvc-5-with-ef-6-in-visual-basic-advanced-entity-framework-scenarios

Related to Entity Framework, Layered Architecture and Dependency Injection:
http://codingandflying.com/2014/06/implementing-a-data-access-layer-with-entity-framework-6-1-part-2/
http://vikutech.blogspot.be/2015/01/architecture-solution-composting-repository-pattern-unit-of-work-dependency-injection-factory-pattern.html
http://blog.longle.net/2013/05/11/genericizing-the-unit-of-work-pattern-repository-pattern-with-entity-framework-in-mvc/
http://www.codeproject.com/Articles/543810/Dependency-Injection-and-Unit-Of-Work-using-Castle
http://www.codeproject.com/Articles/838097/CRUD-Operations-Using-the-Generic-Repository-Pat
http://gaui.is/how-to-mock-the-datacontext-entity-framework/
http://stackoverflow.com/questions/16064902/dependency-injection-in-unit-of-work-pattern-using-repositories
http://imomin.blog.com/archives/200/
https://garfbradazweb.wordpress.com/2012/10/21/repository-and-unit-of-work-design-pattern-asp-net-mvc/
http://www.primaryobjects.com/CMS/Article122
!!!http://www.codeproject.com/Articles/814768/CRUD-Operations-Using-the-Generic-Repository-Patte
http://stackoverflow.com/questions/16572743/generic-repository-with-data-access-layer

!!https://blog.magnusmontin.net/2013/05/30/generic-dal-using-entity-framework/comment-page-1/
http://codereview.stackexchange.com/questions/93733/object-cache-c-take-two

!!!http://simpleinjector.readthedocs.org/en/latest/advanced.html

http://blog.magnusmontin.net/2013/05/30/generic-dal-using-entity-framework/comment-page-1/

https://msdn.microsoft.com/en-us/library/orm-9780596520281-01-20.aspx

!!!http://codereview.stackexchange.com/questions/57127/do-i-need-ninject-when-implementing-dal-with-generic-repository-and-unit-of-work

Fun reading about managing DbContext:
http://mehdi.me/ambient-dbcontext-in-ef6/

Fun reading:
https://sergeromerosoftware.wordpress.com/
http://thatextramile.be/blog

Microsoft:
https://contoso.azurewebsites.net/
http://www.asp.net/mvc

Data Validation:
https://www.simple-talk.com/dotnet/.net-framework/catching-bad-data-in-entity-framework/
http://www.codeproject.com/Articles/10093/Validators-for-Windows-Forms-ValidationProvider-Co

Connectionstring:
http://waldoscode.blogspot.be/2014/11/entity-framework-configurable-database.html

Exception Handling:
!!!!http://www.codeproject.com/Articles/9538/Exception-Handling-Best-Practices-in-NET
http://kenneththorman.blogspot.be/2010/10/windows-forms-net-handling-unhandled.html
https://msdn.microsoft.com/en-us/library/system.windows.forms.application.threadexception.aspx
https://msdn.microsoft.com/en-us/library/bb397417.aspx
http://blogs.msmvps.com/deborahk/global-exception-handler-winforms/
http://stackoverflow.com/questions/4113277/exception-handling-in-n-tier-applications

http://www.codeproject.com/Articles/85569/Exception-Handling-in-Tier-Architecture
http://www.codeproject.com/Articles/14912/A-Simple-Class-to-Catch-Unhandled-Exceptions-in-Wi
https://richnewman.wordpress.com/2007/04/08/top-level-exception-handling-in-windows-forms-applications-part-1/
http://mike.woelmer.com/2009/04/dealing-with-unhandled-exceptions-in-winforms/

http://issuu.com/orangelc/docs/mastering_ninject_for_dependency_in !!!!

LogService:
http://stackoverflow.com/questions/10519567/adding-logservice-to-my-application
http://codereview.stackexchange.com/questions/57909/logging-strategy-setup
http://ayende.com/blog/3474/logging-the-aop-way
http://www.eyecatch.no/blog/logging-with-log4net-in-c/
http://blog.kulman.sk/intercepting-methods-with-ninject-for-error-logging/
http://rationalgeek.com/blog/introducing-ninjectautologging/
http://stackoverflow.com/questions/7040235/how-to-set-up-an-optional-method-interception-with-ninject
http://stackoverflow.com/questions/6386461/ninject-intercept-any-method-with-certain-attribute
http://blog.tonysneed.com/2011/10/21/global-error-handling-in-asp-net-mvc-3-with-ninject/
http://blog.simplecode.eu/post/Creating-a-simple-logging-Interceptor-using-NInject-302
http://simpleinjector.readthedocs.org/en/latest/InterceptionExtensions.html

!!!! http://baharestani.com/2013/12/30/interception-with-ninject/ 
!!!!https://www.simple-talk.com/dotnet/.net-framework/designing-c-software-with-interfaces/


!!!
https://www.simple-talk.com/dotnet/.net-framework/designing-c-software-with-interfaces/

Check Download:
http://sourceforge.net/projects/ntierframework/files/

Encrypting:
http://www.codeproject.com/Tips/598863/EncryptionplusDecryptionplusConnectionplusStringpl
http://www.codeproject.com/Tips/461449/Encrypting-the-passowrd-of-a-connection-string-in

Idea! Use of Settings.settings file and other ideas:
https://weblog.west-wind.com/posts/2012/Dec/28/Building-a-better-NET-Application-Configuration-Class-revisited

PropertyGrid:
http://www.csharp-examples.net/readonly-propertygrid/

Localization:
http://www.codeproject.com/Articles/2138/Globalized-property-grid

AOP:
https://berniecook.wordpress.com/2013/02/09/repository-caching-with-aspect-oriented-programming/
http://jeffbelback.me/posts/2015/06/01/principles-of-aop/
http://www.artisancode.co.uk/2014/07/aspect-orientated-programming/
http://stackoverflow.com/questions/6386461/ninject-intercept-any-method-with-certain-attribute
http://stackoverflow.com/questions/15098757/ninject-conventions-and-interception
https://berniecook.wordpress.com/2013/02/09/repository-caching-with-aspect-oriented-programming/
-> looks very interesting:
http://www.postsharp.net/blog/post/Aspect-Oriented-Programming-vs-Dependency-Injection

AOP: PostSharp:
http://doc.postsharp.net/method-interception
http://doc.postsharp.net/method-decorator
http://doc.postsharp.net/exception-handling

Controls: ProgressBar:
https://emoacht.wordpress.com/2011/10/16/color-customizable-progressbar-for-visual-style/

Java:
http://www.javacodegeeks.com/2011/01/10-tips-proper-application-logging.html


Solve issues: EF reverse engineering -> customize templates
http://sofd.developer-works.com/article/19667010/MIssing+EfTextTemplateHost

Collections:
http://www.claudiobernasconi.ch/2013/07/22/when-to-use-ienumerable-icollection-ilist-and-list/

Replacing special charaters:
http://stackoverflow.com/questions/5459641/replacing-characters-in-c-sharp-ascii

DataGridView:
https://msdn.microsoft.com/en-us/library/wc06dx4f(v=vs.85).aspx
http://www.codeproject.com/Articles/848637/Nested-DataGridView-in-windows-forms-csharp

Localization:
http://stackoverflow.com/questions/12897800/localize-windows-forms-and-change-the-language-at-runtime
http://www.dotnetcurry.com/ShowArticle.aspx?ID=174

Advanced LINQ:
http://www.albahari.com/nutshell/predicatebuilder.aspx

BackgroundWorker:
http://www.codeproject.com/Articles/99143/BackgroundWorker-Class-Sample-for-Beginners
https://msdn.microsoft.com/en-us/library/system.componentmodel.backgroundworker(v=vs.110).aspx
http://www.c-sharpcorner.com/uploadfile/mahesh/backgroundworker-in-C-Sharp/
http://www.c-sharpcorner.com/UploadFile/deepak.sharma00/asynchronous-processing-of-database-records-on-a-winform-ui/

Regex:
https://msdn.microsoft.com/en-us/library/az24scfc(v=vs.110).aspx

BindingList:
http://blogs.msdn.com/b/adonet/archive/2011/02/16/ef-feature-ctp5-code-first-and-winforms-databinding.aspx

http://codebetter.com/jeremymiller/2007/10/23/how-are-you-building-winforms-applications-what-s-hard-about-it/

BetterListView:
http://www.componentowl.com/
http://www.componentowl.com/better-listview-express

datagridview paging:
http://stackoverflow.com/questions/2825771/how-can-we-do-pagination-in-datagridview-in-winform
http://www.codeproject.com/Articles/211551/A-Simple-way-for-Paging-in-DataGridView-in-WinForm

https://blogs.msdn.microsoft.com/codefx/2010/11/26/all-in-one-windows-forms-code-samples/#Download

//linq to entities list contains dynamic
//https://www.nuget.org/packages/System.Linq.Dynamic.Library/
//https://weblog.west-wind.com/posts/2008/Apr/14/Dynamic-Queries-and-LINQ-Expressions
//http://blog.walteralmeida.com/2010/05/advanced-linq-dynamic-linq-library-add-support-for-contains-extension-.html
//https://blogs.msdn.microsoft.com/alexj/2009/03/25/tip-8-how-to-write-where-in-style-queries-using-linq-to-entities/
//https://blogs.msdn.microsoft.com/meek/2008/04/25/using-linq-expressions-to-generate-dynamic-methods/
//http://stackoverflow.com/questions/31302196/dynamically-select-column-name-using-linq-to-entities-ef6
//http://www.codeproject.com/Articles/493917/Dynamic-Querying-with-LINQ-to-Entities-and-Express
//http://stackoverflow.com/questions/3463479/querying-entity-with-linq-using-dyanmic-field-name
//http://stackoverflow.com/questions/1196991/get-property-value-from-string-using-reflection-in-c-sharp/1197004#1197004
//http://stackoverflow.com/questions/30173455/build-expression-tree-for-linq-using-listt-contains-method
//http://stackoverflow.com/questions/17734347/linq-to-entities-contains-with-dynamic-field-and-int32