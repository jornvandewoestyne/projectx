﻿using ProjectX.UiControls.Windows.Forms;
using System;

namespace ProjectX.Gui.WindowsForms.Controls
{
    public partial class MenuUserControl : XMenuUserControl
    {
        #region Constructor
        public MenuUserControl(ProjectX.Domain.Models.Menu menu)
            : base(menu)
        { }
        #endregion

        #region Overrides
        protected override XChildForm CreateNewInstanceUsingFormName(string instance, XMdiChildDetail detail = null)
        {
            return XInstanceHelper.CreateNewInstance(this, instance, detail);
        }

        protected override Type GetInstanceType(string instance)
        {
            return XInstanceHelper.GetInstanceType(this, instance);
        }

        protected override string GetFormName(string actionIdentifier)
        {
            return XInstanceHelper.GetInstanceFormName(this, actionIdentifier);
        }
        #endregion
    }
}
