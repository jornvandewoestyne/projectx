﻿Use of "Reverse Engineer Code First":
**************************************

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 ONLY WORKS WITH VISUAL STUDIO 2013
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Make sure "Entity Framework Power Tools Beta 4" is properly installed!
-> Tools -> Extensions and Updates -> Online -> Search: "Power Tools"

-> Right click on the Project -> Entity Framework -> Reverse Engineer Code First -> Wait untill finished...
-> Remove Models (and Mappings) you don't need.
-> Copy DbSets and OnModelCreating(...) from the generated DbContext into your Context implementations 
   found in the Data Access Layer, as well as to your IContext (only DbSets, -> remove "public virtual").
-> Remove the generated DbContext after you copied the neccessary items.
-> et voilà, up and running!

-> TODO:
	Views: No Nullable<int> primarykeys, check PrimaryKey in Mapping
	General: implement BaseLocalizedEntity, BaseSearchEntity
	General: properties like contact1, contact2,...


Implemented customizations to the "Customize Reverse Engineer Templates":
*********************************************************************
Context.tt: Changes to the properties
--------------------------------------
-> added "public virtual IDbSet":
	"DbSet<<#= set.ElementType.Name #>> <#= set.Name #> { get; set; }"
	replaced by -> "public virtual IDbSet<<#= set.ElementType.Name #>> <#= set.Name #> { get; set; }"

Enity.tt: Adding a BaseType to the entities from which they inhered:
--------------------------------------------------------------------
-> added: "using ProjectX.General.BaseTypes;"
-> added: ": BaseEntity"
	"public partial class <#= efHost.EntityType.Name #>"
	replaced by -> "public partial class <#= efHost.EntityType.Name #> : BaseEntity"


Use of "Customize Reverse Engineer Templates":
***********************************************

ISSUE (BUG!):
Compiling transformation: The type or namespace name ‘EfTextTemplateHost’ could not be found 
(are you missing a using directive or an assembly reference?)

SOLUTION:
-> Source: http://sofd.developer-works.com/article/19667010/MIssing+EfTextTemplateHost
-> First:
	EfTextTemplateHost is part of the EF Powertools. So make sure it is installed correctly.
	Add this to to the T4 file and change the placehoders [] to match your system:
	<#@ assembly name="C:\Users\[username]\AppData\Local\Microsoft\VisualStudio\[vsVersion]\Extensions\[extensionHash]\EFPowerTools.dll" #>
	-> <#@ assembly name="C:\Users\jorn\AppData\Local\Microsoft\VisualStudio\12.0\Extensions\naoczut1.o2q\EFPowerTools.dll" #>

-> Then:
	Rebuild - Delete line - Close Visual Studio -> Reopen -> FIXED!