using System;
using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class Partner : BaseCommonEntity
    {
        public Partner()
        {
            this.PartnerSearches = new List<PartnerSearch>();
            this.Contacts = new List<Contact>();
            this.VatNumbers = new List<VatNumber>();
        }

        public int PartnerID { get; set; }
        public string PartnerCode { get; set; }
        public int MasterContactID { get; set; }
        public int SalesInvoiceContactID { get; set; }
        public int SalesDeliveryContactID { get; set; }
        public Nullable<int> SalesMailingContactID { get; set; }
        public int LanguageID { get; set; }
        public bool IsBlocked { get; set; }
        public virtual ICollection<PartnerSearch> PartnerSearches { get; set; }
        public virtual Language Language { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual Contact SalesDeliveryContact { get; set; }
        public virtual Contact SalesInvoiceContact { get; set; }
        public virtual Contact SalesMailingContact { get; set; }
        public virtual Contact MasterContact { get; set; }
        public virtual ICollection<VatNumber> VatNumbers { get; set; }
    }
}
