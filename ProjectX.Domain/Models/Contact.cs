using ProjectX.General;
using System;
using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class Contact : BaseEntity
    {
        public Contact()
        {
            this.Communications = new List<Communication>();
            this.ContactRoles = new List<ContactRole>();
            this.SalesDeliveryPartners = new List<Partner>();
            this.SalesInvoicePartners = new List<Partner>();
            this.SalesMailingPartners = new List<Partner>();
            this.MasterPartners = new List<Partner>();
            this.Departments = new List<Department>();
        }

        public int ContactID { get; set; }
        public string CallName { get; set; }
        public Nullable<int> PartnerID { get; set; }
        public Nullable<int> CompanyID { get; set; }
        public Nullable<int> PersonID { get; set; }
        public Nullable<int> AddressID { get; set; }
        public Nullable<int> VatNumberID { get; set; }
        public bool IsBlocked { get; set; }
        public virtual Address Address { get; set; }
        public virtual ICollection<Communication> Communications { get; set; }
        public virtual Company Company { get; set; }
        public virtual Partner Partner { get; set; }
        public virtual Person Person { get; set; }
        public virtual VatNumber VatNumber { get; set; }
        public virtual ICollection<ContactRole> ContactRoles { get; set; }
        public virtual ICollection<Partner> SalesDeliveryPartners { get; set; }
        public virtual ICollection<Partner> SalesInvoicePartners { get; set; }
        public virtual ICollection<Partner> SalesMailingPartners { get; set; }
        public virtual ICollection<Partner> MasterPartners { get; set; }
        public virtual ICollection<Department> Departments { get; set; }
    }
}
