using System;
using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class Address : BaseCommonEntity
    {
        public Address()
        {
            this.Contacts = new List<Contact>();
        }

        public int AddressID { get; set; }
        public string Site { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string BoxNumber { get; set; }
        public string Block { get; set; }
        public int DisplayFormatID { get; set; }
        public int LanguageID { get; set; }
        public Nullable<int> CityID { get; set; }
        public virtual City City { get; set; }
        public virtual Language Language { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
