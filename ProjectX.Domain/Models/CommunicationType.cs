using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class CommunicationType : BaseLocalizedEntity
    {
        public CommunicationType()
        {
            this.Communications = new List<Communication>();
        }

        public int CommunicationTypeID { get; set; }
        public virtual ICollection<Communication> Communications { get; set; }
    }
}
