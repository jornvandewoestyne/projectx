using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class Country : BaseLocalizedEntity
    {
        public Country()
        {
            this.Cities = new List<City>();
            this.CompanyCorporationTypes = new List<CompanyCorporationType>();
            this.VatNumbers = new List<VatNumber>();
        }

        public string CountryID { get; set; }
        public string TelephonePrefix { get; set; }
        public string CurrencyID { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual ICollection<City> Cities { get; set; }
        public virtual ICollection<CompanyCorporationType> CompanyCorporationTypes { get; set; }
        public virtual ICollection<VatNumber> VatNumbers { get; set; }
    }
}
