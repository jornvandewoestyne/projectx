﻿namespace ProjectX.Domain.Models
{
    public partial class CustomerPartnerSearch : BaseSearchEntity
    {
        //public int ID { get; set; }
        //public string SearchString { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime DateCreated { get; set; }
    }
}
