
using ProjectX.General;

namespace ProjectX.Domain.Models
{
    public partial class Communication : BaseEntity
    {
        public int CommunicationID { get; set; }
        public int ContactID { get; set; }
        public int CommunicationTypeID { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public bool IsDefault { get; set; }
        public bool IsPrivate { get; set; }
        public bool IsBlocked { get; set; }
        public virtual CommunicationType CommunicationType { get; set; }
        public virtual Contact Contact { get; set; }
    }
}
