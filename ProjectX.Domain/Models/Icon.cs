using ProjectX.General;
using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class Icon : BaseEntity
    {
        public Icon()
        {
            this.Activities = new List<Activity>();
            this.Members = new List<Member>();
            this.Menus = new List<Menu>();
            this.MenuActivities = new List<MenuActivity>();
            this.Modules = new List<Module>();
        }

        public int IconID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Activity> Activities { get; set; }
        public virtual ICollection<Member> Members { get; set; }
        public virtual ICollection<Menu> Menus { get; set; }
        public virtual ICollection<MenuActivity> MenuActivities { get; set; }
        public virtual ICollection<Module> Modules { get; set; }
    }
}
