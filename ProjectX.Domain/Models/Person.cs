using ProjectX.General;
using System;
using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class Person : BaseEntity
    {
        public Person()
        {
            this.Contacts = new List<Contact>();
        }

        public int PersonID { get; set; }
        public string CallName { get; set; }
        public Nullable<int> PersonSalutationID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<int> PersonJobTitleID { get; set; }
        public Nullable<int> DepartmentID { get; set; }
        public Nullable<int> PersonGenderID { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public string Picture { get; set; }
        public int LanguageID { get; set; }
        public bool IsBlocked { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual Language Language { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual Department Department { get; set; }
        public virtual PersonGender PersonGender { get; set; }
        public virtual PersonJobTitle PersonJobTitle { get; set; }
        public virtual PersonSalutation PersonSalutation { get; set; }
    }
}
