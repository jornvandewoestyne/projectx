
using ProjectX.General;

namespace ProjectX.Domain.Models
{
    public partial class Lexicon : BaseEntity
    {
        public int LexiconID { get; set; }
        public string KeyName { get; set; }
        public string Name { get; set; }
        public int LanguageID { get; set; }
        public virtual Language Language { get; set; }
    }
}
