using ProjectX.General;
using System;
using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class Menu : BaseEntity
    {
        public Menu()
        {
            this.MenuActivities = new List<MenuActivity>();
        }

        public int MenuID { get; set; }
        public int ModuleID { get; set; }
        public int MemberID { get; set; }
        public short SortKey { get; set; }
        public Nullable<int> IconID { get; set; }
        public bool IsBlocked { get; set; }
        public System.DateTime DateCreated { get; set; }
        public virtual Member Member { get; set; }
        public virtual Icon Icon { get; set; }
        public virtual Module Module { get; set; }
        public virtual ICollection<MenuActivity> MenuActivities { get; set; }
    }
}
