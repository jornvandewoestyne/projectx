﻿using ProjectX.General;

namespace ProjectX.Domain.Models
{
    public abstract class BaseCommonEntity : BaseEntity, ICommonEntity
    {
        public abstract object[] TitleProperties { get; }

        public abstract BaseEntitySummary EntitySummary { get; }
    }
}
