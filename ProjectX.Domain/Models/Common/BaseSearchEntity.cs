﻿using ProjectX.General;

namespace ProjectX.Domain.Models
{
    public abstract class BaseSearchEntity : BaseEntity, ISearchEntity
    {
        public int ID { get; set; }
        public string SearchString { get; set; }
    }
}
