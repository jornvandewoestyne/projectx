﻿using ProjectX.Domain.Models.Helpers;
using ProjectX.General;

namespace ProjectX.Domain.Models
{
    public abstract class BaseLocalizedEntity : BaseEntity, ILocalizedEntity
    {
        /// <summary>
        /// ...
        /// </summary>
        public string KeyName { get; set; }

        /// <summary>
        /// Returns the localized name of the application's current language.
        /// </summary>
        /// <returns></returns>
        public virtual string Name => this.GetTranslation(nameof(KeyName), false);

        /// <summary>
        /// Returns the localized name of the entity's current language.
        /// </summary>
        /// <returns></returns>
        public virtual string CurrentName => this.GetTranslation(nameof(KeyName), this.CurrentLanguage, false);
    }
}
