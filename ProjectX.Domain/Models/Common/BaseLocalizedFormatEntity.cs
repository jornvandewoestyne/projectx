﻿using ProjectX.Domain.Models.Helpers;

namespace ProjectX.Domain.Models
{
    public abstract class BaseLocalizedFormatEntity : BaseLocalizedEntity
    {
        /// <summary>
        /// Returns the localized name of the application's current language.
        /// </summary>
        /// <returns></returns>
        public override string Name => this.GetTranslation(nameof(KeyName), XLib.Preference.UseDisplayFormat);

        /// <summary>
        /// Returns the localized name of the entity's current language.
        /// </summary>
        /// <returns></returns>
        public override string CurrentName => this.GetTranslation(nameof(KeyName), this.CurrentLanguage, XLib.Preference.UseDisplayFormat);
    }
}
