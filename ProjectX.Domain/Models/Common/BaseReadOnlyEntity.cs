﻿using ProjectX.General;

namespace ProjectX.Domain.Models
{
    public abstract class BaseReadOnlyEntity : BaseEntity, IReadOnlyEntity
    {
    }
}
