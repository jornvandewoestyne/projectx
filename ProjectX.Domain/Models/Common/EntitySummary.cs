﻿using ProjectX.General;

namespace ProjectX.Domain.Models
{
    public sealed class EntitySummary : BaseEntitySummary
    {
        #region Properties      
        public override string Title { get; }
        public override string SubTitle { get; }
        public override string FullName { get; }
        public override string SortName { get; }
        #endregion

        #region Constructors
        internal EntitySummary(string title, string subTitle, string fullName, string sortName)
        {
            this.Title = title;
            this.SubTitle = subTitle;
            this.FullName = fullName;
            this.SortName = sortName;
        }
        #endregion
    }
}
