using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class ContactRoleType : BaseLocalizedEntity
    {
        public ContactRoleType()
        {
            this.ContactRoles = new List<ContactRole>();
        }

        public int ContactRoleTypeID { get; set; }
        public virtual ICollection<ContactRole> ContactRoles { get; set; }
    }
}
