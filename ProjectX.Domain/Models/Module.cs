using System;
using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class Module : BaseLocalizedEntity
    {
        public Module()
        {
            this.Menus = new List<Menu>();
        }

        public int ModuleID { get; set; }
        public short Sortkey { get; set; }
        public string Description { get; set; }
        public Nullable<int> IconID { get; set; }
        public bool IsBlocked { get; set; }
        public System.DateTime DateCreated { get; set; }
        public virtual ICollection<Menu> Menus { get; set; }
        public virtual Icon Icon { get; set; }
    }
}
