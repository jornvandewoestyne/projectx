using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class PersonJobTitle : BaseLocalizedEntity
    {
        public PersonJobTitle()
        {
            this.People = new List<Person>();
        }

        public int PersonJobTitleID { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Person> People { get; set; }
    }
}
