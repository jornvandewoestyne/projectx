using ProjectX.General;
using System;

namespace ProjectX.Domain.Models
{
    public partial class MenuActivity : BaseEntity
    {
        public int MenuID { get; set; }
        public int ActivityID { get; set; }
        public short SortKey { get; set; }
        public Nullable<int> IconID { get; set; }
        public bool IsBlocked { get; set; }
        public System.DateTime DateCreated { get; set; }
        public virtual Activity Activity { get; set; }
        public virtual Menu Menu { get; set; }
        public virtual Icon Icon { get; set; }
    }
}
