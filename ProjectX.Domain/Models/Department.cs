using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class Department : BaseLocalizedEntity
    {
        public Department()
        {
            this.People = new List<Person>();
            this.Contacts = new List<Contact>();
        }

        public int DepartmentID { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Person> People { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
