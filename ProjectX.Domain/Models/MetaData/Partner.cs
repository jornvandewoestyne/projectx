﻿using ProjectX.Domain.Models.Helpers;
using ProjectX.General;
using System.ComponentModel.DataAnnotations;

namespace ProjectX.Domain.Models
{
    [MetadataType(typeof(Partner.PartnerMetadata))]
    public partial class Partner
    {
        #region Overrides
        public override object[] TitleProperties => this.GetTitleProperties();
        public override BaseEntitySummary EntitySummary => this.GetEntitySummary();
        #endregion

        #region Metadata
        internal sealed class PartnerMetadata
        {
            [Display(GroupName = nameof(KeyWord.Details))]
            public string PartnerID { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public string PartnerCode { get; set; }
        }
        #endregion
    }
}
