﻿using ProjectX.Domain.Models.Helpers;
using ProjectX.General;
using System.ComponentModel.DataAnnotations;

namespace ProjectX.Domain.Models
{
    [MetadataType(typeof(City.CityMetadata))]
    public partial class City
    {
        #region Calculated Properties
        [BusinessRule.ComposedOf(nameof(FullCountryName), typeof(Country), nameof(FullCountryName))]
        public string FullCountryName => this.GetValue(nameof(Country.FullCountryName));
        public string CountryName => this.Country?.Name;
        #endregion

        #region Metadata
        internal sealed class CityMetadata
        {
            [Display(GroupName = nameof(KeyWord.Details))]
            public int CityID { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public int ZipCode { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public int Name { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public int FullCountryName { get; set; }
        }
        #endregion
    }
}
