﻿using ProjectX.Domain.Models.Helpers;
using ProjectX.General;
using System.ComponentModel.DataAnnotations;

namespace ProjectX.Domain.Models
{
    [MetadataType(typeof(Person.PersonMetadata))]
    public partial class Person
    {
        #region Calculated Properties
        [BusinessRule.ComposedOf(nameof(FullPersonName), typeof(Person), nameof(LastName), nameof(FirstName))]
        public string FullPersonName => this.GetValue(nameof(FullPersonName));
        //public string FullPersonName => this.GetFullPersonName();

        public string FullPersonNameAndSalutationName => this.GetFullPersonNameAndSalutationName();
        public string PersonSalutationName => this.GetPersonSalutationName();
        #endregion

        #region Metadata
        internal sealed class PersonMetadata
        {
            [Display(GroupName = nameof(KeyWord.Person))]
            public string PersonSalutationName { get; set; }

            [Display(GroupName = nameof(KeyWord.Person))]
            public string FirstName { get; set; }

            [Display(GroupName = nameof(KeyWord.Person))]
            public string LastName { get; set; }

            [Display(GroupName = nameof(KeyWord.Person))]
            public string FullPersonName { get; set; }
        }
        #endregion
    }
}
