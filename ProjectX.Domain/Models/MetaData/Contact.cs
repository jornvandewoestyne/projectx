﻿using ProjectX.Domain.Models.Helpers;
using ProjectX.General;
using System.ComponentModel.DataAnnotations;

namespace ProjectX.Domain.Models
{
    [MetadataType(typeof(Contact.ContactMetadata))]
    public partial class Contact
    {
        #region Calculated Properties
        public string FullName => this.GetFullContactName();
        #endregion

        #region Metadata
        internal sealed class ContactMetadata
        {
            [Display(GroupName = nameof(KeyWord.Details))]
            public int ContactID { get; set; }
        }
        #endregion
    }
}