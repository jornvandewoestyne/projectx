﻿using ProjectX.General;
using System.ComponentModel.DataAnnotations;

namespace ProjectX.Domain.Models
{
    [MetadataType(typeof(VatNumber.VatNumberMetadata))]
    public partial class VatNumber
    {
        #region Metadata
        internal sealed class VatNumberMetadata
        {
            [Display(GroupName = nameof(KeyWord.Details))]
            public string VatIdentificationNumber { get; set; }
        }
        #endregion
    }
}
