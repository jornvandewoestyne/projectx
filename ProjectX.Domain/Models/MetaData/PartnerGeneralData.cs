﻿using ProjectX.Domain.Models.Helpers;
using ProjectX.General;
using ProjectX.General.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectX.Domain.Models
{
    [MetadataType(typeof(PartnerGeneralData.PartnerGeneralDataMetadata))]
    public partial class PartnerGeneralData
    {
        #region Calculated Properties
        [BusinessRule.UseSeparator]
        [BusinessRule.ComposedOf(nameof(FullPartnerName), typeof(PartnerGeneralData), nameof(FullCompanyNameAndExtraName), nameof(FullPersonName))]
        public string FullPartnerName => this.GetValue(nameof(FullPartnerName));
        //public string FullPartnerName => new DefaultDisplayFormat(this, nameof(this.FullPartnerName)).ToString().TryTrimDigits(new char[] { XLib.Token.Separator, XLib.Token.Delimeter });
        //public string FullName => new BaseDisplayFormat(this, nameof(this.FullName), new DisplayFormatSetting("[{0}][ {1}][ ({2})], [{3}][ {4}]", ",", false)).ToString();
        //public string FullPartnerName => this.GetFullPartnerName();

        //[ComposedOf(typeof(PartnerGeneralData), ComposedOf.CompanyName, ComposedOf.CompanyCorporationTypeKeyName)]
        [BusinessRule.ComposedOf(nameof(FullCompanyName), typeof(Company), nameof(FullCompanyName))]
        public string FullCompanyName => this.GetValue(nameof(FullCompanyName));
        //public string FullCompanyName => new BaseDisplayFormat(this, nameof(this.FullCompanyName), new DisplayFormatSetting("[{0}][ {1}]", null, false)).ToString();
        //public string FullCompanyName => this.GetFullCompanyName();

        //[ComposedOf(typeof(PartnerGeneralData), ComposedOf.FullCompanyName, ComposedOf.ExtraName)]
        [BusinessRule.ComposedOf(nameof(FullCompanyNameAndExtraName), typeof(Company), nameof(FullCompanyNameAndExtraName))]
        public string FullCompanyNameAndExtraName => this.GetValue(nameof(FullCompanyNameAndExtraName));
        //public string FullCompanyNameAndExtraName => new BaseDisplayFormat(this, nameof(this.FullCompanyNameAndExtraName), new DisplayFormatSetting("[{0}][ ({1})]", null, false)).ToString();
        //public string FullCompanyNameAndExtraName => this.GetFullCompanyNameAndExtraName();

        [BusinessRule.ComposedOf(nameof(CompanyCorporationTypeName), typeof(Company), nameof(CompanyCorporationTypeName))]
        public string CompanyCorporationTypeName => this.GetCompanyCorporationTypeName();

        [BusinessRule.ComposedOf(nameof(FullPersonName), typeof(Person), nameof(FullPersonName))]
        public string FullPersonName => this.GetValue(nameof(FullPersonName));
        //public string FullPersonName => this.GetFullPersonName();

        [BusinessRule.ComposedOf(nameof(PersonSalutationName), typeof(PartnerGeneralData), nameof(PersonSalutationKeyName))]
        public string PersonSalutationName => this.GetPersonSalutationName();

        [BusinessRule.ComposedOf(nameof(GenderName), typeof(PartnerGeneralData), nameof(GenderKeyName))]
        public string GenderName => this.GetGenderName();

        [BusinessRule.ComposedOf(nameof(FullStreet), typeof(Address), nameof(FullStreet))]
        public string FullStreet => this.GetValue(nameof(FullStreet), this.AddressLanguageID.AsSupportedLanguage());
        //public string FullStreet => this.GetFullStreet();

        [BusinessRule.ComposedOf(nameof(CityName), typeof(PartnerGeneralData), nameof(CityKeyName))]
        public string CityName => this.GetCityName();

        public string CountryName => this.GetTranslation(nameof(CountryKeyName));
        //public string CountryName => this.GetCountryName();

        [BusinessRule.ComposedOf(nameof(FullCountryName), typeof(Country), nameof(FullCountryName))]
        public string FullCountryName => this.GetValue(nameof(FullCountryName));

        public int Test => 10000;
        public DateTime Date => DateTime.Now;
        #endregion

        #region Metadata
        internal sealed class PartnerGeneralDataMetadata
        {
            [DefaultOrderingProperty(Order = 2)]
            [DisplayPreference(DisplayPreference.Code)]
            [Display(GroupName = nameof(KeyWord.Details))]
            public string Code { get; set; }

            [DisplayPreference(DisplayPreference.Code)]
            [Display(GroupName = nameof(KeyWord.Details))]
            public int PartnerID { get; set; }

            [Obsolete]
            [DisplayPreference(DisplayPreference.Code)]
            [Display(GroupName = nameof(KeyWord.Details))]
            public int ContactID { get; set; }

            [DefaultOrderingProperty(Order = 1)]
            [DisplayPreference(DisplayPreference.ExtraLong)]
            [Display(GroupName = nameof(KeyWord.Details))]
            public string FullPartnerName { get; set; }

            [Optional]
            [DisplayPreference(DisplayPreference.Long)]
            [Display(GroupName = nameof(KeyWord.Company))]
            public string FullCompanyName { get; set; }

            [Optional]
            [DisplayPreference(DisplayPreference.Long)]
            [Display(GroupName = nameof(KeyWord.Company))]
            public string CompanyName { get; set; }

            [Optional]
            [DisplayPreference(DisplayPreference.Long)]
            [Display(GroupName = nameof(KeyWord.Company))]
            public string CompanyExtraName { get; set; }

            [Optional]
            [DisplayPreference(DisplayPreference.Long)]
            [Display(GroupName = nameof(KeyWord.Company))]
            public string FullCompanyNameAndExtraName { get; set; }

            [Optional]
            [DisplayPreference(DisplayPreference.Short)]
            [Display(GroupName = nameof(KeyWord.Company))]
            public string CompanyCorporationTypeName { get; set; }

            [Optional]
            [Display(GroupName = nameof(KeyWord.Person))]
            public string PersonSalutationName { get; set; }

            [Optional]
            [Display(GroupName = nameof(KeyWord.Person))]
            public string FirstName { get; set; }

            [Optional]
            [Display(GroupName = nameof(KeyWord.Person))]
            public string LastName { get; set; }

            [Optional]
            [DisplayPreference(DisplayPreference.Long)]
            [Display(GroupName = nameof(KeyWord.Person))]
            public string FullPersonName { get; set; }

            [DisplayPreference(DisplayPreference.Long)]
            [Display(GroupName = nameof(KeyWord.Address))]
            public string FullStreet { get; set; }

            [Optional]
            [Display(GroupName = nameof(KeyWord.Address))]
            public string Street { get; set; }

            [Optional]
            [DisplayPreference(DisplayPreference.Short)]
            [Display(GroupName = nameof(KeyWord.Address))]
            public string HouseNumber { get; set; }

            [Optional]
            [DisplayPreference(DisplayPreference.Short)]
            [Display(GroupName = nameof(KeyWord.Address))]
            public string BoxNumber { get; set; }

            [DisplayPreference(DisplayPreference.Short)]
            [Display(GroupName = nameof(KeyWord.Address))]
            public string ZipCode { get; set; }

            [DisplayPreference(DisplayPreference.Long)]
            [Display(GroupName = nameof(KeyWord.Address))]
            public string CityName { get; set; }

            [Display(GroupName = nameof(KeyWord.Address))]
            public string CountryName { get; set; }

            [Display(GroupName = nameof(KeyWord.Address))]
            public string FullCountryName { get; set; }

            [Display(GroupName = nameof(KeyWord.Company))]
            public string VatIdentificationNumber { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public bool IsBlocked { get; set; }

            [Optional]
            [Display(GroupName = nameof(KeyWord.Person))]
            public Nullable<DateTime> BirthDate { get; set; }

            [Optional]
            [DisplayPreference(DisplayPreference.Short)]
            [Display(GroupName = nameof(KeyWord.Person))]
            public string GenderName { get; set; }

            [Display(GroupName = nameof(KeyWord.Person))]
            public int Test { get; set; }

            [Display(GroupName = nameof(KeyWord.Person))]
            public DateTime Date { get; set; }
        }
        #endregion
    }
}

