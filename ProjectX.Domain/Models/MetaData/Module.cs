﻿using ProjectX.General;
using ProjectX.General.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectX.Domain.Models
{
    [MetadataType(typeof(Module.ModuleMetadata))]
    public partial class Module
    {
        //TODO:
        //DefaultOrderingProperty -> doesn't work on calculated properties
        //check which properties are metadata?

        #region Metadata
        internal sealed class ModuleMetadata
        {
            [Display(GroupName = nameof(KeyWord.Details))]
            public int ModuleID { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public short KeyName { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public short Name { get; set; }

            [DefaultOrderingProperty(Order = 1)]
            [Display(GroupName = nameof(KeyWord.Details))]
            public short Sortkey { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public string Description { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public Nullable<int> IconID { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public bool IsBlocked { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public System.DateTime DateCreated { get; set; }

            //[Display(GroupName = nameof(KeyWord.Details))]
            //public virtual ICollection<Menu> Menus { get; set; }

            //[Display(GroupName = nameof(KeyWord.Details))]
            //public virtual Icon Icon { get; set; }
        }
        #endregion
    }
}
