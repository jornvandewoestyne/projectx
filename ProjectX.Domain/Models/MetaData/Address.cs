﻿using ProjectX.Domain.Models.Helpers;
using ProjectX.General;
using System.ComponentModel.DataAnnotations;

namespace ProjectX.Domain.Models
{
    [MetadataType(typeof(Address.AddressMetadata))]
    public partial class Address
    {
        #region Overrides
        public override object[] TitleProperties => this.GetTitleProperties();
        public override BaseEntitySummary EntitySummary => this.GetEntitySummary();
        #endregion

        #region Calculated Properties
        //TODO: prefix: XLib.Text.BoxNumber.TryToLower(), Mask
        //TEST: => works! [BusinessRule.Format(FriendlyFormatType.BracketsFormat, nameof(Street))]
        [BusinessRule.Format(FriendlyFormatType.AddressBoxNumberFormat, nameof(BoxNumber))]
        [BusinessRule.ComposedOf(nameof(FullStreet), typeof(Address), nameof(Street), nameof(HouseNumber), nameof(BoxNumber))]
        public string FullStreet => this.GetValue(nameof(FullStreet), this.CurrentLanguage);
        //public string FullStreet => this.GetFullStreet();

        public string ZipCode => this.GetZipCode();
        public string CityName => this.GetCityName();

        [BusinessRule.ComposedOf(nameof(FullCountryName), typeof(Country), nameof(FullCountryName))]
        public string FullCountryName => this.GetValue(nameof(FullCountryName));
        public string CountryID => this.City?.CountryID;
        public string CountryName => this.City?.Country?.Name;

        //TODO: test with no address
        public string LanguageName => this.Language?.Name;
        public override SupportedLanguage CurrentLanguage => (this.Language?.LanguageID).AsSupportedLanguage();
        #endregion

        #region Metadata
        internal sealed class AddressMetadata
        {
            [Display(GroupName = nameof(KeyWord.Address))]
            public int AddressID { get; set; }

            [Display(GroupName = nameof(KeyWord.Address))]
            public string FullStreet { get; set; }

            [Display(GroupName = nameof(KeyWord.Address))]
            public string ZipCode { get; set; }

            [Display(GroupName = nameof(KeyWord.Address))]
            public string CityName { get; set; }

            [Display(GroupName = nameof(KeyWord.Address))]
            public string FullCountryName { get; set; }

            [Display(GroupName = nameof(KeyWord.Address))]
            public string LanguageName { get; set; }
        }
        #endregion
    }
}
