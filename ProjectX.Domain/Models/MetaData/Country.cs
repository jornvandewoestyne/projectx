﻿using ProjectX.Domain.Models.Helpers;
using ProjectX.General;
using System.ComponentModel.DataAnnotations;

namespace ProjectX.Domain.Models
{
    [MetadataType(typeof(Country.CountryMetadata))]
    public partial class Country
    {
        #region Calculated Properties
        [BusinessRule.UseSeparator(XLib.Text.JoinSeparator)]
        [BusinessRule.ComposedOf(nameof(FullCountryName), typeof(Country), nameof(CountryID), nameof(CountryName))]
        public string FullCountryName => this.GetValue(nameof(FullCountryName));

        [BusinessRule.ComposedOf(nameof(CountryName), typeof(Country), nameof(Name))]
        public string CountryName => this.GetValue(nameof(CountryName));
        #endregion

        #region Metadata
        internal sealed class CountryMetadata
        {
            [Display(GroupName = nameof(KeyWord.Details))]
            public string CountryID { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public string CurrencyID { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public string Name { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public string KeyName { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public string FullCountryName { get; set; }

            [Display(GroupName = nameof(KeyWord.Details))]
            public int TelephonePrefix { get; set; }
        }
        #endregion
    }
}
