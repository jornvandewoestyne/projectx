﻿using ProjectX.Domain.Models.Helpers;
using ProjectX.General;
using ProjectX.General.Attributes;
using System.ComponentModel.DataAnnotations;

namespace ProjectX.Domain.Models
{
    [MetadataType(typeof(Company.CompanyMetadata))]
    public partial class Company
    {
        #region Calculated Properties
        [BusinessRule.ComposedOf(nameof(FullCompanyName), typeof(Company), nameof(CompanyName), nameof(CompanyCorporationTypeName))]
        public string FullCompanyName => this.GetValue(nameof(FullCompanyName));
        //public string FullCompanyName => new BaseDisplayFormat(this, nameof(this.FullCompanyName), new DisplayFormatSetting("[{0}][ {1}]", null, false)).ToString();
        //public string FullCompanyName => this.GetFullCompanyName();

        [BusinessRule.UseSeparator]
        [BusinessRule.ComposedOf(nameof(FullCompanyNameAndExtraName), typeof(Company), nameof(FullCompanyName), nameof(CompanyExtraName))]
        public string FullCompanyNameAndExtraName => this.GetValue(nameof(FullCompanyNameAndExtraName));
        //public string FullCompanyNameAndExtraName => new BaseDisplayFormat(this, nameof(this.FullCompanyNameAndExtraName), new DisplayFormatSetting("[{0}][ {1}][ ({2})]", null, false)).ToString();
        //public string FullCompanyNameAndExtraName => this.GetFullCompanyNameAndExtraName();

        [BusinessRule.ComposedOf(nameof(CompanyCorporationTypeName), typeof(Company), nameof(CompanyCorporationTypeKeyName))]
        public string CompanyCorporationTypeName => this.CompanyCorporationType?.Name; //this.GetCompanyCorporationTypeName();

        [BusinessRule.ReadOnlyOrderingKey]
        public string CompanyCorporationTypeKeyName => this.CompanyCorporationType?.KeyName;
        #endregion

        #region Metadata
        internal sealed class CompanyMetadata
        {
            [Optional]
            [Display(GroupName = nameof(KeyWord.Company))]
            public string CompanyName { get; set; }

            [Display(GroupName = nameof(KeyWord.Company))]
            public string CompanyExtraName { get; set; }

            [Display(GroupName = nameof(KeyWord.Company))]
            public string FullCompanyName { get; set; }

            [Optional]
            [Display(GroupName = nameof(KeyWord.Company))]
            public string FullCompanyNameAndExtraName { get; set; }

            [Optional]
            [Display(GroupName = nameof(KeyWord.Company))]
            public string CompanyCorporationTypeName { get; set; }

            [Optional]
            [Display(GroupName = nameof(KeyWord.Company))]
            public string CompanyCorporationTypeKeyName { get; set; }
        }
        #endregion
    }
}