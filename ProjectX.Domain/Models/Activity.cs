using System;
using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class Activity : BaseLocalizedEntity
    {
        public Activity()
        {
            this.MenuActivities = new List<MenuActivity>();
        }

        public int ActivityID { get; set; }
        public int ActivityGroupID { get; set; }
        public short SortKey { get; set; }
        public string Description { get; set; }
        public Nullable<int> IconID { get; set; }
        public bool IsBlocked { get; set; }
        public bool MultipleInstancesAllowed { get; set; }
        public System.DateTime DateCreated { get; set; }
        public virtual ActivityGroup ActivityGroup { get; set; }
        public virtual Icon Icon { get; set; }
        public virtual ICollection<MenuActivity> MenuActivities { get; set; }
    }
}
