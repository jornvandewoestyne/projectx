using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class PersonGender : BaseLocalizedEntity
    {
        public PersonGender()
        {
            this.People = new List<Person>();
            this.PersonSalutations = new List<PersonSalutation>();
        }

        public int PersonGenderID { get; set; }
        public virtual ICollection<Person> People { get; set; }
        public virtual ICollection<PersonSalutation> PersonSalutations { get; set; }
    }
}
