using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class ActivityGroup : BaseLocalizedEntity
    {
        public ActivityGroup()
        {
            this.Activities = new List<Activity>();
        }

        public int ActivityGroupID { get; set; }
        public short SortKey { get; set; }
        public string Description { get; set; }
        public bool IsBlocked { get; set; }
        public System.DateTime DateCreated { get; set; }
        public virtual ICollection<Activity> Activities { get; set; }
    }
}
