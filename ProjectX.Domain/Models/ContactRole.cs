
using ProjectX.General;

namespace ProjectX.Domain.Models
{
    public partial class ContactRole : BaseEntity
    {
        public int ContactID { get; set; }
        public int ContactRoleTypeID { get; set; }
        public bool IsBlocked { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual ContactRoleType ContactRoleType { get; set; }
    }
}
