using ProjectX.General;
using System;

namespace ProjectX.Domain.Models
{
    public partial class CustomerKey : BaseEntity
    {
        public int ID { get; set; }
        public string PartnerCode { get; set; }
        public int MasterContactID { get; set; }
        public int SalesInvoiceContactID { get; set; }
        public int SalesDeliveryContactID { get; set; }
        public Nullable<int> SalesMailingContactID { get; set; }
        public int LanguageID { get; set; }
        public bool IsBlocked { get; set; }
    }
}
