using System;
using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class Member : BaseLocalizedEntity
    {
        public Member()
        {
            this.Menus = new List<Menu>();
        }

        public int MemberID { get; set; }
        public short SortKey { get; set; }
        public string Description { get; set; }
        public Nullable<int> IconID { get; set; }
        public bool IsBlocked { get; set; }
        public System.DateTime DateCreated { get; set; }
        public virtual Icon Icon { get; set; }
        public virtual ICollection<Menu> Menus { get; set; }
    }
}
