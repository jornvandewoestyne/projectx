using ProjectX.General;
using System;
using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class VatNumber : BaseEntity
    {
        public VatNumber()
        {
            this.Contacts = new List<Contact>();
        }

        public int VatNumberID { get; set; }
        public Nullable<int> PartnerID { get; set; }
        public string CountryID { get; set; }
        public string VatIdentificationNumber { get; set; }
        public Nullable<System.DateTime> ExpireDate { get; set; }
        public virtual Country Country { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual Partner Partner { get; set; }
    }
}
