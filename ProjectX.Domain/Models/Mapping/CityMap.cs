using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class CityMap : EntityTypeConfiguration<City>
    {
        public CityMap()
        {
            // Primary Key
            this.HasKey(t => t.CityID);

            // Properties
            this.Property(t => t.ZipCode)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.KeyName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.CountryID)
                .IsRequired()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("City", "Locality");
            this.Property(t => t.CityID).HasColumnName("CityID");
            this.Property(t => t.ZipCode).HasColumnName("ZipCode");
            this.Property(t => t.KeyName).HasColumnName("KeyName");
            this.Property(t => t.CountryID).HasColumnName("CountryID");

            // Relationships
            this.HasRequired(t => t.Country)
                .WithMany(t => t.Cities)
                .HasForeignKey(d => d.CountryID);

        }
    }
}
