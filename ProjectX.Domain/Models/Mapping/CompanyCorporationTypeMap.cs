using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class CompanyCorporationTypeMap : EntityTypeConfiguration<CompanyCorporationType>
    {
        public CompanyCorporationTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.CompanyCorporationTypeID);

            // Properties
            this.Property(t => t.CountryID)
                .IsRequired()
                .HasMaxLength(2);

            this.Property(t => t.KeyName)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("CompanyCorporationType", "Partner");
            this.Property(t => t.CompanyCorporationTypeID).HasColumnName("CompanyCorporationTypeID");
            this.Property(t => t.CountryID).HasColumnName("CountryID");
            this.Property(t => t.KeyName).HasColumnName("KeyName");
            this.Property(t => t.Description).HasColumnName("Description");

            // Relationships
            this.HasRequired(t => t.Country)
                .WithMany(t => t.CompanyCorporationTypes)
                .HasForeignKey(d => d.CountryID);

        }
    }
}
