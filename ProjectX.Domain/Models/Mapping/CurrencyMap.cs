using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class CurrencyMap : EntityTypeConfiguration<Currency>
    {
        public CurrencyMap()
        {
            // Primary Key
            this.HasKey(t => t.CurrencyID);

            // Properties
            this.Property(t => t.CurrencyID)
                .IsRequired()
                .HasMaxLength(3);

            this.Property(t => t.Description)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Currency", "Finance");
            this.Property(t => t.CurrencyID).HasColumnName("CurrencyID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Decimals).HasColumnName("Decimals");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
