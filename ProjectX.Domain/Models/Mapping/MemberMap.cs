using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class MemberMap : EntityTypeConfiguration<Member>
    {
        public MemberMap()
        {
            // Primary Key
            this.HasKey(t => t.MemberID);

            // Properties
            this.Property(t => t.MemberID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.KeyName)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("Member", "Navigation");
            this.Property(t => t.MemberID).HasColumnName("MemberID");
            this.Property(t => t.SortKey).HasColumnName("SortKey");
            this.Property(t => t.KeyName).HasColumnName("KeyName");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.IconID).HasColumnName("IconID");
            this.Property(t => t.IsBlocked).HasColumnName("IsBlocked");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");

            // Relationships
            this.HasOptional(t => t.Icon)
                .WithMany(t => t.Members)
                .HasForeignKey(d => d.IconID);

        }
    }
}
