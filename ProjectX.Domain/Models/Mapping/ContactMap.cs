using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class ContactMap : EntityTypeConfiguration<Contact>
    {
        public ContactMap()
        {
            // Primary Key
            this.HasKey(t => t.ContactID);

            // Properties
            this.Property(t => t.CallName)
                .HasMaxLength(15);

            // Table & Column Mappings
            this.ToTable("Contact", "Partner");
            this.Property(t => t.ContactID).HasColumnName("ContactID");
            this.Property(t => t.CallName).HasColumnName("CallName");
            this.Property(t => t.PartnerID).HasColumnName("PartnerID");
            this.Property(t => t.CompanyID).HasColumnName("CompanyID");
            this.Property(t => t.PersonID).HasColumnName("PersonID");
            this.Property(t => t.AddressID).HasColumnName("AddressID");
            this.Property(t => t.VatNumberID).HasColumnName("VatNumberID");
            this.Property(t => t.IsBlocked).HasColumnName("IsBlocked");

            // Relationships
            this.HasMany(t => t.Departments)
                .WithMany(t => t.Contacts)
                .Map(m =>
                    {
                        m.ToTable("ContactDepartment", "Partner");
                        m.MapLeftKey("ContactID");
                        m.MapRightKey("DepartmentID");
                    });

            this.HasOptional(t => t.Address)
                .WithMany(t => t.Contacts)
                .HasForeignKey(d => d.AddressID);
            this.HasOptional(t => t.Company)
                .WithMany(t => t.Contacts)
                .HasForeignKey(d => d.CompanyID);
            this.HasOptional(t => t.Partner)
                .WithMany(t => t.Contacts)
                .HasForeignKey(d => d.PartnerID);
            this.HasOptional(t => t.Person)
                .WithMany(t => t.Contacts)
                .HasForeignKey(d => d.PersonID);
            this.HasOptional(t => t.VatNumber)
                .WithMany(t => t.Contacts)
                .HasForeignKey(d => d.VatNumberID);

        }
    }
}
