using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class CommunicationMap : EntityTypeConfiguration<Communication>
    {
        public CommunicationMap()
        {
            // Primary Key
            this.HasKey(t => t.CommunicationID);

            // Properties
            this.Property(t => t.Text)
                .IsRequired()
                .HasMaxLength(60);

            // Table & Column Mappings
            this.ToTable("Communication", "Partner");
            this.Property(t => t.CommunicationID).HasColumnName("CommunicationID");
            this.Property(t => t.ContactID).HasColumnName("ContactID");
            this.Property(t => t.CommunicationTypeID).HasColumnName("CommunicationTypeID");
            this.Property(t => t.Text).HasColumnName("Text");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.IsDefault).HasColumnName("IsDefault");
            this.Property(t => t.IsPrivate).HasColumnName("IsPrivate");
            this.Property(t => t.IsBlocked).HasColumnName("IsBlocked");

            // Relationships
            this.HasRequired(t => t.CommunicationType)
                .WithMany(t => t.Communications)
                .HasForeignKey(d => d.CommunicationTypeID);
            this.HasRequired(t => t.Contact)
                .WithMany(t => t.Communications)
                .HasForeignKey(d => d.ContactID);

        }
    }
}
