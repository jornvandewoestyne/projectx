using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class DepartmentMap : EntityTypeConfiguration<Department>
    {
        public DepartmentMap()
        {
            // Primary Key
            this.HasKey(t => t.DepartmentID);

            // Properties
            this.Property(t => t.KeyName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Department", "Partner");
            this.Property(t => t.DepartmentID).HasColumnName("DepartmentID");
            this.Property(t => t.KeyName).HasColumnName("KeyName");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
