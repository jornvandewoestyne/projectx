using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class PersonGenderMap : EntityTypeConfiguration<PersonGender>
    {
        public PersonGenderMap()
        {
            // Primary Key
            this.HasKey(t => t.PersonGenderID);

            // Properties
            this.Property(t => t.KeyName)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("PersonGender", "Partner");
            this.Property(t => t.PersonGenderID).HasColumnName("PersonGenderID");
            this.Property(t => t.KeyName).HasColumnName("KeyName");
        }
    }
}
