using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class ProductSearchMap : EntityTypeConfiguration<ProductSearch>
    {
        public ProductSearchMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.SearchString });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SearchString)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("ProductSearch");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.SearchString).HasColumnName("SearchString");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
