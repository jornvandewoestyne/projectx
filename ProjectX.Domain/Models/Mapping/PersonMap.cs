using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class PersonMap : EntityTypeConfiguration<Person>
    {
        public PersonMap()
        {
            // Primary Key
            this.HasKey(t => t.PersonID);

            // Properties
            this.Property(t => t.CallName)
                .HasMaxLength(15);

            this.Property(t => t.FirstName)
                .HasMaxLength(30);

            this.Property(t => t.LastName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Person", "Partner");
            this.Property(t => t.PersonID).HasColumnName("PersonID");
            this.Property(t => t.CallName).HasColumnName("CallName");
            this.Property(t => t.PersonSalutationID).HasColumnName("PersonSalutationID");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.PersonJobTitleID).HasColumnName("PersonJobTitleID");
            this.Property(t => t.DepartmentID).HasColumnName("DepartmentID");
            this.Property(t => t.PersonGenderID).HasColumnName("PersonGenderID");
            this.Property(t => t.BirthDate).HasColumnName("BirthDate");
            this.Property(t => t.Picture).HasColumnName("Picture");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.IsBlocked).HasColumnName("IsBlocked");

            // Relationships
            this.HasRequired(t => t.Language)
                .WithMany(t => t.People)
                .HasForeignKey(d => d.LanguageID);
            this.HasOptional(t => t.Department)
                .WithMany(t => t.People)
                .HasForeignKey(d => d.DepartmentID);
            this.HasOptional(t => t.PersonGender)
                .WithMany(t => t.People)
                .HasForeignKey(d => d.PersonGenderID);
            this.HasOptional(t => t.PersonJobTitle)
                .WithMany(t => t.People)
                .HasForeignKey(d => d.PersonJobTitleID);
            this.HasOptional(t => t.PersonSalutation)
                .WithMany(t => t.People)
                .HasForeignKey(d => d.PersonSalutationID);

        }
    }
}
