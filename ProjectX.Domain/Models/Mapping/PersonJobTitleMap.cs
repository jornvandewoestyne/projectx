using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class PersonJobTitleMap : EntityTypeConfiguration<PersonJobTitle>
    {
        public PersonJobTitleMap()
        {
            // Primary Key
            this.HasKey(t => t.PersonJobTitleID);

            // Properties
            this.Property(t => t.KeyName)
                .IsRequired()
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("PersonJobTitle", "Partner");
            this.Property(t => t.PersonJobTitleID).HasColumnName("PersonJobTitleID");
            this.Property(t => t.KeyName).HasColumnName("KeyName");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
