using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class ContactRoleTypeMap : EntityTypeConfiguration<ContactRoleType>
    {
        public ContactRoleTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.ContactRoleTypeID);

            // Properties
            this.Property(t => t.KeyName)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("ContactRoleType", "Partner");
            this.Property(t => t.ContactRoleTypeID).HasColumnName("ContactRoleTypeID");
            this.Property(t => t.KeyName).HasColumnName("KeyName");
        }
    }
}
