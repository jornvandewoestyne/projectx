using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class VatNumberMap : EntityTypeConfiguration<VatNumber>
    {
        public VatNumberMap()
        {
            // Primary Key
            this.HasKey(t => t.VatNumberID);

            // Properties
            this.Property(t => t.CountryID)
                .IsRequired()
                .HasMaxLength(2);

            this.Property(t => t.VatIdentificationNumber)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("VatNumber", "Partner");
            this.Property(t => t.VatNumberID).HasColumnName("VatNumberID");
            this.Property(t => t.PartnerID).HasColumnName("PartnerID");
            this.Property(t => t.CountryID).HasColumnName("CountryID");
            this.Property(t => t.VatIdentificationNumber).HasColumnName("VatIdentificationNumber");
            this.Property(t => t.ExpireDate).HasColumnName("ExpireDate");

            // Relationships
            this.HasRequired(t => t.Country)
                .WithMany(t => t.VatNumbers)
                .HasForeignKey(d => d.CountryID);
            this.HasOptional(t => t.Partner)
                .WithMany(t => t.VatNumbers)
                .HasForeignKey(d => d.PartnerID);

        }
    }
}
