using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class ContactRoleMap : EntityTypeConfiguration<ContactRole>
    {
        public ContactRoleMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ContactID, t.ContactRoleTypeID });

            // Properties
            this.Property(t => t.ContactID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ContactRoleTypeID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("ContactRole", "Partner");
            this.Property(t => t.ContactID).HasColumnName("ContactID");
            this.Property(t => t.ContactRoleTypeID).HasColumnName("ContactRoleTypeID");
            this.Property(t => t.IsBlocked).HasColumnName("IsBlocked");

            // Relationships
            this.HasRequired(t => t.Contact)
                .WithMany(t => t.ContactRoles)
                .HasForeignKey(d => d.ContactID);
            this.HasRequired(t => t.ContactRoleType)
                .WithMany(t => t.ContactRoles)
                .HasForeignKey(d => d.ContactRoleTypeID);

        }
    }
}
