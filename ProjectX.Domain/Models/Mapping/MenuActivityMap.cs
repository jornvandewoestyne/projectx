using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class MenuActivityMap : EntityTypeConfiguration<MenuActivity>
    {
        public MenuActivityMap()
        {
            // Primary Key
            this.HasKey(t => new { t.MenuID, t.ActivityID });

            // Properties
            this.Property(t => t.MenuID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ActivityID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("MenuActivity", "Navigation");
            this.Property(t => t.MenuID).HasColumnName("MenuID");
            this.Property(t => t.ActivityID).HasColumnName("ActivityID");
            this.Property(t => t.SortKey).HasColumnName("SortKey");
            this.Property(t => t.IconID).HasColumnName("IconID");
            this.Property(t => t.IsBlocked).HasColumnName("IsBlocked");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");

            // Relationships
            this.HasRequired(t => t.Activity)
                .WithMany(t => t.MenuActivities)
                .HasForeignKey(d => d.ActivityID);
            this.HasRequired(t => t.Menu)
                .WithMany(t => t.MenuActivities)
                .HasForeignKey(d => d.MenuID);
            this.HasOptional(t => t.Icon)
                .WithMany(t => t.MenuActivities)
                .HasForeignKey(d => d.IconID);

        }
    }
}
