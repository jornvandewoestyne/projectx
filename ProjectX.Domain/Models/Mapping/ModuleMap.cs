using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class ModuleMap : EntityTypeConfiguration<Module>
    {
        public ModuleMap()
        {
            // Primary Key
            this.HasKey(t => t.ModuleID);

            // Properties
            this.Property(t => t.ModuleID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.KeyName)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("Module", "Navigation");
            this.Property(t => t.ModuleID).HasColumnName("ModuleID");
            this.Property(t => t.Sortkey).HasColumnName("Sortkey");
            this.Property(t => t.KeyName).HasColumnName("KeyName");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.IconID).HasColumnName("IconID");
            this.Property(t => t.IsBlocked).HasColumnName("IsBlocked");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");

            // Relationships
            this.HasOptional(t => t.Icon)
                .WithMany(t => t.Modules)
                .HasForeignKey(d => d.IconID);

        }
    }
}
