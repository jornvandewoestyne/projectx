using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class MenuMap : EntityTypeConfiguration<Menu>
    {
        public MenuMap()
        {
            // Primary Key
            this.HasKey(t => t.MenuID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Menu", "Navigation");
            this.Property(t => t.MenuID).HasColumnName("MenuID");
            this.Property(t => t.ModuleID).HasColumnName("ModuleID");
            this.Property(t => t.MemberID).HasColumnName("MemberID");
            this.Property(t => t.SortKey).HasColumnName("SortKey");
            this.Property(t => t.IconID).HasColumnName("IconID");
            this.Property(t => t.IsBlocked).HasColumnName("IsBlocked");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");

            // Relationships
            this.HasRequired(t => t.Member)
                .WithMany(t => t.Menus)
                .HasForeignKey(d => d.MemberID);
            this.HasOptional(t => t.Icon)
                .WithMany(t => t.Menus)
                .HasForeignKey(d => d.IconID);
            this.HasRequired(t => t.Module)
                .WithMany(t => t.Menus)
                .HasForeignKey(d => d.ModuleID);

        }
    }
}
