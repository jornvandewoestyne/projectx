using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class LanguageMap : EntityTypeConfiguration<Language>
    {
        public LanguageMap()
        {
            // Primary Key
            this.HasKey(t => t.LanguageID);

            // Properties
            this.Property(t => t.LanguageCode)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.KeyName)
                .IsRequired()
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("Language", "Locality");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.LanguageCode).HasColumnName("LanguageCode");
            this.Property(t => t.KeyName).HasColumnName("KeyName");
        }
    }
}
