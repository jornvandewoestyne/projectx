using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class ActivityGroupMap : EntityTypeConfiguration<ActivityGroup>
    {
        public ActivityGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.ActivityGroupID);

            // Properties
            this.Property(t => t.ActivityGroupID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.KeyName)
                .IsRequired()
                .HasMaxLength(15);

            // Table & Column Mappings
            this.ToTable("ActivityGroup", "Navigation");
            this.Property(t => t.ActivityGroupID).HasColumnName("ActivityGroupID");
            this.Property(t => t.SortKey).HasColumnName("SortKey");
            this.Property(t => t.KeyName).HasColumnName("KeyName");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.IsBlocked).HasColumnName("IsBlocked");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
        }
    }
}
