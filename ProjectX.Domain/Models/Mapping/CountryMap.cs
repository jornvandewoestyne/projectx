using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class CountryMap : EntityTypeConfiguration<Country>
    {
        public CountryMap()
        {
            // Primary Key
            this.HasKey(t => t.CountryID);

            // Properties
            this.Property(t => t.CountryID)
                .IsRequired()
                .HasMaxLength(2);

            this.Property(t => t.KeyName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.TelephonePrefix)
                .HasMaxLength(10);

            this.Property(t => t.CurrencyID)
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("Country", "Locality");
            this.Property(t => t.CountryID).HasColumnName("CountryID");
            this.Property(t => t.KeyName).HasColumnName("KeyName");
            this.Property(t => t.TelephonePrefix).HasColumnName("TelephonePrefix");
            this.Property(t => t.CurrencyID).HasColumnName("CurrencyID");

            // Relationships
            this.HasOptional(t => t.Currency)
                .WithMany(t => t.Countries)
                .HasForeignKey(d => d.CurrencyID);

        }
    }
}
