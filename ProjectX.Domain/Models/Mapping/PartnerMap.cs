using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class PartnerMap : EntityTypeConfiguration<Partner>
    {
        public PartnerMap()
        {
            // Primary Key
            this.HasKey(t => t.PartnerID);

            // Properties
            this.Property(t => t.PartnerCode)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("Partner", "Partner");
            this.Property(t => t.PartnerID).HasColumnName("PartnerID");
            this.Property(t => t.PartnerCode).HasColumnName("PartnerCode");
            this.Property(t => t.MasterContactID).HasColumnName("MasterContactID");
            this.Property(t => t.SalesInvoiceContactID).HasColumnName("SalesInvoiceContactID");
            this.Property(t => t.SalesDeliveryContactID).HasColumnName("SalesDeliveryContactID");
            this.Property(t => t.SalesMailingContactID).HasColumnName("SalesMailingContactID");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.IsBlocked).HasColumnName("IsBlocked");

            // Relationships
            this.HasRequired(t => t.Language)
                .WithMany(t => t.Partners)
                .HasForeignKey(d => d.LanguageID);
            this.HasRequired(t => t.SalesDeliveryContact)
                .WithMany(t => t.SalesDeliveryPartners)
                .HasForeignKey(d => d.SalesDeliveryContactID);
            this.HasRequired(t => t.SalesInvoiceContact)
                .WithMany(t => t.SalesInvoicePartners)
                .HasForeignKey(d => d.SalesInvoiceContactID);
            this.HasOptional(t => t.SalesMailingContact)
                .WithMany(t => t.SalesMailingPartners)
                .HasForeignKey(d => d.SalesMailingContactID);
            this.HasRequired(t => t.MasterContact)
                .WithMany(t => t.MasterPartners)
                .HasForeignKey(d => d.MasterContactID);

        }
    }
}
