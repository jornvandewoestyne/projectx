using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class IconMap : EntityTypeConfiguration<Icon>
    {
        public IconMap()
        {
            // Primary Key
            this.HasKey(t => t.IconID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("Icon", "System");
            this.Property(t => t.IconID).HasColumnName("IconID");
            this.Property(t => t.Name).HasColumnName("Name");
        }
    }
}
