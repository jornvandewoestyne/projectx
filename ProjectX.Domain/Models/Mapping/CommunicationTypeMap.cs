using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class CommunicationTypeMap : EntityTypeConfiguration<CommunicationType>
    {
        public CommunicationTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.CommunicationTypeID);

            // Properties
            this.Property(t => t.KeyName)
                .IsRequired()
                .HasMaxLength(15);

            // Table & Column Mappings
            this.ToTable("CommunicationType", "Partner");
            this.Property(t => t.CommunicationTypeID).HasColumnName("CommunicationTypeID");
            this.Property(t => t.KeyName).HasColumnName("KeyName");
        }
    }
}
