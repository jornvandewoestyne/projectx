using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class PersonSalutationMap : EntityTypeConfiguration<PersonSalutation>
    {
        public PersonSalutationMap()
        {
            // Primary Key
            this.HasKey(t => t.PersonSalutationID);

            // Properties
            this.Property(t => t.KeyName)
                .IsRequired()
                .HasMaxLength(15);

            // Table & Column Mappings
            this.ToTable("PersonSalutation", "Partner");
            this.Property(t => t.PersonSalutationID).HasColumnName("PersonSalutationID");
            this.Property(t => t.PersonGenderID).HasColumnName("PersonGenderID");
            this.Property(t => t.KeyName).HasColumnName("KeyName");

            // Relationships
            this.HasRequired(t => t.PersonGender)
                .WithMany(t => t.PersonSalutations)
                .HasForeignKey(d => d.PersonGenderID);

        }
    }
}
