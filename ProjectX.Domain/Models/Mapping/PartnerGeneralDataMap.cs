using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class PartnerGeneralDataMap : EntityTypeConfiguration<PartnerGeneralData>
    {
        public PartnerGeneralDataMap()
        {
            // Primary Key
            this.HasKey(t => t.ContactID);

            // Properties
            this.Property(t => t.ContactID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            //TODO: is this OK?
            this.Property(t => t.PartnerID)
                .IsRequired();

            this.Property(t => t.Code)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("PartnerGeneralData", "Partner");
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.PartnerID).HasColumnName("PartnerID");
            this.Property(t => t.ContactID).HasColumnName("ContactID");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.CompanyExtraName).HasColumnName("CompanyExtraName");
            this.Property(t => t.CompanyCorporationTypeKeyName).HasColumnName("CompanyCorporationTypeKeyName");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Street).HasColumnName("Street");
            this.Property(t => t.HouseNumber).HasColumnName("HouseNumber");
            this.Property(t => t.BoxNumber).HasColumnName("BoxNumber");
            this.Property(t => t.ZipCode).HasColumnName("ZipCode");
            this.Property(t => t.CityKeyName).HasColumnName("CityKeyName");
            this.Property(t => t.CountryID).HasColumnName("CountryID");
            this.Property(t => t.CountryKeyName).HasColumnName("CountryKeyName");
            this.Property(t => t.AddressLanguageID).HasColumnName("AddressLanguageID");
            this.Property(t => t.VatIdentificationNumber).HasColumnName("VatIdentificationNumber");
            this.Property(t => t.IsBlocked).HasColumnName("IsBlocked");
            this.Property(t => t.BirthDate).HasColumnName("BirthDate");
            this.Property(t => t.GenderKeyName).HasColumnName("GenderKeyName");
            this.Property(t => t.PersonSalutationKeyName).HasColumnName("PersonSalutationKeyName");
        }
    }
}
