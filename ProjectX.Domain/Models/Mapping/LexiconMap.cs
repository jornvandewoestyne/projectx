using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class LexiconMap : EntityTypeConfiguration<Lexicon>
    {
        public LexiconMap()
        {
            // Primary Key
            this.HasKey(t => t.LexiconID);

            // Properties
            this.Property(t => t.KeyName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Lexicon", "Localization");
            this.Property(t => t.LexiconID).HasColumnName("LexiconID");
            this.Property(t => t.KeyName).HasColumnName("KeyName");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");

            // Relationships
            this.HasRequired(t => t.Language)
                .WithMany(t => t.Lexicons)
                .HasForeignKey(d => d.LanguageID);

        }
    }
}
