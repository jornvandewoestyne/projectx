using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class CustomerKeyMap : EntityTypeConfiguration<CustomerKey>
    {
        public CustomerKeyMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.PartnerCode, t.MasterContactID, t.SalesInvoiceContactID, t.SalesDeliveryContactID, t.LanguageID, t.IsBlocked });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PartnerCode)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.MasterContactID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SalesInvoiceContactID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SalesDeliveryContactID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.LanguageID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CustomerKey", "Partner");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.PartnerCode).HasColumnName("PartnerCode");
            this.Property(t => t.MasterContactID).HasColumnName("MasterContactID");
            this.Property(t => t.SalesInvoiceContactID).HasColumnName("SalesInvoiceContactID");
            this.Property(t => t.SalesDeliveryContactID).HasColumnName("SalesDeliveryContactID");
            this.Property(t => t.SalesMailingContactID).HasColumnName("SalesMailingContactID");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.IsBlocked).HasColumnName("IsBlocked");
        }
    }
}
