using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class ActivityMap : EntityTypeConfiguration<Activity>
    {
        public ActivityMap()
        {
            // Primary Key
            this.HasKey(t => t.ActivityID);

            // Properties
            this.Property(t => t.ActivityID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.KeyName)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("Activity", "Navigation");
            this.Property(t => t.ActivityID).HasColumnName("ActivityID");
            this.Property(t => t.ActivityGroupID).HasColumnName("ActivityGroupID");
            this.Property(t => t.SortKey).HasColumnName("SortKey");
            this.Property(t => t.KeyName).HasColumnName("KeyName");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.IconID).HasColumnName("IconID");
            this.Property(t => t.IsBlocked).HasColumnName("IsBlocked");
            this.Property(t => t.MultipleInstancesAllowed).HasColumnName("MultipleInstancesAllowed");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");

            // Relationships
            this.HasRequired(t => t.ActivityGroup)
                .WithMany(t => t.Activities)
                .HasForeignKey(d => d.ActivityGroupID);
            this.HasOptional(t => t.Icon)
                .WithMany(t => t.Activities)
                .HasForeignKey(d => d.IconID);

        }
    }
}
