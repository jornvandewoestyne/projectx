using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class CompanyMap : EntityTypeConfiguration<Company>
    {
        public CompanyMap()
        {
            // Primary Key
            this.HasKey(t => t.CompanyID);

            // Properties
            this.Property(t => t.CallName)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.CompanyName)
                .IsRequired()
                .HasMaxLength(60);

            this.Property(t => t.CompanyExtraName)
                .HasMaxLength(60);

            // Table & Column Mappings
            this.ToTable("Company", "Partner");
            this.Property(t => t.CompanyID).HasColumnName("CompanyID");
            this.Property(t => t.CallName).HasColumnName("CallName");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.CompanyExtraName).HasColumnName("CompanyExtraName");
            this.Property(t => t.CompanyCorporationTypeID).HasColumnName("CompanyCorporationTypeID");
            this.Property(t => t.IsBlocked).HasColumnName("IsBlocked");

            // Relationships
            this.HasOptional(t => t.CompanyCorporationType)
                .WithMany(t => t.Companies)
                .HasForeignKey(d => d.CompanyCorporationTypeID);

        }
    }
}
