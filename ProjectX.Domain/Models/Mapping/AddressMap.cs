using System.Data.Entity.ModelConfiguration;

namespace ProjectX.Domain.Models.Mapping
{
    public class AddressMap : EntityTypeConfiguration<Address>
    {
        public AddressMap()
        {
            // Primary Key
            this.HasKey(t => t.AddressID);

            // Properties
            this.Property(t => t.Site)
                .HasMaxLength(60);

            this.Property(t => t.Street)
                .HasMaxLength(50);

            this.Property(t => t.HouseNumber)
                .HasMaxLength(10);

            this.Property(t => t.BoxNumber)
                .HasMaxLength(10);

            this.Property(t => t.Block)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("Address", "Partner");
            this.Property(t => t.AddressID).HasColumnName("AddressID");
            this.Property(t => t.Site).HasColumnName("Site");
            this.Property(t => t.Street).HasColumnName("Street");
            this.Property(t => t.HouseNumber).HasColumnName("HouseNumber");
            this.Property(t => t.BoxNumber).HasColumnName("BoxNumber");
            this.Property(t => t.Block).HasColumnName("Block");
            this.Property(t => t.CityID).HasColumnName("CityID");
            this.Property(t => t.DisplayFormatID).HasColumnName("DisplayFormatID");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");

            // Relationships
            this.HasRequired(t => t.Language)
                .WithMany(t => t.Addresses)
                .HasForeignKey(d => d.LanguageID);
            this.HasOptional(t => t.City)
                .WithMany(t => t.Addresses)
                .HasForeignKey(d => d.CityID);

        }
    }
}
