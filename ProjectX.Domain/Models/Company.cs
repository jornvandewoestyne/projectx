using ProjectX.General;
using System;
using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class Company : BaseEntity
    {
        public Company()
        {
            this.Contacts = new List<Contact>();
        }

        public int CompanyID { get; set; }
        public string CallName { get; set; }
        public string CompanyName { get; set; }
        public string CompanyExtraName { get; set; }
        public Nullable<int> CompanyCorporationTypeID { get; set; }
        public bool IsBlocked { get; set; }
        public virtual CompanyCorporationType CompanyCorporationType { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
