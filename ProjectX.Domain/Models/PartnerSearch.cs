namespace ProjectX.Domain.Models
{
    public partial class PartnerSearch : BaseSearchEntity
    {
        //public int ID { get; set; }
        //public string SearchString { get; set; }
        public System.DateTime DateCreated { get; set; }
        public bool IsActive { get; set; }
        public virtual Partner Partner { get; set; }
    }
}
