using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class City : BaseLocalizedFormatEntity
    {
        public City()
        {
            this.Addresses = new List<Address>();
        }

        public int CityID { get; set; }
        public string ZipCode { get; set; }
        public string CountryID { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
        public virtual Country Country { get; set; }
    }
}
