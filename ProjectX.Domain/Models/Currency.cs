using ProjectX.General;
using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class Currency : BaseEntity
    {
        public Currency()
        {
            this.Countries = new List<Country>();
        }

        public string CurrencyID { get; set; }
        public string Description { get; set; }
        public short Decimals { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<Country> Countries { get; set; }
    }
}
