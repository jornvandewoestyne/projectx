using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class Language : BaseLocalizedEntity
    {
        public Language()
        {
            this.Addresses = new List<Address>();
            this.Lexicons = new List<Lexicon>();
            this.Partners = new List<Partner>();
            this.People = new List<Person>();
        }

        public int LanguageID { get; set; }
        public string LanguageCode { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
        public virtual ICollection<Lexicon> Lexicons { get; set; }
        public virtual ICollection<Partner> Partners { get; set; }
        public virtual ICollection<Person> People { get; set; }
    }
}
