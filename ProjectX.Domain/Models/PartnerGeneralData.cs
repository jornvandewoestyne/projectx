using System;

namespace ProjectX.Domain.Models
{
    public partial class PartnerGeneralData : BaseReadOnlyEntity
    {
        public string Code { get; set; }
        public int PartnerID { get; set; }
        public int ContactID { get; set; }
        public string CompanyName { get; set; }
        public string CompanyExtraName { get; set; }
        public string CompanyCorporationTypeKeyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string BoxNumber { get; set; }
        public string ZipCode { get; set; }
        public string CityKeyName { get; set; }
        public string CountryID { get; set; }
        public string CountryKeyName { get; set; }
        public Nullable<int> AddressLanguageID { get; set; }
        public string VatIdentificationNumber { get; set; }
        public bool IsBlocked { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public string GenderKeyName { get; set; }
        public string PersonSalutationKeyName { get; set; }
    }
}
