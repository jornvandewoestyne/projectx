using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class PersonSalutation : BaseLocalizedEntity
    {
        public PersonSalutation()
        {
            this.People = new List<Person>();
        }

        public int PersonSalutationID { get; set; }
        public int PersonGenderID { get; set; }
        public virtual ICollection<Person> People { get; set; }
        public virtual PersonGender PersonGender { get; set; }
    }
}
