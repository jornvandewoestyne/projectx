
using ProjectX.General;

namespace ProjectX.Domain.Models
{
    public partial class Employee : BaseEntity
    {
        public int EmployeeID { get; set; }
        public int PersonID { get; set; }
        public string NationalIDNumber { get; set; }
        public virtual Person Person { get; set; }
    }
}
