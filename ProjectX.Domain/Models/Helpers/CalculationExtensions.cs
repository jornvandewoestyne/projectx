﻿using ProjectX.General;
using System;

//TODO: summary
//TODO: check if (entity != null)?

namespace ProjectX.Domain.Models.Helpers
{
    internal static class CalculationExtensions
    {
        #region GetValue
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="propertyName"></param>
        /// <param name="translate"></param>
        /// <returns></returns>
        public static string GetValue(this IBaseEntity entity, string propertyName, bool translate = XLib.Preference.TranslateEntityArguments)
        {
            return entity.GetValue(propertyName, XLib.Preference.CurrentLanguage, translate);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="propertyName"></param>
        /// <param name="language"></param>
        /// <param name="translate"></param>
        /// <returns></returns>
        public static string GetValue(this IBaseEntity entity, string propertyName, SupportedLanguage language, bool translate = XLib.Preference.TranslateEntityArguments)
        {
            return new BusinessRule(entity, propertyName, language, translate).ToString();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="propertyName"></param>
        /// <param name="useDisplayFormat"></param>
        /// <returns></returns>
        public static string GetTranslation(this IBaseEntity entity, string propertyName, bool useDisplayFormat = false)
        {
            return GetTranslation(entity, propertyName, XLib.Preference.CurrentLanguage, useDisplayFormat);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="propertyName"></param>
        /// <param name="language"></param>
        /// <param name="useDisplayFormat"></param>
        /// <returns></returns>
        public static string GetTranslation(this IBaseEntity entity, string propertyName, SupportedLanguage language, bool useDisplayFormat = false)
        {
            var key = entity.GetValueOf(propertyName)?.ToString();
            var translation = StringHelper.Translation(key, language);
            if (XLib.Preference.UseDisplayFormat && useDisplayFormat)
            {
                var displayFormat = StringHelper.Translation(FriendlyFormatType.TranslationFormat, language);
                if (key != translation)
                {
                    return StringHelper.Format(displayFormat, key, translation);
                }
            }
            return translation;
        }
        #endregion

        #region ILocalizedEntity
        [Obsolete]
        public static string GetName(this ILocalizedEntity entity, bool useDisplayFormat)
        {
            return entity.GetTranslation(nameof(entity.KeyName), useDisplayFormat);
        }
        #endregion

        //TODO: get persistent displayFormat and trimDigits

        #region Defaults
        [Obsolete]
        private static string GetDefaultLocalizedName(this string key, bool logResolvedException, bool useDisplayFormat = false)
        {
            //if (XLib.Preference.UseDisplayFormat && useDisplayFormat)
            //{
            //    var setting = new DisplayFormatSetting("{0}[ ({1})]", null, logResolvedException);
            //    var localization = new Translation(key);
            //    var result = new DefaultLocalizedDisplayFormat(setting, localization);
            //    return result.ToString();
            //}
            //else
            //{
            return StringHelper.Translation(key);
            //}
        }
        #endregion

        #region GetTitle
        public static object[] GetTitleProperties(this Address entity)
        {
            return new object[] { entity?.FullStreet, entity?.CityName };
        }

        public static object[] GetTitleProperties(this Partner entity)
        {
            //TODO
            return new object[] { entity?.PartnerCode, entity?.MasterContact?.FullName };
        }
        #endregion

        #region GetEntitySummary
        public static EntitySummary GetEntitySummary(this Address entity)
        {
            var title = StringHelper.Title(entity?.FullStreet, entity?.CityName);
            var subTitle = entity?.FullCountryName;
            var fullName = StringHelper.Title(title, subTitle);
            var sortName = title;
            return new EntitySummary(title, subTitle, fullName, sortName);
        }

        public static EntitySummary GetEntitySummary(this Partner entity)
        {
            var entityMember = entity?.MasterContact;
            var title = StringHelper.Title(entity?.PartnerCode, entityMember.FullName);
            var subTitle = entityMember.Address?.CityName;
            var fullName = entityMember.FullName;
            var sortName = entityMember.FullName;
            return new EntitySummary(title, subTitle, fullName, sortName);
        }
        #endregion

        #region Partner
        public static string GetFullPartnerName(this PartnerGeneralData entity)
        {
            //string[] names = null;
            //PropertyInfo property = null;
            //var type = typeof(PartnerGeneralData);
            //var proceed = ReflectionExtensions.TryGetProperty<PartnerGeneralData>("FullPartnerName", out property);
            //if (proceed)
            //{
            //    var attribute = property.GetCustomAttribute<ComposedOfAttribute>();
            //    if (attribute != null)
            //    {
            //        names = attribute.ValidMemberNames;
            //    }
            //}

            //var type = entity.GetType();
            //var properties = PropertyHelper.GetProperties(type);
            ////var test = properties.Single(x => x.Name == names[XLib.Number.Index0]).Getter(entity);
            //var composed = properties.FirstOrDefault(x => x.Name == "FullName").ComposedOf.ValidMemberNames;
            //var zero = properties.FirstOrDefault(x => x.Name == composed[XLib.Number.Index0]).Getter(entity);
            //var one = properties.FirstOrDefault(x => x.Name == composed[XLib.Number.Index1]).Getter(entity);

            //var composed = PropertyHelper.GetProperty(type, "FullName").ComposedOf.ValidMemberNames;
            //var zero = PropertyHelper.GetProperty(type, composed[XLib.Number.Index0]).Getter(entity);
            //var one = PropertyHelper.GetProperty(type, composed[XLib.Number.Index1]).Getter(entity);

            var setting = new DisplayFormatSetting("[{0}][, {1}]", ",", false);
            //var result = new FullPartnerNameDisplayFormat(setting, entity[names[XLib.Number.Index0]]?.ToString(), entity[names[XLib.Number.Index1]]?.ToString());
            //var result = new FullPartnerNameDisplayFormat(setting, zero?.ToString(), one?.ToString());
            var result = new FullPartnerNameDisplayFormat(setting, entity.FullCompanyNameAndExtraName, entity.FullPersonName);
            return result.ToString();
        }

        public static string GetCompanyCorporationTypeName(this PartnerGeneralData entity)
        {
            return StringHelper.Translation(entity.CompanyCorporationTypeKeyName);
        }

        public static string GetPersonSalutationName(this PartnerGeneralData entity)
        {
            return StringHelper.Translation(entity.PersonSalutationKeyName);
        }

        public static string GetGenderName(this PartnerGeneralData entity)
        {
            return StringHelper.Translation(entity.GenderKeyName);
        }

        public static string GetCityName(this PartnerGeneralData entity)
        {
            return entity.CityKeyName.GetDefaultLocalizedName(false);
        }

        public static string GetCountryName(this PartnerGeneralData entity)
        {
            return entity.CountryKeyName.GetDefaultLocalizedName(false);
        }
        #endregion

        #region Contact
        public static string GetFullContactName(this Contact entity)
        {
            string companyName = null;
            string personName = null;
            if (entity.Company != null)
            {
                companyName = entity.Company.FullCompanyNameAndExtraName;
            }
            if (entity.Person != null)
            {
                personName = entity.Person.FullPersonName;
            }
            var setting = new DisplayFormatSetting("[{0}][, {1}]", ",", true);
            var result = new FullContactNameDisplayFormat(setting, companyName, personName);
            return result.ToString();
        }
        #endregion

        #region Company
        public static string GetCompanyCorporationTypeName(this Company entity)
        {
            if (entity.CompanyCorporationType != null)
            {
                return entity.CompanyCorporationType.Name;
            }
            return null;
        }

        public static string GetFullCompanyName(this Company entity)
        {
            //TODO: private default method
            var setting = new DisplayFormatSetting("{0}[ {1}]", null, true);
            var result = new FullCompanyNameDisplayFormat(setting, entity.CompanyName, entity.CompanyCorporationTypeName);
            return result.ToString();
        }

        public static string GetFullCompanyName(this PartnerGeneralData entity)
        {
            //TODO: private default method
            var setting = new DisplayFormatSetting("{0}[ {1}]", null, false);
            var result = new FullCompanyNameDisplayFormat(setting, entity.CompanyName, entity.CompanyCorporationTypeName);
            return result.ToString();
        }

        public static string GetFullCompanyNameAndExtraName(this Company entity)
        {
            //TODO: private default method
            var setting = new DisplayFormatSetting("{0}[ ({1})]", null, true);
            var result = new FullCompanyNameAndExtraNameDisplayFormat(setting, entity.FullCompanyName, entity.CompanyExtraName);
            return result.ToString();
        }

        public static string GetFullCompanyNameAndExtraName(this PartnerGeneralData entity)
        {
            //TODO: private default method
            var setting = new DisplayFormatSetting("{0}[ ({1})]", null, false);
            var result = new FullCompanyNameAndExtraNameDisplayFormat(setting, entity.FullCompanyName, entity.CompanyExtraName);

            //var type = entity.GetType();
            //var composed = PropertyHelper.GetProperty(type, "FullCompanyNameAndExtraName").ComposedOf.ValidOrderingNames;
            //var zero = PropertyHelper.GetProperty(type, composed[XLib.Number.Index0]).Getter(entity);
            //var one = PropertyHelper.GetProperty(type, composed[XLib.Number.Index1]).Getter(entity);
            //var two = PropertyHelper.GetProperty(type, composed[XLib.Number.Index2]).Getter(entity);
            //var result = new FullCompanyNameAndExtraNameDisplayFormat(setting, zero?.ToString() + " " + one?.ToString(), two?.ToString());

            return result.ToString();
        }
        #endregion

        #region Person
        public static string GetPersonSalutationName(this Person entity)
        {
            if (entity.PersonSalutation != null)
            {
                return entity.PersonSalutation.Name;
            }
            return null;
        }

        public static string GetFullPersonName(this Person entity)
        {
            //TODO: private default method
            var setting = new DisplayFormatSetting("[{0}][ {1}]", null, true);
            //var result = new FullPersonNameDisplayFormat(setting, entity.FirstName, entity.LastName);

            var type = entity.GetType();
            var composed = PropertyCache.GetCachedProperty(type, "FullName").ComposedOf.MemberNames;
            var zero = PropertyCache.GetCachedProperty(type, composed[XLib.MagicNumber.Index0]).Value(entity);
            var one = PropertyCache.GetCachedProperty(type, composed[XLib.MagicNumber.Index1]).Value(entity);
            var result = new FullPersonNameDisplayFormat(setting, zero?.ToString(), one?.ToString());

            return result.ToString();
        }

        public static string GetFullPersonName(this PartnerGeneralData entity)
        {
            //TODO: private default method
            var setting = new DisplayFormatSetting("[{0}][ {1}]", null, false);
            //var result = new FullPersonNameDisplayFormat(setting, entity.FirstName, entity.LastName);

            var type = typeof(Person); //entity.GetType();
            var composed = PropertyCache.GetCachedProperty(type, "FullName").ComposedOf.MemberNames;
            var zero = PropertyCache.GetCachedProperty(entity.GetType(), composed[XLib.MagicNumber.Index0]).Value(entity);
            var one = PropertyCache.GetCachedProperty(entity.GetType(), composed[XLib.MagicNumber.Index1]).Value(entity);
            var result = new FullPersonNameDisplayFormat(setting, zero?.ToString(), one?.ToString());

            return result.ToString();
        }

        public static string GetFullPersonNameAndSalutationName(this Person entity)
        {
            var setting = new DisplayFormatSetting("[{0}][ {1}][ {2}]", null, true);
            var result = new FullPersonNameAndSalutationNameDisplayFormat(setting, entity.PersonSalutationName, entity.FirstName, entity.LastName);
            return result.ToString();
        }
        #endregion

        #region Address
        public static string GetZipCode(this Address entity)
        {
            if (entity.City != null)
            {
                return entity.City.ZipCode;
            }
            return null;
        }

        public static string GetCityName(this Address entity)
        {
            if (entity.City != null)
            {
                return entity.City.Name;
            }
            return null;
        }

        public static string GetCountryName(this Address entity)
        {
            if (entity.City != null)
            {
                return entity.City.FullCountryName;
            }
            return null;
        }

        public static string GetFullStreet(this Address entity)
        {
            //TODO: private default method
            var addressSetting = new DisplayFormatSetting("{0}[ {1}][ {2}]", null, true);
            var boxNumberSetting = new DisplayFormatSetting("{0} {1}", null, true);
            var result = new FullAddressDisplayFormat(addressSetting, boxNumberSetting, entity.Street, entity.HouseNumber, entity.BoxNumber);
            return result.ToString();
        }

        public static string GetFullStreet(this PartnerGeneralData entity)
        {
            //TODO: private default method
            var addressSetting = new DisplayFormatSetting("{0}[ {1}][ {2}]", null, false);
            var boxNumberSetting = new DisplayFormatSetting("{0} {1}", null, false);
            var result = new FullAddressDisplayFormat(addressSetting, boxNumberSetting, entity.Street, entity.HouseNumber, entity.BoxNumber);
            return result.ToString();
        }
        #endregion

        #region City
        public static string GetCountryName(this City entity)
        {
            if (entity.Country != null)
            {
                return entity.Country.Name;
            }
            return null;
        }
        #endregion
    }
}
