using System.Collections.Generic;

namespace ProjectX.Domain.Models
{
    public partial class CompanyCorporationType : BaseLocalizedEntity
    {
        public CompanyCorporationType()
        {
            this.Companies = new List<Company>();
        }

        public int CompanyCorporationTypeID { get; set; }
        public string CountryID { get; set; }
        public string Description { get; set; }
        public virtual Country Country { get; set; }
        public virtual ICollection<Company> Companies { get; set; }
    }
}
