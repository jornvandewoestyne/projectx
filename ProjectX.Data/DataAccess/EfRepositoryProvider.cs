﻿using ProjectX.General;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.Data
{
    internal sealed class EfRepositoryProvider<TEntity> : IRepositoryProvider<TEntity>
        where TEntity : BaseEntity
    {
        #region Fields
        private int _count;
        #endregion

        #region Properties
        public EfRepository<TEntity> Repository { get; }
        public IQueryable<TEntity> Query { get; set; }

        public int Count
        {
            get => this._count;
            set => this._count = this.Repository.Count = value;
        }
        #endregion

        #region Constructors
        public EfRepositoryProvider(EfRepository<TEntity> repository)
        {
            this.Repository = repository;
            this.Query = this.Repository.DbSet.AsNoTracking().AsQueryable();
        }
        #endregion

        #region Methods
        public IQueryable<TEntity> Join(HashSet<int> keys, string propertyName)
        {
            return this.Query = this.Query.TryJoin(keys, master => master[propertyName], foreign => foreign, (master, foreign) => master);
        }

        public IQueryable<TEntity> Take(Ordering ordering, Paging paging)
        {
            return this.Query = this.Query.TryTake<TEntity>(ordering, paging);
        }

        public IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> filter)
        {
            return this.Query = this.Query.TryWhere<TEntity>(filter);
        }

        //using System.Data.Entity (EntityFramework):
        public IQueryable<TEntity> Include(Including<TEntity> including)
        {
            if (including != null && including.IsSafe)
            {
                foreach (var property in including.ValidProperties)
                {
                    var propertyInfo = property.PropertyInfo;
                    this.Query = this.Query.Include(propertyInfo.Name);
                }
            }
            return this.Query;
        }

        //using System.Data.Entity (EntityFramework):
        public async Task<int> CountAsync(IQueryable<TEntity> source, CancellationToken cancellationToken)
        {
            //await this.Repository.DbContext.TryToConnectAsync(cancellationToken);
            return this.Count = await source.CountAsync(cancellationToken);
        }

        //using System.Data.Entity (EntityFramework):
        public async Task<IEnumerable<TEntity>> ToListAsyncSafe(IQueryable<TEntity> source, CancellationToken cancellationToken)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }
            cancellationToken.ThrowIfCancellationRequested();
            return (source is IDbAsyncEnumerable)
                ? await source.ToListAsync<TEntity>(cancellationToken)
                : await source.AsToListAsync<TEntity>(cancellationToken);
        }
        #endregion
    }
}
