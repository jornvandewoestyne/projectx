﻿using ProjectX.Data.Helpers;
using ProjectX.Data.Models;
using ProjectX.Domain.Models;
using ProjectX.General;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

#region Source
//https://www.itworld.com/article/2700950/development/a-generic-repository-for--net-entity-framework-6-with-async-operations.html

//https://stackoverflow.com/questions/6417886/how-should-i-expose-the-total-record-count-and-ienumable-collection-of-paged-rec
//http://blog.gauffin.org/2013/01/repository-pattern-done-right/
//http://codetunnel.io/should-you-return-iqueryablet-from-your-repositories/
//http://codetunnel.io/how-to-properly-return-a-paged-result-set-from-your-repository/

//TODO: count???
//https://www.codeproject.com/articles/526874/repositorypluspattern-cplusdoneplusright
//TODO: create temptable when joining needed?
//http://weblogs.thinktecture.com/pawel/2016/04/entity-framework-using-sqlbulkcopy-and-temp-tables.html

//Paging
//https://www.codeproject.com/kb/grid/datagridboundedpaging.aspx
//https://msdn.microsoft.com/en-us/library/ms171624.aspx
//https://www.codeproject.com/Articles/38018/DatGridView-Filtering-User-Control
//https://docs.microsoft.com/en-us/aspnet/mvc/overview/getting-started/getting-started-with-ef-using-mvc/sorting-filtering-and-paging-with-the-entity-framework-in-an-asp-net-mvc-application


//CHECK!!
//https://www.red-gate.com/simple-talk/dotnet/net-tools/entity-framework-performance-and-what-you-can-do-about-it/
#endregion

//TODO: make methods safe!!! (NullReferenceExceptions)

namespace ProjectX.Data
{
    /// <summary>
    /// ...
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public sealed class EfRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        #region Properties
        internal EfDbContext DbContext { get; }
        internal DbSet<TEntity> DbSet { get; }
        internal int Count { get; set; }
        #endregion

        #region Constructor
        public EfRepository(IContext context)
        {
            //Attention: This line of code is tightly coupled!
            this.DbContext = context as EfDbContext;
            this.DbSet = this.DbContext.Set<TEntity>();
        }
        #endregion

        #region Methods: NavigationProperties -> interface IRepository<TEntity>
        public IEnumerable<System.Reflection.PropertyInfo> GetNavigationProperties()
        {
            return this.DbContext.GetNavigationProperties<TEntity>();
        }
        #endregion

        #region Methods: EntityKeys -> interface IRepository<TEntity>
        public EntityKey GetKey(TEntity entity)
        {
            return this.GetKey<TEntity>(entity, false);
        }

        public EntityKeyName GetKeyName()
        {
            return this.GetKeyName<TEntity>(false);
        }

        public EntityKeyType GetKeyType()
        {
            return this.GetKeyType<TEntity>(false);
        }
        #endregion

        #region Methods: CRUD -> interface IRepository<TEntity>
        public IEnumerable<TEntity> GetData(Ordering ordering = null, Including<TEntity> including = null)
        {
            return this.GetData<TEntity>(null, ordering, null, including, false);
        }

        public async Task<IEnumerable<TEntity>> GetDataAsync(CancellationToken cancellationToken, Ordering ordering = null, Including<TEntity> including = null)
        {
            return await this.GetDataAsync<TEntity>(null, ordering, null, including, false, cancellationToken);
        }

        public IEnumerable<TEntity> GetData(Expression<Func<TEntity, bool>> filter, Ordering ordering, Including<TEntity> including)
        {
            return this.GetData<TEntity>(filter, ordering, null, including, false);
        }

        public async Task<IEnumerable<TEntity>> GetDataAsync(Expression<Func<TEntity, bool>> filter, Ordering ordering, Including<TEntity> including, CancellationToken cancellationToken)
        {
            return await this.GetDataAsync<TEntity>(filter, ordering, null, including, false, cancellationToken);
        }

        public IKeySet<int> GetData<TSearchEntity>(Expression<Func<TEntity, bool>> simpleFilter, Expression<Func<TEntity, bool>> combinedFilter, IKeySet<int> keySet, EntityKeyName entityKeyName, string keyword, bool initialSearch, Ordering ordering)
            where TSearchEntity : BaseSearchEntity
        {
            return this.GetData<TEntity, TSearchEntity>(simpleFilter, combinedFilter, keySet, entityKeyName, keyword, initialSearch, ordering, null);
        }

        public async Task<IKeySet<int>> GetDataAsync<TSearchEntity>(Expression<Func<TEntity, bool>> simpleFilter, Expression<Func<TEntity, bool>> combinedFilter, IKeySet<int> keySet, EntityKeyName entityKeyName, string keyword, bool initialSearch, Ordering ordering, CancellationToken cancellationToken)
            where TSearchEntity : BaseSearchEntity
        {
            return await this.GetDataAsync<TEntity, TSearchEntity>(simpleFilter, combinedFilter, keySet, entityKeyName, keyword, initialSearch, ordering, null, cancellationToken);
        }

        public ICollectionPage<TEntity> GetDataPage(Ordering ordering, Paging paging)
        {
            var entities = this.GetData<TEntity>(null, ordering, paging, null, true);
            return entities.GetDataPage<TEntity>(paging, this.Count, null);
        }

        public async Task<ICollectionPage<TEntity>> GetDataPageAsync(Ordering ordering, Paging paging, CancellationToken cancellationToken)
        {
            var entities = await this.GetDataAsync<TEntity>(null, ordering, paging, null, true, cancellationToken);
            return await entities.GetDataPageAsync<TEntity>(paging, this.Count, null, cancellationToken);
        }

        public ICollectionPage<TEntity> GetDataPage(IKeySet<int> keySet, EntityKeyName entityKeyName, Ordering ordering, Paging paging)
        {
            var entities = this.GetData<TEntity>(keySet, entityKeyName, ordering, paging, null);
            return entities.GetDataPage<TEntity>(paging, this.Count, keySet);
        }

        public async Task<ICollectionPage<TEntity>> GetDataPageAsync(IKeySet<int> keySet, EntityKeyName entityKeyName, Ordering ordering, Paging paging, CancellationToken cancellationToken)
        {
            var entities = await this.GetDataAsync<TEntity>(keySet, entityKeyName, ordering, paging, null, cancellationToken);
            return await entities.GetDataPageAsync<TEntity>(paging, this.Count, keySet, cancellationToken);
        }

        public TEntity GetEntity(EntityKey entityKey, Including<TEntity> including)
        {
            return this.FindEntity<TEntity>(entityKey, including, false);
        }

        public async Task<TEntity> GetEntityAsync(EntityKey entityKey, Including<TEntity> including, CancellationToken cancellationToken)
        {
            return await this.FindEntityAsync<TEntity>(entityKey, including, false, cancellationToken);
        }


        //TODO: ...
        public TEntity GetFirst(Expression<Func<TEntity, bool>> predicate)
        {
            return this.DbSet.AsNoTracking().FirstOrDefault(predicate);
        }

        public void Insert(TEntity entity)
        {
            this.DbSet.Add(entity);
        }

        public TEntity InsertAndSave(TEntity entity)
        {
            this.DbSet.Add(entity);
            this.DbContext.SaveChanges();
            return entity;
        }

        public void Update(TEntity entity)
        {
            this.DbSet.Attach(entity);
            this.DbContext.Entry(entity).State = EntityState.Modified;
        }

        public void UpdateById(EntityKey entityKey)
        {
            Update(GetEntity(entityKey, null));
        }

        public void Delete(TEntity entity)
        {
            if (this.DbContext.Entry(entity).State == EntityState.Detached)
            {
                this.DbSet.Attach(entity);
            }
            this.DbSet.Remove(entity);
        }

        public void DeleteByID(EntityKey entityKey)
        {
            Delete(GetEntity(entityKey, null));
        }
        #endregion

        ////TODO: test, keep?
        //public void Dispose()
        //{
        //    //DbContext.Dispose();
        //}
    }
}
