﻿using ProjectX.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace ProjectX.Data
{
    internal sealed class EfRepositoryEntityKey : EntityKeyHelper
    {
        #region Fields
        private static readonly Lazy<EfRepositoryEntityKey> _lazyInstance =
            new Lazy<EfRepositoryEntityKey>(() => new EfRepositoryEntityKey());
        #endregion

        #region Properties
        public static EntityKeyHelper Instance => _lazyInstance.Value;
        #endregion

        #region Constructors
        private EfRepositoryEntityKey() { }
        #endregion

        #region Overrides
        protected override string[] GetKeyNames(IContext context, Type type)
        {
            var objectContext = ((IObjectContextAdapter)context).ObjectContext;
            //create method CreateObjectSet with the generic parameter of the base-type
            var method = typeof(ObjectContext)
                .GetMethod("CreateObjectSet", Type.EmptyTypes)
                .MakeGenericMethod(type);
            dynamic objectSet = method.Invoke(objectContext, null);
            IEnumerable<dynamic> keyMembers = objectSet.EntitySet.ElementType.KeyMembers;
            return keyMembers.Select(k => (string)k.Name).ToArray();
        }
        #endregion
    }
}
