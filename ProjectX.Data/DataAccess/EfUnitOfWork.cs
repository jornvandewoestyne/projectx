﻿using System;

namespace ProjectX.Data
{
    public sealed class EfUnitOfWork : BaseUnitOfWork
    {
        #region Contructors
        public EfUnitOfWork(IContext context) : base(context)
        { }
        #endregion

        #region Methods: Overrides
        protected override IRepository<TEntity> GetRepositoryInstance<TEntity>(IContext context)
        {
            return new Lazy<EfRepository<TEntity>>(() => new EfRepository<TEntity>(context)).Value;
            //return new EfRepository<TEntity>(context);
        }
        #endregion
    }
}
