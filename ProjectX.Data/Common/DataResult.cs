﻿using ProjectX.Domain.Models;
using ProjectX.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

//TODO: safe null checks!
//TODO: return notifications when paging or ordering -> null!

namespace ProjectX.Data
{
    internal sealed class DataResult<TEntity> : IDataResult<TEntity> where TEntity : BaseEntity
    {
        #region Properties
        public IRepositoryProvider<TEntity> Provider { get; }
        public Expression<Func<TEntity, bool>> Filter { get; private set; }
        public IKeySet<int> KeySet { get; private set; }
        public HashSet<int> Keys => this.KeySet.ResolveNull().Items;
        public Ordering Ordering { get; }
        public Paging Paging { get; private set; }
        public Including<TEntity> Including { get; }
        public EntityKeyName EntityKeyName { get; }
        public string PropertyName => this.EntityKeyName.ResolveNull().ToString();
        public bool PerformFilter => this.Filter != null;
        public bool PerformInclude => this.Including != null;
        public bool PerformJoin { get; private set; }
        public bool PerformCount { get; private set; }
        public bool HasNotifications { get; private set; }
        #endregion

        #region Constructors
        public DataResult(IRepositoryProvider<TEntity> provider, Expression<Func<TEntity, bool>> filter, Ordering ordering, Paging paging, Including<TEntity> including, bool performCount)
            : this(provider, filter, null, null, ordering, paging, including, performCount)
        { }

        public DataResult(IRepositoryProvider<TEntity> provider, IKeySet<int> keySet, EntityKeyName entityKeyName, Ordering ordering, Paging paging, Including<TEntity> including)
            : this(provider, null, keySet, entityKeyName, ordering, paging, including, false)
        {
            this.SetDefaultStrategy();
        }

        public DataResult(IRepositoryProvider<TEntity> provider, Expression<Func<TEntity, bool>> simpleFilter, Expression<Func<TEntity, bool>> combinedFilter, IKeySet<int> keySet, EntityKeyName entityKeyName, string keyword, bool initialSearch, Ordering ordering, Including<TEntity> including)
            : this(provider, null, keySet, entityKeyName, ordering, null, including, false)
        {
            this.SetKeySetStrategy(simpleFilter, combinedFilter, keyword.ResolveNull(), initialSearch);
        }

        private DataResult(IRepositoryProvider<TEntity> provider, Expression<Func<TEntity, bool>> filter, IKeySet<int> keySet, EntityKeyName entityKeyName, Ordering ordering, Paging paging, Including<TEntity> including, bool performCount)
        {
            this.Provider = provider;
            this.Filter = filter;
            this.KeySet = keySet;
            this.EntityKeyName = entityKeyName;
            this.Ordering = ordering;
            this.Paging = paging;
            this.Including = including;
            this.PerformCount = performCount;
        }
        #endregion

        #region Public Methods -> IDataResult<TEntity>
        public IEnumerable<TEntity> GetData()
        {
            var query = this.Provider.Query;
            if (this.PerformFilter)
            {
                query = this.Provider.Where(this.Filter);
            }
            if (this.PerformInclude)
            {
                query = this.Provider.Include(this.Including);
            }
            if (this.PerformJoin)
            {
                query = this.Provider.Join(this.Keys, this.PropertyName);
            }
            if (this.PerformCount || (this.Paging != null && this.Paging.TakeAll))
            {
                this.Provider.Count = query.Count();
                if (this.Provider.Count >= XLib.Limit.MaxItemsToBeReturned)
                {
                    this.Paging.TakeAll = false;
                }
            }
            query = this.Provider.Take(this.Ordering, this.Paging);
            return query.ToList<TEntity>();
        }

        public async Task<IEnumerable<TEntity>> GetDataAsync(CancellationToken cancellationToken)
        {
            var query = this.Provider.Query;
            if (this.PerformFilter)
            {
                query = this.Provider.Where(this.Filter);
            }
            if (this.PerformInclude)
            {
                query = this.Provider.Include(this.Including);
            }
            if (this.PerformJoin)
            {
                query = this.Provider.Join(this.Keys, this.PropertyName);
            }
            if (this.PerformCount || (this.Paging != null && this.Paging.TakeAll))
            {
                cancellationToken.ThrowIfCancellationRequested();
                this.Provider.Count = await this.Provider.CountAsync(query, cancellationToken);
                if (this.Provider.Count >= XLib.Limit.MaxItemsToBeReturned)
                {
                    this.Paging.TakeAll = false;
                }
            }
            query = this.Provider.Take(this.Ordering, this.Paging);
            return await this.Provider.ToListAsyncSafe(query, cancellationToken);
        }

        public IKeySet<int> GetKeySet<TSearchEntity>()
            where TSearchEntity : BaseSearchEntity
        {
            if (this.HasNotifications)
            {
                return this.KeySet;
            }
            return this.GetData().AsKeySet<TEntity, TSearchEntity>(this.KeySet.Notifications);
        }

        public async Task<IKeySet<int>> GetKeySetAsync<TSearchEntity>(CancellationToken cancellationToken)
            where TSearchEntity : BaseSearchEntity
        {
            if (this.HasNotifications)
            {
                return this.KeySet;
            }
            var items = await this.GetDataAsync(cancellationToken);
            return items.AsKeySet<TEntity, TSearchEntity>(this.KeySet.Notifications);
        }
        #endregion

        #region Private Methods
        private void SetDefaultStrategy()
        {
            if (this.KeySet != null && this.KeySet.Items != null)
            {
                var keys = this.Keys;
                var totalItems = this.Provider.Count = keys.Count;
                if (totalItems <= XLib.Limit.MaxForOptimization)
                {
                    this.Filter = keys.Contains<TEntity>(this.PropertyName);
                }
                else
                {
                    this.PerformJoin = true;
                }
            }
            else
            {
                this.PerformCount = true;
            }
        }

        private void SetKeySetStrategy(Expression<Func<TEntity, bool>> simpleFilter, Expression<Func<TEntity, bool>> combinedFilter, string keyword, bool initialSearch)
        {
            var notifications = this.KeySet.ResolveNull().Notifications;
            if (initialSearch)
            {
                //TODO: make extension with bool/out KeySet OR use design pattern
                if (keyword.Length < XLib.Limit.MinLengthKeyWordForOptimization)
                {
                    notifications.Add(new Notification(FriendlyMessageType.TooManyResults, false, keyword));
                    this.HasNotifications = true;
                }
                else
                {
                    this.Filter = simpleFilter;
                }
            }
            else if (this.KeySet != null && this.KeySet.Items != null)
            {
                var keys = this.Keys;
                var totalItems = keys.Count;
                //TODO: make extension with bool/out KeySet OR use design pattern
                if (totalItems > XLib.Limit.MaxForOptimization
                    && keyword.Length < XLib.Limit.MinLengthKeyWordForOptimization)
                {
                    notifications.Add(new Notification(FriendlyMessageType.TooManyResults, false, keyword));
                    this.HasNotifications = true;
                }
                else if (totalItems <= XLib.Limit.MaxForOptimization)
                {
                    this.Filter = combinedFilter;
                }
                else
                {
                    this.Filter = simpleFilter;
                    this.PerformJoin = true;
                }
            }
            else
            {
                //TODO: this should be TakeAll (not SkipAll)???
                this.Paging = new Paging(true);
            }
            if (this.HasNotifications)
            {
                this.KeySet = new List<int>().AsKeySet<int>(notifications);
            }
        }
        #endregion
    }
}
