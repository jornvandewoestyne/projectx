﻿using ProjectX.General;
using System;
using System.Threading;
using System.Threading.Tasks;

//TODO: lazy dictionary? https://blogs.endjin.com/2015/10/using-lazy-and-concurrentdictionary-to-ensure-a-thread-safe-run-once-lazy-loaded-collection/
//https://www.infoworld.com/article/3227207/c-sharp/how-to-perform-lazy-initialization-in-c.html

namespace ProjectX.Data
{
    public abstract class BaseUnitOfWork : IUnitOfWork, IDisposable
    {
        #region Properties
        protected IContext Context { get; }
        //Needs to be static with default value! (don't change!)
        //TODO: TEST Remove static -> avoiding memory-leaks!
        //protected static Dictionary<Type, object> Repositories { get; } = new Dictionary<Type, object>();
        protected LazyConcurrentDictionary<Type, object> Repositories { get; } = new LazyConcurrentDictionary<Type, object>();
        #endregion

        #region Constructors
        protected BaseUnitOfWork(IContext context)
        {
            this.Context = context;
            this.Disposed = false;
        }
        #endregion

        #region Methods: Abstract
        protected abstract IRepository<TEntity> GetRepositoryInstance<TEntity>(IContext context) where TEntity : BaseEntity;
        #endregion

        #region Methods: -> interface IUnitOfWork
        public void TryToConnect()
        {
            this.Context.TryToConnect();
        }

        public async Task TryToConnectAsync(CancellationToken cancellationToken)
        {
            await this.Context.TryToConnectAsync(cancellationToken);
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity
        {
            var repository = Repositories.GetOrAdd(typeof(TEntity), result => (object)this.GetRepositoryInstance<TEntity>(this.Context));
            return repository as IRepository<TEntity>;
            ////Checks if the Dictionary Key contains the Model class:
            //if (Repositories.Keys.Contains(typeof(TEntity)))
            //{
            //    //Return the repository for that Model class:
            //    return Repositories[typeof(TEntity)] as IRepository<TEntity>;
            //}
            ////If the repository for that Model class doesn't exist, create it:
            //var repositoryInstance = this.GetRepositoryInstance<TEntity>(this.Context);
            ////new Lazy<SomeBigObject>(() => new SomeBigObject());
            ////Add it to the dictionary:
            //Repositories.Add(typeof(TEntity), repositoryInstance);
            //return repositoryInstance;
        }

        public void Save()
        {
            this.Context.SaveChanges();
        }
        #endregion

        #region Methods: -> interface IDisposable
        protected bool Disposed { get; private set; }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.Disposed)
            {
                if (disposing)
                {
                    //foreach (var item in Repositories.concurrentDictionary)
                    //{
                    //    var x = item.Value as IDisposable;
                    //    x.Dispose();
                    //}
                    this.Context.Dispose();
                }
                this.Disposed = true;
            }
        }
        #endregion
    }
}

