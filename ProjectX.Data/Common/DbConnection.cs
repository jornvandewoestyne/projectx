﻿using System.Configuration;

//TODO: multiple database providers https://dev.mysql.com/doc/connector-net/en/connector-net-entityframework60.html

namespace ProjectX.Data
{
    public sealed class DbConnection
    {
        #region Properties
        public string ConnectionString => this.GetConnectionString();
        #endregion

        #region Methods
        /// <summary>
        /// Returns the current ConnectionString from the application config file
        /// </summary>
        private string GetConnectionString()
        {
            string name = ConfigurationManager.AppSettings["DbSettings"];
            ConnectionStringSettings config = ConfigurationManager.ConnectionStrings[name];
            if (config != null)
            {
                return config.ToString();
            }
            return "none";
        }
        #endregion
    }
}
