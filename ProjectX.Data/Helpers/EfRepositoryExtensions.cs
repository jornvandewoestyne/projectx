﻿using ProjectX.Domain.Models;
using ProjectX.General;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.Data.Helpers
{
    /// <summary>
    /// ...
    /// </summary>
    internal static class EfRepositoryExtensions
    {
        #region Methods: EntityKeys
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="entity"></param>
        /// <param name="useBaseType"></param>
        /// <returns></returns>
        public static EntityKey GetKey<TEntity>(this EfRepository<TEntity> repository, TEntity entity, bool useBaseType)
            where TEntity : BaseEntity
        {
            return EfRepositoryEntityKey.Instance.GetKeys<TEntity>(entity, repository.DbContext, useBaseType);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="useBaseType"></param>
        /// <returns></returns>
        public static EntityKeyName GetKeyName<TEntity>(this EfRepository<TEntity> repository, bool useBaseType)
            where TEntity : BaseEntity
        {
            return EfRepositoryEntityKey.Instance.GetKeyNames<TEntity>(repository.DbContext, useBaseType);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="useBaseType"></param>
        /// <returns></returns>
        public static EntityKeyType GetKeyType<TEntity>(this EfRepository<TEntity> repository, bool useBaseType)
            where TEntity : BaseEntity
        {
            return EfRepositoryEntityKey.Instance.GetTypes<TEntity>(repository.DbContext, useBaseType);
        }

        /// <summary>
        /// Convert to the correct value type if the key is provided as String, to avoid an InvalidCastException.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entityKey"></param>
        /// <param name="repository"></param>
        /// <param name="useBaseType"></param>
        /// <returns></returns>
        private static object[] Cast<TEntity>(this EntityKey entityKey, EfRepository<TEntity> repository, bool useBaseType)
            where TEntity : BaseEntity
        {
            return EfRepositoryEntityKey.Instance.Cast<TEntity>(entityKey, repository.DbContext, useBaseType);
        }
        #endregion

        #region Methods: GetData
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="filter"></param>
        /// <param name="ordering"></param>
        /// <param name="paging"></param>
        /// <param name="including"></param>
        /// <param name="performCount"></param>
        /// <returns></returns>
        public static IEnumerable<TEntity> GetData<TEntity>(this EfRepository<TEntity> repository, Expression<Func<TEntity, bool>> filter, Ordering ordering, Paging paging, Including<TEntity> including, bool performCount)
            where TEntity : BaseEntity
        {
            return repository
                .AsDataResult<TEntity>(filter, ordering, paging, including, performCount)
                .GetData();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="filter"></param>
        /// <param name="ordering"></param>
        /// <param name="paging"></param>
        /// <param name="including"></param>
        /// <param name="performCount"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<TEntity>> GetDataAsync<TEntity>(this EfRepository<TEntity> repository, Expression<Func<TEntity, bool>> filter, Ordering ordering, Paging paging, Including<TEntity> including, bool performCount, CancellationToken cancellationToken)
            where TEntity : BaseEntity
        {
            return await repository
                .AsDataResult<TEntity>(filter, ordering, paging, including, performCount)
                .GetDataAsync(cancellationToken);
        }


        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="keySet"></param>
        /// <param name="entityKeyName"></param>
        /// <param name="ordering"></param>
        /// <param name="paging"></param>
        /// <param name="including"></param>
        /// <returns></returns>
        public static IEnumerable<TEntity> GetData<TEntity>(this EfRepository<TEntity> repository, IKeySet<int> keySet, EntityKeyName entityKeyName, Ordering ordering, Paging paging, Including<TEntity> including)
            where TEntity : BaseEntity
        {
            return repository
                .AsDataResult<TEntity>(keySet, entityKeyName, ordering, paging, including)
                .GetData();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="keySet"></param>
        /// <param name="entityKeyName"></param>
        /// <param name="ordering"></param>
        /// <param name="paging"></param>
        /// <param name="including"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<TEntity>> GetDataAsync<TEntity>(this EfRepository<TEntity> repository, IKeySet<int> keySet, EntityKeyName entityKeyName, Ordering ordering, Paging paging, Including<TEntity> including, CancellationToken cancellationToken)
            where TEntity : BaseEntity
        {
            return await repository
                .AsDataResult<TEntity>(keySet, entityKeyName, ordering, paging, including)
                .GetDataAsync(cancellationToken);
        }
        #endregion

        #region Methods: KeySet
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TSearchEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="simpleFilter"></param>
        /// <param name="combinedFilter"></param>
        /// <param name="keySet"></param>
        /// <param name="entityKeyName"></param>
        /// <param name="keyword"></param>
        /// <param name="initialSearch"></param>
        /// <param name="ordering"></param>
        /// <param name="including"></param>
        /// <returns></returns>
        public static IKeySet<int> GetData<TEntity, TSearchEntity>(this EfRepository<TEntity> repository, Expression<Func<TEntity, bool>> simpleFilter, Expression<Func<TEntity, bool>> combinedFilter, IKeySet<int> keySet, EntityKeyName entityKeyName, string keyword, bool initialSearch, Ordering ordering, Including<TEntity> including)
            where TEntity : BaseEntity
            where TSearchEntity : BaseSearchEntity
        {
            return repository
                .AsDataResult<TEntity>(simpleFilter, combinedFilter, keySet, entityKeyName, keyword, initialSearch, ordering, including)
                .GetKeySet<TSearchEntity>();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TSearchEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="simpleFilter"></param>
        /// <param name="combinedFilter"></param>
        /// <param name="keySet"></param>
        /// <param name="entityKeyName"></param>
        /// <param name="keyword"></param>
        /// <param name="initialSearch"></param>
        /// <param name="ordering"></param>
        /// <param name="including"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async Task<IKeySet<int>> GetDataAsync<TEntity, TSearchEntity>(this EfRepository<TEntity> repository, Expression<Func<TEntity, bool>> simpleFilter, Expression<Func<TEntity, bool>> combinedFilter, IKeySet<int> keySet, EntityKeyName entityKeyName, string keyword, bool initialSearch, Ordering ordering, Including<TEntity> including, CancellationToken cancellationToken)
            where TEntity : BaseEntity
            where TSearchEntity : BaseSearchEntity
        {
            return await repository
                .AsDataResult<TEntity>(simpleFilter, combinedFilter, keySet, entityKeyName, keyword, initialSearch, ordering, including)
                .GetKeySetAsync<TSearchEntity>(cancellationToken);
        }
        #endregion

        #region Methods: Find
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="entityKey"></param>
        /// <param name="including"></param>
        /// <param name="useBaseType"></param>
        /// <returns></returns>
        public static TEntity FindEntity<TEntity>(this EfRepository<TEntity> repository, EntityKey entityKey, Including<TEntity> including, bool useBaseType)
            where TEntity : BaseEntity
        {
            var id = entityKey.Cast<TEntity>(repository, useBaseType);
            var entity = repository.DbSet.Find(id);
            if (including != null && including.IsSafe)
            {
                foreach (var property in including.ValidProperties)
                {
                    var propertyInfo = property.PropertyInfo;
                    if (property.IsCollection)
                    {
                        repository.DbContext.Entry(entity).Collection(propertyInfo.Name).Load();
                    }
                    else
                    {
                        repository.DbContext.Entry(entity).Reference(propertyInfo.Name).Load();
                    }
                }
            }
            return entity.Validate(id);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="entityKey"></param>
        /// <param name="including"></param>
        /// <param name="useBaseType"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async Task<TEntity> FindEntityAsync<TEntity>(this EfRepository<TEntity> repository, EntityKey entityKey, Including<TEntity> including, bool useBaseType, CancellationToken cancellationToken)
            where TEntity : BaseEntity
        {
            //TODO: include, not working yet
            var id = entityKey.Cast<TEntity>(repository, useBaseType);
            var entity = await repository.DbSet.FindAsync(cancellationToken, id);
            if (including != null)
            {
                //var navigationProperties = repository.GetNavigationProperties();
                foreach (var property in including.ValidProperties)
                {
                    //if (!navigationProperties.Contains(property.PropertyInfo)) continue;
                    var propertyInfo = property.PropertyInfo;
                    if (property.IsCollection)
                    {
                        await repository.DbContext.Entry(entity).Collection(propertyInfo.Name).LoadAsync(cancellationToken);
                    }
                    else
                    {
                        await repository.DbContext.Entry(entity).Reference(propertyInfo.Name).LoadAsync(cancellationToken);
                    }
                }
            }
            return entity.Validate(id);
        }
        #endregion

        #region methods: Helpers
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="keySet"></param>
        /// <param name="entityKeyName"></param>
        /// <param name="ordering"></param>
        /// <param name="paging"></param>
        /// <param name="including"></param>
        /// <returns></returns>
        private static IDataResult<TEntity> AsDataResult<TEntity>(this EfRepository<TEntity> repository, IKeySet<int> keySet, EntityKeyName entityKeyName, Ordering ordering, Paging paging, Including<TEntity> including)
            where TEntity : BaseEntity
        {
            var provider = new EfRepositoryProvider<TEntity>(repository);
            return provider.AsDataResult<TEntity>(keySet, entityKeyName, ordering, paging, including);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="filter"></param>
        /// <param name="ordering"></param>
        /// <param name="paging"></param>
        /// <param name="including"></param>
        /// <param name="performCount"></param>
        /// <returns></returns>
        private static IDataResult<TEntity> AsDataResult<TEntity>(this EfRepository<TEntity> repository, Expression<Func<TEntity, bool>> filter, Ordering ordering, Paging paging, Including<TEntity> including, bool performCount)
            where TEntity : BaseEntity
        {
            var provider = new EfRepositoryProvider<TEntity>(repository);
            return provider.AsDataResult<TEntity>(filter, ordering, paging, including, performCount);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="simpleFilter"></param>
        /// <param name="combinedFilter"></param>
        /// <param name="keySet"></param>
        /// <param name="entityKeyName"></param>
        /// <param name="keyword"></param>
        /// <param name="initialSearch"></param>
        /// <param name="ordering"></param>
        /// <param name="including"></param>
        /// <returns></returns>
        public static IDataResult<TEntity> AsDataResult<TEntity>(this EfRepository<TEntity> repository, Expression<Func<TEntity, bool>> simpleFilter, Expression<Func<TEntity, bool>> combinedFilter, IKeySet<int> keySet, EntityKeyName entityKeyName, string keyword, bool initialSearch, Ordering ordering, Including<TEntity> including)
            where TEntity : BaseEntity
        {
            var provider = new EfRepositoryProvider<TEntity>(repository);
            return provider.AsDataResult<TEntity>(simpleFilter, combinedFilter, keySet, entityKeyName, keyword, initialSearch, ordering, including);
        }
        #endregion
    }
}