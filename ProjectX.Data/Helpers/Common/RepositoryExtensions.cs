﻿using ProjectX.Data.Models;
using ProjectX.General;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

//TODO: check null??

namespace ProjectX.Data.Helpers
{
    internal static class RepositoryExtensions
    {
        #region Methods: Entity
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static TEntity Validate<TEntity>(this TEntity entity, object[] id)
            where TEntity : BaseEntity
        {
            return EntityHelper.Validate(entity, id);
        }
        #endregion

        #region Methods: CollectionPage
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="source"></param>
        /// <param name="paging"></param>
        /// <param name="totalItems"></param>
        /// <param name="keySet"></param>
        /// <returns></returns>
        public static ICollectionPage<TEntity> GetDataPage<TEntity>(this IEnumerable<TEntity> source, Paging paging, int totalItems, IKeySet<int> keySet)
            where TEntity : BaseEntity
        {
            return AsyncHelper.RunSync<ICollectionPage<TEntity>>
                (() => source.AsCollectionPage<TEntity>(paging, totalItems, keySet));
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="source"></param>
        /// <param name="paging"></param>
        /// <param name="totalItems"></param>
        /// <param name="keySet"></param>
        /// <returns></returns>
        public static async Task<ICollectionPage<TEntity>> GetDataPageAsync<TEntity>(this IEnumerable<TEntity> source, Paging paging, int totalItems, IKeySet<int> keySet, CancellationToken cancellationToken)
            where TEntity : BaseEntity
        {
            return await source.AsCollectionPage<TEntity>(paging, totalItems, keySet, cancellationToken);
        }
        #endregion

        #region DataResult
        public static IDataResult<TEntity> AsDataResult<TEntity>(this IRepositoryProvider<TEntity> provider, IKeySet<int> keySet, EntityKeyName entityKeyName, Ordering ordering, Paging paging, Including<TEntity> including)
            where TEntity : BaseEntity
        {
            return new DataResult<TEntity>(provider, keySet, entityKeyName, ordering, paging, including);
        }

        public static IDataResult<TEntity> AsDataResult<TEntity>(this IRepositoryProvider<TEntity> provider, Expression<Func<TEntity, bool>> filter, Ordering ordering, Paging paging, Including<TEntity> including, bool performCount)
            where TEntity : BaseEntity
        {
            return new DataResult<TEntity>(provider, filter, ordering, paging, including, performCount);
        }

        public static IDataResult<TEntity> AsDataResult<TEntity>(this IRepositoryProvider<TEntity> provider, Expression<Func<TEntity, bool>> simpleFilter, Expression<Func<TEntity, bool>> combinedFilter, IKeySet<int> keySet, EntityKeyName entityKeyName, string keyword, bool initialSearch, Ordering ordering, Including<TEntity> including)
            where TEntity : BaseEntity
        {
            return new DataResult<TEntity>(provider, simpleFilter, combinedFilter, keySet, entityKeyName, keyword, initialSearch, ordering, including);
        }
        #endregion
    }
}
