using CodeFirstStoredProcs;
using ProjectX.Data.StoredProcedures;
using ProjectX.Domain.Models;
using ProjectX.Domain.Models.Mapping;
using ProjectX.General;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

//TODO: Check Models of Entity Framework for meaningless names... Contact1, Contact2, ... (@General)

namespace ProjectX.Data.Models
{
    public class EfDbContext : DbContext, IContext
    {
        #region Constructors
        public EfDbContext()
            : base(new ProjectX.Data.DbConnection().ConnectionString)
        {
            //this.Configuration.LazyLoadingEnabled = false;
            this.ConnectionString = this.Database?.Connection?.ConnectionString;
            this.InitializeStoredProcs();
        }
        #endregion

        #region Properties: -> IContext
        public string ConnectionString { get; }
        #endregion

        #region Properties: -> IContext (IDbSets) - Entity FrameWork
        public virtual IDbSet<PartnerSearch> PartnerSearches { get; set; }
        public virtual IDbSet<ProductSearch> ProductSearches { get; set; }
        public virtual IDbSet<Currency> Currencies { get; set; }
        public virtual IDbSet<Employee> Employees { get; set; }
        public virtual IDbSet<City> Cities { get; set; }
        public virtual IDbSet<Country> Countries { get; set; }
        public virtual IDbSet<Language> Languages { get; set; }
        public virtual IDbSet<Lexicon> Lexicons { get; set; }
        public virtual IDbSet<Activity> Activities { get; set; }
        public virtual IDbSet<ActivityGroup> ActivityGroups { get; set; }
        public virtual IDbSet<Member> Members { get; set; }
        public virtual IDbSet<Menu> Menus { get; set; }
        public virtual IDbSet<MenuActivity> MenuActivities { get; set; }
        public virtual IDbSet<Module> Modules { get; set; }
        public virtual IDbSet<Address> Addresses { get; set; }
        public virtual IDbSet<Communication> Communications { get; set; }
        public virtual IDbSet<CommunicationType> CommunicationTypes { get; set; }
        public virtual IDbSet<Company> Companies { get; set; }
        public virtual IDbSet<CompanyCorporationType> CompanyCorporationTypes { get; set; }
        public virtual IDbSet<Contact> Contacts { get; set; }
        public virtual IDbSet<ContactRole> ContactRoles { get; set; }
        public virtual IDbSet<ContactRoleType> ContactRoleTypes { get; set; }
        public virtual IDbSet<Department> Departments { get; set; }
        public virtual IDbSet<Partner> Partners { get; set; }
        public virtual IDbSet<Person> People { get; set; }
        public virtual IDbSet<PersonGender> PersonGenders { get; set; }
        public virtual IDbSet<PersonJobTitle> PersonJobTitles { get; set; }
        public virtual IDbSet<PersonSalutation> PersonSalutations { get; set; }
        public virtual IDbSet<VatNumber> VatNumbers { get; set; }
        public virtual IDbSet<Icon> Icons { get; set; }
        public virtual IDbSet<CustomerGeneralData> CustomerGeneralDatas { get; set; }
        public virtual IDbSet<CustomerPartnerSearch> CustomerPartnerSearches { get; set; }
        public virtual IDbSet<CustomerKey> CustomerKeys { get; set; }
        public virtual IDbSet<PartnerGeneralData> PartnerGeneralDatas { get; set; }
        #endregion

        #region Properties: -> Stored Procedures
        //REMINDER: Check the returntype of the stored procedure when the model has changed...
        [StoredProcAttributes.Name(nameof(CustomerSearchByString))]
        [StoredProcAttributes.ReturnTypes(typeof(CustomerGeneralData))]
        public StoredProc<CustomerSearchByString> CustomerSearchByString { get; set; }
        #endregion

        #region Methods
        public override string ToString()
        {
            if (this != null)
            {
                return StringHelper.ArrowStyle(base.ToString(), this.ConnectionString).RemoveLineBreaks();
            }
            return String.Empty;
        }
        #endregion

        #region Methods: -> IContext
        public IEnumerable<System.Reflection.PropertyInfo> GetNavigationProperties<T>() where T : BaseEntity
        {
            var type = typeof(T);
            var elementType = ((IObjectContextAdapter)this).ObjectContext.CreateObjectSet<T>().EntitySet.ElementType;
            return elementType.NavigationProperties.Select(property => type.GetProperty(property.Name));
        }

        public void TryToConnect()
        {
            try
            {
                var connection = this.Database.Connection;
                if (connection.Database.IsEmptyString())
                {
                    connection = new EfDbContext().Database.Connection;
                }
                using (connection)
                {
                    connection.Open();
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task TryToConnectAsync(CancellationToken cancellationToken)
        {
            try
            {
                var connection = this.Database.Connection;
                if (connection.Database.IsEmptyString())
                {
                    connection = new EfDbContext().Database.Connection;
                }
                //https://stackoverflow.com/questions/57337189/sqlconnection-openasync-blocks-ui-when-sql-server-is-shutdown

                using (connection)
                {
                    //TODO: change timeout in dbsettings, now set at 5 seconds, default = 15
                    //var timeout = connection.ConnectionTimeout;
                    //connection.ConnectionString += "Asynchronous Processing=true;";
                    cancellationToken.ThrowIfCancellationRequested();
                    await Task.Run(async () => await connection.OpenAsync(cancellationToken));
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Methods: -> Entity FrameWork
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //TODO: http://alexwolfthoughts.com/creating-a-generic-repository-with-entity-framework-and-mvc/
            //TODO: http://ask.programmershare.com/2993_11979447/

            modelBuilder.Configurations.Add(new PartnerSearchMap());
            modelBuilder.Configurations.Add(new ProductSearchMap());
            modelBuilder.Configurations.Add(new CurrencyMap());
            modelBuilder.Configurations.Add(new EmployeeMap());
            modelBuilder.Configurations.Add(new CityMap());
            modelBuilder.Configurations.Add(new CountryMap());
            modelBuilder.Configurations.Add(new LanguageMap());
            modelBuilder.Configurations.Add(new LexiconMap());
            modelBuilder.Configurations.Add(new ActivityMap());
            modelBuilder.Configurations.Add(new ActivityGroupMap());
            modelBuilder.Configurations.Add(new MemberMap());
            modelBuilder.Configurations.Add(new MenuMap());
            modelBuilder.Configurations.Add(new MenuActivityMap());
            modelBuilder.Configurations.Add(new ModuleMap());
            modelBuilder.Configurations.Add(new AddressMap());
            modelBuilder.Configurations.Add(new CommunicationMap());
            modelBuilder.Configurations.Add(new CommunicationTypeMap());
            modelBuilder.Configurations.Add(new CompanyMap());
            modelBuilder.Configurations.Add(new CompanyCorporationTypeMap());
            modelBuilder.Configurations.Add(new ContactMap());
            modelBuilder.Configurations.Add(new ContactRoleMap());
            modelBuilder.Configurations.Add(new ContactRoleTypeMap());
            modelBuilder.Configurations.Add(new DepartmentMap());
            modelBuilder.Configurations.Add(new PartnerMap());
            modelBuilder.Configurations.Add(new PersonMap());
            modelBuilder.Configurations.Add(new PersonGenderMap());
            modelBuilder.Configurations.Add(new PersonJobTitleMap());
            modelBuilder.Configurations.Add(new PersonSalutationMap());
            modelBuilder.Configurations.Add(new VatNumberMap());
            modelBuilder.Configurations.Add(new IconMap());
            modelBuilder.Configurations.Add(new CustomerGeneralDataMap());
            modelBuilder.Configurations.Add(new CustomerPartnerSearchMap());
            modelBuilder.Configurations.Add(new CustomerKeyMap());
            modelBuilder.Configurations.Add(new PartnerGeneralDataMap());
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}