﻿using ProjectX.General;
using System;
using System.Collections.Generic;

#region Source
//SOURCE: https://michaelmairegger.wordpress.com/2013/03/30/find-primary-keys-from-entities-from-dbcontext/
#endregion

namespace ProjectX.Data.Models
{
    internal abstract class EntityKeyHelper
    {
        //TODO: concurrentdictionary?
        #region Properties
        private Dictionary<Type, string[]> Dictionary { get; } = new Dictionary<Type, string[]>();
        #endregion

        #region Methods: Abstract
        protected abstract string[] GetKeyNames(IContext context, Type type);
        #endregion

        #region Methods
        public EntityKeyName GetKeyNames<T>(IContext context, bool useBaseType) where T : BaseEntity
        {
            var type = typeof(T);
            //retreive the base type
            if (useBaseType)
            {
                while (type.BaseType != typeof(object))
                {
                    type = type.BaseType;
                }
            }
            this.Dictionary.TryGetValue(type, out var keys);
            if (keys != null)
            {
                return keys.AsEntityKeyName();
            }
            var keyNames = this.GetKeyNames(context, type);
            this.Dictionary.Add(type, keyNames);
            return keyNames.AsEntityKeyName();
        }

        public EntityKey GetKeys<T>(T entity, IContext context, bool useBaseType) where T : BaseEntity
        {
            if (entity != null)
            {
                var keyNames = this.GetKeyNames<T>(context, useBaseType).Members;
                var type = typeof(T);
                var keys = new object[keyNames.Length];
                for (int i = 0; i < keyNames.Length; i++)
                {
                    keys[i] = type.GetProperty(keyNames[i]).GetValue(entity, null);
                }
                return keys.AsEntityKey();
            }
            return null;
        }

        public EntityKeyType GetTypes<T>(IContext context, bool useBaseType) where T : BaseEntity
        {
            var keyNames = this.GetKeyNames<T>(context, useBaseType).Members;
            var type = typeof(T);
            var types = new Type[keyNames.Length];
            for (int i = 0; i < keyNames.Length; i++)
            {
                types[i] = type.GetProperty(keyNames[i]).PropertyType;
            }
            return types.AsEntityKeyType();
        }

        /// <summary>
        /// Convert to the correct value type if the key is provided as String, to avoid an InvalidCastException.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entityKey"></param>
        /// <param name="context"></param>
        /// <param name="useBaseType"></param>
        /// <returns></returns>
        public object[] Cast<T>(EntityKey entityKey, IContext context, bool useBaseType)
            where T : BaseEntity
        {
            if (entityKey != null)
            {
                var keys = entityKey.Keys;
                if (keys != null && keys.Length > XLib.MagicNumber.IntZero)
                {
                    var length = keys.Length;
                    var types = this.GetTypes<T>(context, useBaseType).Types;
                    for (int i = 0; i < length; i++)
                    {
                        if (keys[i] is String)
                        {
                            var type = types[i];
                            //TODO: Implement for all known id types...
                            if (type == typeof(Int32))
                            {
                                keys[i] = Convert.ToInt32(keys[i]);
                            }
                            else if (type == typeof(Guid))
                            {
                                keys[i] = (Guid)keys[i];
                            }
                        }
                    }
                }
                return keys;
            }
            return null;
        }
        #endregion
    }
}
