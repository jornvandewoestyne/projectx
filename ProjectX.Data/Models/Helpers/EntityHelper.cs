﻿using ProjectX.General;

namespace ProjectX.Data.Models
{
    internal sealed class EntityHelper
    {
        #region Methods
        /// <summary>
        /// Validates a given entity.
        /// <para>-> throws a NullEntityException when null.</para>
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static TEntity Validate<TEntity>(TEntity entity, object[] id) where TEntity : BaseEntity
        {
            if (entity == null)
            {
                var type = typeof(TEntity);
                var parameters = id.TryArrayToString<object>();
                throw new NullEntityException(
                    StringHelper.Concat(type, AppLibrary.ExceptionMessages.NullEntityException, parameters), new System.NullReferenceException());
            }
            return entity;
        }
        #endregion
    }
}
