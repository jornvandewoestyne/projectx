﻿using ProjectX.Domain.Models;
using System;
using System.Collections.Generic;

namespace ProjectX.Data
{
    [Obsolete]
    public interface IStoredProcedure
    {
        /// <summary>
        /// Returns all customers general data by search string (read-only).
        /// </summary>
        /// <returns>List containing the resulting repository set.</returns>
        IEnumerable<CustomerGeneralData> GetAllCustomerGeneralDataByString(string input);
    }
}
