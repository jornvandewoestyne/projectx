﻿using ProjectX.Domain.Models;
using ProjectX.General;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.Data
{
    public interface IRepository<TEntity> where TEntity : BaseEntity //: IDisposable where TEntity : BaseEntity
    {
        #region Methods: NavigationProperties
        /// <summary>
        /// ...
        /// </summary>
        /// <returns></returns>
        IEnumerable<System.Reflection.PropertyInfo> GetNavigationProperties();
        #endregion

        #region Methods: EntityKeys
        /// <summary>
        /// Returns the primary key(s) of a given entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        /// 
        EntityKey GetKey(TEntity entity);

        /// <summary>
        /// Returns the names of the primary key(s) of a given entitytype.
        /// </summary>
        /// <returns></returns>
        EntityKeyName GetKeyName();

        /// <summary>
        /// Returns the type(s) of the primary key(s) of a given entitytype.
        /// </summary>
        EntityKeyType GetKeyType();
        #endregion

        #region Methods: CRUD
        /// <summary>
        /// Returns all entities.
        /// </summary>
        /// <param name="ordering"></param>
        /// <param name="including"></param>
        /// <returns></returns>
        IEnumerable<TEntity> GetData(Ordering ordering = null, Including<TEntity> including = null);

        /// <summary>
        /// Returns all entities asynchronously.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <param name="ordering"></param>
        /// <param name="including"></param>
        /// <returns></returns>
        Task<IEnumerable<TEntity>> GetDataAsync(CancellationToken cancellationToken, Ordering ordering = null, Including<TEntity> including = null);

        /// <summary>
        /// Returns an IEnumerable based on the query, order clause and including.
        /// </summary>
        /// <param name="filter">Link query for filtering.</param>
        /// <param name="ordering"></param>
        /// <param name="including"></param>
        /// <returns>IEnumerable containing the resulting entity set.</returns>
        IEnumerable<TEntity> GetData(Expression<Func<TEntity, bool>> filter, Ordering ordering, Including<TEntity> including);

        /// <summary>
        /// Returns an IEnumerable based on the query, order clause and paging, asynchronously.
        /// </summary>
        /// <param name="filter">Link query for filtering.</param>
        /// <param name="ordering">...</param>
        /// <returns>IEnumerable containing the resulting entity set.</returns>
        Task<IEnumerable<TEntity>> GetDataAsync(Expression<Func<TEntity, bool>> filter, Ordering ordering, Including<TEntity> including, CancellationToken cancellationToken);

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TSearchEntity"></typeparam>
        /// <param name="simpleFilter"></param>
        /// <param name="combinedFilter"></param>
        /// <param name="keySet"></param>
        /// <param name="entityKeyName"></param>
        /// <param name="keyword"></param>
        /// <param name="initialSearch"></param>
        /// <param name="ordering"></param>
        /// <returns></returns>
        IKeySet<int> GetData<TSearchEntity>(Expression<Func<TEntity, bool>> simpleFilter, Expression<Func<TEntity, bool>> combinedFilter, IKeySet<int> keySet, EntityKeyName entityKeyName, string keyword, bool initialSearch, Ordering ordering) where TSearchEntity : BaseSearchEntity;

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TSearchEntity"></typeparam>
        /// <param name="simpleFilter"></param>
        /// <param name="combinedFilter"></param>
        /// <param name="keySet"></param>
        /// <param name="entityKeyName"></param>
        /// <param name="keyword"></param>
        /// <param name="initialSearch"></param>
        /// <param name="ordering"></param>
        /// <returns></returns>
        Task<IKeySet<int>> GetDataAsync<TSearchEntity>(Expression<Func<TEntity, bool>> simpleFilter, Expression<Func<TEntity, bool>> combinedFilter, IKeySet<int> keySet, EntityKeyName entityKeyName, string keyword, bool initialSearch, Ordering ordering, CancellationToken cancellationToken) where TSearchEntity : BaseSearchEntity;

        /// <summary>
        /// Returns all entities.
        /// </summary>
        /// ...
        /// <returns></returns>
        ICollectionPage<TEntity> GetDataPage(Ordering ordering, Paging paging);

        /// <summary>
        /// Returns all entities.
        /// </summary>
        /// ...
        /// <returns></returns>
        Task<ICollectionPage<TEntity>> GetDataPageAsync(Ordering ordering, Paging paging, CancellationToken cancellationToken);

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="keySet"></param>
        /// <param name="entityKeyName"></param>
        /// <param name="ordering"></param>
        /// <param name="paging"></param>
        /// <returns></returns>
        ICollectionPage<TEntity> GetDataPage(IKeySet<int> keySet, EntityKeyName entityKeyName, Ordering ordering, Paging paging);

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="keySet"></param>
        /// <param name="entityKeyName"></param>
        /// <param name="ordering"></param>
        /// <param name="paging"></param>
        /// <returns></returns>
        Task<ICollectionPage<TEntity>> GetDataPageAsync(IKeySet<int> keySet, EntityKeyName entityKeyName, Ordering ordering, Paging paging, CancellationToken cancellationToken);

        /// <summary>
        /// Returns an entity based on primary key(s).
        /// </summary>
        /// <param name="id">Primary Key.</param>
        /// <returns></returns>
        TEntity GetEntity(EntityKey entityKey, Including<TEntity> including);

        /// <summary>
        /// Returns an entity based on primary key(s) asynchronously.
        /// </summary>
        /// <param name="id">Primary Key.</param>
        /// <returns></returns>
        Task<TEntity> GetEntityAsync(EntityKey entityKey, Including<TEntity> including, CancellationToken cancellationToken);

        /// <summary>
        /// Returns the first matching entity based on the query.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        TEntity GetFirst(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Inserts a single entity into the DbSet
        /// </summary>
        /// <param name="entity"></param>
        void Insert(TEntity entity);

        /// <summary>
        /// Inserts a single entity into the DbSet
        /// </summary>
        /// <param name="entity"></param>
        TEntity InsertAndSave(TEntity entity);

        /// <summary>
        /// Updates an entity.
        /// </summary>
        /// <param name="entity"></param>
        void Update(TEntity entity);

        /// <summary>
        /// Updates an entity by primary key.
        /// </summary>
        /// <param name="id">Primary Key.</param>
        void UpdateById(EntityKey entityKey);

        /// <summary>
        /// Deletes an entity.
        /// </summary>
        /// <param name="entity"></param>
        void Delete(TEntity entity);

        /// <summary>
        /// Deletes an entity by primary key.
        /// </summary>
        /// <param name="id">Primary Key.</param>
        void DeleteByID(EntityKey entityKey);
        #endregion
    }
}
