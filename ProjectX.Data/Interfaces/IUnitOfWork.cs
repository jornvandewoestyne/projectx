﻿using ProjectX.General;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.Data
{
    public interface IUnitOfWork : IDisposable
    {
        #region Methods
        /// <summary>
        /// Returns the requested repository.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity;

        /// <summary>
        /// Tests the current connection.
        /// </summary>
        void TryToConnect();

        /// <summary>
        /// Tests the current connection asynchronously.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task TryToConnectAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Used to save the changes to the underlying data store.
        /// </summary>
        void Save();
        #endregion
    }
}
