﻿using ProjectX.Domain.Models;
using ProjectX.General;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.Data
{
    internal interface IDataResult<TEntity> where TEntity : BaseEntity
    {
        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> GetData();

        /// <summary>
        /// ...
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<TEntity>> GetDataAsync(CancellationToken cancellationToken);

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TSearchEntity"></typeparam>
        /// <returns></returns>
        IKeySet<int> GetKeySet<TSearchEntity>() where TSearchEntity : BaseSearchEntity;

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TSearchEntity"></typeparam>
        /// <returns></returns>
        Task<IKeySet<int>> GetKeySetAsync<TSearchEntity>(CancellationToken cancellationToken) where TSearchEntity : BaseSearchEntity;
        #endregion
    }
}
