﻿using ProjectX.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.Data
{
    internal interface IRepositoryProvider<TEntity> where TEntity : BaseEntity
    {
        #region Properties
        IQueryable<TEntity> Query { get; set; }
        int Count { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// .Join is used to ensure optimal performance with (too) many results.
        /// </summary>
        /// <param name="keys"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        IQueryable<TEntity> Join(HashSet<int> keys, string propertyName);

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="ordering"></param>
        /// <param name="paging"></param>
        /// <returns></returns>
        IQueryable<TEntity> Take(Ordering ordering, Paging paging);

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> filter);

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="including"></param>
        /// <returns></returns>
        IQueryable<TEntity> Include(Including<TEntity> including);

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        Task<int> CountAsync(IQueryable<TEntity> source, CancellationToken cancellationToken);

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        Task<IEnumerable<TEntity>> ToListAsyncSafe(IQueryable<TEntity> source, CancellationToken cancellationToken);
        #endregion
    }
}
