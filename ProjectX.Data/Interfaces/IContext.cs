﻿//TODO: coupled to Entity Framework!
using CodeFirstStoredProcs;
using ProjectX.Data.StoredProcedures;
using ProjectX.Domain.Models;
using ProjectX.General;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;

#region Source
//http://techbrij.com/service-layer-entity-framework-asp-net-mvc-unit-testing
#endregion

namespace ProjectX.Data
{
    public interface IContext : IDisposable
    {
        #region Properties
        string ConnectionString { get; }
        #endregion

        //REMINDER: Add IDbSets accordingly with existing entities when the model has changed...
        #region Properties: -> IDbSet (Auto-generated Reverse Engineer Code First)
        IDbSet<PartnerSearch> PartnerSearches { get; set; }
        IDbSet<ProductSearch> ProductSearches { get; set; }
        IDbSet<Currency> Currencies { get; set; }
        IDbSet<Employee> Employees { get; set; }
        IDbSet<City> Cities { get; set; }
        IDbSet<Country> Countries { get; set; }
        IDbSet<Language> Languages { get; set; }
        IDbSet<Lexicon> Lexicons { get; set; }
        IDbSet<Activity> Activities { get; set; }
        IDbSet<ActivityGroup> ActivityGroups { get; set; }
        IDbSet<Member> Members { get; set; }
        IDbSet<Menu> Menus { get; set; }
        IDbSet<MenuActivity> MenuActivities { get; set; }
        IDbSet<Module> Modules { get; set; }
        IDbSet<Address> Addresses { get; set; }
        IDbSet<Communication> Communications { get; set; }
        IDbSet<CommunicationType> CommunicationTypes { get; set; }
        IDbSet<Company> Companies { get; set; }
        IDbSet<CompanyCorporationType> CompanyCorporationTypes { get; set; }
        IDbSet<Contact> Contacts { get; set; }
        IDbSet<ContactRole> ContactRoles { get; set; }
        IDbSet<ContactRoleType> ContactRoleTypes { get; set; }
        IDbSet<Department> Departments { get; set; }
        IDbSet<Partner> Partners { get; set; }
        IDbSet<Person> People { get; set; }
        IDbSet<PersonGender> PersonGenders { get; set; }
        IDbSet<PersonJobTitle> PersonJobTitles { get; set; }
        IDbSet<PersonSalutation> PersonSalutations { get; set; }
        IDbSet<VatNumber> VatNumbers { get; set; }
        IDbSet<Icon> Icons { get; set; }
        IDbSet<CustomerGeneralData> CustomerGeneralDatas { get; set; }
        IDbSet<CustomerPartnerSearch> CustomerPartnerSearches { get; set; }
        IDbSet<CustomerKey> CustomerKeys { get; set; }
        IDbSet<PartnerGeneralData> PartnerGeneralDatas { get; set; }
        #endregion

        //REMINDER: Add Stored Procedures accordingly with existing entities when the model has changed...
        #region Properties: -> Stored Procedures (manual)
        /// <summary>
        /// (Stored Procedure) -> Returns CustomerGeneralData
        /// </summary>
        StoredProc<CustomerSearchByString> CustomerSearchByString { get; set; }
        #endregion

        #region Methods
        //TODO: Remove due to leaky implementation!!!!

        /// <summary>
        /// Sets the generic DbSet.
        /// </summary>
        //DbSet<TEntity> Set<TEntity>() where TEntity : class;

        ///// <summary>
        ///// Sets the generic DbEntityEntry.
        ///// </summary>
        //DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Saves changes to the IContext.
        /// </summary>
        int SaveChanges();

        ///// <summary>
        ///// Disposes the IContext.
        ///// </summary>
        //void Dispose();

        /// <summary>
        /// Tests the current IContext connection.
        /// </summary>
        void TryToConnect();

        /// <summary>
        /// Tests the current IContext connection asynchronously.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task TryToConnectAsync(CancellationToken cancellationToken);

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IEnumerable<System.Reflection.PropertyInfo> GetNavigationProperties<T>() where T : BaseEntity;
        #endregion
    }
}
