﻿using CodeFirstStoredProcs;

namespace ProjectX.Data.StoredProcedures
{
    /// <summary>
    /// (Stored Procedure) -> Parameters object
    /// </summary>
    public partial class CustomerSearchByString
    {
        [StoredProcAttributes.Name(nameof(SearchString))]
        [StoredProcAttributes.ParameterType(System.Data.SqlDbType.VarChar)]
        public string SearchString { get; set; }
    }
}
