﻿using CodeFirstStoredProcs;
using ProjectX.Domain.Models;
using System.Collections.Generic;

#region Source
//SOURCE: http://www.codeproject.com/Articles/179481/Code-First-Stored-Procedures
//CHECK: https://msdn.microsoft.com/en-us/data/dn468673.aspx
#endregion

namespace ProjectX.Data.StoredProcedures
{
    public class EfStoredProcedure : IStoredProcedure
    {
        #region Properties
        protected IContext DbContext { get; }

        //TODO: BaseStoredProcedure
        protected StoredProc<CustomerSearchByString> CustomerSearchByString => this.DbContext.CustomerSearchByString;
        #endregion

        #region Constructors
        public EfStoredProcedure(IContext context)
        {
            this.DbContext = context;
        }
        #endregion

        #region Methods: -> IStoredProcedure
        public IEnumerable<CustomerGeneralData> GetAllCustomerGeneralDataByString(string input)
        {
            // create input parameters object 
            var parameters = new CustomerSearchByString() { SearchString = input };
            // call the stored proc 
            var result = this.CustomerSearchByString.CallStoredProc(parameters);
            // get our results into a usable object 
            return result.ToList<CustomerGeneralData>();
        }
        #endregion
    }
}
