﻿using ProjectX.General;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public static class XWeakEventHandler
    {
        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="action"></param>
        /// <param name="callback"></param>
        /// <param name="targets"></param>
        public static void Subscribe(this IXLeakable parent, Event action, EventHandler<EventArgs> callback, params IXLeakable[] targets)
        {
            foreach (var target in targets.Where(target => target != null))
            {
                var success = true;
                var handler = new WeakEventHandler<EventArgs>(callback);
                if (parent.EventCollection.TryAdd(handler, target))
                {
                    switch (action)
                    {
                        case Event.Click:
                            target.Click += handler.Event;
                            break;
                        case Event.DoubleClick:
                            if (target is IXDataPageViewer viewer)
                            {
                                viewer.DataViewDoubleClick += handler.Event;
                                break;
                            }
                            target.DoubleClick += handler.Event;
                            break;
                        case Event.Load:
                            if (target is Form form)
                            {
                                form.Load += handler.Event;
                                break;
                            }
                            success = false;
                            break;
                        case Event.MouseEnter:
                            target.MouseEnter += handler.Event;
                            break;
                        case Event.MouseHover:
                            target.MouseHover += handler.Event;
                            break;
                        case Event.MouseLeave:
                            target.MouseLeave += handler.Event;
                            break;
                        case Event.SelectionChangeCommitted:
                            if (target is ComboBox comboBox)
                            {
                                comboBox.SelectionChangeCommitted += handler.Event;
                                break;
                            }
                            success = false;
                            break;
                        default:
                            success = false;
                            break;
                    }
                    target.OnFailed(parent, handler, success);
                }
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="action"></param>
        /// <param name="callback"></param>
        /// <param name="targets"></param>
        public static void Subscribe(this IXLeakable parent, CancelEvent action, EventHandler<CancelEventArgs> callback, params IXLeakable[] targets)
        {
            foreach (var target in targets.Where(target => target != null))
            {
                var success = true;
                var handler = new WeakEventHandler<CancelEventArgs>(callback);
                if (parent.EventCollection.TryAdd(handler, target))
                {
                    switch (action)
                    {
                        case CancelEvent.Closing:
                            if (target is Form control)
                            {
                                control.Closing += handler.Event;
                                break;
                            }
                            success = false;
                            break;
                        default:
                            success = false;
                            break;
                    }
                    target.OnFailed(parent, handler, success);
                }
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="action"></param>
        /// <param name="callback"></param>
        /// <param name="targets"></param>
        public static void Subscribe(this IXLeakable parent, MouseEvent action, EventHandler<MouseEventArgs> callback, params IXLeakable[] targets)
        {
            foreach (var target in targets.Where(target => target != null))
            {
                var success = true;
                var handler = new WeakEventHandler<MouseEventArgs>(callback);
                if (parent.EventCollection.TryAdd(handler, target))
                {
                    switch (action)
                    {
                        case MouseEvent.MouseDoubleClick:
                            if (target is Control control)
                            {
                                control.MouseDoubleClick += handler.Event;
                                break;
                            }
                            success = false;
                            break;
                        case MouseEvent.MouseDown:
                            target.MouseDown += handler.Event;
                            break;
                        case MouseEvent.MouseMove:
                            target.MouseMove += handler.Event;
                            break;
                        case MouseEvent.MouseUp:
                            target.MouseUp += handler.Event;
                            break;
                        default:
                            success = false;
                            break;
                    }
                    target.OnFailed(parent, handler, success);
                }
            }
        }

        public static void Subscribe(this IXLeakable parent, ListViewEvent action, EventHandler<ListViewItemSelectionChangedEventArgs> callback, params IXLeakable[] targets)
        {
            foreach (var target in targets.Where(target => target != null))
            {
                var success = true;
                var handler = new WeakEventHandler<ListViewItemSelectionChangedEventArgs>(callback);
                if (parent.EventCollection.TryAdd(handler, target))
                {
                    switch (action)
                    {
                        case ListViewEvent.ItemSelectionChanged:
                            if (target is ListView control)
                            {
                                control.ItemSelectionChanged += handler.Event;
                                break;
                            }
                            success = false;
                            break;
                        default:
                            success = false;
                            break;
                    }
                    target.OnFailed(parent, handler, success);
                }
            }
        }
        #endregion

        #region Methods: Helpers
        private static bool OnFailed<TEventArgs>(this IXLeakable target, IXLeakable parent, WeakEventHandler<TEventArgs> handler, bool success)
            where TEventArgs : EventArgs
        {
            if (!success)
            {
                return parent.EventCollection.TryRemove(handler, target);
            }
            return success;
        }
        #endregion
    }
}