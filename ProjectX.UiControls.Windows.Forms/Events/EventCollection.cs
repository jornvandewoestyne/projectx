﻿using ProjectX.General;
using ProjectX.Infrastructure.LogAttributes;
using System;
using System.Collections.Generic;

namespace ProjectX.UiControls.Windows.Forms
{
    public sealed class EventCollection : DisposableItem
    {
        #region Fields
        private HashSet<string> Items { get; set; } = new HashSet<string>();
        #endregion

        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEventArgs"></typeparam>
        /// <param name="eventHandler"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public bool TryAdd<TEventArgs>(WeakEventHandler<TEventArgs> eventHandler, IXLeakable target)
            where TEventArgs : EventArgs
        {
            var item = this.GetItem(eventHandler, target);
            return this.Items.Add(item);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEventArgs"></typeparam>
        /// <param name="eventHandler"></param>
        /// <param name="target"></param>
        /// <param name="throwException"></param>
        /// <returns></returns>
        [Log]
        public bool TryRemove<TEventArgs>(WeakEventHandler<TEventArgs> eventHandler, IXLeakable target, bool throwException = true)
            where TEventArgs : EventArgs
        {
            var result = false;
            var item = this.GetItem(eventHandler, target);
            if (this.Items.Contains(item))
            {
                result = this.Items.Remove(item);
            }
            if (throwException)
            {
                throw new NotImplementedException(item);
            }
            return result;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEventArgs"></typeparam>
        /// <param name="eventHandler"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public bool Contains<TEventArgs>(WeakEventHandler<TEventArgs> eventHandler, IXLeakable target)
            where TEventArgs : EventArgs
        {
            var item = this.GetItem(eventHandler, target);
            return this.Items.Contains(item);
        }
        #endregion

        #region Methods: Helpers
        private string GetItem<TEventArgs>(WeakEventHandler<TEventArgs> eventHandler, IXLeakable control)
            where TEventArgs : EventArgs
        {
            return eventHandler.GetQualifiedName(control?.Name);
        }
        #endregion

        #region DisposableItem
        protected override void Finish()
        {
            //No actions needed
        }

        protected override void Unsubscribe()
        {
            //No actions needed
        }

        protected override void Unbind()
        {
            this.Items.Clear();
            this.Items = null;
        }
        #endregion
    }
}
