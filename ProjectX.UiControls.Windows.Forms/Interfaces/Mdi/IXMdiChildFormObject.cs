﻿using ProjectX.Business;
using ProjectX.General;
using System.Drawing;

//TODO: summary

namespace ProjectX.UiControls.Windows.Forms
{
    interface IXMdiChildFormObject
    {
        #region Properties
        XMdiChildDetail MdiChildDetail { get; }
        IService Service { get; }
        EntityKey EntityKey { get; }
        string EntityFullName { get; }
        Bitmap Picture { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="title"></param>
        /// <param name="confirmToClose"></param>
        /// <param name="showHeader"></param>
        void SetUserControlValues(string title, XMdiChildDetail detail);

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="title"></param>
        /// <param name="confirmToClose"></param>
        /// <param name="showHeader"></param>
        void SetUserControlValues(string title, bool confirmToClose, bool showHeader);
        #endregion
    }
}
