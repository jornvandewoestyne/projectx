﻿using ProjectX.Domain.Models;
using ProjectX.DTO;
using ProjectX.General;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXEntityPresenter<TEntity> : IXEntityPresenter
        where TEntity : BaseCommonEntity
    {
        #region Properties
        TEntity Entity { get; }
        #endregion

        #region Methods
        EntitySummaryDTO GetEntitySummary(IXSearchContainer container, ICommonEntity entity, XProgressInformer informer);
        Task<EntitySummaryDTO> GetEntitySummaryAsync(IXSearchContainer container, ICommonEntity entity, XProgressInformer informer, CancellationToken cancellationToken);

        TEntity GetEntity(EntityKey entityKey);
        Task<TEntity> GetEntityAsync(EntityKey entityKey, CancellationToken cancellationToken);
        #endregion
    }
}
