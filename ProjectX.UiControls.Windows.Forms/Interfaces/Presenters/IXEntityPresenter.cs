﻿using ProjectX.General;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXEntityPresenter
    {
        #region Properties
        Type DataType { get; }
        EntityKey EntityKey { get; }
        EntityKeyName EntityKeyName { get; }
        #endregion

        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="entityKey"></param>
        /// <returns></returns>
        /// <remarks>PostSharp LogAttribute is required -> Forces to reload data, when Entity is not found!</remarks>
        Task<bool> SetEntityAsync(EntityKey entityKey, XProgressInformer informer, CancellationToken cancellationToken);

        Ordering GetOrdering(params OrderingSetting[] settings);
        #endregion
    }
}
