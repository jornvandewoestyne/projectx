﻿using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXCollectionPresenter : IXEntityPresenter
    {
        /// <summary>
        /// ...
        /// </summary>
        /// <returns></returns>
        Task<bool> GetDataAsync(IXSearchContainer container, XProgressInformer informer, CancellationToken cancellationToken, bool initialLoad);
    }
}
