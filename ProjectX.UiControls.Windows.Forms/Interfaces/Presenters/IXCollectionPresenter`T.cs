﻿using ProjectX.Domain.Models;
using ProjectX.General;
using System.Threading;
using System.Threading.Tasks;

//TODO: summary

//TODO: rename to ISearchPresenterGeneric

namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXCollectionPresenter<TEntity, TData> : IXEntityPresenter<TEntity>
        where TEntity : BaseCommonEntity
        where TData : BaseEntity
    {
        #region Properties
        ICollectionPage<TData> Data { get; set; }
        #endregion

        #region Methods
        Task<ICollectionPage<TData>> GetDataPageAsync(XProgressInformer informer, SearchString searchString, Ordering ordering, Paging paging, CancellationToken cancellationToken);
        #endregion
    }
}
