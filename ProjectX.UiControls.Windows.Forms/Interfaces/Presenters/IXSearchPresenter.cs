﻿using ProjectX.General;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXSearchPresenter : IXCollectionPresenter, IDisposable
    {
        //TODO: make IXSearchView interface -> searchstring, paging, ordering, mdidetail,...?
        //IXSearchContainer Container { get; set; }

        #region Methods
        Task<bool> PopulateDataAsync(IXSearchContainer container, CancellationToken cancellationToken);
        EntityKey GetSelectedKey(IXSearchContainer container, bool showMessage);
        Task<bool> ShowEntityAsync(IXSearchContainer container, XProgressInformer informer, CancellationToken cancellationToken);
        #endregion
    }
}
