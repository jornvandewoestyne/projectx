﻿using ProjectX.General;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

//TODO: events -> search, edit, cancel,...

namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXSearchContainer : IDisposable
    {
        #region Properties
        /// <summary>
        /// ...
        /// </summary>
        Paging Paging { get; set; }

        /// <summary>
        /// ...
        /// </summary>
        Ordering Ordering { get; set; }

        /// <summary>
        /// Object which holds the search string.
        /// </summary>
        SearchString SearchString { get; set; }

        /// <summary>
        /// ...
        /// </summary>
        XMdiChildDetail MdiChildDetail { get; }

        /// <summary>
        /// ...
        /// </summary>
        IXSearchPresenter Presenter { get; }
        #endregion

        #region Properties: Controls
        /// <summary>
        /// Control which holds the search string.
        /// </summary>
        TextBox TextBox { get; }

        /// <summary>
        /// ...
        /// </summary>
        IXDataPageViewer DataViewer { get; }
        #endregion

        #region Events
        //check! https://www.c-sharpcorner.com/article/C-Sharp-defensive-event-publishing-using-interfaces/
        //event EventHandler ShowEntity;
        #endregion

        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        /// <returns></returns>
        Task<bool> SearchAsync(bool initialLoad);

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="showNoSelection"></param>
        /// <returns></returns>
        Task<bool> ShowEntityAsync(bool showNoSelection, bool showIsOpen);

        /// <summary>
        /// ...
        /// </summary>
        void Cancel();

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        //XMdiChildDetail GetMdiChildDetail(string title);
        #endregion
    }
}
