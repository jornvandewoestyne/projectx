﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXConditionStrategy
    {
        /// <summary>
        /// Evaluate the strategy.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        bool GetResult(Component control);

        /// <summary>
        /// Validate the strategy.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="lockable"></param>
        /// <param name="strategy"></param>
        /// <returns></returns>
        bool Validate(Component control, IXLockable lockable);

        /// <summary>
        /// Validate the strategy.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="freezable"></param>
        /// <param name="strategy"></param>
        /// <returns></returns>
        bool Validate(Component control, IXFreezable freezable);

        /// <summary>
        /// Validate the strategy.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        bool Validate(Component control, Expression<Func<Control, bool>> propertySelector);
    }
}
