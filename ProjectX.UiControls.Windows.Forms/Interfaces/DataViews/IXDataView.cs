﻿using System;
using System.Drawing;

namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXDataView : IXDataViewer, IXLeakable, IDisposable
    {
        bool Visible { get; set; }

        Color DisabledBackColor { get; }

        /// <summary>
        /// Remove all of the given objects from the control.
        /// </summary>
        /// <param name="modelObjects">Collection of objects to be removed</param>
        /// <remarks>
        /// <para>Nulls and model objects that are not in the ListView are silently ignored.</para>
        /// <para>This method is thread-safe.</para>
        /// </remarks>
        void Reset();
        //void Refresh();
    }
}