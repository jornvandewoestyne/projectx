﻿using ProjectX.General;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXDataViewer : IDisposable
    {
        #region Methods
        bool Populate<TEntity>(ICollectionPage<TEntity> collection, EntityKeyName entityKeyName)
            where TEntity : BaseEntity;

        Task<bool> PopulateAsync<TEntity>(ICollectionPage<TEntity> collection, EntityKeyName entityKeyName, CancellationToken cancellationToken)
            where TEntity : BaseEntity;

        EntityKey GetSelectedKeyValue(EntityKeyName entityKeyName, bool showMessage = true);
        #endregion
    }
}
