﻿using System;
using System.ComponentModel;

namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXDataPageViewer : IXDataViewer, IDisposable
    {
        #region Properties
        [Browsable(false)]
        IXDataView DataView { get; }
        #endregion

        #region Events
        [Category("Action"), Description("Occurs when the DataView is double-clicked.")]
        event EventHandler DataViewDoubleClick;
        #endregion
    }
}
