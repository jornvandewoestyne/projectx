﻿namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXFreezableUnSafe : IXFreezable
    {
        bool Enabled { get; set; }
    }
}
