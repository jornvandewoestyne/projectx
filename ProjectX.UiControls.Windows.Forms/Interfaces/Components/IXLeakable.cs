﻿using System;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXLeakable : IDisposable
    {
        #region Properties
        /// <summary>
        /// ...
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// ...
        /// </summary>
        EventCollection EventCollection { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        void Subscribe();
        #endregion

        #region Events
        event EventHandler Click;
        event EventHandler DoubleClick;
        event EventHandler MouseEnter;
        event EventHandler MouseHover;
        event EventHandler MouseLeave;
        event MouseEventHandler MouseDown;
        event MouseEventHandler MouseMove;
        event MouseEventHandler MouseUp;
        #endregion
    }
}
