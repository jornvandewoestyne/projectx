﻿using ProjectX.General;

namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXLockable
    {
        Work Work { get; set; }
        bool Enabled { get; set; }

        /// <summary>
        /// Validate the given control.
        /// </summary>
        /// <param name="strategy"></param>
        void Validate(IXLockable lockable, IXConditionStrategy strategy);
    }
}
