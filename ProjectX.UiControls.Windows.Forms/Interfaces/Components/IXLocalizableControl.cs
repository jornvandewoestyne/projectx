﻿using ProjectX.General;
using System;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXLocalizableControl : IXLeakable, IDisposable
    {
        #region Properties
        /// <summary>
        /// ...
        /// </summary>
        string Text { get; set; }

        /// <summary>
        /// ...
        /// </summary>
        ToolTip ToolTip { get; set; }

        /// <summary>
        /// ...
        /// </summary>
        FriendlyTextType XControlText { get; set; }

        /// <summary>
        /// ...
        /// </summary>
        FriendlyToolTipType XToolTipText { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        void Localize();
        #endregion
    }
}
