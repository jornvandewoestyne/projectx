﻿
namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXFreezable
    {
        bool BlockInput { get; set; }
        bool CanBlockInput { get; set; }

        /// <summary>
        /// Validate the given control.
        /// </summary>
        /// <param name="strategy"></param>
        void Validate(IXFreezable freezable, IXConditionStrategy strategy);
    }
}

