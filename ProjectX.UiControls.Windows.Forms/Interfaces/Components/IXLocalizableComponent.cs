﻿namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXLocalizableComponent : IXLocalizableControl
    {
        #region Properties
        /// <summary>
        /// Gets or sets the text that appears as a System.Windows.Forms.ToolTip for a control.
        /// </summary>
        string ToolTipText { get; set; }
        #endregion
    }
}
