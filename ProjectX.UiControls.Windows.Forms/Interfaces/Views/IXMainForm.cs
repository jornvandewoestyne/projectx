﻿using ProjectX.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ProjectX.UiControls.Windows.Forms
{
    public interface IXMainForm : IXLeakable, ISynchronizeInvoke
    {
        #region Properties
        List<XMenuUserControl> MenuControls { get; }
        #endregion

        #region Methods
        void Show(ApplicationMenuDTO applicationMenu, bool maximize = false);
        void CloseChildForm();
        void Hide();
        void Quit();
        #endregion

        #region Events
        event EventHandler Closed;
        event CancelEventHandler Closing;
        #endregion
    }
}
