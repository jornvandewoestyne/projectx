﻿namespace ProjectX.UiControls.Windows.Forms
{
    interface IXDataSourceMembers<TKey, TValue>
    {
        #region Properties
        TKey Key { get; }
        TValue Value { get; }
        #endregion  
    }
}
