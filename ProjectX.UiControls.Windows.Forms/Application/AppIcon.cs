﻿using System.Drawing;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public static class AppIcon
    {
        public static Icon SetIcon(this Form form, Icon icon = null)
        {
            if (icon == null)
            {
                return form.Icon = Infrastructure.Properties.Resources.LogoMultiSize;
            }
            return form.Icon = icon;
        }

        public static void DisposeIcon(this Form form)
        {
            form.Icon?.Dispose();
            form.Icon = null;
        }
    }
}
