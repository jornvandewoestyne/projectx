﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using ProjectX.Infrastructure.LogAttributes;
using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class XApplicationSettings
    {
        #region Constructors
        private XApplicationSettings() { }
        #endregion

        #region Methods
        public static T SetDefaults<T>() where T : IXMainForm
        {
            //TODO: set currentlanguage
            XMainThread.Initialize(Environment.CurrentManagedThreadId);
            SetApplicationDefaults();
            SetDependencies();
            SetCulture();
            return XApplicationProvider.Start<T>();
        }
        #endregion

        #region Methods: Helpers
        private static void SetApplicationDefaults()
        {
            //USE ONLY FOR DEBUGGING PURPOSES
            //Application.SetUnhandledExceptionMode(UnhandledExceptionMode.Automatic);

            // Don't dispatch exceptions to Application.ThreadException
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.ThrowException);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            // Start showing the SplashForm:
            Splasher.Instance.Show();
        }

        [Log]
        private static void SetDependencies()
        {
            var success = false;
            try
            {
                //REQUIRED:
                XApplicationProvider.SetDependencies();
                success = true;
            }
            catch (Exception exception)
            {
                var message = ApplicationMessage.GenericMessage(ApplicationError.FatalException(exception).InnerException.ToString());
                //Nothing is set yet, so use XMessageBoxPlus directly, only for specific this situation:
                new XMessageBoxPlus().Show(message, XLib.Text.Bug.ToUpper());
                XApplication.Exit();
            }
            finally
            {
                //REQUIRED:
                if (success) { XApplicationProvider.SetTranslator(); }
                //Enforce StartApplicationException to log the startup:
                throw new StartApplicationException();
            }
        }

        private static void SetCulture()
        {
            var currentCulture = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
            var newCultureInfo = new CultureInfo(currentCulture.Name);
            var numberFormat = currentCulture.NumberFormat;
            numberFormat.NumberDecimalSeparator = XLib.CultureInfo.NumberDecimalSeparator;
            numberFormat.NumberGroupSeparator = XLib.CultureInfo.NumberGroupSeparator;
            numberFormat.CurrencyDecimalSeparator = XLib.CultureInfo.CurrencyDecimalSeparator;
            numberFormat.CurrencyGroupSeparator = XLib.CultureInfo.CurrencyGroupSeparator;
            var dateTimeFormat = currentCulture.DateTimeFormat;
            dateTimeFormat.DateSeparator = XLib.CultureInfo.DateSeparator;
            newCultureInfo.DateTimeFormat = dateTimeFormat;
            newCultureInfo.NumberFormat = numberFormat;
            Application.CurrentCulture = newCultureInfo;
        }
        #endregion
    }
}
