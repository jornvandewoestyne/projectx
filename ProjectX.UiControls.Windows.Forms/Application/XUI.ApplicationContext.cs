﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using System;
using System.ComponentModel;
using System.Windows.Forms;

#region SOURCE
//http://etutorials.org/Programming/visual-c-sharp/Part+III+Programming+Windows+Forms/Chapter+11+An+Introduction+to+Windows+Forms/Controlling+a+Windows+Forms+Application/
#endregion

namespace ProjectX.UiControls.Windows.Forms
{
    public sealed class XApplicationContext : ApplicationContext
    {
        #region Properties
        [Obsolete("Do not use this property, use '" + nameof(XApplication) + "." + nameof(XApplication.MainForm) + "' instead!", true)]
        public new IXMainForm MainForm { get; private set; }
        #endregion

        #region Constructors
        public XApplicationContext(IXMainForm form) : base(null)
        {
            form.Closed += (s, e) => XApplication.Exit();
            form.Subscribe(CancelEvent.Closing, this.FormClosing, form);
        }
        #endregion

        #region Methods
        [XIntercept(Strategy = ExceptionStrategy.Exit)]
        private void FormClosing(object sender, CancelEventArgs e)
        {
            if (!XApplication.ExitRequested)
            {
                var message = ApplicationMessage.CloseApplicationQuestion();
                var proceed = XMessenger.ConfirmWarning(message);
                e.Cancel = !proceed;
                if (proceed)
                {
                    XApplication.MainForm?.UIInvoke(x => x.Hide());
                    throw new CloseApplicationException();
                }
            }
        }
        #endregion
    }
}
