﻿using ProjectX.General;
using System;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public sealed class XApplication
    {
        #region Fields
        private static bool isOpen = false;
        private static bool isReady = false;
        #endregion

        #region Properties
        private static WeakReference<IXMainForm> Reference { get; set; }
        public static IXMainForm MainForm => Reference?.TryGet();
        public static XApplicationContext ApplicationContext { get; private set; }
        public static bool ExitRequested { get; set; }
        public static string Path => System.IO.Path.GetDirectoryName(Application.ExecutablePath);

        public static bool IsOpen
        {
            get
            {
                if (!isOpen)
                {
                    return isOpen = IsOpened();
                }
                return isOpen;
            }
            private set => isOpen = value;
        }

        public static bool IsReady
        {
            get
            {
                if (!isReady)
                {
                    return isReady = IsFullyOpened();
                }
                return isReady;
            }
            private set => isReady = value;
        }
        #endregion

        #region Methods
        public static T SetMainForm<T>(T form) where T : IXMainForm
        {
            Reference = (form as IXMainForm).AsWeakReference<IXMainForm>();
            return (T)MainForm;
        }

        public static XApplicationContext SetApplicationContext(IXMainForm form)
        {
            return ApplicationContext = new XApplicationContext(form);
        }

        [Obsolete]
        public static void TryQuit()
        {
            XApplication.ExitRequested = true;
            if (MainForm != null)
            {
                MainForm.Quit();
            }
            else
            {
                Application.ExitThread();
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <returns></returns>
        public static bool Exit()
        {
            try
            {
                using (var splasher = Splasher.Instance)
                {
                    XApplication.ExitRequested = true;
                    HideApplication();
                    splasher.Show();
                    ApplicationContext?.ExitThread();
                    Application.ExitThread();
                    Environment.Exit(Environment.ExitCode);
                }
            }
            finally
            {
                Environment.Exit(Environment.ExitCode);
            }
            return true;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="block"></param>
        public static void BlockInput(bool block)
        {
            var openForms = Application.OpenForms;
            foreach (Form openForm in openForms)
            {
                if (openForm as IXApplicationForm != null && !(openForm is SplashForm))
                {
                    openForm.UIInvoke(x => ((IXApplicationForm)x).BlockInput(block));
                }
            }
        }

        public static void HideApplication()
        {
            if (MainForm != null)
            {
                ((Form)MainForm).UIInvoke(x => x.Hide());
            }
            else
            {
                var openForms = Application.OpenForms;
                foreach (Form openForm in openForms)
                {
                    if (openForm is IXMainForm && !(openForm is SplashForm))
                    {
                        openForm.UIInvoke(x => openForm.Hide());
                        break;
                    }
                }
            }
        }

        public static bool HasOpenDialogForm()
        {
            var openForms = Application.OpenForms;
            foreach (Form openForm in openForms)
            {
                if (openForm.TopMost && !(openForm is SplashForm))
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region Methods: Helpers
        private static bool IsOpened()
        {
            var openForms = Application.OpenForms;
            foreach (Form openForm in openForms)
            {
                if (openForm is IXMainForm)
                {
                    return true;
                }
            }
            return false;
        }

        private static bool IsFullyOpened()
        {
            if (MainForm != null)
            {
                return ((XForm)MainForm).IsReady;
            }
            else
            {
                var openForms = Application.OpenForms;
                foreach (Form openForm in openForms)
                {
                    if (openForm is IXMainForm)
                    {
                        return ((XForm)openForm).IsReady;
                    }
                }
            }
            return false;
        }
        #endregion
    }
}
