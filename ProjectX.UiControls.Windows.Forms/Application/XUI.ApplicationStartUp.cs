﻿using ProjectX.DTO;
using ProjectX.General;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class XApplicationStartUp
    {
        #region Properties
        public static CancellationTokenSource CancellationTokenSource { get; private set; } = new CancellationTokenSource();
        public static CancellationToken CancellationToken { get; private set; }
        #endregion

        #region Constructors
        private XApplicationStartUp() { }
        #endregion

        #region Methods
        [XIntercept(Strategy = ExceptionStrategy.Default)]
        public static void Cancel()
        {
            CancellationTokenSource?.Cancel(true);
        }

        public static async Task RunAsync()
        {
            try
            {
                await InitAsync();
            }
            finally
            {
                //At least ensure to close the SplashForm:
                Splasher.Instance.Dispose();
            }
        }
        #endregion

        #region Methods: Helpers
        private static async Task InitAsync()
        {
            SetPropertyCache();
            using (var dependency = new XDI().Get<XDataConnector>())
            {
                await dependency.Instance.TestConnectionAsync(CancellationToken);
            }
            await StartApp();
        }

        private static async Task StartApp()
        {
            if (!XApplication.ExitRequested)
            {
                using (var dependency = new XDI().Get<XDtoPresenter>())
                {
                    var menu = await dependency.Instance.GetAsync<ApplicationMenuDTO>(CancellationToken);
                    XApplication.MainForm?.Show(menu);
                }
            }
            else
            {
                XApplication.Exit();
            }
        }

        private static void SetPropertyCache()
        {
            try
            {
                var assemblyName = typeof(ProjectX.Domain.Models.BaseCommonEntity).Assembly.GetName();
                var types = Assembly.Load(assemblyName).GetTypes().Where(type => type.IsSubclassOf(typeof(BaseEntity)));
                foreach (var type in types)
                {
                    PropertyCache.GetCachedProperties(type);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
