﻿using ProjectX.Infrastructure.DependencyInjection;

namespace ProjectX.UiControls.Windows.Forms
{
    public sealed class XDI
    {
        #region Methods
        public DI<object> Get()
        {
            return new XDI().Get<object>();
        }

        public DI<T> Get<T>()
        {
            var type = typeof(T);
            switch (type.Name)
            {
                case nameof(XMessenger):
                    return new DI<T>(ApplicationDependencies.MessengerTypes);
                default:
                    return new DI<T>();
            }
        }
        #endregion
    }
}
