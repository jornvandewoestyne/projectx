﻿using ProjectX.Business;
using ProjectX.Domain.Models;
using ProjectX.DTO;
using ProjectX.General;
using ProjectX.Infrastructure.LogAttributes;
using System;
using System.Threading;
using System.Threading.Tasks;

//Check: https://blog.stephencleary.com/2013/01/async-oop-3-properties.html

namespace ProjectX.UiControls.Windows.Forms
{
    public abstract class XBaseCollectionPresenter<TEntity, TData> : DisposableObject, IXSearchPresenter, IXCollectionPresenter<TEntity, TData>, IDisposable
        where TEntity : BaseCommonEntity
        where TData : BaseEntity
    {
        #region Properties
        protected IService Service { get; private set; }
        #endregion

        #region Properties -> IXSearchPresenter
        public TEntity Entity { get; private set; }
        public EntityKey EntityKey { get; private set; }
        public EntityKeyName EntityKeyName => GetEntityKeyName(); 
        public Type DataType => this.Data.GetEnumeratedType<TData>();
        #endregion

        #region Properties -> IXCollectionPresenter`T
        public ICollectionPage<TData> Data { get; set; }
        #endregion

        #region Constructors
        public XBaseCollectionPresenter(IService service)
        {
            this.Service = service;
        }
        #endregion

        #region Methods -> IXCollectionPresenter`T
        //[XIntercept(Strategy = ExceptionStrategy.Data)]
        public virtual EntitySummaryDTO GetEntitySummary(IXSearchContainer container, ICommonEntity entity, XProgressInformer informer)
        {
            return AsyncHelper.RunSync(() => this.GetEntitySummaryAsync(container, entity, informer, CancellationToken.None));
        }

        [XIntercept(Strategy = ExceptionStrategy.Data)]
        public virtual async Task<EntitySummaryDTO> GetEntitySummaryAsync(IXSearchContainer container, ICommonEntity entity, XProgressInformer informer, CancellationToken cancellationToken)
        {
            var menuBloc = container.MdiChildDetail.FormIdentifier;
            using (var dependency = new XDI().Get<XDtoPresenter>())
            {
                return await dependency.Instance.GetSummaryAsync(entity, menuBloc, informer, cancellationToken);
            }
        }

        public virtual TEntity GetEntity(EntityKey entityKey)
        {
            //TODO: check
            using (var dependency = new XDI().Get<XEntityPresenter<TEntity>>())
            {
                return this.Entity = dependency.Instance.Get(entityKey);
            }
        }

        public virtual async Task<TEntity> GetEntityAsync(EntityKey entityKey, CancellationToken cancellationToken)
        {
            //TODO: "using" gives Context Disposed exception with retries!
            return this.Entity = await new XDI().Get<XEntityPresenter<TEntity>>().Instance.GetAsync(entityKey, cancellationToken);
        }

        public abstract Task<ICollectionPage<TData>> GetDataPageAsync(XProgressInformer informer, SearchString searchString, Ordering ordering, Paging paging, CancellationToken cancellationToken);
        #endregion

        #region Methods -> ISearchPresenter
        //[XIntercept(Strategy = ExceptionStrategy.Data)]
        public virtual async Task<bool> GetDataAsync(IXSearchContainer container, XProgressInformer informer, CancellationToken cancellationToken, bool initialLoad)
        {
            var searchString = container.SearchString;
            var ordering = container?.Ordering;
            var paging = initialLoad ? new Paging(initialLoad) : container.Paging;
            this.Data = await GetDataPageAsync(informer, searchString, ordering, paging, cancellationToken);
            return this.Data != null;
        }

        public virtual async Task<bool> PopulateDataAsync(IXSearchContainer container, CancellationToken cancellationToken)
        {
            var entityKeyName = await Task.Run(() => this.EntityKeyName);
            return await container.DataViewer.PopulateAsync(this.Data, entityKeyName, cancellationToken);
        }

        public virtual EntityKey GetSelectedKey(IXSearchContainer container, bool showMessage)
        {
            return this.EntityKey = container.DataViewer.GetSelectedKeyValue(this.EntityKeyName, showMessage);
        }

        //[Log]
        [XIntercept(Strategy = ExceptionStrategy.Data)]
        public virtual async Task<bool> SetEntityAsync(EntityKey entityKey, XProgressInformer informer, CancellationToken cancellationToken)
        {
            //LogAttribute is required -> Forces to reload data, when Entity is not found!
            var entity = this.Entity = await this.GetEntityAsync(entityKey, cancellationToken)
                .RunWithTimeoutAsync(informer);
            return entity != null;
        }

        public virtual async Task<bool> ShowEntityAsync(IXSearchContainer container, XProgressInformer informer, CancellationToken cancellationToken)
        {
            var entity = this.Entity;
            var form = container as XChildSearchForm;
            //TODO: context disposed
            var summary = await this.GetEntitySummaryAsync(container, entity, informer, cancellationToken);
            var detail = this.GetMdiChildDetail(form, summary);
            var title = summary?.Title;
            var fullTitle = summary?.ToString();
            return await entity.CreateNewInstance<TEntity, XMenuUserControl>(form, detail, title, fullTitle)
                .RunWithTimeoutAsync(informer);
        }

        public virtual Ordering GetOrdering(params OrderingSetting[] settings)
        {
            using (var dependency = new XDI().Get<XDataPresenter<TData>>())
            {
                return dependency.Instance.OrderBy(settings).Ordering;
            }
        }
        #endregion

        #region Methods: Helpers
        private Activity GetActivity(MenuGroupAction action)
        {
            using (var dependency = new XDI().Get<XEntityPresenter<Activity>>())
            {
                return dependency.Instance.Get(action);
            }
        }

        private XMdiChildDetail GetMdiChildDetail(XChildSearchForm form, EntitySummaryDTO summary)
        {
            using (var dependency = new XDI().Get<XEntityPresenter<Activity>>())
            {
                var activity = dependency.Instance.Include(x => x.ActivityGroup).Get(MenuGroupAction.Edit);
                var detail = new XMdiChildDetail(summary?.Title, activity, activity?.ActivityGroup, form.MdiChildDetail.FormIdentifier, XLib.Preference.ShowChildHeaderDefault, XLib.Preference.ConfirmToCloseOnEditForm);
                return detail;
            }
        }

        private EntityKeyName GetEntityKeyName()
        {
            using (var dependency = new XDI().Get<XKeyPresenter<TEntity>>())
            {
                return dependency.Instance.GetEntityKeyName();
            }
        }
        #endregion

        #region Methods: DisposableObject
        public override void Dispose(bool disposing)
        {
            if (disposing && !IsDisposed)
            {
                this.Service?.Dispose();
                this.Service = null;
                this.Data?.Dispose();
                this.Data = null;
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
