﻿using ProjectX.Business;
using ProjectX.Domain.Models;
using ProjectX.General;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.UiControls.Windows.Forms
{
    public sealed class XCustomerCollectionPresenter : XBaseCollectionPresenter<Partner, CustomerGeneralData>
    {
        #region Constructors
        public XCustomerCollectionPresenter(IService service) : base(service)
        { }
        #endregion

        #region Methods -> Overrides
        public override async Task<ICollectionPage<CustomerGeneralData>> GetDataPageAsync(XProgressInformer informer, SearchString searchString, Ordering ordering, Paging paging, CancellationToken cancellationToken)
        {
            using (var dependency = new XDI().Get<XPagePresenter<CustomerGeneralData>>())
            {
                var result = await dependency.Instance
                    .OrderBy(ordering)
                    .PageBy(paging)
                    .GetAsync(searchString, informer, cancellationToken);
                return result.CollectionPage as ICollectionPage<CustomerGeneralData>;
            }
        }
        #endregion
    }
}
