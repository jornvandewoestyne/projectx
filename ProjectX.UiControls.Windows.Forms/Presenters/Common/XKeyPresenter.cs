﻿using ProjectX.Business;
using ProjectX.General;

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class XKeyPresenter : XServiceProvider
    {
        #region Constructors
        public XKeyPresenter(IService service) : base(service) { }
        #endregion

        #region Methods: EntityKeys
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public EntityKey GetEntityKey<TSource>(TSource entity) where TSource : BaseEntity
        {
            return this.Service.GetEntityKey<TSource>(entity);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <returns></returns>
        public EntityKeyName GetEntityKeyName<TSource>() where TSource : BaseEntity
        {
            return this.Service.GetEntityKeyName<TSource>();
        }
        #endregion
    }
}
