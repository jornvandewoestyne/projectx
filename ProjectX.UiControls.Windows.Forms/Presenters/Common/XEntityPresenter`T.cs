﻿using ProjectX.Business;
using ProjectX.General;
using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// ...
    /// </summary>
    /// <typeparam name="TSource"></typeparam>
    public sealed class XEntityPresenter<TSource> : XDataPresenter<TSource> where TSource : BaseEntity
    {
        #region Constructors
        public XEntityPresenter(IService service) : base(service) { }
        #endregion

        #region Methods: Include
        /// <summary>
        /// Specify the related objects to include in the query results and also includes all the navigation properties of the given entity type (when no parameters are passed).
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public new XEntityPresenter<TSource> Include(params Expression<Func<TSource, object>>[] includes)
        {
            return base.Include(includes) as XEntityPresenter<TSource>;
        }
        #endregion

        #region Methods: OrderBy
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="ordering"></param>
        /// <returns></returns>
        public new XEntityPresenter<TSource> OrderBy(Ordering ordering)
        {
            return base.OrderBy(ordering) as XEntityPresenter<TSource>;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="properties"></param>
        /// <returns></returns>
        public new XEntityPresenter<TSource> OrderBy(params Expression<Func<TSource, object>>[] properties)
        {
            return base.OrderBy(properties) as XEntityPresenter<TSource>;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public new XEntityPresenter<TSource> OrderBy(params OrderingSetting<TSource>[] settings)
        {
            return base.OrderBy(settings) as XEntityPresenter<TSource>;
        }
        #endregion

        #region Methods: Data
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TSource Get(Enum key)
        {
            return this.Get(Convert.ToInt32(key));
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TSource Get(int key)
        {
            return this.Get(key.AsEntityKey());
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="entityKey"></param>
        /// <returns></returns>
        public TSource Get(EntityKey entityKey)
        {
            return this.Service.GetEntity(entityKey, this.Including);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<TSource> GetAsync(Enum key, CancellationToken cancellationToken)
        {
            return await this.GetAsync(Convert.ToInt32(key), cancellationToken);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<TSource> GetAsync(int key, CancellationToken cancellationToken)
        {
            return await this.GetAsync(key.AsEntityKey(), cancellationToken);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="entityKey"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<TSource> GetAsync(EntityKey entityKey, CancellationToken cancellationToken)
        {
            return await this.Service.GetEntityAsync(entityKey, this.Including, cancellationToken);
        }
        #endregion
    }
}
