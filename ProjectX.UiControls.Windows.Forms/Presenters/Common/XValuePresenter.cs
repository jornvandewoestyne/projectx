﻿using ProjectX.Business;
using ProjectX.Domain.Models;

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class XValuePresenter : XServiceProvider
    {
        #region Constructors
        public XValuePresenter(IService service) : base(service) { }
        #endregion

        #region Methods: FullName
        public string GetFullName<TSource>(TSource entity) where TSource : BaseCommonEntity
        {
            return this.Service.GetFullName<TSource>(entity);
        }
        #endregion
    }
}