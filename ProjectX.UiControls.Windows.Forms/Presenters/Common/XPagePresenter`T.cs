﻿using ProjectX.Business;
using ProjectX.General;
using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// ...
    /// </summary>
    /// <typeparam name="TSource"></typeparam>
    public sealed class XPagePresenter<TSource> : XDataPresenter<TSource> where TSource : BaseEntity
    {
        #region Properties
        public Paging Paging { get; private set; } = Paging.None;
        #endregion

        #region Constructors
        public XPagePresenter(IService service) : base(service) { }
        #endregion

        #region Methods: PageBy
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="paging"></param>
        /// <returns></returns>
        public XPagePresenter<TSource> PageBy(Paging paging)
        {
            this.Paging = paging;
            return this;
        }
        #endregion

        #region Methods: Include
        /// <summary>
        /// Specify the related objects to include in the query results and also includes all the navigation properties of the given entity type (when no parameters are passed).
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public new XPagePresenter<TSource> Include(params Expression<Func<TSource, object>>[] includes)
        {
            return base.Include(includes) as XPagePresenter<TSource>;
        }
        #endregion

        #region Methods: OrderBy
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="ordering"></param>
        /// <returns></returns>
        public new XPagePresenter<TSource> OrderBy(Ordering ordering)
        {
            return base.OrderBy(ordering) as XPagePresenter<TSource>;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="properties"></param>
        /// <returns></returns>
        public new XPagePresenter<TSource> OrderBy(params Expression<Func<TSource, object>>[] properties)
        {
            return base.OrderBy(properties) as XPagePresenter<TSource>;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public new XPagePresenter<TSource> OrderBy(params OrderingSetting<TSource>[] settings)
        {
            return base.OrderBy(settings) as XPagePresenter<TSource>;
        }
        #endregion

        #region Methods: CollectionPage
        public ICollectionPage Get(SearchString searchString)
        {
            return XTaskHelper.Run(searchString, this.Ordering, this.Paging, Run);

            ICollectionPage Run(SearchString s, Ordering o, Paging p)
            {
                return this.Service.GetDataPage<TSource>(s, o, p);
            }
        }

        public async Task<ICollectionPage> GetAsync(SearchString searchString, XProgressInformer informer, CancellationToken cancellationToken)
        {
            return await XTaskHelper.RunAsync(searchString, this.Ordering, this.Paging, informer, cancellationToken, RunAsync);

            async Task<ICollectionPage> RunAsync(SearchString s, Ordering o, Paging p, CancellationToken c)
            {
                cancellationToken.ThrowIfCancellationRequested();
                //await Task.Run() => ensures responsiveness at each moment (in case of an exception)
                return await Task.Run(() => this.Service.GetDataPageAsync<TSource>(s, o, p, c), c);
            }
        }
        #endregion
    }
}
