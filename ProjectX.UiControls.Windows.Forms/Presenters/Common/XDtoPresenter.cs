﻿using ProjectX.Business;
using ProjectX.DTO;
using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class XDtoPresenter : XServiceProvider
    {
        #region Constructors
        public XDtoPresenter(IService service) : base(service) { }
        #endregion

        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Get<T>() where T : IKeyValueDTO
        {
            return AsyncHelper.RunSync(() => this.GetAsync<T>(CancellationToken.None));
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cancellationToken"></param>
        /// <param name="informer"></param>
        /// <returns></returns>
        [XIntercept(Strategy = ExceptionStrategy.Data)]
        public async Task<T> GetAsync<T>(CancellationToken cancellationToken, XProgressInformer informer = null)
            where T : IKeyValueDTO
        {
            using (var splasher = Splasher.Instance)
            {
                splasher.Show(ApplicationProgressInfo.StartUpTask());
                return await this.Service.GetDTOAsync<T>(cancellationToken).RunWithTimeoutAsync(informer);
            }
        }

        public EntitySummaryDTO GetSummary(ICommonEntity entity, MenuBloc menuBloc, XProgressInformer informer)
        {
            return AsyncHelper.RunSync(() => this.GetSummaryAsync(entity, menuBloc, informer, CancellationToken.None));
        }

        //[XIntercept(Strategy = ExceptionStrategy.Data)]
        public async Task<EntitySummaryDTO> GetSummaryAsync(ICommonEntity entity, MenuBloc menuBloc, XProgressInformer informer, CancellationToken cancellationToken)
        {
            return await this.Service.GetSummaryAsync(entity, menuBloc, cancellationToken).RunWithTimeoutAsync(informer);
        }
        #endregion
    }
}