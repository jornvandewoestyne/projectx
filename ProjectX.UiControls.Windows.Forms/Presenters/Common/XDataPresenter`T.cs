﻿using ProjectX.Business;
using ProjectX.General;
using System;
using System.Linq.Expressions;

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// ...
    /// </summary>
    /// <typeparam name="TSource"></typeparam>
    public class XDataPresenter<TSource> : XServiceProvider where TSource : BaseEntity
    {
        #region Properties
        public Including<TSource> Including { get; protected set; } = Including<TSource>.None;
        public Ordering Ordering { get; protected set; } = Ordering.None;
        #endregion

        #region Constructors
        public XDataPresenter(IService service) : base(service) { }
        #endregion

        #region Methods: Include
        /// <summary>
        /// Specify the related objects to include in the query results and also includes all the navigation properties of the given entity type (when no parameters are passed).
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public XDataPresenter<TSource> Include(params Expression<Func<TSource, object>>[] includes)
        {
            var navigationProperties = this.Service.GetNavigationProperties<TSource>();
            this.Including = new Including<TSource>(navigationProperties, includes);
            return this;
        }
        #endregion

        #region Methods: OrderBy
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="ordering"></param>
        /// <returns></returns>
        public XDataPresenter<TSource> OrderBy(Ordering ordering)
        {
            this.Ordering = ordering;
            return this;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public XDataPresenter<TSource> OrderBy(params OrderingSetting[] settings)
        {
            this.Ordering = new Ordering<TSource>(settings).OrderBy;
            return this;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public XDataPresenter<TSource> OrderBy(params OrderingSetting<TSource>[] settings)
        {
            this.Ordering = new Ordering<TSource>(settings).OrderBy;
            return this;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="properties"></param>
        /// <returns></returns>
        public XDataPresenter<TSource> OrderBy(params Expression<Func<TSource, object>>[] properties)
        {
            this.Ordering = new Ordering<TSource>(properties).OrderBy;
            return this;
        }
        #endregion
    }
}
