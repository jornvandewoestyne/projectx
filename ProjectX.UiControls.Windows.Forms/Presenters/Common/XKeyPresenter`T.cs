﻿using ProjectX.Business;
using ProjectX.General;

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// ...
    /// </summary>
    /// <typeparam name="TSource"></typeparam>
    public sealed class XKeyPresenter<TSource> : XDataPresenter<TSource> where TSource : BaseEntity
    {
        #region Constructors
        public XKeyPresenter(IService service) : base(service) { }
        #endregion

        #region Methods: EntityKeys
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public EntityKey GetEntityKey(TSource entity)
        {
            using (var dependency = new XDI().Get<XKeyPresenter>())
            {
                return dependency.Instance.GetEntityKey<TSource>(entity);
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <returns></returns>
        public EntityKeyName GetEntityKeyName()
        {
            using (var dependency = new XDI().Get<XKeyPresenter>())
            {
                return dependency.Instance.GetEntityKeyName<TSource>();
            }
        }
        #endregion
    }
}
