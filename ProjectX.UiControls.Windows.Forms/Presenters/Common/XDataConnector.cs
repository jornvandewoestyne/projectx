﻿using ProjectX.Business;
using ProjectX.Infrastructure.Localization;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class XDataConnector : XServiceProvider
    {
        #region Constructors
        public XDataConnector(IService service) : base(service) { }
        #endregion

        #region Methods: Test Connection
        /// <summary>
        /// Tests the current connection.
        /// </summary>
        public void TestConnection()
        {
            XTaskHelper.Run(Run);

            void Run()
            {
                using (var splasher = Splasher.Instance)
                {
                    splasher.Show(ApplicationProgressInfo.ConnectingToDataBaseTask());
                    this.Service.TestConnection();
                }
            }
        }

        /// <summary>
        /// Tests the current connection asynchronously.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task TestConnectionAsync(CancellationToken cancellationToken, XProgressInformer informer = null)
        {
            await XTaskHelper.RunAsync(informer, cancellationToken, RunAsync);

            async Task RunAsync(CancellationToken token)
            {
                using (var splasher = Splasher.Instance)
                {
                    splasher.Show(ApplicationProgressInfo.ConnectingToDataBaseTask());
                    await this.Service.TestConnectionAsync(token);
                }
            }
        }
        #endregion
    }
}
