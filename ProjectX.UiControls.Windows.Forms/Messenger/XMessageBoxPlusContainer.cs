﻿using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    internal partial class XMessageBoxPlusContainer : Form
    {
        #region Constructor
        public XMessageBoxPlusContainer(string title)
        {
            this.InitializeComponent();
            this.SetTitle(title);
            this.SetIcon();
            this.mainPictureBox.Image = ProjectX.Infrastructure.Properties.Resources.Animation;
        }
        #endregion

        #region Methods
        private void SetTitle(string title)
        {
            this.Text = title;
        }
        #endregion

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            this.DisposeIcon();
            this.mainPictureBox?.Dispose();
            base.Dispose(disposing);
        }
    }
}
