﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using ProjectX.Infrastructure.MessageService;
using System.Drawing;
using System.Windows.Forms;

#region Source
//SOURCE: http://www.codeproject.com/Articles/18612/TopMost-MessageBox
#endregion

//TODO: Modify to TaskMessageBox or notification

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// Displays a System.Windows.Forms.MessageBox as a top most window with animation in the windows taskbar.
    /// </summary>
    internal class XMessageBoxPlus : DisposableItem, IMessageBox
    {
        public XMessageBoxPlus()
        {
            XApplication.BlockInput(true);
        }

        #region Properties
        private static MessageBoxButtons _Buttons => MessageBoxButtons.OK;
        private static MessageBoxIcon _Icon => MessageBoxIcon.None;
        private static MessageBoxDefaultButton _DefaultButton => MessageBoxDefaultButton.Button1;
        #endregion

        #region Methods
        /// <summary>
        /// Displays a <see cref="MessageBox"/> but as a TopMost window.
        /// </summary>
        /// <param name="message">The text to appear in the message box.</param>
        /// <param name="title">The title of the message box.</param>
        /// <returns>The button pressed.</returns>
        public DialogResult Show(FriendlyMessage message, string title)
        {
            return Show(message, title, _Buttons, _Icon, _DefaultButton);
        }

        /// <summary>
        /// Displays a <see cref="MessageBox"/> but as a TopMost window.
        /// </summary>
        /// <param name="message">The text to appear in the message box.</param>
        /// <param name="title">The title of the message box.</param>
        /// <param name="icon">The icon to display in the message box.</param>
        /// <returns>The button pressed.</returns>
        public DialogResult Show(FriendlyMessage message, string title, MessageBoxIcon icon)
        {
            return Show(message, title, _Buttons, icon, _DefaultButton);
        }

        /// <summary>
        /// Displays a <see cref="MessageBox"/> but as a TopMost window.
        /// </summary>
        /// <param name="message">The text to appear in the message box.</param>
        /// <param name="title">The title of the message box.</param>
        /// <param name="buttons">The buttons to display in the message box.</param>
        /// <returns>The button pressed.</returns>
        public DialogResult Show(FriendlyMessage message, string title, MessageBoxButtons buttons)
        {
            return Show(message, title, buttons, _Icon, _DefaultButton);
        }

        /// <summary>
        /// Displays a <see cref="MessageBox"/> but as a TopMost window.
        /// </summary>
        /// <param name="message">The text to appear in the message box.</param>
        /// <param name="title">The title of the message box.</param>
        /// <param name="buttons">The buttons to display in the message box.</param>
        /// <param name="icon">The icon to display in the message box.</param>
        /// <returns>The button pressed.</returns>
        public DialogResult Show(FriendlyMessage message, string title, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            return Show(message, title, buttons, icon, _DefaultButton);
        }

        /// <summary>
        /// Displays a <see cref="MessageBox"/> but as a TopMost window.
        /// </summary>
        /// <param name="message">The text to appear in the message box.</param>
        /// <param name="title">The title of the message box.</param>
        /// <param name="buttons">The buttons to display in the message box.</param>
        /// <param name="icon">The icon to display in the message box.</param>
        /// <param name="defaultButton">Specifies the default selected button.</param>
        /// <returns>The button pressed.</returns>
        public DialogResult Show(FriendlyMessage message, string title, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton)
        {
            // Create a host form that is a TopMost window which will be the parent of the MessageBox.
            using (var form = new XMessageBoxPlusContainer(title))
            {
                // We do not want anyone to see this window so position it off the visible screen and make it as small as possible.
                form.Size = new Size(100, 100);
                form.StartPosition = FormStartPosition.Manual;
                var rectangle = SystemInformation.VirtualScreen;
                form.Location = new Point(rectangle.Bottom + 10, rectangle.Right + 10);
                form.ShowOnTop();
                // Finally show the MessageBox with the form just created as its owner
                return MessageBox.Show(form, message.Message, title, buttons, icon, defaultButton);
            }
        }
        #endregion

        #region DisposableItem
        protected override void Finish()
        {
            XApplication.BlockInput(false);
        }

        protected override void Unsubscribe()
        {
            //No actions needed
        }

        protected override void Unbind()
        {
            //No actions needed
        }
        #endregion
    }
}