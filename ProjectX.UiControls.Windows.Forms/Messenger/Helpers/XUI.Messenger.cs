﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using ProjectX.Infrastructure.MessageService;
using System;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public sealed class XMessenger : IDisposable
    {
        #region Properties
        public IMessenger Messenger { get; private set; }
        #endregion

        #region Constructors
        public XMessenger(IMessenger messenger)
        {
            this.Messenger = messenger;
        }
        #endregion

        #region Methods: Messenger
        /// <summary>
        /// Shows messenger info.
        /// </summary>
        /// <param name="message"></param>
        public static void ShowInfo(FriendlyMessage message)
        {
            Show(message, LogLevel.Info, true);
        }

        /// <summary>
        /// Shows messenger warning.
        /// </summary>
        /// <param name="message"></param>
        public static void ShowWarning(FriendlyMessage message)
        {
            Show(message, LogLevel.Warning, true);
        }

        /// <summary>
        /// Shows messenger error.
        /// </summary>
        /// <param name="message"></param>
        public static void ShowError(FriendlyMessage message)
        {
            Show(message, LogLevel.Error, true);
        }

        /// <summary>
        /// Shows a messenger question with "Yes/No" buttons and returns a boolean result.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool ConfirmQuestion(FriendlyMessage message)
        {
            var dialogResult = Show(message, LogLevel.Question, MessengerButton.YesNo);
            if (dialogResult == DialogResult.No)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Shows a messenger warning with "Yes/No" buttons and returns a boolean result.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool ConfirmWarning(FriendlyMessage message)
        {
            var dialogResult = Show(message, LogLevel.Warning, MessengerButton.YesNo);
            if (dialogResult == DialogResult.No)
            {
                return false;
            }
            return true;
        }
        #endregion

        #region Methods: Messenger Custom Messages
        /// <summary>
        /// Shows messenger info: "NoSelection".
        /// </summary>
        /// <param name="showMessage"></param>
        public static void ShowMessageNoSelection(bool showMessage)
        {
            if (showMessage)
            {
                var message = ApplicationMessage.DataGridViewWithNoSelectionAdvice();
                ShowWarning(message);
            }
        }

        /// <summary>
        /// Shows messenger info: "IsOpen".
        /// </summary>
        /// <param name="showMessage"></param>
        public static void ShowMessageIsOpen(bool showMessage)
        {
            if (showMessage)
            {
                var message = ApplicationMessage.FormAlreadyOpenedAlert();
                ShowInfo(message);
            }
        }

        /// <summary>
        /// Shows a messenger "RetryReload" question with "Yes/No" buttons.
        /// <para>Returns true when confirmed. </para>
        /// </summary>
        /// <returns></returns>
        public static bool ConfirmRetryReload()
        {
            var message = ApplicationMessage.RetryReloadQuestion();
            return ConfirmQuestion(message);
        }
        #endregion

        #region Methods: Helpers
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="message"></param>
        /// <param name="logLevel"></param>
        /// <param name="showMessage"></param>
        private static void Show(FriendlyMessage message, LogLevel logLevel, bool showMessage)
        {
            using (var dependency = new XDI().Get<XMessenger>())
            {
                var messenger = dependency.Instance.Messenger;
                messenger.Show(message, logLevel, showMessage);
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="message"></param>
        /// <param name="logLevel"></param>
        /// <param name="buttons"></param>
        /// <returns></returns>
        private static DialogResult Show(FriendlyMessage message, LogLevel logLevel, MessengerButton buttons)
        {
            using (var dependency = new XDI().Get<XMessenger>())
            {
                var messenger = dependency.Instance.Messenger;
                return (DialogResult)messenger.Show(message, logLevel, buttons);
            }
        }
        #endregion

        #region Methods: IDisposable
        /// <summary>
        /// ...
        /// </summary>
        public void Dispose()
        {
            this.Messenger.Dispose();
            this.Messenger = null;
        }
        #endregion
    }
}
