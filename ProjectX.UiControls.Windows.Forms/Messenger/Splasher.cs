﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

#region Source
// SOURCE: http://www.codeproject.com/Articles/3542/How-to-do-Application-Initialization-while-showing
#endregion

namespace ProjectX.UiControls.Windows.Forms
{
    public sealed class Splasher : DisposableItem
    {
        #region Singleton
        private static readonly Lazy<Splasher> _lazy = new Lazy<Splasher>(() => new Splasher());

        public static Splasher Instance { get { return _lazy.Value; } }

        private Splasher()
        { }
        #endregion

        #region Properties
        private WeakReference<SplashForm> Reference { get; set; }
        private SplashForm SplashForm => this.Reference?.TryGet();
        private Thread SplashThread { get; set; }
        private string Text { get; set; }
        private string Title { get; set; }

        [XIntercept(Strategy = ExceptionStrategy.Default)]
        private string Status
        {
            set
            {
                if (SplashForm == null) { return; }
                XApplication.BlockInput(true);
                SplashForm.UIInvoke(x => x.StatusInfo = value);
                if (!SplashForm.Visible)
                {
                    SplashForm.UIInvoke(x => x.ShowOnTop());
                }
                SplashForm.UIInvoke(x => x.ActivateForm(), x => x.SetOpacityVisible());
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Shows the SplashForm.
        /// </summary>
        public void Show()
        {
            Show(null, null, XLib.MagicNumber.IntZero);
        }

        /// <summary>
        /// Shows the SplashForm.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="delay"></param>
        public void Show(FriendlyMessage message, int delay = XLib.Preference.DefaultSplasherDelay)
        {
            Show(message, null, delay);
        }

        /// <summary>
        /// Shows the SplashForm.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="title"></param>
        /// <param name="delay"></param>
        public void Show(FriendlyMessage message, string title, int delay)
        {
            if (SplashThread == null)
            {
                SplashThread = new Thread(() => ShowThread(message, delay))
                {
                    //Make sure thread closes/disposes in all circumstances, even a crash
                    IsBackground = true
                };
                SplashThread.Start();
                //Get some delay to ensure SplashForm is started:
                Thread.Sleep(2);
            }
            //Continue:
            Text = message?.Message;
            Title = title;
            SetStatus(message, delay);
        }

        /// <summary>
        /// ...
        /// </summary>
        [XIntercept(Strategy = ExceptionStrategy.Default)]
        public void HandleEscKey()
        {
            //Neccessary to close the Splasher to keep the UI responsive in all circumstances:
            Splasher.Instance.Close(XLib.MagicNumber.IntZero);
            if (!XApplication.IsReady)
            {
                throw new CloseApplicationException();
            }
        }
        #endregion

        #region Methods: Helpers
        /// <summary>
        /// Internally used as a thread function: Showing the form and starting the messageloop for it.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="delay"></param>
        private void ShowThread(FriendlyMessage message, int delay = XLib.Preference.DefaultSplasherDelay)
        {
            //Local function which implements the asynchronous startup:
            async void StartUpHandlerAsync(object startUp)
            {
                Reference = new SplashForm(Text, Title).AsWeakReference();
                SplashForm?.Subscribe<IXLeakable>();
                await Task.Run(() => SetStatus(message, delay));
            }

            using (var context = new WindowsFormsSynchronizationContext())
            {
                SynchronizationContext.SetSynchronizationContext(context);
                try
                {
                    context.Post(StartUpHandlerAsync, null);
                    Application.Run(SplashForm);
                }
                finally
                {
                    SynchronizationContext.SetSynchronizationContext(null);
                }
            }
        }

        /// <summary>
        /// Closes the Splasher.
        /// <para>Prefer to use the .Dispose() method instead!</para>
        /// </summary>
        /// <param name="delay"></param>
        private void Close(int delay = XLib.Preference.DefaultSplasherDelay)
        {
            if (SplashThread == null) { return; }
            if (SplashForm == null) { return; }
            try
            {
                if (delay > XLib.MagicNumber.IntZero) { Thread.Sleep(delay); }
                SplashForm.UIInvoke(x => x.Dispose());
            }
            catch
            {
                throw;
            }
            finally
            {
                XApplication.BlockInput(false);
                SplashThread = null;
                Reference = null;
            }
        }

        /// <summary>
        /// Hides the Splasher.
        /// </summary>
        /// <param name="delay"></param>
        private void Hide(int delay = XLib.Preference.DefaultSplasherDelay)
        {
            if (SplashThread == null) { return; }
            if (SplashForm == null) { return; }
            try
            {
                if (delay > XLib.MagicNumber.IntZero) { Thread.Sleep(delay); }
                SplashForm.UIInvoke(x => x.Hide());
                SplashForm.UIInvoke(x => x.SetOpacityHide());

            }
            catch
            {
                throw;
                //this.Close();
            }
            finally
            {
                XApplication.BlockInput(false);
            }
        }

        /// <summary>
        /// Sets the Status of the SplashForm with optional delay.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="delay"></param>
        private void SetStatus(FriendlyMessage message, int delay = XLib.Preference.DefaultSplasherDelay)
        {
            if (XApplication.ExitRequested)
            {
                message = ApplicationMessage.CloseApplicationAlert();
                delay = XLib.Preference.DefaultShowClosingDuration;
            }
            Status = message?.Message;
            if (delay > XLib.MagicNumber.IntZero) { Thread.Sleep(delay); }
        }
        #endregion

        #region DisposableItem
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            this.IsDisposed = false;
        }

        protected override void Finish()
        {
            XApplication.BlockInput(false);
        }

        protected override void Unsubscribe()
        {
            //No actions needed
        }

        protected override void Unbind()
        {
            if (XApplication.IsOpen)
            {
                this.Hide();
            }
        }
        #endregion
    }
}