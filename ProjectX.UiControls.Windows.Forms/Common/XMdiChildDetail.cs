﻿using ProjectX.Domain.Models;
using ProjectX.DTO;
using ProjectX.General;
using System;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public sealed class XMdiChildDetail : DisposableItem, IXDataSourceMembers<Form, string> //TODO: find alternative for IDataSourceMembers
    {
        public MainInfoDTO Instance { get; set; }

        #region Properties
        private WeakReference<XChildForm> Reference { get; set; }
        public XChildForm OpenForm => this.Reference?.TryGet();
        public MenuBloc FormIdentifier { get; set; }
        public string Title { get; set; }
        //TODO: make it a setter
        public string SortName => this.OpenForm?.EntityFullName;
        public Activity Activity { get; set; }
        public ActivityGroup ActivityGroup { get; set; }
        public bool ShowHeader { get; set; }
        public bool ConfirmToClose { get; set; }

        public string FormIdentifierName => StringHelper.Translation(this.FormIdentifier);
        public string ActivityName => this.Activity.Name;
        public string ActivityGroupName => this.ActivityGroup.Name;
        public bool MultipleInstances => this.Activity.MultipleInstancesAllowed;
        public MenuGroupAction ActionIdentifier => (MenuGroupAction)this.Activity.ActivityID;

        /// <summary>
        /// Returns the sequence number of the action, used for sorting of open items per group.
        /// </summary>
        public int SequenceNumber => (int)ActionIdentifier;
        #endregion

        #region Constructors
        public XMdiChildDetail(Activity activity, ActivityGroup activityGroup, MenuBloc formIdentifier)
            : this(null, null, activity, activityGroup, formIdentifier, false, false)
        { }

        public XMdiChildDetail(string title, Activity activity, ActivityGroup activityGroup, MenuBloc formIdentifier, bool showHeader, bool confirmToClose)
            : this(null, title, activity, activityGroup, formIdentifier, showHeader, confirmToClose)
        { }

        public XMdiChildDetail(XChildForm openform, string title, Activity activity, ActivityGroup activityGroup, MenuBloc formIdentifier, bool showHeader, bool confirmToClose)
        {
            this.Reference = openform.AsWeakReference();
            this.Title = title;
            this.Activity = activity;
            this.ActivityGroup = activityGroup;
            this.FormIdentifier = formIdentifier;
            this.ShowHeader = showHeader;
            this.ConfirmToClose = confirmToClose;
        }
        #endregion

        #region Properties: -> IDataSourceMembers
        public Form Key
        {
            get { return this.OpenForm; }
        }
        public string Value
        {
            get { return this.Title; }
        }
        #endregion

        #region DisposableItem
        protected override void Finish()
        {
            //No actions needed
        }

        protected override void Unsubscribe()
        {
            //No actions needed
        }

        protected override void Unbind()
        {
            this.Reference = null;
            this.Title = null;
            this.Activity = null;
            this.ActivityGroup = null;
        }
        #endregion
    }
}