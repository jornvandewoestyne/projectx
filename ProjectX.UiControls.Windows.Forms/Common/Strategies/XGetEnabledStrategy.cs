﻿using System.ComponentModel;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    internal sealed class XGetEnabledStrategy : XStrategy, IXConditionStrategy
    {
        public override bool GetResult(Component component)
        {
            if (component is Control control)
            {
                return control.Enabled;
            }
            return false;
        }
    }
}
