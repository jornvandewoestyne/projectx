﻿using System.ComponentModel;

namespace ProjectX.UiControls.Windows.Forms
{
    internal class XBooleanStrategy : XStrategy, IXConditionStrategy
    {
        private bool Result { get; } = false;

        public XBooleanStrategy(bool bResult)
        {
            this.Result = bResult;
        }

        public override bool GetResult(Component control)
        {
            return this.Result;
        }
    }
}
