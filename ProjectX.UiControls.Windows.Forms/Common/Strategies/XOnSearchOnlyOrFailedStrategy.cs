﻿using ProjectX.General;
using System.ComponentModel;

namespace ProjectX.UiControls.Windows.Forms
{
    internal sealed class XOnSearchOnlyOrFailedStrategy : XStrategy, IXConditionStrategy
    {
        public override bool GetResult(Component control)
        {
            var result = false;
            if (control is IXLockable lockable)
            {
                switch (lockable.Work)
                {
                    case Work.None:
                    case Work.Close:
                    case Work.Search:
                        result = true;
                        break;
                    default:
                        break;
                }
            }
            return result;
        }
    }
}
