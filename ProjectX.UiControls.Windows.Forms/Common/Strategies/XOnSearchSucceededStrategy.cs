﻿using System.ComponentModel;

namespace ProjectX.UiControls.Windows.Forms
{
    internal sealed class XOnSearchSucceededStrategy : XStrategy, IXConditionStrategy
    {
        public override bool GetResult(Component control)
        {
            var result = false;
            if (control is IXLockable lockable)
            {
                switch (lockable.Work)
                {
                    default:
                        result = true;
                        break;
                }
            }
            return result;
        }
    }
}
