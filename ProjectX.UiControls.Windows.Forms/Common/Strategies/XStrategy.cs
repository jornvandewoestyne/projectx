﻿using ProjectX.General;
using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    internal abstract class XStrategy : IXConditionStrategy
    {
        #region IXConditionStrategy Methods
        public abstract bool GetResult(Component control);

        public virtual bool Validate(Component control, IXLockable lockable)
        {
            return lockable.Enabled = GetResult(control);
        }

        public virtual bool Validate(Component control, IXFreezable freezable)
        {
            var result = GetResult(control);
            if (control is IXFreezableUnSafe freezableUnsafe)
            {
                freezableUnsafe.Enabled = !result;
            }
            return freezable.BlockInput = result;
        }

        public bool Validate(Component control, Expression<Func<Control, bool>> propertySelector)
        {
            var lambda = (LambdaExpression)propertySelector;
            if (lambda.Body.NodeType != ExpressionType.MemberAccess)
            {
                //TODO: string + customException
                throw new InvalidOperationException("Expression must be a MemberExpression");
            }
            var propertyName = ((MemberExpression)lambda.Body).Member.Name;
            return this.Validate(control, propertyName);
        }
        #endregion

        #region Helpers
        private bool Validate(Component control, string propertyName)
        {
            var result = this.GetResult(control);
            var found = control.GetType().TryGetProperty(propertyName, out var property);
            if (found) //&& property.PropertyType == typeof(bool))
            {
                property.SetValue(control, result);
            }
            return result;
        }
        #endregion
    }
}
