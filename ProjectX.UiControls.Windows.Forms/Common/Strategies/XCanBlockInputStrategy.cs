﻿using System.ComponentModel;

namespace ProjectX.UiControls.Windows.Forms
{
    internal sealed class XCanBlockInputStrategy : XBooleanStrategy, IXConditionStrategy
    {
        public XCanBlockInputStrategy(bool bResult)
            : base(bResult)
        { }

        public override bool Validate(Component control, IXFreezable freezable)
        {
            //TODO: base.Validate??
            return freezable.CanBlockInput = GetResult(control);
        }
    }
}
