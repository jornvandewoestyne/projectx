﻿using ProjectX.General;
using ProjectX.Infrastructure.LogAttributes;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

#region Links
//Check: https://stephenhaunts.com/2014/10/14/using-async-and-await-to-update-the-ui-thread/
//https://www.codeproject.com/Articles/1220062/Display-loading-indicator-in-Windows-Form-app-16
//https://www.codeproject.com/Articles/14841/How-to-write-a-loading-circle-animation-in-NET
//https://stackoverflow.com/questions/18013523/when-correctly-use-task-run-and-when-just-async-await !!!!!!!!!!!!!!!!

//https://codereview.stackexchange.com/questions/151140/creating-multiple-threads-to-process-items-in-a-listview
#endregion

//TODO: summary

//TODO: press "ESC" opens customer????

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// ...
    /// </summary>
    public partial class XChildSearchForm : XChildForm, IXMdiChildSearchBase, IXSearchContainer //TODO: keep IXMdiChildSearchBase?
    {
        #region Fields
        private Ordering _ordering;
        #endregion

        #region Properties
        public Paging Paging { get; set; }
        public SearchString SearchString { get; set; }
        public IXSearchPresenter Presenter { get; }

        public Ordering Ordering
        {
            get => this._ordering ?? this.Presenter.GetOrdering();
            set => this._ordering = value ?? this.Presenter.GetOrdering();
        }
        #endregion

        #region Properties: Controls
        /// <summary>
        /// Control which holds the search string.
        /// </summary>
        public TextBox TextBox => this.searchTextBox;
        public XPictureBox PictureBox => this.mainPictureBox;
        public IXDataPageViewer DataViewer => this.mainDataViewer;
        protected XPanel ButtonsPanel => this.buttonsPanel;
        public XButtonView ViewButton => this.viewButton;
        protected XButtonSearch SearchButton => this.searchButton;
        protected XButtonDelete DeleteButton => this.deleteButton;
        protected XToolStrip ToolStrip => this.mainToolStrip;
        protected XToolStripButtonView ViewToolStripButton => this.viewToolStripButton;
        protected XToolStripButtonSearch SearchToolStripButton => this.searchToolStripButton;
        protected XToolStripButtonDelete DeleteToolStripButton => this.deleteToolStripButton;
        #endregion

        #region Constructors
        protected XChildSearchForm()
            : this(null, null)
        { }

        public XChildSearchForm(XMdiChildDetail detail, IXSearchPresenter presenter)
            : base(detail)
        {
            this.InitializeComponent();
            this.Presenter = presenter;

            //TODO:
            //this.ToolStrip.ImageScalingSize = new System.Drawing.Size(48, 48);
        }
        #endregion

        #region Events
        protected override async void LoadForm(object sender, EventArgs e)
        {
            base.LoadForm(sender, e);
            bool result = await this.SearchAsync(true);
        }

        private async void Search(object sender, EventArgs e)
        {
            bool result = await this.SearchAsync(false);
        }

        private async void ShowEntity(object sender, EventArgs e)
        {
            bool result = await this.ShowEntityAsync(XLib.Preference.ShowNoSelection, XLib.Preference.ShowInstanceIsOpen);
        }

        private void Cancel(object sender, EventArgs e)
        {
            this.Cancel();
        }
        #endregion

        #region Enable/Disable Controls
        //public virtual void SetButtons(bool bResult)
        //{
        //    if (bResult)
        //    {
        //        EnableEditButtons(this.ButtonsPanel, this.ViewButton, this.SearchButton);
        //        EnableEditToolStripButtons(this.ToolStrip, this.ViewToolStripButton, this.SearchToolStripButton);
        //    }
        //    else
        //    {
        //        EnableEditButtons(this.ButtonsPanel, this.SearchButton);
        //        EnableEditToolStripButtons(this.ToolStrip, this.SearchToolStripButton);
        //    }
        //}

        //public virtual void SetPanels(bool bResult)
        //{
        //    //TODO: make extension
        //    this.ButtonsPanel.Enabled = bResult;
        //    this.ToolStrip.Enabled = bResult;
        //}

        //protected void EnableEditButtons(Panel panel, params Button[] buttonsToEnable)
        //{
        //    panel.EnableButtons(buttonsToEnable);
        //}

        //protected void EnableEditToolStripButtons(ToolStrip toolStrip, params ToolStripButton[] buttonsToEnable)
        //{
        //    toolStrip.EnableToolStripButtons(buttonsToEnable);
        //}
        #endregion

        //#region Methods: Search
        //private void SearchOnLoad(XDataListView dataGridView, TextBox textBox, SearchString searchString)
        //{
        //    //TODO: ???
        //    //if (dataGridView.DataSource == null)
        //    //{
        //    //    Search(textBox, searchString);
        //    //}
        //}
        //#endregion

        #region Methods -> ISearchPresenter Members
        //[XIntercept(Strategy = ExceptionStrategy.Default)]
        //public void Cancel()
        //{
        //    //TODO: 
        //    try
        //    {
        //        this.CancellationTokenSource.Cancel(true);
        //    }
        //    catch (ObjectDisposedException)
        //    {
        //        //TODO: show message "click to cancel when running" or completely ignore and do nothing?
        //        //throw;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        [Log]
        public async Task<bool> SearchAsync(bool initialLoad)
        {
            return await this.RunSearchAsync(initialLoad);
        }

        [Log]
        public async Task<bool> ShowEntityAsync(bool showNoSelection, bool showIsOpen)
        {
            return await this.RunShowEntityAsync(showNoSelection, showIsOpen);
        }
        #endregion

        #region Methods: Add to OpenChilds
        protected void AddToOpenChild(XChildForm newForm, string newTitle, XMdiChildDetail newDetail)
        {
            WinFormXMenuUserControlHelper.AddToOpenChild<XMenuUserControl>(this, this.MdiChildDetail, newForm, newTitle, newDetail);
        }

        public Form InstanceIsOpen(EntityKey entityKey)
        {
            return WinFormXMenuUserControlHelper.InstanceIsOpen<XMenuUserControl>(this, this.MdiChildDetail, entityKey);
        }
        #endregion

        #region [OBSOLOTE] Methods: Service/Entity Helpers
        //TODO -> move to presenter?
        //protected Activity GetActivity(MenuGroupAction action)
        //{
        //    var entityKey = action.TryConvertToArray().AsEntityKey();
        //    return this.Service.GetEntity<Activity>(entityKey);
        //}
        #endregion

        #region [OBSOLOTE] Methods: Helpers
        //public XMdiChildDetail GetMdiChildDetail(string title)
        //{
        //    XMdiChildDetail detail = new XMdiChildDetail()
        //    {
        //        Title = title,
        //        Activity = GetActivity(MenuGroupAction.Edit),
        //        FormIdentifier = this.MdiChildDetail.FormIdentifier,
        //        ShowHeader = XLib.Preference.ShowChildHeaderDefault,
        //        ConfirmToClose = XLib.Preference.ConfirmToCloseOnEditForm
        //    };
        //    return detail;
        //}

        //public void SetAnimationImage()
        //{
        //    //Give some delay to avoid flickering:
        //    Task.Delay(20);
        //    this.PictureBox.Image = ProjectX.Infrastructure.Properties.Resources.Animation;
        //}

        //public void ResetAnimationImage()
        //{
        //    //Give some delay to avoid flickering:
        //    Task.Delay(20);
        //    this.UIThread(delegate
        //    {
        //        this.PictureBox.Image = this.Picture;
        //    });
        //    //this.PictureBox.Image = this.Picture;
        //}
        #endregion

        #region DisposableForm
        protected override void DisposeComponents()
        {
            this.components?.Dispose();
            base.DisposeComponents();
            this.PictureBox.Dispose();
            this.DataViewer.Dispose();
            this.Presenter?.Dispose();
        }

        protected override void Unsubscribe()
        {
            base.Unsubscribe();
        }

        protected override void Unbind()
        {
            base.Unbind();
        }
        #endregion

        #region IXLeakable Members
        public override void Subscribe()
        {
            this.Subscribe(Event.Load, this.LoadForm, this);
            this.Subscribe(Event.Click, this.Search, this.SearchButton, this.SearchToolStripButton);
            this.Subscribe(Event.Click, this.ShowEntity, this.ViewButton, this.ViewToolStripButton);
            this.Subscribe(Event.Click, this.Cancel, this.PictureBox);
            this.Subscribe(Event.DoubleClick, this.ShowEntity, this.mainDataViewer);
            base.Subscribe();
        }
        #endregion
    }
}
