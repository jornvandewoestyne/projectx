﻿using ProjectX.Business;
using ProjectX.DTO;
using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using ProjectX.Infrastructure.LogAttributes;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public partial class XMainForm : XBorderlessForm, IXMainForm //XApplicationForm, IXMainForm
    {
        #region Properties
        public IService Service { get; }
        public ApplicationMenuDTO ApplicationMenu { get; set; }

        public List<XModuleUserControl> ModuleControls { get; }
        public List<XMenuUserControl> MenuControls { get; }
        #endregion

        #region Properties: Controls
        public XButton MenuButton => this.menuButton;
        public XPanelMenu MenuPanel => this.menuPanel;
        public XPanelBuffered TitlePanel => this.titlePanel;
        #endregion

        #region Constructors
        public XMainForm()
            : this(null)
        { }

        public XMainForm(IService service)
        {
            InitializeComponent();
            this.Subscribe<IXLeakable>();
            this.Text = AppTitle.ToString();
            this.SetIcon();
            //Initialize ModulePanel:
            this.ModuleControls = new List<XModuleUserControl>();
            //Initialize MenuPanel:
            this.MenuControls = new List<XMenuUserControl>();
            //Set the service (business layer):
            this.Service = service;
        }
        #endregion

        #region Methods: IXMainForm
        [XIntercept(Strategy = General.ExceptionStrategy.Default)]
        public virtual void Quit()
        {
            this.Close();
        }

        public void Show(ApplicationMenuDTO applicationMenu, bool maximize = false)
        {
            this.ApplicationMenu = applicationMenu;
            this.UIInvoke(x => x.ShowForm(maximize));
        }
        #endregion

        #region Events
        //[Log]
        private void LoadForm(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void ShowMenu(object sender, EventArgs e)
        {
            ShowMenu();
        }
        #endregion

        #region Methods: Initialize
        [Log(logOnSuccess: true)]
        private void LoadForm()
        {
            if (this.Service != null)
            {
                //Show some information:
                Splasher.Instance.Show(ApplicationProgressInfo.InitializingTask());
                //Set Main Header:
                SetUserControlMainHeaderValues(this.Text, true);
                //Show Startscreen:
                var form = (XChildForm)CreateInstanceStartScreen(this);
                //Add items to the menu dynamically:
                CreateMenu(this.MenuPanel, this.TitlePanel, form);
                //Collapse all panels of the menu:
                CollapseUserControlPanels(this.MenuPanel);
                //Activate Main Screen:
                SetScreenFormat(true);
            }
        }
        #endregion

        #region Methods: Screen
        private void SetScreenFormat(bool maximize)
        {
            Splasher.Instance.Show(ApplicationProgressInfo.OpeningTask());
            //this.Show(maximize);
            this.ShowMdiContainer(maximize);
        }
        #endregion

        #region Methods: Close Childs
        public void CloseChildForm()
        {
            //non-MDI:
            Form form = (Form)this.childContainer.Controls[XLib.MagicNumber.Index0];
            //var container = this.ParentForm as XMainForm;
            //var controls = container.childContainer.Controls;
            //IXMdiChildFormObject iForm = null;

            //Foreach is not needed = TEST
            //foreach (ContainerControl control in this.childContainer.Controls)
            //{
            //    if (control.Visible && control is Form)
            //    {
            //        form = control as Form;
            //        break;
            //    }
            //}
            //MessageBox.Show("MAIN " + form?.ToString() + " *** " + Form.ActiveForm?.Name);
            if (this.IsMdiContainer)
            {
                form = this.ActiveMdiChild;
            }
            CloseAndRemoveFromOpenChilds(form);
        }

        private void CloseAndRemoveFromOpenChilds(Form form)
        {
            WinFormXMenuUserControlHelper.CloseAndRemoveFromOpenChilds<XMenuUserControl>(form);
        }
        #endregion

        #region Methods: Create/Show StartScreen
        private Form CreateInstanceStartScreen(Form parent)
        {
            return WinFormXHelper.CreateInstanceStartScreen(parent);
        }

        private Form ActivateStartScreen()
        {
            Form form = this;
            return form.ActivateStartScreen();
        }
        #endregion

        #region Methods: Create/Show Connection
        private Form CreateInstanceConnection()
        {
            return WinFormXHelper.CreateInstanceConnection();
        }
        #endregion

        #region Methods: Create Application Menu
        private void CreateMenu(Panel panel, Panel panelOnTop, XChildForm form)
        {
            //Add application menu items dynamically:
            AddUserControlsPanels(this.ApplicationMenu, form, panel);
            //Reorder added usercontrols:
            ReOrderUserControlPanels(panel, panelOnTop);
        }
        #endregion

        #region Methods: UserControl
        private void ShowMenu()
        {
            ActivateStartScreen();
            CollapseUserControlPanels(this.MenuPanel);
        }

        private void SetUserControlMainHeaderValues(string title, bool confirmToClose)
        {
            //this.MainHeader.SetUserControlValues(title, confirmToClose);
        }

        private void CollapseUserControlPanels(Panel groupPanel, Panel excludePanel = null, bool hideAll = true)
        {
            WinFormXMenuUserControlHelper.CollapseUserControlPanels<XMenuUserControl>(groupPanel, excludePanel, hideAll);
        }

        private void AddUserControlsPanels(ApplicationMenuDTO applicationMenu, XChildForm form, Panel panel)
        {
            Splasher.Instance.Show(ApplicationProgressInfo.LoadingModuleTask());
            foreach (var module in applicationMenu.Items)
            {
                var uModule = new XModuleUserControl(module.Key);
                panel.Controls.Add(uModule);
                uModule.Height = uModule.MinimumHeight;
                uModule.Dock = DockStyle.Top;
                //Add control to List:
                this.ModuleControls.Add(uModule);
                var menuItems = module.Value;
                foreach (var menu in menuItems)
                {
                    var uMenu = CreateInstanceMenuUserControl(menu);
                    panel.Controls.Add(uMenu);
                    uMenu.Height = uMenu.MinimumHeight;
                    uMenu.Dock = DockStyle.Top;
                    //Add control to List:
                    this.MenuControls.Add(uMenu);
                    //Add StartScreen to openChilds List:
                    uMenu.AddToOpenChilds(form, form?.Text);
                    uModule.AddUserControls(uMenu);
                }
            }
        }

        private void ReOrderUserControlPanels(Panel panel, Panel panelOnTop)
        {
            //Reorder (reverse order) items: set "Menu" as first item on top:
            panel.Controls.SetChildIndex(panelOnTop, panel.Controls.Count);
            //Reorder (reverse order) usercontrols:
            foreach (Control control in panel.Controls)
            {
                if (control is XMenuUserControl || control is XModuleUserControl)
                {
                    panel.Controls.SetChildIndex(control, XLib.MagicNumber.Index0);
                }
            }
        }

        protected virtual XMenuUserControl CreateInstanceMenuUserControl(Domain.Models.Menu menu)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region IXLeakable Members
        public override void Subscribe()
        {
            this.Subscribe(Event.Load, this.LoadForm, this);
            this.Subscribe(Event.Click, this.ShowMenu, this.MenuButton);
            base.Subscribe();
        }
        #endregion
    }
}
