﻿using System;

namespace ProjectX.UiControls.Windows.Forms
{
    partial class XChildSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XChildSearchForm));
            System.Progress<string> progress_11 = new System.Progress<string>();
            this.searchPanel = new System.Windows.Forms.Panel();
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.buttonsPanel = new XPanel();
            this.searchButton = new XButtonSearch();
            this.deleteButton = new XButtonDelete();
            this.viewButton = new XButtonView();
            this.searchGroupBox = new XGroupBox();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.mainPictureBox = new XPictureBox();
            this.mainDataViewer = new XDataPageViewer();
            this.mainToolStrip = new XToolStrip();
            this.searchToolStripButton = new XToolStripButtonSearch();
            this.viewToolStripButton = new XToolStripButtonView();
            this.deleteToolStripButton = new XToolStripButtonDelete();
            this.searchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.buttonsPanel.SuspendLayout();
            this.searchGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainPictureBox)).BeginInit();
            this.mainToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // childHeaderUserControl
            // 
            this.childHeaderUserControl.Title = "";
            // 
            // searchPanel
            // 
            this.searchPanel.Controls.Add(this.mainSplitContainer);
            this.searchPanel.Controls.Add(this.mainToolStrip);
            this.searchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchPanel.Location = new System.Drawing.Point(0, 0);
            this.searchPanel.Name = "searchPanel";
            this.searchPanel.Size = new System.Drawing.Size(600, 457);
            this.searchPanel.TabIndex = 1;
            this.searchPanel.Tag = "";
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 32);
            this.mainSplitContainer.Name = "mainSplitContainer";
            this.mainSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.buttonsPanel);
            this.mainSplitContainer.Panel1.Controls.Add(this.searchGroupBox);
            this.mainSplitContainer.Panel1MinSize = 105;
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.mainDataViewer);
            this.mainSplitContainer.Size = new System.Drawing.Size(600, 425);
            this.mainSplitContainer.SplitterDistance = 105;
            this.mainSplitContainer.TabIndex = 2;
            // 
            // buttonsPanel
            // 
            this.buttonsPanel.BackColor = System.Drawing.Color.Transparent;
            this.buttonsPanel.Controls.Add(this.searchButton);
            this.buttonsPanel.Controls.Add(this.deleteButton);
            this.buttonsPanel.Controls.Add(this.viewButton);
            this.buttonsPanel.Location = new System.Drawing.Point(15, 14);
            this.buttonsPanel.Name = "buttonsPanel";
            this.buttonsPanel.Size = new System.Drawing.Size(80, 82);
            this.buttonsPanel.TabIndex = 10;
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(0, 58);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(80, 23);
            this.searchButton.TabIndex = 1;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            // 
            // deleteButton
            // 
            this.deleteButton.Enabled = false;
            this.deleteButton.Location = new System.Drawing.Point(0, 0);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(80, 23);
            this.deleteButton.TabIndex = 3;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            // 
            // viewButton
            // 
            this.viewButton.Enabled = false;
            this.viewButton.Location = new System.Drawing.Point(0, 29);
            this.viewButton.Name = "viewButton";
            this.viewButton.Size = new System.Drawing.Size(80, 23);
            this.viewButton.TabIndex = 2;
            this.viewButton.Text = "Edit";
            this.viewButton.UseVisualStyleBackColor = true;
            // 
            // searchGroupBox
            // 
            this.searchGroupBox.Controls.Add(this.searchTextBox);
            this.searchGroupBox.Controls.Add(this.mainPictureBox);
            this.searchGroupBox.Location = new System.Drawing.Point(110, 10);
            this.searchGroupBox.Name = "searchGroupBox";
            this.searchGroupBox.Size = new System.Drawing.Size(370, 85);
            this.searchGroupBox.TabIndex = 0;
            this.searchGroupBox.TabStop = false;
            this.searchGroupBox.Text = "Zoeken";
            // 
            // searchTextBox
            // 
            this.searchTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.searchTextBox.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.searchTextBox.Location = new System.Drawing.Point(81, 20);
            this.searchTextBox.Multiline = true;
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(274, 50);
            this.searchTextBox.TabIndex = 0;
            // 
            // mainPictureBox
            // 
            this.mainPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.mainPictureBox.Location = new System.Drawing.Point(15, 20);
            this.mainPictureBox.Name = "mainPictureBox";
            this.mainPictureBox.Size = new System.Drawing.Size(50, 50);
            this.mainPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.mainPictureBox.TabIndex = 0;
            this.mainPictureBox.TabStop = false;
            // 
            // mainDataViewer
            // 
            this.mainDataViewer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mainDataViewer.BackColor = System.Drawing.Color.LightSteelBlue;
            this.mainDataViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainDataViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDataViewer.Location = new System.Drawing.Point(0, 0);
            this.mainDataViewer.Margin = new System.Windows.Forms.Padding(0);
            this.mainDataViewer.Name = "mainDataViewer";
            this.mainDataViewer.Padding = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.mainDataViewer.Size = new System.Drawing.Size(600, 316);
            this.mainDataViewer.TabIndex = 0;
            this.mainDataViewer.Work = ProjectX.General.Work.None;
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.AutoSize = false;
            this.mainToolStrip.BackColor = System.Drawing.Color.White;
            this.mainToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.mainToolStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.searchToolStripButton,
            this.viewToolStripButton,
            this.deleteToolStripButton});
            this.mainToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.mainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Padding = new System.Windows.Forms.Padding(5, 0, 1, 0);
            this.mainToolStrip.Size = new System.Drawing.Size(600, 32);
            this.mainToolStrip.TabIndex = 2;
            // 
            // searchToolStripButton
            // 
            this.searchToolStripButton.AutoSize = false;
            this.searchToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.searchToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("searchToolStripButton.Image")));
            this.searchToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.searchToolStripButton.Name = "searchToolStripButton";
            this.searchToolStripButton.Size = new System.Drawing.Size(30, 30);
            this.searchToolStripButton.Text = "Search";
            this.searchToolStripButton.Work = ProjectX.General.Work.Search;
            // 
            // viewToolStripButton
            // 
            this.viewToolStripButton.AutoSize = false;
            this.viewToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.viewToolStripButton.Enabled = false;
            this.viewToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("viewToolStripButton.Image")));
            this.viewToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.viewToolStripButton.Name = "viewToolStripButton";
            this.viewToolStripButton.Size = new System.Drawing.Size(30, 30);
            this.viewToolStripButton.Text = "Edit";
            this.viewToolStripButton.Work = ProjectX.General.Work.View;
            // 
            // deleteToolStripButton
            // 
            this.deleteToolStripButton.AutoSize = false;
            this.deleteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteToolStripButton.Enabled = false;
            this.deleteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("deleteToolStripButton.Image")));
            this.deleteToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.deleteToolStripButton.Name = "deleteToolStripButton";
            this.deleteToolStripButton.Size = new System.Drawing.Size(30, 30);
            this.deleteToolStripButton.Text = "Delete";
            this.deleteToolStripButton.Work = ProjectX.General.Work.Delete;
            // 
            // XChildSearchForm
            // 
            this.AcceptButton = this.searchButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(600, 480);
            this.Controls.Add(this.searchPanel);
            this.Name = "XChildSearchForm";
            this.ProgressManager = progress_11;
            this.Tag = "";
            this.Text = "Null";
            this.Controls.SetChildIndex(this.childHeaderUserControl, 0);
            this.Controls.SetChildIndex(this.searchPanel, 0);
            this.searchPanel.ResumeLayout(false);
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.buttonsPanel.ResumeLayout(false);
            this.searchGroupBox.ResumeLayout(false);
            this.searchGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainPictureBox)).EndInit();
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected XPictureBox mainPictureBox;
        protected System.Windows.Forms.Panel searchPanel; //TODO: don't use XPanel (IXLockable) for now => locks the .gif animation!!!
        protected System.Windows.Forms.SplitContainer mainSplitContainer;
        protected XGroupBox searchGroupBox;
        protected System.Windows.Forms.TextBox searchTextBox;
        protected XDataPageViewer mainDataViewer;
        protected XPanel buttonsPanel;
        protected XButtonSearch searchButton;
        protected XButtonDelete deleteButton;
        protected XButtonView viewButton;
        protected XToolStrip mainToolStrip;
        protected XToolStripButtonView viewToolStripButton;
        protected XToolStripButtonDelete deleteToolStripButton;
        protected XToolStripButtonSearch searchToolStripButton;

    }
}