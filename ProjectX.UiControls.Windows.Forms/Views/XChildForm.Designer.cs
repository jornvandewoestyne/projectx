﻿namespace ProjectX.UiControls.Windows.Forms
{
    partial class XChildForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        ///// <summary>
        ///// Clean up any resources being used.
        ///// </summary>
        ///// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.childHeaderUserControl = new ProjectX.UiControls.Windows.Forms.XChildHeaderUserControl();
            this.xInformerUserControl = new ProjectX.UiControls.Windows.Forms.XInformerUserControl();
            this.SuspendLayout();
            // 
            // childHeaderUserControl
            // 
            this.childHeaderUserControl.BackColor = System.Drawing.Color.White;
            this.childHeaderUserControl.ConfirmToClose = false;
            this.childHeaderUserControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.childHeaderUserControl.Location = new System.Drawing.Point(0, 0);
            this.childHeaderUserControl.Name = "childHeaderUserControl";
            this.childHeaderUserControl.Size = new System.Drawing.Size(284, 25);
            this.childHeaderUserControl.TabIndex = 3;
            this.childHeaderUserControl.Title = null;
            this.childHeaderUserControl.Work = ProjectX.General.Work.None;
            // 
            // xInformerUserControl
            // 
            this.xInformerUserControl.BackColor = System.Drawing.Color.White;
            this.xInformerUserControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.xInformerUserControl.Location = new System.Drawing.Point(0, 238);
            this.xInformerUserControl.Name = "xInformerUserControl";
            this.xInformerUserControl.Padding = new System.Windows.Forms.Padding(3);
            this.xInformerUserControl.Size = new System.Drawing.Size(284, 23);
            this.xInformerUserControl.TabIndex = 4;
            // 
            // XChildForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.childHeaderUserControl);
            this.Controls.Add(this.xInformerUserControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "XChildForm";
            this.ShowInTaskbar = false;
            this.ResumeLayout(false);

        }

        #endregion

        protected XChildHeaderUserControl childHeaderUserControl;
        private XInformerUserControl xInformerUserControl;
    }
}