﻿
namespace ProjectX.UiControls.Windows.Forms
{
    partial class XMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XMainForm));
            this.titlePanel = new XPanelBuffered();
            this.menuButton = new XButton();
            this.menuImageList = new System.Windows.Forms.ImageList(this.components);
            this.menuPanel = new XPanelMenu();
            this.childContainer = new XPanel();
            this.titlePanel.SuspendLayout();
            this.menuPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // titlePanel
            // 
            this.titlePanel.BackColor = System.Drawing.Color.SlateGray;
            this.titlePanel.Controls.Add(this.menuButton);
            this.titlePanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.titlePanel.Location = new System.Drawing.Point(0, 0);
            this.titlePanel.Name = "titlePanel";
            this.titlePanel.Size = new System.Drawing.Size(177, 38);
            this.titlePanel.TabIndex = 4;
            // 
            // menuButton
            // 
            this.menuButton.BackColor = System.Drawing.Color.CornflowerBlue;
            this.menuButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuButton.FlatAppearance.BorderColor = System.Drawing.Color.CornflowerBlue;
            this.menuButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.menuButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
            this.menuButton.ForeColor = System.Drawing.Color.White;
            this.menuButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.menuButton.ImageKey = "Menu";
            this.menuButton.ImageList = this.menuImageList;
            this.menuButton.Location = new System.Drawing.Point(0, 0);
            this.menuButton.Name = "menuButton";
            this.menuButton.Padding = new System.Windows.Forms.Padding(2, 0, 5, 0);
            this.menuButton.Size = new System.Drawing.Size(177, 38);
            this.menuButton.TabIndex = 5;
            this.menuButton.Text = "Menu";
            this.menuButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.menuButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.menuButton.UseVisualStyleBackColor = false;
            // 
            // menuImageList
            // 
            this.menuImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("menuImageList.ImageStream")));
            this.menuImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.menuImageList.Images.SetKeyName(0, "Menu");
            // 
            // menuPanel
            // 
            this.menuPanel.AutoScroll = true;
            this.menuPanel.BackColor = System.Drawing.Color.White;
            this.menuPanel.Controls.Add(this.titlePanel);
            this.menuPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuPanel.Location = new System.Drawing.Point(0, 70);
            this.menuPanel.Name = "menuPanel";
            this.menuPanel.Padding = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.menuPanel.Size = new System.Drawing.Size(178, 491);
            this.menuPanel.TabIndex = 4;
            // 
            // childContainer
            // 
            this.childContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.childContainer.Location = new System.Drawing.Point(178, 70);
            this.childContainer.Margin = new System.Windows.Forms.Padding(0);
            this.childContainer.Name = "childContainer";
            this.childContainer.Size = new System.Drawing.Size(1006, 491);
            this.childContainer.TabIndex = 8;
            // 
            // XMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1184, 561);
            this.ControlBox = false;
            this.Controls.Add(this.childContainer);
            this.Controls.Add(this.menuPanel);
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "XMainForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Controls.SetChildIndex(this.menuPanel, 0);
            this.Controls.SetChildIndex(this.childContainer, 0);
            this.titlePanel.ResumeLayout(false);
            this.menuPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private XPanelBuffered titlePanel;
        private System.Windows.Forms.ImageList menuImageList;
        private XPanelMenu menuPanel;
        private XButton menuButton;
        public XPanel childContainer;
    }
}

