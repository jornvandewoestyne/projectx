﻿namespace ProjectX.UiControls.Windows.Forms
{
    partial class ConnectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConnectionForm));
            this.confirmButton = new ProjectX.UiControls.Windows.Forms.XButtonConfirm();
            this.cancelButton = new ProjectX.UiControls.Windows.Forms.XButtonCancel();
            this.connectionsListView = new System.Windows.Forms.ListView();
            this.connectionsListViewValueColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.formImageList = new System.Windows.Forms.ImageList(this.components);
            this.connectionsSplitContainer = new System.Windows.Forms.SplitContainer();
            this.detailPropertyGrid = new System.Windows.Forms.PropertyGrid();
            this.infoPanel = new XPanel();
            this.infoLabel = new XLabel();
            ((System.ComponentModel.ISupportInitialize)(this.connectionsSplitContainer)).BeginInit();
            this.connectionsSplitContainer.Panel1.SuspendLayout();
            this.connectionsSplitContainer.Panel2.SuspendLayout();
            this.connectionsSplitContainer.SuspendLayout();
            this.infoPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // confirmButton
            // 
            this.confirmButton.Location = new System.Drawing.Point(182, 275);
            this.confirmButton.Size = new System.Drawing.Size(75, 23);
            this.confirmButton.TabIndex = 2;
            this.confirmButton.UseVisualStyleBackColor = true;
            this.confirmButton.Click += new System.EventHandler(this.UpdateDbSettings);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(263, 275);
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.Cancel);
            // 
            // connectionsListView
            // 
            this.connectionsListView.BackColor = System.Drawing.Color.White;
            this.connectionsListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.connectionsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.connectionsListViewValueColumnHeader});
            this.connectionsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.connectionsListView.FullRowSelect = true;
            this.connectionsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.connectionsListView.HideSelection = false;
            this.connectionsListView.Location = new System.Drawing.Point(0, 1);
            this.connectionsListView.MultiSelect = false;
            this.connectionsListView.Name = "connectionsListView";
            this.connectionsListView.ShowItemToolTips = true;
            this.connectionsListView.Size = new System.Drawing.Size(350, 109);
            this.connectionsListView.TabIndex = 0;
            this.connectionsListView.UseCompatibleStateImageBehavior = false;
            this.connectionsListView.View = System.Windows.Forms.View.Details;
            this.connectionsListView.SelectedIndexChanged += new System.EventHandler(this.ShowDetails);
            // 
            // connectionsListViewValueColumnHeader
            // 
            this.connectionsListViewValueColumnHeader.Text = "Connecties";
            this.connectionsListViewValueColumnHeader.Width = 257;
            // 
            // formImageList
            // 
            this.formImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("formImageList.ImageStream")));
            this.formImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.formImageList.Images.SetKeyName(0, "Info");
            this.formImageList.Images.SetKeyName(1, "Database");
            // 
            // connectionsSplitContainer
            // 
            this.connectionsSplitContainer.BackColor = System.Drawing.SystemColors.Control;
            this.connectionsSplitContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.connectionsSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.connectionsSplitContainer.IsSplitterFixed = true;
            this.connectionsSplitContainer.Location = new System.Drawing.Point(0, 50);
            this.connectionsSplitContainer.Name = "connectionsSplitContainer";
            this.connectionsSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // connectionsSplitContainer.Panel1
            // 
            this.connectionsSplitContainer.Panel1.AutoScroll = true;
            this.connectionsSplitContainer.Panel1.Controls.Add(this.connectionsListView);
            this.connectionsSplitContainer.Panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.connectionsSplitContainer.Panel1.Padding = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.connectionsSplitContainer.Panel1MinSize = 110;
            // 
            // connectionsSplitContainer.Panel2
            // 
            this.connectionsSplitContainer.Panel2.AutoScroll = true;
            this.connectionsSplitContainer.Panel2.Controls.Add(this.detailPropertyGrid);
            this.connectionsSplitContainer.Panel2.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.connectionsSplitContainer.Panel2MinSize = 90;
            this.connectionsSplitContainer.Size = new System.Drawing.Size(350, 210);
            this.connectionsSplitContainer.SplitterDistance = 110;
            this.connectionsSplitContainer.SplitterWidth = 1;
            this.connectionsSplitContainer.TabIndex = 99;
            this.connectionsSplitContainer.TabStop = false;
            // 
            // detailPropertyGrid
            // 
            this.detailPropertyGrid.BackColor = System.Drawing.Color.White;
            this.detailPropertyGrid.CommandsBorderColor = System.Drawing.Color.LightGray;
            this.detailPropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailPropertyGrid.HelpBorderColor = System.Drawing.Color.White;
            this.detailPropertyGrid.HelpVisible = false;
            this.detailPropertyGrid.LineColor = System.Drawing.SystemColors.ControlDark;
            this.detailPropertyGrid.Location = new System.Drawing.Point(0, 0);
            this.detailPropertyGrid.Name = "detailPropertyGrid";
            this.detailPropertyGrid.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.detailPropertyGrid.Size = new System.Drawing.Size(350, 98);
            this.detailPropertyGrid.TabIndex = 1;
            this.detailPropertyGrid.TabStop = false;
            this.detailPropertyGrid.ViewBorderColor = System.Drawing.Color.White;
            // 
            // infoPanel
            // 
            this.infoPanel.BackColor = System.Drawing.Color.White;
            this.infoPanel.Controls.Add(this.infoLabel);
            this.infoPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoPanel.Location = new System.Drawing.Point(0, 0);
            this.infoPanel.Name = "infoPanel";
            this.infoPanel.Size = new System.Drawing.Size(350, 50);
            this.infoPanel.TabIndex = 99;
            // 
            // infoLabel
            // 
            this.infoLabel.BackColor = System.Drawing.Color.White;
            this.infoLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infoLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.infoLabel.ImageKey = "Database";
            this.infoLabel.ImageList = this.formImageList;
            this.infoLabel.Location = new System.Drawing.Point(0, 0);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.infoLabel.Size = new System.Drawing.Size(350, 50);
            this.infoLabel.TabIndex = 99;
            this.infoLabel.Text = "Kies een geldige connectie uit de lijst.";
            this.infoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ConnectionForm
            // 
            this.AcceptButton = this.confirmButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(350, 310);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.confirmButton);
            this.Controls.Add(this.connectionsSplitContainer);
            this.Controls.Add(this.infoPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConnectionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConnectionForm";
            this.TopMost = true;
            this.XControlText = ProjectX.General.FriendlyTextType.ConnectionManagement;
            this.Load += new System.EventHandler(this.LoadForm);
            this.connectionsSplitContainer.Panel1.ResumeLayout(false);
            this.connectionsSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.connectionsSplitContainer)).EndInit();
            this.connectionsSplitContainer.ResumeLayout(false);
            this.infoPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ProjectX.UiControls.Windows.Forms.XButtonConfirm confirmButton;
        private ProjectX.UiControls.Windows.Forms.XButtonCancel cancelButton;
        private System.Windows.Forms.ListView connectionsListView;
        private System.Windows.Forms.ColumnHeader connectionsListViewValueColumnHeader;
        private System.Windows.Forms.ImageList formImageList;
        private System.Windows.Forms.SplitContainer connectionsSplitContainer;
        private System.Windows.Forms.PropertyGrid detailPropertyGrid;
        private XPanel infoPanel;
        private XLabel infoLabel;
    }
}