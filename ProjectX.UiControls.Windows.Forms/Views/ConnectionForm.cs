﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using ProjectX.Infrastructure.LogAttributes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public partial class ConnectionForm : XApplicationForm
    {
        #region Fields
        ConnectionStringSettings _setting = null;
        #endregion

        #region Properties
        private ConnectionStringSettings Setting
        {
            get { return this._setting; }
            set { this._setting = value; }
        }

        private string Info
        {
            get { return this.Label.Text; }
        }
        #endregion

        #region Properties: Controls
        private XLabel Label
        {
            get { return this.infoLabel; }
        }

        private ListView ListView
        {
            get { return this.connectionsListView; }
        }

        private PropertyGrid PropertyGrid
        {
            get { return this.detailPropertyGrid; }
        }
        #endregion

        #region Constructor
        public ConnectionForm()
        {
            InitializeComponent();
        }
        #endregion

        #region Events
        private void LoadForm(object sender, EventArgs e)
        {
            FillListview(this.ListView, SettingsHelper.GetConnectionStrings());
        }

        private void ShowDetails(object sender, EventArgs e)
        {
            ShowDetails(this.ListView);
        }

        private void UpdateDbSettings(object sender, EventArgs e)
        {
            UpdateDbSettings();
        }

        private void Cancel(object sender, EventArgs e)
        {
            CancelConnectionRestore();
        }
        #endregion

        #region Methods
        private void FillListview(ListView listview, List<ConnectionStringSettings> list)
        {
            listview.Items.Clear();
            foreach (var item in list)
            {
                var listItem = new ListViewItem(item.Name);
                listItem.Tag = item;
                listview.Items.Add(listItem);
            }
        }

        private void ShowDetails(ListView listview)
        {
            try
            {
                if (listview.SelectedItems.Count > XLib.MagicNumber.IntZero)
                {
                    this.Setting = (ConnectionStringSettings)listview.SelectedItems[XLib.MagicNumber.Index0].Tag;
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                this.Setting = null;
            }
            finally
            {
                ShowConnectionDetail(this.Setting);
            }
        }

        private void ShowConnectionDetail(ConnectionStringSettings setting)
        {
            ConnectionDetail detail = SetConnectionDetail(setting);
            this.PropertyGrid.SelectedObject = detail;
        }

        [Log]
        private void UpdateDbSettings()
        {
            //Splasher.Show();
            if (this.Setting != null)
            {
                using (this)
                {
                    var success = SettingsHelper.UpdateDbSettings(this.Setting.Name,
                        AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
                    //TODO:
                    //NinjectCompositionRoot.Get<IService>().TestConnection();
                    if (success)
                    {
                        this.DialogResult = DialogResult.OK;
                        throw new LogSuccessException();
                    }
                    //TODO: test which exceptions can occur on UpdateSettings = false
                    else
                    {
                        //TODO: FriendlyMessage
                        var message = StringHelper.TryFormat("{1} {0}{0}{2} {0}{0}{3}",
                            Environment.NewLine,
                            ApplicationMessage.SettingsAlert().Message,
                            ApplicationMessage.ContactAdministratorAdvice().Message,
                            ApplicationMessage.RetryAlert().Message);
                        throw new FatalSqlException(message, new Exception());
                    }
                }
            }
            else
            {
                var message = ApplicationMessage.GenericMessage(this.Info);
                XMessenger.ShowWarning(message);
                return;
            }
        }

        private ConnectionDetail SetConnectionDetail(ConnectionStringSettings setting)
        {
            var detail = new ConnectionDetail();
            detail.Name = setting.Name;
            detail.ProviderName = setting.ProviderName;
            detail.ConnectionString = setting.ConnectionString;
            return detail;
        }

        private void CancelConnectionRestore()
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion
    }
}
