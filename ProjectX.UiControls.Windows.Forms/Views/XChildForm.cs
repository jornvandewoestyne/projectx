﻿using ProjectX.Business;
using ProjectX.General;
using System;
using System.ComponentModel;
using System.Drawing;

//TODO: summary

namespace ProjectX.UiControls.Windows.Forms
{
    public partial class XChildForm : XApplicationForm, IXMdiChildFormObject
    {
        private Progress<string> _progressManager;

        public Progress<string> ProgressManager
        {
            get => this._progressManager.ResolveNull();
            set => this._progressManager = value;
        }

        #region Properties -> IMdiChildFormObject Members
        [Browsable(false)]
        public XMdiChildDetail MdiChildDetail { get; private set; }

        //TODO
        [Browsable(false)]
        public IService Service => new XDI().Get<XServiceProvider>().Instance.Service;

        [Browsable(false)]
        public EntityKey EntityKey => GetEntityKey();

        [Browsable(false)]
        public string EntityFullName => GetEntityFullName();

        [Browsable(true), Description("Holds the picture of the form.")]
        public Bitmap Picture { get; set; }
        #endregion

        #region Properties: Controls
        public XChildHeaderUserControl ChildHeader => this.childHeaderUserControl;
        public XInformerUserControl Informer => this.xInformerUserControl;
        #endregion

        #region Constructors
        protected XChildForm()
            : this(null)
        { }

        public XChildForm(XMdiChildDetail detail)
        {
            InitializeComponent();
            this.MdiChildDetail = detail;
        }
        #endregion

        #region Events
        protected virtual void LoadForm(object sender, System.EventArgs e)
        {
            SetUserControlValues(this.Text, this.MdiChildDetail);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the primary key(s) of a given entity.
        /// </summary>
        /// <returns></returns>
        protected virtual EntityKey GetEntityKey()
        {
            return null;
        }

        /// <summary>
        /// Returns the FullName of a given entity.
        /// </summary>
        /// <returns></returns>
        protected virtual string GetEntityFullName()
        {
            return null;
        }
        #endregion

        #region Methods: -> IMdiChildFormObject Members
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="title"></param>
        /// <param name="detail"></param>
        public virtual void SetUserControlValues(string title, XMdiChildDetail detail)
        {
            this.ChildHeader.SetUserControlValues(title, detail);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="title"></param>
        /// <param name="confirmToClose"></param>
        /// <param name="showHeader"></param>
        public virtual void SetUserControlValues(string title, bool confirmToClose, bool showHeader)
        {
            this.ChildHeader.SetUserControlValues(title, confirmToClose, showHeader);
        }
        #endregion

        #region IXLeakable Members
        public override void Subscribe()
        {
            this.Subscribe(Event.Load, this.LoadForm, this);
            base.Subscribe();
        }
        #endregion

        #region DisposableForm
        protected override void DisposeComponents()
        {
            this.components?.Dispose();
            base.DisposeComponents();

            this.Service?.Dispose();
            this.Picture?.Dispose();
            this.ChildHeader?.Dispose();
            this.CancellationTokenSource?.Dispose();
            this.MdiChildDetail?.Dispose();
        }

        protected override void Unsubscribe()
        {
            base.Unsubscribe();
        }

        protected override void Unbind()
        {
            this.Picture = null;
            this.childHeaderUserControl = null;
            this.CancellationTokenSource = null;
            this.MdiChildDetail = null;
            base.Unbind();
        }
        #endregion
    }
}
