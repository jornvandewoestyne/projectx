using ProjectX.General;
using System;
using System.Windows.Forms;

#region Source
/// <summary>
/// SOURCE: http://www.codeproject.com/Articles/3542/How-to-do-Application-Initialization-while-showing
/// </summary>
#endregion

namespace ProjectX.UiControls.Windows.Forms
{
    //TODO: Make ApplicationTitle dynamic... (load value from app.config?)
    public class SplashForm : XApplicationForm
    {
        #region Properties
        private string OriginalTitle { get; set; }
        private string Info { get; set; } = XLib.Text.DefaultInfo;
        private string DefaultInfo { get; } = XLib.Text.DefaultInfo;

        /// <summary>
        /// Holds the information shown to the user.
        /// </summary>
        public string StatusInfo
        {
            get { return this.Info; }
            set
            {
                if (value.IsEmptyString())
                {
                    value = DefaultInfo;
                }
                this.Info = value;
                ChangeStatusText();
            }
        }
        #endregion

        #region Properties: Controls
        public XLabel TitleLabel => this.titleLabel;
        public XLabel InfoLabel => this.infoLabel;
        public XPictureBox PictureBox => this.applicationPictureBox;
        #endregion

        #region Constructor
        public SplashForm(string text, string title)
        {
            InitializeComponent();
            this.Subscribe<IXLeakable>();
            this.SetUserControlValues();
            //Make sure the form is not visible (Hide + Opacity):
            this.Hide();
            this.SetOpacityHide();
            this.OriginalTitle = this.Text;
            this.StatusInfo = text;
            if (!title.IsEmptyString())
            {
                this.Text = title;
            }
        }
        #endregion

        #region Overrides
        public override void Subscribe()
        {
            this.Subscribe(MouseEvent.MouseDown, this.OnStartDrag, this.InfoLabel, this.TitleLabel, this.PictureBox);
            base.Subscribe();
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            this.CenterToScreen();
        }
        #endregion

        #region Methods: Initialize
        private void SetUserControlValues()
        {
            this.ShowOnTop();
            this.OriginalTitle = this.Text;
            this.TitleLabel.Text = this.AppTitle.ToString();
            this.SetIcon();
            this.animationPictureBox.Image = ProjectX.Infrastructure.Properties.Resources.Animation;
            this.applicationPictureBox.Image = ProjectX.Infrastructure.Properties.Resources.StartScreen;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Method added to reset the default title
        /// </summary>
        public void ResetTitle()
        {
            this.Text = this.OriginalTitle;
        }

        [XIntercept(Strategy = ExceptionStrategy.Default)]
        public void SetOpacityVisible()
        {
            this.Opacity = 100;
            this.SetIcon();
            this.animationPictureBox.Image = ProjectX.Infrastructure.Properties.Resources.Animation;
            this.applicationPictureBox.Image = ProjectX.Infrastructure.Properties.Resources.StartScreen;
        }

        [XIntercept(Strategy = ExceptionStrategy.Default)]
        public void SetOpacityHide()
        {
            this.Opacity = XLib.MagicNumber.IntZero;
            this.Icon?.Dispose();
            this.animationPictureBox.Image?.Dispose();
            this.applicationPictureBox.Image?.Dispose();
            this.animationPictureBox.Image = null;
            this.applicationPictureBox.Image = null;
        }

        public void OnStartDrag(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if ((e.Clicks == 1))
                {
                    NativeMethods.ReleaseCapture();
                    NativeMethods.SendMessage(this.Handle, (int)WindowMessages.WM_NCLBUTTONDOWN, (int)HitTestValues.HTCAPTION, 0);
                }
            }
        }

        private void ChangeStatusText()
        {
            this.InfoLabel.UIInvoke(x => x.Text = this.StatusInfo);
            this.UIInvoke(x => x.Update());
        }

        [XIntercept(Strategy = ExceptionStrategy.Default)]
        protected override bool ProcessDialogKey(Keys keyData)
        {
            // Handle or Ignore/block the "Esc" key:
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                Splasher.Instance.HandleEscKey();
                return true;
            }
            // Ignore/block the "Alt + F4" combination:
            else if (keyData == (Keys.Alt | Keys.F4))
            {
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            this.DisposeIcon();
            this.animationPictureBox?.Dispose();
            this.applicationPictureBox?.Dispose();
            this.animationPictureBox = null;
            this.applicationPictureBox = null;
            base.Dispose(disposing);
        }
        #endregion

        #region Windows Form Designer: Controls - DO NOT MODIFY! (unless you know what you are doing)
        private XPictureBox animationPictureBox;
        private XPictureBox applicationPictureBox;
        private XLabel infoLabel;
        private XLabel titleLabel;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        #endregion

        #region Windows Form Designer generated code - DO NOT MODIFY! (unless you know what you are doing)
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.animationPictureBox = new XPictureBox();
            this.titleLabel = new XLabel();
            this.infoLabel = new XLabel();
            this.applicationPictureBox = new XPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.animationPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // animationPictureBox
            // 
            this.animationPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.animationPictureBox.Location = new System.Drawing.Point(10, 203);
            this.animationPictureBox.Name = "animationPictureBox";
            this.animationPictureBox.Size = new System.Drawing.Size(25, 25);
            this.animationPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.animationPictureBox.TabIndex = 4;
            this.animationPictureBox.TabStop = false;
            // 
            // titleLabel
            // 
            this.titleLabel.BackColor = System.Drawing.Color.Transparent;
            this.titleLabel.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.ForeColor = System.Drawing.Color.Gray;
            this.titleLabel.Location = new System.Drawing.Point(97, 21);
            this.titleLabel.Margin = new System.Windows.Forms.Padding(0);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(120, 30);
            this.titleLabel.TabIndex = 2;
            this.titleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.titleLabel.UseWaitCursor = true;
            // 
            // infoLabel
            // 
            this.infoLabel.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.infoLabel.ForeColor = System.Drawing.Color.Gray;
            this.infoLabel.Location = new System.Drawing.Point(0, 200);
            this.infoLabel.Margin = new System.Windows.Forms.Padding(0);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.infoLabel.Size = new System.Drawing.Size(300, 30);
            this.infoLabel.TabIndex = 1;
            this.infoLabel.Text = "...";
            this.infoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // applicationPictureBox
            // 
            this.applicationPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.applicationPictureBox.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.applicationPictureBox.Location = new System.Drawing.Point(0, 0);
            this.applicationPictureBox.Margin = new System.Windows.Forms.Padding(0);
            this.applicationPictureBox.Name = "applicationPictureBox";
            this.applicationPictureBox.Size = new System.Drawing.Size(300, 200);
            this.applicationPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.applicationPictureBox.TabIndex = 0;
            this.applicationPictureBox.TabStop = false;
            // 
            // SplashForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(300, 230);
            this.Controls.Add(this.animationPictureBox);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.infoLabel);
            this.Controls.Add(this.applicationPictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SplashForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.animationPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationPictureBox)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
    }
}
