﻿using ProjectX.Business;
using ProjectX.General;
using System;

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// ...
    /// </summary>
    public class XServiceProvider : DisposableObject, IDisposable
    {
        #region Properties
        public IService Service { get; private set; }
        #endregion

        #region Constructors
        public XServiceProvider(IService service)
        {
            this.Service = service;
        }
        #endregion

        #region Methods: DisposableObject
        public override void Dispose(bool disposing)
        {
            if (disposing && !IsDisposed)
            {
                this.Service?.Dispose();
                this.Service = null;
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
