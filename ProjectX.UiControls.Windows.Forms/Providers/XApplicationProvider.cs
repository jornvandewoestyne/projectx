﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using ProjectX.Infrastructure.MessageService;
using ProjectX.Plugin.Dependencies;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class XApplicationProvider
    {
        #region Singleton
        private static readonly Lazy<XApplicationProvider> _provider = new Lazy<XApplicationProvider>(() => new XApplicationProvider());

        public static XApplicationProvider Instance { get { return _provider.Value; } }

        private XApplicationProvider()
        { }
        #endregion

        #region Methods:
        public static ITranslator SetTranslator()
        {
            return new TranslationCollection().Get();
        }

        public static async Task<ITranslator> SetTranslatorAsync(CancellationToken cancellationToken)
        {
            return await new TranslationCollection().GetAsync(cancellationToken);
        }

        public static void SetDependencies()
        {
            //REQUIRED: set the default and/or required modules
            new DependencySetter().UseWinForms(new MessageBoxType<XMessageBoxPlus>().DependencyType);
        }

        public static T Start<T>() where T : IXMainForm
        {
            var dependency = new XDI().Get<T>();
            var form = dependency.Instance;
            XApplication.SetMainForm(form);
            XApplication.SetApplicationContext(form);
            return form;
        }
        #endregion
    }
}

