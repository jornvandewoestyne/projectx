﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XLabel : Label, IXFreezable, IXLeakable
    {
        #region IXLeakable Members
        [Browsable(false)]
        public EventCollection EventCollection { get; set; } = new EventCollection();

        public virtual void Subscribe() { }
        #endregion

        #region IXFreezable Members
        [Browsable(false), DefaultValue(false)]
        public bool BlockInput { get; set; }

        [Browsable(false), DefaultValue(true)]
        public bool CanBlockInput { get; set; } = true;

        public virtual void Validate(IXFreezable freezable, IXConditionStrategy strategy)
        {
            strategy?.Validate(this, freezable);
        }

        public override Cursor Cursor
        {
            get
            {
                if (this.CanBlockInput)
                {
                    return base.Cursor;
                }
                return Cursors.Default;
            }
            set
            {
                base.Cursor = value;
            }
        }

        protected override void WndProc(ref Message m)
        {
            var block = this.BlockInput(ref m);
            if (!block)
            {
                base.WndProc(ref m);
            }
        }
        #endregion

        #region IDisposable
        protected override void Dispose(bool disposing)
        {
            //this.ToolTip?.Dispose();
            this.EventCollection?.Dispose();
            base.Dispose(disposing);
        }
        #endregion

        public XLabel()
        {
            ImageAlign = ContentAlignment.MiddleLeft;
            //this.SetStyle(ControlStyles.UserPaint |
            //            ControlStyles.AllPaintingInWmPaint |
            //            ControlStyles.OptimizedDoubleBuffer,
            //            true);

            //this.Image = ProjectX.Infrastructure.Properties.Resources.Customer.ToBitmap();
        }

        private Image _image;
        public new Image Image
        {
            get { return _image; }

            set
            {
                const int spacing = 4;

                if (_image != null)
                    Padding = new Padding(Padding.Left - spacing - _image.Width, Padding.Top, Padding.Right, Padding.Bottom);

                if (value != null)
                    Padding = new Padding(Padding.Left + spacing + value.Width, Padding.Top, Padding.Right, Padding.Bottom);

                _image = value;
            }
        }

        //protected override void OnResize(EventArgs e)
        //{
        //    if (this.Visible) { this.Refresh(); }
        //    //base.OnResize(e);
        //}

        protected override void OnPaint(PaintEventArgs e)
        {
            if (Image != null)
            {
                Rectangle r = CalcImageRenderBounds(Image, ClientRectangle, ImageAlign);
                e.Graphics.DrawImage(Image, r);
            }

            base.OnPaint(e); // Paint text
        }
    }
}
