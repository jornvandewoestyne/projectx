﻿using System.Windows.Forms;

//TODO: add summary...

namespace ProjectX.UiControls.Windows.Forms
{
    public static class WinFormXSplitContainerExtensions
    {
        #region Methods: SplitContainer
        public static void CollapsePanel2(this SplitContainer splitContainer, PictureBox pictureBox, bool bCollapse)
        {
            if (bCollapse)
            {
                pictureBox.Image = ProjectX.Infrastructure.Properties.Resources.Expand_Right;
            }
            else
            {
                pictureBox.Image = ProjectX.Infrastructure.Properties.Resources.Expand_Left;
            }
            splitContainer.Panel2Collapsed = bCollapse;
        }
        #endregion
    }
}
