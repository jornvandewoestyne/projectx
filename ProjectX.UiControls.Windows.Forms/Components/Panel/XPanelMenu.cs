﻿using System.Drawing;
using System.Windows.Forms;

//TODO: add click event per subButton...
//TODO: close, minimize, maximize, new, ...?

namespace ProjectX.UiControls.Windows.Forms
{
    public class XPanelMenu : XPanelBuffered//, IXLocalizableControl, IXLockable
    {
        #region Constructor
        public XPanelMenu()
            : base(nameof(XPanelMenu), DialogResult.None)
        { }
        #endregion    

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            ControlPaint.DrawBorder(e.Graphics, ClientRectangle,
                Color.Transparent, 0, ButtonBorderStyle.None,
                Color.Transparent, 0, ButtonBorderStyle.None,
                Color.LightSteelBlue, 1, ButtonBorderStyle.Solid,
                Color.Transparent, 0, ButtonBorderStyle.None);
        }
    }
}
