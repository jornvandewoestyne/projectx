﻿using ProjectX.General;
using System.ComponentModel;
using System.Windows.Forms;

//TODO: add click event per subButton...
//TODO: close, minimize, maximize, new, ...?

namespace ProjectX.UiControls.Windows.Forms
{
    public class XPanel : Panel, IXLocalizableControl, IXLockable, IXFreezable
    {
        //https://stackoverflow.com/questions/8046560/how-to-stop-flickering-c-sharp-winforms
        #region Overrides
        //do not use this on panels with objectlistviews!!!!!
        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        CreateParams cp = base.CreateParams;
        //        if (!DesignMode)
        //        {
        //            //WS_EX_COMPOSITED. Prevents flickering, with BUG on ObjectListview
        //            cp.ExStyle |= 0x02000000;
        //        }
        //        return cp;
        //    }
        //}
        #endregion

        //protected override void OnPaintBackground(PaintEventArgs e) { }

        #region Constructor
        public XPanel()
            : this(nameof(XPanel), DialogResult.None)
        { }

        internal XPanel(string name, DialogResult result, FriendlyTextType text = FriendlyTextType.None, FriendlyToolTipType toolTip = FriendlyToolTipType.None, Work work = Work.None, bool enabled = true)
        {
            //this.SetDefaults(name, result, text, toolTip, work, enabled);
            //this.DoubleBuffered = true;
            //this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw | ControlStyles.AllPaintingInWmPaint, true);
            //this.SetStyle(ControlStyles.UserPaint |
            //ControlStyles.AllPaintingInWmPaint |
            //ControlStyles.OptimizedDoubleBuffer,
            //true);
        }
        #endregion

        #region IXFreezable Members
        [Browsable(false), DefaultValue(false)]
        public bool BlockInput { get; set; }

        [Browsable(false), DefaultValue(true)]
        public bool CanBlockInput { get; set; } = true;

        public virtual void Validate(IXFreezable freezable, IXConditionStrategy strategy)
        {
            strategy?.Validate(this, freezable);
        }

        public override Cursor Cursor
        {
            get
            {
                if (this.CanBlockInput)
                {
                    return base.Cursor;
                }
                return Cursors.Default;
            }
            set
            {
                base.Cursor = value;
            }
        }

        protected override void WndProc(ref Message m)
        {
            var block = this.BlockInput(ref m);
            if (!block)
            {
                base.WndProc(ref m);
            }
        }
        #endregion

        #region IXLockable Members
        public Work Work { get; set; }

        public virtual void Validate(IXLockable lockable, IXConditionStrategy strategy)
        {
            strategy?.Validate(this, lockable);
        }
        #endregion

        #region ILocalizableControl Members
        [Browsable(false)]
        public ToolTip ToolTip { get; set; }

        [Browsable(false)]
        public FriendlyTextType XControlText { get; set; }

        [Browsable(false)]
        public FriendlyToolTipType XToolTipText { get; set; }

        public void Localize()
        {
            this.TryLocalize();
        }
        #endregion

        #region IXLeakable Members
        [Browsable(false)]
        public EventCollection EventCollection { get; set; } = new EventCollection();

        public virtual void Subscribe() { }
        #endregion

        #region IDisposable
        protected override void Dispose(bool disposing)
        {
            this.ToolTip?.Dispose();
            this.EventCollection?.Dispose();
            base.Dispose(disposing);
        }
        #endregion
    }
}
