﻿using ProjectX.General;
using System.Windows.Forms;

//TODO: add click event per subButton...
//TODO: close, minimize, maximize, new, ...?

namespace ProjectX.UiControls.Windows.Forms
{
    public class XPanelBuffered : XPanel//, IXLocalizableControl, IXLockable
    {
        //https://stackoverflow.com/questions/8046560/how-to-stop-flickering-c-sharp-winforms
        #region Property Overrides
        //do not use this on panels with objectlistviews!!!!!
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                if (!DesignMode)
                {
                    //WS_EX_COMPOSITED. Prevents flickering, with BUG on ObjectListview
                    cp.ExStyle |= 0x02000000;
                }
                return cp;
            }
        }
        #endregion

        //protected override void OnPaintBackground(PaintEventArgs e) { }

        #region Constructor
        public XPanelBuffered()
            : this(nameof(XPanelBuffered), DialogResult.None)
        { }

        internal XPanelBuffered(string name, DialogResult result, FriendlyTextType text = FriendlyTextType.None, FriendlyToolTipType toolTip = FriendlyToolTipType.None, Work work = Work.None, bool enabled = true)
            : base(name, result, text, toolTip, work, enabled)
        { }
        #endregion
    }
}
