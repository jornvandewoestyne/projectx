﻿using ProjectX.General;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XForm : XDisposableForm, IApplicationContainer, IXLocalizableControl
    {
        #region IApplicationContainer Members
        [Browsable(false), DefaultValue(false)]
        public bool IsReady { get; set; } = false;

        [Browsable(false)]
        public AppTitle AppTitle => AppTitle.Instance;
        #endregion

        #region ILocalizableControl Members
        [Browsable(false)]
        public ToolTip ToolTip { get; set; }

        [Browsable(false)]
        public FriendlyTextType XControlText { get; set; }

        [Browsable(false)]
        public FriendlyToolTipType XToolTipText { get; set; }

        public void Localize()
        {
            this.TryLocalize();
        }
        #endregion

        #region IXLeakable Members
        public override void Subscribe()
        {
            base.Subscribe();
        }
        #endregion

        #region Overrides
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.Localize();
            this.Subscribe<IXLeakable>();
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            this.ActivateForm();
            if (!this.DesignMode)
            {
                this.IsReady = true;
            }
        }
        #endregion

        #region DisposableForm
        protected override void DisposeComponents()
        {
            var baseControl = this as Control;
            var collection = baseControl.GetControls<IXLocalizableControl>().ToList();
            foreach (var item in collection)
            {
                var control = item.TryGet();
                control?.ToolTip?.Dispose();
                control?.EventCollection?.Dispose();
                var userControl = item.TryGet() as UserControl;
                userControl?.Dispose();
                control?.Dispose();
            }
        }

        protected override void Unsubscribe()
        {
            //No actions needed
        }

        protected override void Unbind()
        {
            //this.DisposeIcon();
            this.ToolTip?.Dispose();
            this.EventCollection?.Dispose();
        }
        #endregion
    }
}
