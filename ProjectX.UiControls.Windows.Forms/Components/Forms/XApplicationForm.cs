﻿using ProjectX.General;
using System;
using System.ComponentModel;
using System.Threading;

namespace ProjectX.UiControls.Windows.Forms
{
    public partial class XApplicationForm : XForm, IXApplicationForm
    {
        //Declare the object that will manage progress,and will be used to get the progress form our background thread
        [Browsable(false)]
        public CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();//Declare a cancellation token source

        [Browsable(false)]
        public CancellationToken CancellationToken { get; set; } //Declare a cancellation token object, we will populate this token from the token source, and pass it to the Task constructer.

        [XIntercept(Strategy = ExceptionStrategy.Default, MaxRetries = (uint)NumberConstant.None)]
        public void Cancel()
        {
            //TODO: 
            try
            {
                this.CancellationTokenSource?.Cancel(true);
            }
            catch (ObjectDisposedException)
            {
                //TODO: show message "click to cancel when running" or completely ignore and do nothing?
                //throw;
            }
            catch
            {
                throw;
            }
        }


        //TODO: application logo
        #region Constructor
        public XApplicationForm()
        {
            InitializeComponent();
            this.IsReady = false;
            this.Text = this.AppTitle.ToString();
        }
        #endregion

        #region Methods: IXApplicationForm
        public void BlockInput(bool block)
        {
            this.UseWaitCursor = block;
            this.SetFreezable<IXFreezable>(new XBooleanStrategy(block));
        }
        #endregion

        #region DisposableForm
        protected override void DisposeComponents()
        {
            this.components?.Dispose();
            base.DisposeComponents();
        }

        protected override void Unsubscribe()
        {
            base.Unsubscribe();
        }

        protected override void Unbind()
        {
            this.CancellationTokenSource?.Dispose();
            this.CancellationTokenSource = null;
            base.Unbind();
        }
        #endregion
    }
}
