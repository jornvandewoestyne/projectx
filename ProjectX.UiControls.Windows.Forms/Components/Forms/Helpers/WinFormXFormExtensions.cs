﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public static class WinFormXFormExtensions
    {
        #region Methods: Form - Activate Form
        /// <summary>
        /// Activates the form and gives focus.
        /// </summary>
        public static void ActivateForm(this Form form, bool maximize = false)
        {
            form.BringToFront();
            form.Activate();
            form.Focus();
            form.Maximize(maximize);
        }
        #endregion

        #region Methods: Form - Show Form
        /// <summary>
        /// Shows the form and gives focus.
        /// </summary>
        public static void ShowForm(this Form form, bool maximize = false)
        {
            form.Show();
            form.ActivateForm(maximize);
        }

        /// <summary>
        /// Make the form the active form and make it TopMost.
        /// </summary>
        public static void ShowOnTop(this Form form, bool maximize = false)
        {
            form.ShowForm(maximize);
            form.BringToFront();
            form.TopMost = true;
        }

        /// <summary>
        /// Shows the Mdi main form and give focus.
        /// </summary>
        public static void ShowMdiContainer(this Form form, bool maximize = false)
        {
            //TODO: Maximized with form controlbox!!!
            if (form.IsMdiContainer)
            {
                //if (windowstate == FormWindowState.Maximized)
                //{
                //    form.Bounds = Screen.PrimaryScreen.WorkingArea;
                //    form.CenterToScreen();
                //}
                //else
                //{
                //    form.WindowState = windowstate;
                //    form.CenterToScreen();
                //}
            }
            //non-MDI
            //else
            //{
            //    form.WindowState = windowstate;
            //}
            form.ActivateForm(maximize);
        }

        /// <summary>
        /// Shows the StartScreen as a Mdi Child Form.
        /// </summary>
        public static Form ActivateStartScreen(this Form parent)
        {
            if (parent.IsMdiContainer)
            {
                foreach (Form form in parent.MdiChildren)
                {
                    if (form is StartScreenForm)
                    {
                        form.ActivateMdiChildForm();
                        return form;
                    }
                }
            }
            //non-MDI
            else
            {
                var container = parent as XMainForm;
                foreach (Form form in container.childContainer.Controls)
                {
                    if (form is StartScreenForm)
                    {
                        form.ActivateMdiChildForm();
                        return form;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Shows the Mdi Child Form with current text, parent, dockstyle and windowstate.
        /// </summary>
        public static void ActivateMdiChildForm(this Form form)
        {
            if (form.IsMdiChild)
            {
                form.ShowForm();
            }
            //non-MDI
            else
            {
                //MessageBox.Show("Activate " + form?.ToString());
                //var container = form.ParentForm as XMainForm;
                //container.childContainer.Controls.SetChildIndex(form, XLib.Number.Index0);
                //TODO: test, seems form not activated when allready openened
                form.TopLevel = false;
                //form.ActivateForm();
                form.ShowForm();
                //TODO: Check out, slow!!!
                form.BringToFront();
                form.ActivateForm();
                form?.ParentForm?.Refresh();
            }
        }

        /// <summary>
        /// Shows the Mdi Child Form.
        /// </summary>
        public static void ShowMdiChildForm(this Form form, Form parent)
        {
            form.ShowMdiChildForm(parent, form.Text, DockStyle.Fill, FormWindowState.Normal);
        }

        /// <summary>
        /// Shows the Mdi Child Form.
        /// </summary>
        public static void ShowMdiChildForm(this Form form, Form parent, string text)
        {
            form.ShowMdiChildForm(parent, text, DockStyle.Fill, FormWindowState.Normal);
        }

        /// <summary>
        /// Shows the Mdi Child Form.
        /// </summary>
        public static void ShowMdiChildForm(this Form form, Form parent, string text, DockStyle dockStyle, FormWindowState windowstate)
        {
            if (parent.IsMdiContainer)
            {
                form.MdiParent = parent;
                form.Dock = dockStyle;
            }
            else
            {
                //non-MDI
                var container = parent as XMainForm;
                //Test:
                //var test = new XDataPageViewer();
                //test.Dock = dockStyle;
                //container.Controls.Add(test);
                ////test.Parent = container.childContainer;

                //test.DataView.Reset();
                //return;

                //parent.WindowState = FormWindowState.Normal;

                form.TopLevel = false;
                form.Dock = dockStyle;
                //container.childContainer.Controls.Add(form);
                form.Parent = container.childContainer;
                form.BringToFront();
            }
            //TODO: mouseposition, is their a better/cleaner way?
            //Get current cursor position:
            int x = Cursor.Position.X;
            int y = Cursor.Position.Y;
            //To avoid flickering with FormBorderStyle set to "None", show the form as Minimized first:
            if (parent.IsMdiContainer) { form.WindowState = FormWindowState.Minimized; }
            form.Text = text;
            form.ShowForm();
            form.WindowState = windowstate;
            //Set the cursor position to previous position after "FormWindowState.Minimized", which moves it to the taskbar:
            Cursor.Position = new Point(x, y);
        }
        #endregion

        #region Methods: Form - WindowState
        /// <summary>
        /// Maximizes the form.
        /// </summary>
        public static void Maximize(this Form form, bool maximize = true)
        {
            if (maximize && form.WindowState != FormWindowState.Maximized)
            {
                form.SuspendLayout();
                form.WindowState = FormWindowState.Maximized;
                form.ResumeLayout();
            }
        }
        #endregion

        #region Methods: Form - CenterToScreen
        /// <summary>
        /// Centers form to screen workingarea.
        /// </summary>
        public static void CenterToScreen(this Form form)
        {
            if (!form.IsMdiChild)
            {
                var screen = Screen.FromControl(form);
                var workingArea = screen.WorkingArea;
                form.Location = new Point()
                {
                    X = Math.Max(workingArea.X, workingArea.X + (workingArea.Width - form.Width) / 2),
                    Y = Math.Max(workingArea.Y, workingArea.Y + (workingArea.Height - form.Height) / 2)
                };
            }
        }
        #endregion    
    }
}
