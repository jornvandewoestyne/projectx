﻿using System.Windows.Forms;

//TODO: add summary...

namespace ProjectX.UiControls.Windows.Forms
{
    public static class WinFormXHelper
    {
        #region Methods: Form - Create Instance
        /// <summary>
        /// Creates an instance of the StartScreen.
        /// </summary>
        public static Form CreateInstanceStartScreen(Form parent)
        {
            //Only MDI:
            //if (parent.IsMdiContainer)
            //{
            //    var form = new StartScreenForm();
            //    form.ShowMdiChildForm(parent);
            //    return form;
            //}
            //return null;
            //non-MDI
            //if (parent.IsMdiContainer)
            //{
            var form = new StartScreenForm();
            form.ShowMdiChildForm(parent);
            return form;
            //}
            //return null;
        }

        /// <summary>
        /// Creates an instance of the connection form.
        /// </summary>
        public static Form CreateInstanceConnection()
        {
            var form = new ConnectionForm();
            form.ShowDialog();
            return form;
        }
        #endregion
    }
}
