﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XDisposableForm : Form, IXLeakable
    {
        #region Methods
        protected override void Dispose(bool disposing)
        {
            // TODO: dispose managed state (managed objects)
            // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
            // TODO: set large fields to null.
            if (disposing) { this.DisposeComponents(); }
            this.Unsubscribe();
            this.Unbind();
            base.Dispose(disposing);
        }

        /// <summary>
        /// ...
        /// </summary>
        protected virtual void DisposeComponents()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Unsubscribe possible events.
        /// </summary>
        protected virtual void Unsubscribe()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Set large fields to null.
        /// </summary>
        protected virtual void Unbind()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region IXLeakable Members
        [Browsable(false)]
        public EventCollection EventCollection { get; set; } = new EventCollection();

        /// <summary>
        /// Subscribe possible events.
        /// </summary>
        public virtual void Subscribe()
        {
            //throw new NotImplementedException();
        }
        #endregion
    }
}
