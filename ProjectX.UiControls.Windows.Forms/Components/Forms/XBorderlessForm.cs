﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public partial class XBorderlessForm : XApplicationForm
    {
        #region IXLeakable Members
        public override void Subscribe()
        {
            base.Subscribe();
        }
        #endregion

        #region DisposableForm
        protected override void DisposeComponents()
        {
            this.components?.Dispose();
            this.xFormFrame?.Dispose();
            base.DisposeComponents();
        }

        protected override void Unsubscribe()
        {
            base.Unsubscribe();
        }

        protected override void Unbind()
        {
            this.xFormFrame = null;
            base.Unbind();
        }
        #endregion

        #region Properties
        private Rectangle PreviousBounds { get; set; }
        private FormWindowState PreviousWindowState { get; set; } = FormWindowState.Minimized;
        private Rectangle SnappedBounds { get; set; }
        private bool IsSnapped;
        public bool IsDragging { get; set; }
        #endregion

        #region Property Overrides
        int originalExStyle = -1;
        public bool enableFormLevelDoubleBuffering = true;
        //public bool UseFormLevelDoubleBuffering = true;

        protected override CreateParams CreateParams
        {
            get
            {
                //if (originalExStyle == -1)
                //{
                //    originalExStyle = base.CreateParams.ExStyle;
                //    //cp.Style &= ~(int)WindowStyle.WS_THICKFRAME;
                //}

                CreateParams cp = base.CreateParams;
                if (!DesignMode)
                {
                    cp.Style |= (int)WindowStyle.WS_MINIMIZEBOX;
                    cp.Style |= (int)WindowStyle.WS_THICKFRAME;
                    if (this.WindowState == FormWindowState.Maximized)
                    {
                        enableFormLevelDoubleBuffering = false;
                        cp.Style |= (int)WindowStyle.WS_MAXIMIZE;
                        cp.Style &= ~(int)WindowStyle.WS_THICKFRAME;
                    }
                    //WS_EX_COMPOSITED. Prevents flickering, but with BUG on ObjectListview
                    //cp.ExStyle |= 0x02000000;
                    if (enableFormLevelDoubleBuffering)
                    {
                        cp.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED
                    }
                    else
                    {
                        cp.ExStyle &= ~0x02000000;
                        //cp.ExStyle = originalExStyle;
                        //if (this.WindowState == FormWindowState.Maximized) { cp.Style |= (int)WindowStyle.WS_MAXIMIZE; cp.Style &= ~(int)WindowStyle.WS_THICKFRAME; }
                        //MessageBox.Show("Test");
                    }
                }
                return cp;
            }
        }
        #endregion

        #region Constructor
        public XBorderlessForm()
        {
            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
            //this.ResizeRedraw = true;
            InitializeComponent();
        }
        #endregion

        #region Method Overrides
        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            if (!DesignMode)
            {
                this.SetMaximizedBounds();
                this.PreviousBounds = new Rectangle(this.DesktopBounds.Location, this.DesktopBounds.Size);
                this.PreviousWindowState = this.WindowState;
            }
        }

        //protected override void OnShown(EventArgs e)
        //{
        //    this.SetFormLevelDoubleBuffering(false);
        //    base.OnShown(e);
        //}

        protected override void OnResizeBegin(EventArgs e)
        {
            this.SetFormLevelDoubleBuffering(true);
            //enableFormLevelDoubleBuffering = true;
            //this.MaximizeBox = false;
            base.OnResizeBegin(e);
        }

        protected override void OnResizeEnd(EventArgs e)
        {
            this.SetFormLevelDoubleBuffering(false);
            //enableFormLevelDoubleBuffering = false;
            //this.MaximizeBox = false;
            base.OnResizeEnd(e);
        }

        //Don't use this!!!!
        //protected override void OnLoad(EventArgs e)
        //{
        //    this.SetFormLevelDoubleBuffering(false);
        //    //this.Visible = false;
        //    base.OnLoad(e);
        //    //this.Visible = true;
        //}

        protected override void WndProc(ref Message m)
        {
            if (IsDisposed) return;
            if (DesignMode)
            {
                base.WndProc(ref m);
                return;
            }
            switch (m.Msg)
            {
                //case (int)WindowMessages.WM_ERASEBKGND:
                //    this.WmRestoreMaximizedBounds(ref m);
                //    break;
                //case (int)WindowMessages.WM_NCCALCSIZE:
                //    // Provides new coordinates for the window client area.
                //    WmNCCalcSize(ref m);
                //    break;
                case (int)WindowMessages.WM_SIZE:
                    this.WmSize(ref m);
                    break;
                case (int)WindowMessages.WM_SYSCOMMAND:
                    this.WmSysCommand(ref m);
                    break;
                case (int)WindowMessages.WM_GETMINMAXINFO:
                    //case (int)WindowMessages.WM_NCCALCSIZE:
                    this.WmRestoreMaximizedBounds(ref m);
                    break;
                case (int)WindowMessages.WM_WINDOWPOSCHANGED:
                    this.WmWindowPosChanged(ref m);
                    break;
                //case (int)WindowMessages.WM_WINDOWPOSCHANGING:
                //case (int)WindowMessages.WM_MOVE:
                //    this.WmRestoreMaximizedBounds(ref m);
                //    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        #endregion

        #region Override Helpers
        private void SetFormLevelDoubleBuffering(bool setting)
        {
            enableFormLevelDoubleBuffering = setting;
            if (enableFormLevelDoubleBuffering != setting)
            {
                this.MaximizeBox = false;
            }
        }

        private void WmRestoreMaximizedBounds(ref Message m)
        {
            if (Form.ActiveForm == this)
            {
                //enableFormLevelDoubleBuffering = false;
                this.xFormFrame?.UpdateButtons();
                this.SetMaximizedBounds(ref m);
            }
            else
            {
                base.WndProc(ref m);
            }
        }

        private void WmSize(ref Message m)
        {
            enableFormLevelDoubleBuffering = true;
            //this.MaximizeBox = true;
            var command = m.WParam.ToInt32();
            if (command == NativeConstants.SIZE_MINIMIZED)
            {
                this.FormBorderStyle = FormBorderStyle.Sizable;
            }
            else if (command == NativeConstants.SIZE_MAXIMIZED || this.FormBorderStyle != FormBorderStyle.None)
            {
                this.SetFormLevelDoubleBuffering(true); //enableFormLevelDoubleBuffering = true;
                this.FormBorderStyle = FormBorderStyle.None;
                this.PreviousWindowState = this.WindowState;
                if (this.WindowState == FormWindowState.Normal)
                {
                    this.SetFormLevelDoubleBuffering(true); //enableFormLevelDoubleBuffering = true;
                    //this.CenterToScreen();
                    this.xFormFrame?.UpdateButtons();
                    if (IsSnapped)
                    {
                        //this.CenterToScreen();
                        this.SuspendLayout();
                        this.Size = this.SnappedBounds.Size;
                        this.Location = this.SnappedBounds.Location;
                        this.ResumeLayout();
                    }
                    else
                    {
                        //var isOnScreen = IsOnScreen(this.RestoreBounds.Location, this.RestoreBounds.Size, 1);
                        //if (!isOnScreen)
                        //{
                        //    //MessageBox.Show("not on screen WmSize");
                        //    this.CenterToScreen();
                        //}
                        this.SuspendLayout();
                        this.Size = this.PreviousBounds.Size;
                        this.Location = this.RestoreBounds.Location; // this.PreviousBounds.Location;
                        this.ResumeLayout();
                    }
                    //Neccessary!
                    this.MaximizeBox = false;
                    //this.FormBorderStyle = FormBorderStyle.None;
                }
                else
                {
                    //MessageBox.Show(IsDragging.ToString());
                    //Catching aerosnap on maximize:
                    if (this.IsDragging)
                    {
                        this.IsDragging = false;
                        NativeMethods.SendMessage(this.Handle, (int)WindowMessages.WM_SYSCOMMAND, (int)SystemCommands.SC_MAXIMIZE, 0);
                    }
                    //TODO: test RestoreBoundsToCurrentScreen
                    //this.WmNCCalcSize(ref m);
                    this.RestoreBoundsToCurrentScreen();
                    //this.IsDragging = false;
                }
            }
            enableFormLevelDoubleBuffering = false;
            base.WndProc(ref m);
            m.Result = NativeConstants.TRUE;

        }

        //private bool MaximizeAeroSnap { get; set; } = true;

        [DllImport("user32.dll")]
        extern static int GetWindowLong(IntPtr hWnd, int nIndex);
        const int GWL_STYLE = -16;

        public static bool NativeWindowHasBorder(IntPtr hWnd)
        {
            return (GetWindowLong(hWnd, GWL_STYLE) & ((int)WindowStyle.WS_BORDER | (int)WindowStyle.WS_THICKFRAME)) != XLib.MagicNumber.IntZero;
        }

        private void WmSysCommand(ref Message m)
        {
            enableFormLevelDoubleBuffering = true;
            var command = m.WParam.ToInt32();
            switch (command)
            {
                case (int)SystemCommands.SC_MINIMIZE:
                    //TODO: IsSnapped
                    this.IsSnapped = GetIsSnapped();
                    if (this.IsSnapped)
                    {
                        this.SnappedBounds = new Rectangle(this.DesktopBounds.Location, this.DesktopBounds.Size);
                    }
                    this.PreviousWindowState = this.WindowState;
                    break;
                case (int)SystemCommands.SC_MAXIMIZE:
                case (int)SystemCommands.SC_MAXIMIZE2:
                    //MessageBox.Show("Test");
                    //this.MaximizeAeroSnap = true;
                    this.xFormFrame?.UpdateButtons();
                    this.PreviousWindowState = this.WindowState;
                    //this.RestoreBoundsToCurrentScreen();
                    break;
                case (int)SystemCommands.SC_RESTORE2: // FIRST: While Dragging
                    this.xFormFrame?.UpdateButtons();
                    //DefWndProc(ref m);
                    //UpdateBounds();
                    //var pos = (WINDOWPOS)Marshal.PtrToStructure(m.LParam, typeof(WINDOWPOS));
                    //SetWindowRegion(m.HWnd, 0, 0, pos.cx, pos.cy);
                    var rectangle = this.PreviousBounds;
                    if (rectangle.Contains(PointToClient(Control.MousePosition)))
                    {
                        //MessageBox.Show("SC_RESTORE2");
                        this.WindowState = FormWindowState.Normal;
                        this.FormBorderStyle = FormBorderStyle.None;
                        if (this.Location.Y < XLib.MagicNumber.IntZero) { this.Location = new Point(this.Location.X, 10); }
                    }
                    else
                    {
                        Point point;
                        var mousePoint = PointToClient(Control.MousePosition); // (Cursor.Position);
                        if (mousePoint.X < Width / 2)
                        {
                            point = mousePoint.X < rectangle.Width / 2 ?
                               new Point(Cursor.Position.X - mousePoint.X, Cursor.Position.Y - mousePoint.Y) :
                               new Point(Cursor.Position.X - rectangle.Width / 2, Cursor.Position.Y - mousePoint.Y);
                        }
                        else
                        {
                            point = Width - mousePoint.X < rectangle.Width / 2 ?
                               new Point(Cursor.Position.X - rectangle.Width + Width - mousePoint.X, Cursor.Position.Y - mousePoint.Y) :
                               new Point(Cursor.Position.X - rectangle.Width / 2, Cursor.Position.Y - mousePoint.Y);
                        }
                        this.WindowState = FormWindowState.Normal;
                        this.FormBorderStyle = FormBorderStyle.None;
                        if (point.Y < XLib.MagicNumber.IntZero) { point.Y = 10; }
                        this.Location = point;
                    }
                    this.Size = rectangle.Size;
                    //this.PreviousWindowState = this.WindowState;
                    break;
                case (int)SystemCommands.SC_RESTORE: //SECONDLY: On doubleclick or after minimized or after WINDOWS KEY + DOWN
                    this.xFormFrame?.UpdateButtons();
                    //if (this.PreviousWindowState == FormWindowState.Minimized)
                    //{
                    //    enableFormLevelDoubleBuffering = true;
                    //    //this.xFormFrame?.UpdateButtons();
                    //    if (IsSnapped)
                    //    {
                    //        this.Size = this.SnappedBounds.Size;
                    //        this.Location = this.SnappedBounds.Location;
                    //    }
                    //    else
                    //    {
                    //        this.Size = this.PreviousBounds.Size;
                    //        this.Location = this.PreviousBounds.Location;
                    //    }
                    //    enableFormLevelDoubleBuffering = false;
                    //    //this.FormBorderStyle = FormBorderStyle.None;
                    //}
                    //else
                    //{
                    if (this.PreviousWindowState == FormWindowState.Minimized)
                    {
                        //this.FormBorderStyle = FormBorderStyle.Sizable;
                    }
                    else
                    {
                        //MessageBox.Show("Test");
                        this.SetFormLevelDoubleBuffering(true); //enableFormLevelDoubleBuffering = true;
                                                                //var isOnScreen = IsOnScreen(this.RestoreBounds.Location, this.RestoreBounds.Size, 1);
                                                                //if (!isOnScreen)
                                                                //{
                                                                //    //MessageBox.Show("not on screen");
                                                                //    this.CenterToScreen();
                                                                //}
                        this.Size = this.PreviousBounds.Size;
                        this.Location = this.PreviousBounds.Location; //this.RestoreBounds.Location;

                        //with WINDOWS KEY + DOWN!!
                        //TODO: this.CreateParams.Style == 50528256!!!!
                        //TODO: no border with WINDOWS KEY + DOWN!! trick = minimized then normal = works but not good
                        //if ((this.CreateParams.Style | (int)WindowStyle.WS_THICKFRAME) != XLib.Number.IntZero)
                        if (!NativeWindowHasBorder(this.Handle))
                        {
                            //UpdateStyles();
                            this.WindowState = FormWindowState.Minimized;
                            this.WindowState = FormWindowState.Normal;
                        }

                        //if (this.CreateParams.Style == 50528256)
                        //{
                        //    this.WindowState = FormWindowState.Minimized;
                        //    this.WindowState = FormWindowState.Normal;
                        //}

                        this.SetFormLevelDoubleBuffering(false); //enableFormLevelDoubleBuffering = false;


                    }
                    //}
                    //if (this.PreviousWindowState != FormWindowState.Minimized) { this.Visible = false; }
                    //this.PreviousWindowState = this.WindowState;
                    break;
            }
            enableFormLevelDoubleBuffering = false;
            base.WndProc(ref m);
            m.Result = NativeConstants.TRUE;
        }

        private void SetWindowRegion(IntPtr hwnd, int left, int top, int right, int bottom)
        {
            //Creates rectangle shape form, without creates rounded angles (not good)
            var hrg = new HandleRef((object)this, NativeMethods.CreateRectRgn(0, 0, 0, 0));
            var r = NativeMethods.GetWindowRgn(hwnd, hrg.Handle);
            NativeMethods.GetRgnBox(hrg.Handle, out var box);
            if (box.left != left || box.top != top || box.right != right || box.bottom != bottom)
            {
                var hr = new HandleRef((object)this, NativeMethods.CreateRectRgn(left, top, right, bottom));
                NativeMethods.SetWindowRgn(hwnd, hr.Handle, NativeMethods.IsWindowVisible(hwnd));
            }
        }

        protected void WmNCCalcSize(ref Message m)
        {
            //return;

            //_previousSize = Size;
            //_previousLocation = Location;

            //return;
            // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/windowing/windows/windowreference/windowmessages/wm_nccalcsize.asp
            // http://groups.google.pl/groups?selm=OnRNaGfDEHA.1600%40tk2msftngp13.phx.gbl

            var r = (RECT)Marshal.PtrToStructure(m.LParam, typeof(RECT));
            var max = MinMaxState == FormWindowState.Maximized;

            if (max)
            {
                //MaximizeWindow(_maximized);

                //_maximized = true;

                //_previousSize = Size;
                //_previousLocation = Location;

                var x = NativeMethods.GetSystemMetrics(NativeConstants.SM_CXSIZEFRAME);
                var y = NativeMethods.GetSystemMetrics(NativeConstants.SM_CYSIZEFRAME);
                var p = NativeMethods.GetSystemMetrics(NativeConstants.SM_CXPADDEDBORDER);
                var w = x + p;
                var h = y + p;

                //MessageBox.Show(w.ToString() + " " + p.ToString() + " " + h.ToString());

                //fix: not good
                //w -= 10;

                r.left += w;
                r.top += h;
                r.right -= w;
                r.bottom -= h;

                var appBarData = new APPBARDATA();
                appBarData.cbSize = Marshal.SizeOf(typeof(APPBARDATA));
                var autohide = (NativeMethods.SHAppBarMessage(NativeConstants.ABM_GETSTATE, ref appBarData) & NativeConstants.ABS_AUTOHIDE) != XLib.MagicNumber.IntZero;
                if (autohide) r.bottom -= 1;

                this.MaximizedBounds = new Rectangle(x, y, r.left + r.right, r.top + r.bottom);

                //Marshal.StructureToPtr(r, m.LParam, true);
                //m.Result = IntPtr.Zero;
            }
            else
            {
                base.WndProc(ref m);
                //m.Result = NativeConstants.TRUE;
            }
            m.Result = IntPtr.Zero;
        }

        private void WmWindowPosChanged(ref Message m)
        {
            //this.xFormFrame?.UpdateButtons();

            //DefWndProc(ref m);
            //UpdateBounds();
            //var pos = (WINDOWPOS)Marshal.PtrToStructure(m.LParam, typeof(WINDOWPOS));
            //SetWindowRegion(m.HWnd, 0, 0, pos.cx, pos.cy);
            //this.PreviousWindowState = this.WindowState;
            //m.Result = NativeConstants.TRUE;


            //return;

            this.SetFormLevelDoubleBuffering(true); //enableFormLevelDoubleBuffering = true;
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.xFormFrame?.UpdateButtons();
                //MessageBox.Show("maximize");
                //this.RestoreBoundsToCurrentScreen();
            }
            if (this.WindowState == FormWindowState.Normal && this.PreviousWindowState == FormWindowState.Maximized)
            {
                this.xFormFrame.UpdateButtons();
                this.FormBorderStyle = FormBorderStyle.None;
            }
            else
            {
                if (WindowState == FormWindowState.Normal)
                {
                    //TODO: make it work without null refernce control '?'
                    this.xFormFrame?.UpdateButtons();
                    var isValidPosition = this.Location.Y >= XLib.MagicNumber.IntZero;
                    var isOnScreen = IsOnScreen(this.Location, this.Size, 1);
                    if (isValidPosition && isOnScreen && this.WindowState != FormWindowState.Minimized)
                    {
                        //TODO: if (this.Location.Y < XLib.Number.IntZero) { this.Location = new Point(this.Location.X, 10); }
                        this.PreviousBounds = new Rectangle(this.DesktopBounds.Location, this.DesktopBounds.Size);
                    }
                }
                this.SetFormLevelDoubleBuffering(false); //enableFormLevelDoubleBuffering = false;
                base.WndProc(ref m);
                //m.Result = NativeConstants.TRUE;
            }
            enableFormLevelDoubleBuffering = false;
            this.PreviousWindowState = this.WindowState;
            m.Result = NativeConstants.TRUE;
        }

        private void SetMaximizedBounds(ref Message m)
        {
            ////this.WmNCCalcSize(ref m);
            ////return;

            this.SetMaximizedBounds();

            base.WndProc(ref m);
            m.Result = NativeConstants.TRUE;
        }

        private void SetMaximizedBounds()
        {


            //TODO: reset?
            var rect = Screen.GetWorkingArea(Control.MousePosition); // Screen.FromHandle(this.Handle).WorkingArea;
            rect.Location = new Point(0, 0);
            //https://github.com/angelsix/fasetto-word/issues/11: Window maximize on the wrong screen when using AeroSnap
            rect.Width -= 1;
            rect.Height -= 1;
            this.MaximizedBounds = rect;
            return;

            //MaximizedBounds = Screen.FromHandle(this.Handle).Primary ? Screen.FromHandle(this.Handle).WorkingArea : Screen.FromHandle(this.Handle).Bounds;
            ////this.MaximizeBox = false;
            //return;
            //var monitorHandle = MonitorFromWindow(Handle, MONITOR_DEFAULTTONEAREST);
            //var monitorInfo = new MONITORINFOEX();
            //GetMonitorInfo(new HandleRef(null, monitorHandle), monitorInfo);
            //var point = new Point(monitorInfo.rcWork.left, monitorInfo.rcWork.top);

            //var screen = Screen.FromPoint(point); //new Point(Cursor.Position.X, Cursor.Position.Y)); //Screen.FromControl(this);
            //var bounds = screen.Bounds;
            //var area = screen.Primary ? screen.WorkingArea : bounds;
            //var x = XLib.Number.IntZero; // area.X - bounds.X;
            //var y = XLib.Number.IntZero; // area.Y - bounds.Y;
            //this.MaximizedBounds = new Rectangle(x, y, area.Width, area.Height);
        }

        private void RestoreBoundsToCurrentScreen()
        {
            //MessageBox.Show("RestoreBoundsToCurrentScreen");
            //SetMaximizedBounds(ref m);
            SetMaximizedBounds();
            ////return;
            MaximizeWindow(true);
            //   //enableFormLevelDoubleBuffering = true;
            //   var centerFormMaximized = new Point(this.DesktopBounds.Left + this.DesktopBounds.Width / 2, this.DesktopBounds.Top + this.DesktopBounds.Height / 2);
            //var centerFormRestored = new Point(this.RestoreBounds.Left + this.RestoreBounds.Width / 2, this.RestoreBounds.Top + this.RestoreBounds.Height / 2);
            //var screenMaximized = Screen.FromPoint(centerFormMaximized);
            //var screenRestored = Screen.FromPoint(centerFormRestored);
            //var newRestoredLocation = this.PreviousBounds.Location;
            //newRestoredLocation.Offset(100, 100);
        }



        #region MyRegion
        //https://stackoverflow.com/questions/9321549/handling-aerosnap-message-in-wndproc

        ////register the hook
        //public static void WindowInitialized(Window window)
        //{
        //    IntPtr handle = (new WindowInteropHelper(window)).Handle;
        //    var hwndSource = HwndSource.FromHwnd(handle);
        //    if (hwndSource != null)
        //    {
        //        hwndSource.AddHook(WindowProc);
        //    }
        //}

        ////the important bit
        //private static IntPtr WindowProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        //{
        //    switch (msg)
        //    {
        //        case 0x0046: //WINDOWPOSCHANGING
        //            var winPos = (WINDOWPOS)Marshal.PtrToStructure(lParam, typeof(WINDOWPOS));
        //            var monitorInfo = new MONITORINFO();
        //            IntPtr monitorContainingApplication = MonitorFromWindow(hwnd, MonitorDefaultToNearest);
        //            GetMonitorInfo(monitorContainingApplication, monitorInfo);
        //            RECT rcWorkArea = monitorInfo.rcWork;
        //            //check for a framechange - but ignore initial draw. x,y is top left of current monitor so must be a maximise
        //            if (((winPos.flags & SWP_FRAMECHANGED) == SWP_FRAMECHANGED) && (winPos.flags & SWP_NOSIZE) != SWP_NOSIZE && winPos.x == rcWorkArea.left && winPos.y == rcWorkArea.top)
        //            {
        //                //set max size to the size of the *current* monitor
        //                var width = Math.Abs(rcWorkArea.right - rcWorkArea.left);
        //                var height = Math.Abs(rcWorkArea.bottom - rcWorkArea.top);
        //                winPos.cx = width;
        //                winPos.cy = height;
        //                Marshal.StructureToPtr(winPos, lParam, true);
        //                handled = true;
        //            }
        //            break;
        //    }
        //    return (IntPtr)0;
        //}

        ////all the helpers for dealing with this COM crap
        //[DllImport("user32")]
        //internal static extern bool GetMonitorInfo(IntPtr hMonitor, MONITORINFO lpmi);

        //[DllImport("user32")]
        //internal static extern IntPtr MonitorFromWindow(IntPtr handle, int flags);

        //private const int MonitorDefaultToNearest = 0x00000002;

        //[StructLayout(LayoutKind.Sequential)]
        //public struct WINDOWPOS
        //{
        //    public IntPtr hwnd;
        //    public IntPtr hwndInsertAfter;
        //    public int x;
        //    public int y;
        //    public int cx;
        //    public int cy;
        //    public int flags;
        //}

        //[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        //public class MONITORINFO
        //{
        //    public int cbSize = Marshal.SizeOf(typeof(MONITORINFO));
        //    public RECT rcMonitor;
        //    public RECT rcWork;
        //    public int dwFlags;
        //}

        //[StructLayout(LayoutKind.Sequential, Pack = XLib.Number.IntZero)]
        //public struct RECT
        //{
        //    public int left;
        //    public int top;
        //    public int right;
        //    public int bottom;
        //}
        #endregion

        [DllImport("user32.dll")]
        public static extern IntPtr MonitorFromWindow(IntPtr hwnd, uint dwFlags);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool GetMonitorInfo(HandleRef hmonitor, [In, Out] MONITORINFOEX info);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto, Pack = 4)]
        public class MONITORINFOEX
        {
            public int cbSize = Marshal.SizeOf(typeof(MONITORINFOEX));
            public RECT rcMonitor = new RECT();
            public RECT rcWork = new RECT();
            public int dwFlags = XLib.MagicNumber.IntZero;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public char[] szDevice = new char[32];
        }

        public FormWindowState MinMaxState
        {
            get
            {
                var s = NativeMethods.GetWindowLong(Handle, NativeConstants.GWL_STYLE);
                var max = (s & (int)WindowStyle.WS_MAXIMIZE) > XLib.MagicNumber.IntZero;
                if (max) return FormWindowState.Maximized;
                var min = (s & (int)WindowStyle.WS_MINIMIZE) > XLib.MagicNumber.IntZero;
                if (min) return FormWindowState.Minimized;
                return FormWindowState.Normal;
            }
        }

        private const int MONITOR_DEFAULTTONEAREST = 2;

        private void MaximizeWindow(bool maximize)
        {
            return;
            //if (!MaximizeBox || !ControlBox) return;

            //_maximized = maximize;

            if (this.WindowState == FormWindowState.Maximized)
            {
                var test = Screen.GetWorkingArea(Control.MousePosition);
                this.Location = new Point(test.Left, test.Top);
                //MessageBox.Show("Test");
                return;
                var monitorHandle = MonitorFromWindow(Handle, MONITOR_DEFAULTTONEAREST);
                var monitorInfo = new MONITORINFOEX();
                GetMonitorInfo(new HandleRef(null, monitorHandle), monitorInfo);
                //_previousSize = Size;
                //_previousLocation = Location;
                //Size = new Size(monitorInfo.rcWork.Width(), monitorInfo.rcWork.Height());
                this.Location = new Point(monitorInfo.rcWork.left, monitorInfo.rcWork.top);
                //MessageBox.Show("Maximized");
            }
            else
            {
                //MessageBox.Show("Normal");
                //Size = _previousSize;
                //Location = _previousLocation;
            }
        }
        #endregion

        // Return True if a certain percent of a rectangle is shown across the total screen area of all monitors, otherwise return False.
        public bool IsOnScreen(System.Drawing.Point RecLocation, System.Drawing.Size RecSize, double MinPercentOnScreen = 0.2)
        {
            var PixelsVisible = XLib.MagicNumber.DoubleZero;
            System.Drawing.Rectangle Rec = new System.Drawing.Rectangle(RecLocation, RecSize);

            foreach (var Scrn in Screen.AllScreens)
            {
                System.Drawing.Rectangle r = System.Drawing.Rectangle.Intersect(Rec, Scrn.WorkingArea);
                // intersect rectangle with screen
                if (r.Width != XLib.MagicNumber.IntZero & r.Height != XLib.MagicNumber.IntZero)
                {
                    PixelsVisible += (r.Width * r.Height);
                    // tally visible pixels
                }
            }
            return PixelsVisible >= (Rec.Width * Rec.Height) * MinPercentOnScreen;
        }

        public bool GetIsSnapped()
        {
            //var screen = Screen.FromPoint(new Point(Cursor.Position.X, Cursor.Position.Y)); //Screen.FromControl(this);
            //var bounds = screen.Bounds;
            //var area = screen.Primary ? screen.WorkingArea : bounds;
            //return this.Height >= area.Height;

            //https://stackoverflow.com/questions/47888651/win32-detect-if-window-is-maximized-docked-to-half-screen-win-key-left-right
            var monitorHandle = MonitorFromWindow(Handle, MONITOR_DEFAULTTONEAREST);
            var monitorInfo = new MONITORINFOEX();
            GetMonitorInfo(new HandleRef(null, monitorHandle), monitorInfo);
            var point = new Point(monitorInfo.rcWork.left, monitorInfo.rcWork.top);

            var screen = Screen.FromPoint(point); //new Point(Cursor.Position.X, Cursor.Position.Y)); //Screen.FromControl(this);
            var bounds = screen.Bounds;
            var area = screen.Primary ? screen.WorkingArea : bounds;
            return this.Height >= area.Height - 1;

            return this.WindowState == FormWindowState.Normal
                && (this.Left != area.Left ||
                this.Top != area.Top ||
                this.Right != area.Right ||
                this.Bottom != area.Bottom ||
                this.Height >= area.Height);
        }
    }
}
