﻿namespace ProjectX.UiControls.Windows.Forms
{
    partial class XBorderlessForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        ///// <summary>
        ///// Clean up any resources being used.
        ///// </summary>
        ///// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xFormFrame = new ProjectX.UiControls.Windows.Forms.UserControls.XFormHeader();
            this.SuspendLayout();
            // 
            // xFormFrame
            // 
            this.xFormFrame.BackColor = System.Drawing.Color.White;
            this.xFormFrame.Dock = System.Windows.Forms.DockStyle.Top;
            this.xFormFrame.Location = new System.Drawing.Point(0, 0);
            this.xFormFrame.Name = "xFormFrame";
            this.xFormFrame.Padding = new System.Windows.Forms.Padding(2);
            this.xFormFrame.Size = new System.Drawing.Size(800, 70);
            this.xFormFrame.TabIndex = 0;
            // 
            // XBorderlessForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.xFormFrame);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "XBorderlessForm";
            this.Text = "XBorderlessForm";
            this.ResumeLayout(false);

        }

        #endregion

        private ProjectX.UiControls.Windows.Forms.UserControls.XFormHeader xFormFrame;
    }
}