﻿using System.Windows.Forms;

//TODO: add summary...

namespace ProjectX.UiControls.Windows.Forms
{
    public static class WinFormXListViewControlExtensions
    {
        #region Methods: ListView
        public static int GetResizeHeight(this ListView listview, int minimumHeight, int spacing = 10)
        {
            //Resize to ensure combobox is visible:
            var size = listview.Top;
            if (listview.Items.Count > XLib.MagicNumber.IntZero)
            {
                //Expand to number of items in the listview:
                var itemHeight = listview.Items[XLib.MagicNumber.Index0].Bounds.Height;
                size += itemHeight * listview.Items.Count;
                if (listview.Groups.Count > XLib.MagicNumber.IntZero)
                {
                    //Expand to number of groups in the listview, with extra spacing:
                    size += (itemHeight + spacing) * listview.Groups.Count;
                }
            }
            return size;
        }
        #endregion
    }
}
