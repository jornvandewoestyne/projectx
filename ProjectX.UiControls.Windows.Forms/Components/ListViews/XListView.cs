﻿using ProjectX.General;
using System.ComponentModel;
using System.Windows.Forms;

//SOURCE: https://stackoverflow.com/questions/442817/c-sharp-flickering-listview-on-update

namespace ProjectX.UiControls.Windows.Forms
{
    public class XListView : ListView, IXLeakable, IXLockable, IXFreezableUnSafe //TODO: convert to ObjectListView
    {
        #region Constructors
        public XListView()
        {
            //Activate double buffering
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);

            //Enable the OnNotifyMessage event so we get a chance to filter out 
            // Windows messages before they get to the form's WndProc
            this.SetStyle(ControlStyles.EnableNotifyMessage, true);

            //this.DisabledBackColor = this.BackColor;            
            //this.OverrideDefaultRenderer();
        }
        #endregion

        //#region Properties
        //public Color DisabledBackColor { get; }
        //#endregion

        ///// <summary>
        ///// Override to avoid greyed-out listview backcolor on populate.
        ///// </summary>
        ///// <param name="e"></param>
        //protected override void OnEnabledChanged(EventArgs e)
        //{
        //    this.OverrideDisabledBackColor();
        //}

        #region IXFreezable Members
        [Browsable(false), DefaultValue(false)]
        public bool BlockInput { get; set; }

        [Browsable(false), DefaultValue(true)]
        public bool CanBlockInput { get; set; } = true;

        public virtual void Validate(IXFreezable freezable, IXConditionStrategy strategy)
        {
            strategy?.Validate(this, freezable);
        }

        //protected override void OnItemSelectionChanged(ListViewItemSelectionChangedEventArgs e)
        //{
        //    if (this.BlockInput && this.CanBlockInput)
        //    {
        //        if (e.IsSelected) e.Item.Selected = false;
        //    }
        //    base.OnItemSelectionChanged(e);
        //}

        //public override Cursor Cursor
        //{
        //    get
        //    {
        //        if (this.CanBlockInput)
        //        {
        //            return base.Cursor;
        //        }
        //        return Cursors.Default;
        //    }
        //    set
        //    {
        //        base.Cursor = value;
        //    }
        //}

        //protected override void WndProc(ref Message m)
        //{
        //    var block = this.BlockInput(ref m);
        //    if (!block)
        //    {
        //        base.WndProc(ref m);
        //    }
        //}
        #endregion

        #region IXLockable Members
        public Work Work { get; set; }

        public virtual void Validate(IXLockable lockable, IXConditionStrategy strategy)
        {
            strategy?.Validate(this, lockable);
        }
        #endregion       

        #region Methods
        protected override void OnNotifyMessage(Message m)
        {
            //Filter out the WM_ERASEBKGND message
            if (m.Msg != 0x14)
            {
                base.OnNotifyMessage(m);
            }
        }
        #endregion

        #region IXLeakable Members
        [Browsable(false)]
        public EventCollection EventCollection { get; set; } = new EventCollection();

        public virtual void Subscribe() { }
        #endregion

        #region IDisposable
        protected override void Dispose(bool disposing)
        {
            //this.ToolTip?.Dispose();
            this.EventCollection?.Dispose();
            base.Dispose(disposing);
        }
        #endregion
    }
}
