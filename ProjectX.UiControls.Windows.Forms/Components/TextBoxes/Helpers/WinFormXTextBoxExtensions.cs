﻿using ProjectX.General;
using System.Text;
using System.Windows.Forms;

//TODO: add summary...

namespace ProjectX.UiControls.Windows.Forms
{
    public static class WinFormXTextBoxExtensions
    {
        #region Methods: TextBox
        public static void SelectText(this TextBox textBox, bool selectAll)
        {
            if (selectAll)
            {
                textBox.SelectionStart = XLib.MagicNumber.IntZero;
                textBox.SelectionLength = textBox.Text.Length;
            }
            else
            {
                textBox.SelectionStart = textBox.Text.Length;
            }
        }

        public static SearchString CleanText(this TextBox textBox, bool cleanInput)
        {
            SearchString result = null;
            var stringBuilder = new StringBuilder();
            foreach (string line in textBox.Lines)
            {
                stringBuilder.AppendFormat("{0} ", line);
            }
            result = stringBuilder.AsSearchString();
            if (cleanInput)
            {
                textBox.Text = result.ToString();
            }
            textBox.Refresh();
            return result;
        }
        #endregion
    }
}
