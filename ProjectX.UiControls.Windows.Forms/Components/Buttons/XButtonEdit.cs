﻿using ProjectX.General;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XButtonEdit : XButton
    {
        public XButtonEdit()
            : base("editButton", DialogResult.None, FriendlyTextType.Edit, FriendlyToolTipType.Edit, Work.Edit)
        { }
    }
}
