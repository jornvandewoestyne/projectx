﻿using ProjectX.General;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XButtonCancel : XButton
    {
        public XButtonCancel()
            : base("cancelButton", DialogResult.Cancel, FriendlyTextType.Cancel, FriendlyToolTipType.Cancel, Work.Cancel)
        { }
    }
}
