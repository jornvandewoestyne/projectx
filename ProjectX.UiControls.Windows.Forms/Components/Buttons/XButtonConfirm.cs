﻿using ProjectX.General;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XButtonConfirm : XButton
    {
        public XButtonConfirm()
            : base("confirmButton", DialogResult.OK, FriendlyTextType.OK, FriendlyToolTipType.OK, Work.Confirm)
        { }
    }
}
