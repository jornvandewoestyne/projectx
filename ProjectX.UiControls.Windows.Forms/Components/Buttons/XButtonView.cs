﻿using ProjectX.General;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XButtonView : XButton
    {
        public XButtonView()
            : base("viewButton", DialogResult.None, FriendlyTextType.View, FriendlyToolTipType.View, Work.View)
        { }
    }
}
