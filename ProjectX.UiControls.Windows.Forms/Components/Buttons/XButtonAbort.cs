﻿using ProjectX.General;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XButtonAbort : XButton
    {
        public XButtonAbort()
            : base("abortButton", DialogResult.None, FriendlyTextType.Abort, FriendlyToolTipType.Abort, Work.Abort)
        { }
    }
}
