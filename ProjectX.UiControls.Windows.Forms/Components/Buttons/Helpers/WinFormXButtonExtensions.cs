﻿using ProjectX.General;
using System.Reflection;
using System.Windows.Forms;

//TODO: add summary...

namespace ProjectX.UiControls.Windows.Forms
{
    public static class WinFormXButtonExtensions
    {
        #region Methods: Button
        internal static void SetDefaults(this XButton control, string name, DialogResult result, FriendlyTextType text, FriendlyToolTipType toolTip, Work work = Work.None, bool enabled = true)
        {
            control.Name = name;
            control.Work = work;
            control.Enabled = enabled;
            control.XControlText = text;
            control.XToolTipText = toolTip;
            control.DialogResult = result;
            if (text != FriendlyTextType.None)
            {
                control.Text = text.ToString();
            }
            control.SetDoubleBuffered();
        }

        //https://www.csharp-examples.net/set-doublebuffered/
        public static void SetDoubleBuffered(this Control control)
        {
            // set instance non-public property with name "DoubleBuffered" to true
            typeof(Control).InvokeMember("DoubleBuffered",
                BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                null, control, new object[] { true });
        }

        ///// <summary>
        ///// ...
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="panel"></param>
        ///// <param name="buttonsToEnable"></param>
        //public static void EnableButtons<T>(this Panel panel, params T[] buttonsToEnable)
        //    where T : Button
        //{
        //    foreach (T button in panel.Controls)
        //    {
        //        button.Enabled = false;
        //    }
        //    if (buttonsToEnable != null)
        //    {
        //        panel.Enabled = true;
        //        foreach (T button in buttonsToEnable)
        //        {
        //            button.Enabled = true;
        //        }
        //    }
        //}

        ///// <summary>
        ///// ...
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="panel"></param>
        ///// <param name="entity"></param>
        ///// <param name="detail"></param>
        ///// <param name="buttonsToEnable"></param>
        //public static void EnableButtons<T>(this Panel panel, object entity, XMdiChildDetail detail, params T[] buttonsToEnable)
        //    where T : Button
        //{
        //    if (detail.ActionIdentifier == MenuGroupAction.Edit)
        //    {
        //        bool bEnable = true;
        //        if (entity == null)
        //        {
        //            bEnable = false;
        //        }
        //        foreach (T button in panel.Controls)
        //        {
        //            button.Enabled = bEnable;
        //        }
        //    }
        //    else if (detail.ActionIdentifier == MenuGroupAction.New)
        //    {
        //        foreach (T button in panel.Controls)
        //        {
        //            button.Enabled = false;
        //        }
        //        foreach (T button in buttonsToEnable)
        //        {
        //            button.Enabled = true;
        //        }
        //    }
        //}
        #endregion
    }
}
