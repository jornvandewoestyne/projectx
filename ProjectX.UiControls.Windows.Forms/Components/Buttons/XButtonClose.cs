﻿using ProjectX.General;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XButtonClose : XButton
    {
        //TODO: FriendlyTextType
        public XButtonClose()
            : base("closeButton", DialogResult.Cancel, FriendlyTextType.None, FriendlyToolTipType.Close, Work.Close)
        { }
    }
}
