﻿using ProjectX.General;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XButtonDelete : XButton
    {
        public XButtonDelete()
            : base("deleteButton", DialogResult.None, FriendlyTextType.Delete, FriendlyToolTipType.Delete, Work.Delete)
        { }
    }
}
