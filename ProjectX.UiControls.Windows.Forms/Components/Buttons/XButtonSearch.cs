﻿using ProjectX.General;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XButtonSearch : XButton
    {
        public XButtonSearch()
            : base("searchButton", DialogResult.None, FriendlyTextType.Search, FriendlyToolTipType.Search, Work.Search)
        { }
    }
}
