﻿using ProjectX.General;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XButtonSave : XButton
    {
        public XButtonSave()
            : base("saveButton", DialogResult.None, FriendlyTextType.Save, FriendlyToolTipType.Save, Work.Save)
        { }
    }
}