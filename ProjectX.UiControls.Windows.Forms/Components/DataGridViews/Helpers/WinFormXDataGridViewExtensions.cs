﻿using ProjectX.General;
using ProjectX.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public static class WinFormXDataGridViewExtensions
    {
        //TODO: Keep default's here?
        #region Fields: Constants
        private const bool DefaultIncludeObsoleteOrOptionalProperties = false;
        private const DataGridViewSelectionMode DefaultDataGridViewSelectionMode = DataGridViewSelectionMode.FullRowSelect;
        private const DataGridViewAutoSizeColumnsMode DefaultDataGridViewAutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        #endregion

        #region Methods: DataGridView
        /// <summary>
        /// Fills the given datagridview with data.
        /// <para>Returns false when no rows are returned.</para>
        /// </summary>
        /// <returns></returns>
        public static bool FillDataGrid<TEntity>(this DataGridView dataGridView, ICollectionPage<TEntity> collection, EntityKeyName entityKeyName, bool includeObsoleteOrOptionalProperties = DefaultIncludeObsoleteOrOptionalProperties, DataGridViewAutoSizeColumnsMode autoSizeColumnsMode = DefaultDataGridViewAutoSizeColumnsMode, DataGridViewSelectionMode selectionMode = DefaultDataGridViewSelectionMode)
            where TEntity : BaseEntity
        {
            if (collection != null && collection.Items != null)
            {
                //Select only the requested properties:
                //TODO: provide requested propertynames

                //TODO: performance!!!

                //TODO: move SelectDynamic to Data layer?
                //var source = collection.Items.SelectDynamic<TEntity>(includeObsoleteOrOptionalProperties).ToList();
                var source = collection.Items;
                var notifications = collection.Notifications;
                //Fill the datagrid:
                if (source != null)
                {
                    var count = collection.TotalItems;
                    var itemsPerPage = collection.ItemsPerPage;
                    dataGridView.SetHeaders(count, notifications);
                    //TODO: AlternatingRowsDefaultCellStyle Default
                    //dataGridView.AlternatingRowsDefaultCellStyle.BackColor = AppLibrary.Colors.DefaultSystemColor;
                    //dataGridView.DataSource = source;
                    dataGridView.Invoke((Action)(() => dataGridView.DataSource = source));
                    dataGridView.HideIdColumn(entityKeyName);
                    dataGridView.TranslateColumnHeaders();
                    //dataGridView.SelectionMode = selectionMode;
                    if (count > XLib.MagicNumber.IntZero && autoSizeColumnsMode != DataGridViewAutoSizeColumnsMode.None && itemsPerPage <= XLib.Limit.MaxItemsPerPage)
                    {
                        dataGridView.AutoResizeColumns(autoSizeColumnsMode);
                    }

                    //TODO
                    var edit = new DataGridViewImageColumn();
                    dataGridView.Columns.Add(edit);
                    edit.Name = "Edit";
                    edit.Image = ProjectX.Infrastructure.Properties.Resources.Edit.ToBitmap(new System.Drawing.Size(12, 12));
                    edit.Width = 30;

                    var btn = new DataGridViewButtonColumn();
                    dataGridView.Columns.Add(btn);
                    //btn.HeaderText = "Click Data";
                    btn.Text = "Edit";
                    btn.Name = "btn";
                    //btn.UseColumnTextForButtonValue = true;
                    //btn.FlatStyle = FlatStyle.Flat;
                    btn.Width = 30;

                    //TODO: make extension
                    var width = XLib.MagicNumber.IntZero;
                    foreach (DataGridViewColumn column in dataGridView.Columns)
                    {
                        if (column.Visible)
                        {
                            width += column.Width;
                        }
                    }
                    dataGridView.Columns.Add("Fill", "");
                    //MessageBox.Show(SystemInformation.VerticalScrollBarWidth.ToString());
                    //MessageBox.Show(dataGridView.RowHeadersWidth.ToString());
                    if (!dataGridView.Controls.OfType<VScrollBar>().First().Visible)
                    {
                        width = width - SystemInformation.VerticalScrollBarWidth;
                        //MessageBox.Show("Visible");
                        //modif = SystemInformation.VerticalScrollBarWidth;
                    }
                    dataGridView.Columns[dataGridView.Columns.Count - 1].Width = dataGridView.Bounds.Width - width - SystemInformation.VerticalScrollBarWidth - dataGridView.RowHeadersWidth;


                    dataGridView.Refresh();
                    if (count != XLib.MagicNumber.IntZero)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Returns the entity key of the datagridview's selected row.
        /// </summary>
        /// <param name="dataGridView"></param>
        /// <param name="entityKeyName"></param>
        /// <param name="showMessage"></param>
        /// <returns></returns>
        public static EntityKey GetSelectedKey(this DataGridView dataGridView, EntityKeyName entityKeyName, bool showMessage = true)
        {
            try
            {
                //Get the current row:
                var currentRow = dataGridView.CurrentRow;
                //When no item is selected:
                if (currentRow == null)
                {
                    XMessenger.ShowMessageNoSelection(showMessage);
                    return XLib.Key.NotExcistingEntityKey;
                }
                //Get identifier:
                var entityKey = dataGridView.GetRowKey(currentRow, entityKeyName);
                //TODO: TEST "NullEntityException":
                //var id = new object[XLib.Number.Index1] { ((int)currentRow.Cells[XLib.Number.Index0].Value + 999999) };
                return entityKey;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Hides all the key column(s) of the given datagridview.
        /// </summary>
        /// <param name="dataGridView"></param>
        /// <param name="entityKeyName"></param>
        public static void HideIdColumn(this DataGridView dataGridView, EntityKeyName entityKeyName)
        {
            if (entityKeyName != null && entityKeyName.Members != null && dataGridView.Columns.Count > XLib.MagicNumber.IntZero)
            {
                int length = entityKeyName.Members.Length;
                for (int i = 0; i < length; i++)
                {
                    string column = entityKeyName.Members[i];
                    if (dataGridView.Columns.Contains(column))
                    {
                        dataGridView.Columns[column].Visible = false;
                    }
                }
            }
        }

        /// <summary>
        /// Returns the entity id of the datagridview's selected row.
        /// </summary>
        /// <param name="dataGridView"></param>
        /// <param name="row"></param>
        /// <param name="entityKeyName"></param>
        /// <returns></returns>
        private static EntityKey GetRowKey(this DataGridView dataGridView, DataGridViewRow row, EntityKeyName entityKeyName)
        {
            if (entityKeyName != null && entityKeyName.Members != null && dataGridView.Columns.Count > XLib.MagicNumber.IntZero)
            {
                int length = entityKeyName.Members.Length;
                var keys = new object[length];
                var missingColumns = new List<string>();
                for (int i = 0; i < length; i++)
                {
                    string column = entityKeyName.Members[i];
                    if (dataGridView.Columns.Contains(column))
                    {
                        keys[i] = row.Cells[column].Value;
                    }
                    else
                    {
                        missingColumns.Add(column);
                    }
                }
                if (missingColumns.Count > XLib.MagicNumber.IntZero)
                {
                    var parameters = entityKeyName.ToString();
                    var missing = missingColumns.ToArray().TryArrayToString<string>();
                    //TODO: "!" -> gets a space
                    var message = StringHelper.Sentence(AppLibrary.ExceptionMessages.MissingKeyValueException, parameters, "!", missing);
                    throw new MissingKeyValueException(message);
                }
                return keys.AsEntityKey();
            }
            return null;
        }
        #endregion

        #region Headers
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="dataGridView"></param>
        /// <param name="count"></param>
        private static void SetHeaders(this DataGridView dataGridView, int count, IList<Notification> notifications)
        {
            dataGridView.Columns.Clear();
            if (count == XLib.MagicNumber.IntZero)
            {
                //TODO: notifications
                if (notifications != null && notifications.Count > XLib.MagicNumber.IntZero)
                {
                    dataGridView.Columns.Add("TooManyResults", "Too many results -> " + notifications[XLib.MagicNumber.Index0].Message);
                }
                else
                {
                    dataGridView.Columns.Add(XLib.Text.NoDataFound, XLib.Text.NoDataFoundText);
                }
                //TODO: Make defaultWidth
                dataGridView.Columns[XLib.MagicNumber.Index0].Width = dataGridView.Bounds.Width - SystemInformation.VerticalScrollBarWidth - dataGridView.RowHeadersWidth;
            }
            else
            {
                dataGridView.AutoGenerateColumns = true;
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="dataGridView"></param>
        private static void TranslateColumnHeaders(this DataGridView dataGridView)
        {
            if (dataGridView != null)
            {
                var columns = dataGridView.Columns;
                var length = columns.Count;
                for (int i = 0; i < length; i++)
                {
                    columns[i].HeaderText = StringHelper.Translation(columns[i].HeaderText);
                }
            }
        }
        #endregion
    }
}