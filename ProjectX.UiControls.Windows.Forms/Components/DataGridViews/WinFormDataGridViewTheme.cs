﻿using System;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public static class WinFormDataGridViewTheme
    {
        #region CustomGrid
        static System.Windows.Forms.DataGridViewCellStyle dateCellStyle = new System.Windows.Forms.DataGridViewCellStyle { Alignment = DataGridViewContentAlignment.MiddleRight };
        static System.Windows.Forms.DataGridViewCellStyle amountCellStyle = new System.Windows.Forms.DataGridViewCellStyle
        {
            Alignment = DataGridViewContentAlignment.MiddleRight,
            Format = "N2"
        };
        //static System.Windows.Forms.DataGridViewCellStyle gridCellStyle = new System.Windows.Forms.DataGridViewCellStyle
        //{
        //    Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft,
        //    BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(79)), Convert.ToInt32(Convert.ToByte(129)), Convert.ToInt32(Convert.ToByte(189))),
        //    Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0)),
        //    ForeColor = System.Drawing.SystemColors.ControlLightLight,
        //    SelectionBackColor = System.Drawing.SystemColors.Highlight,
        //    SelectionForeColor = System.Drawing.SystemColors.HighlightText,
        //    WrapMode = System.Windows.Forms.DataGridViewTriState.True
        //};
        static System.Windows.Forms.DataGridViewCellStyle gridCellStyle = new System.Windows.Forms.DataGridViewCellStyle
        {
            Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft,
            BackColor = System.Drawing.Color.White, //AppLibrary.Colors.ActiveColor,
            Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0)),
            ForeColor = System.Drawing.SystemColors.HotTrack,
            SelectionBackColor = System.Drawing.SystemColors.Highlight,
            SelectionForeColor = System.Drawing.SystemColors.HighlightText,
            WrapMode = System.Windows.Forms.DataGridViewTriState.True
        };
        static System.Windows.Forms.DataGridViewCellStyle gridCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle
        {
            Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft,
            BackColor = System.Drawing.SystemColors.ControlLightLight,
            Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0)),
            ForeColor = System.Drawing.SystemColors.ControlText,
            SelectionBackColor = System.Drawing.Color.BlanchedAlmond, //System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(155)), Convert.ToInt32(Convert.ToByte(187)), Convert.ToInt32(Convert.ToByte(89))),
            SelectionForeColor = System.Drawing.Color.Black,
            WrapMode = System.Windows.Forms.DataGridViewTriState.False
        };
        static System.Windows.Forms.DataGridViewCellStyle gridCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle
        {
            Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft,
            BackColor = System.Drawing.Color.White,
            Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0)),
            ForeColor = System.Drawing.SystemColors.WindowText,
            SelectionBackColor = System.Drawing.Color.BlanchedAlmond, //System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(155)), Convert.ToInt32(Convert.ToByte(187)), Convert.ToInt32(Convert.ToByte(89))),
            SelectionForeColor = System.Drawing.Color.Black,
            WrapMode = System.Windows.Forms.DataGridViewTriState.True
        };
        public static void applyGridTheme(this DataGridView grid)
        {
            //return;
            grid.AllowUserToAddRows = false;
            grid.AllowUserToDeleteRows = false;
            grid.BackgroundColor = System.Drawing.SystemColors.Window;
            grid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            grid.ColumnHeadersDefaultCellStyle = gridCellStyle;
            grid.ColumnHeadersHeight = 22;
            grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            grid.DefaultCellStyle = gridCellStyle2;
            grid.EnableHeadersVisualStyles = false;
            grid.GridColor = System.Drawing.SystemColors.GradientInactiveCaption;
            grid.ReadOnly = true;
            grid.RowHeadersVisible = true;
            grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            grid.RowHeadersDefaultCellStyle = gridCellStyle3;
            grid.AutoResizeRowHeadersWidth(DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders);
            grid.Font = gridCellStyle.Font;
        }
        public static void setGridRowHeader(this DataGridView dgv, bool hSize = false)
        {
            //dgv.TopLeftHeaderCell.Value = "NO ";
            dgv.TopLeftHeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.AutoResizeRowHeadersWidth(DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders);
            foreach (DataGridViewColumn cCol in dgv.Columns)
            {
                if (cCol.ValueType.ToString() == typeof(DateTime).ToString())
                {
                    cCol.DefaultCellStyle = dateCellStyle;
                }
                else if (cCol.ValueType.ToString() == typeof(decimal).ToString() | cCol.ValueType.ToString() == typeof(double).ToString())
                {
                    cCol.DefaultCellStyle = amountCellStyle;
                }
            }
            if (hSize)
            {
                dgv.RowHeadersWidth = dgv.RowHeadersWidth + 16;
            }
            dgv.AutoResizeColumns();
        }
        //public static void rowPostPaint_HeaderCount(object sender, DataGridViewRowPostPaintEventArgs e)
        //{
        //    //set rowheader count
        //    DataGridView grid = (DataGridView)sender;
        //    string rowIdx = (e.RowIndex + 1).ToString();
        //    dynamic centerFormat = new StringFormat();
        //    centerFormat.Alignment = StringAlignment.Center;
        //    centerFormat.LineAlignment = StringAlignment.Center;
        //    Rectangle headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height - sender.rows(e.RowIndex).DividerHeight);
        //    e.Graphics.DrawString(rowIdx, grid.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
        //}

        //https://social.msdn.microsoft.com/Forums/windows/en-US/146d836f-cbf6-458d-87f4-dbc377d9421d/draw-rectangle-or-bold-border-on-selected-cell-in-datagridview?forum=winformsdatacontrols
        //border on selected cell
        //private void dataGridView_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        //{
        //    if (e.RowIndex >= XLib.Number.IntZero && e.ColumnIndex >= XLib.Number.IntZero)
        //    {
        //        if (dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Selected == true)
        //        {
        //            e.Paint(e.CellBounds, DataGridViewPaintParts.All & ~DataGridViewPaintParts.Border);
        //            using (Pen p = new Pen(Color.Red, 1))
        //            {
        //                Rectangle rect = e.CellBounds;
        //                rect.Width -= 2;
        //                rect.Height -= 2;
        //                e.Graphics.DrawRectangle(p, rect);
        //            }
        //            e.Handled = true;
        //        }
        //    }
        //}
        #endregion
    }
}