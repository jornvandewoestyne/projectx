﻿using System;
using System.Drawing;
using System.Windows.Forms;

//https://docs.microsoft.com/en-us/dotnet/framework/winforms/controls/customize-the-appearance-of-rows-in-the-datagrid


namespace ProjectX.UiControls.Windows.Forms
{
    public class ProjectXDataGridView : DataGridView
    {
        private DataGridViewRow row = null;
        //private const Int32 CUSTOM_CONTENT_HEIGHT = 30;

        public ProjectXDataGridView()
            : base()
        {
            //this.SelectionMode = DataGridViewSelectionMode.CellSelect;
            //// Set a cell padding to provide space for the top of the focus 
            //// rectangle and for the content that spans multiple columns. 
            //Padding newPadding = new Padding(0, 1, 0, CUSTOM_CONTENT_HEIGHT);
            //this.RowTemplate.DefaultCellStyle.Padding = newPadding;

            //// Set the selection background color to transparent so 
            //// the cell won't paint over the custom selection background.
            //this.RowTemplate.DefaultCellStyle.SelectionBackColor =
            //    Color.Transparent;

            //this.MultiSelect = true;
            //this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            //this.CellPainting += new DataGridViewCellPaintingEventHandler(dataGridView_CellPainting);
            //this.SelectionChanged += new EventHandler(test);
            //this.RowStateChanged += new DataGridViewRowStateChangedEventHandler(DataGVEmployee_RowStateChanged);

            //this.CellFormatting += new DataGridViewCellFormattingEventHandler(dgvStatus_CellFormatting);
            //this.CellLeave += new DataGridViewCellEventHandler(dataGridView_CellLeave);
            //this.CellEnter += new DataGridViewCellEventHandler(dataGridView_CellEnter);
            //this.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;

            //// Attach handlers to DataGridView events.
            //this.ColumnWidthChanged += new DataGridViewColumnEventHandler(DataGridView_ColumnWidthChanged);
            //this.RowPrePaint += new DataGridViewRowPrePaintEventHandler(DataGridView_RowPrePaint);
            //this.RowPostPaint += new DataGridViewRowPostPaintEventHandler(DataGridView_RowPostPaint);
            ////this.CurrentCellChanged += new
            ////    EventHandler(dataGridView1_CurrentCellChanged);
            ////this.RowHeightChanged += new
            ////    DataGridViewRowEventHandler(dataGridView1_RowHeightChanged);
            //this.RowPostPaint += new DataGridViewRowPostPaintEventHandler(musicGridView_RowPostPaint);


            //this.CurrentCellChanged += new EventHandler(HandleCurrentCellChanged);
            //this.RowPostPaint += new DataGridViewRowPostPaintEventHandler(HandleRowPostPaint);
            //this.RowPrePaint += new DataGridViewRowPrePaintEventHandler(HandleRowPrePaint);
            this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void test(object sender, EventArgs e)
        {
            //foreach (DataGridViewRow item in this.Rows)
            //{
            //    if (this.CurrentRow == item)
            //    {
            //        item.DefaultCellStyle.BackColor = Color.Lavender;
            //    }
            //    else
            //    {
            //        //MessageBox.Show("test");
            //        item.DefaultCellStyle.BackColor = Color.White;
            //    }
            //}
            //if (row != null)
            //{
            //    row.DefaultCellStyle.BackColor = Color.White;
            //    this.CurrentRow.DefaultCellStyle.BackColor = Color.Lavender;
            //}

        }

        //protected override void OnRowStateChanged(int rowIndex, DataGridViewRowStateChangedEventArgs e)
        //{
        //    base.OnRowStateChanged(rowIndex, e);
        //    if (rowIndex > -1)
        //    {
        //        DataGridViewRow row = this.Rows[rowIndex];
        //        if (row.Selected)
        //        {
        //            Color oldColor = this.CurrentRow.DefaultCellStyle.SelectionBackColor;
        //            e.Row.DefaultCellStyle.SelectionBackColor = Color.FromArgb(oldColor.R < 235 ? oldColor.R + 20 : 0,
        //                                oldColor.G, oldColor.B);
        //        }
        //        else if (!row.Selected)
        //        {
        //            e.Row.DefaultCellStyle.SelectionBackColor = Color.White;
        //        }
        //    }
        //}


        void DataGVEmployee_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (e.StateChanged == DataGridViewElementStates.Selected)
            {
                e.Row.DefaultCellStyle.BackColor = Color.Lavender;
            }
            else
            {
                e.Row.DefaultCellStyle.BackColor = Color.White;
            }
        }

        private void dgvStatus_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //if (this.SelectedRows == null) return;
            if (this.SelectedRows != null && e.RowIndex == this.SelectedRows[XLib.MagicNumber.Index1].Index)
                e.CellStyle.BackColor = Color.Red;
        }

        const float BORDER_WIDTH = 2f;                                          // specify the width of selection border  
        int oldCellY = -1;



        private void HandleCurrentCellChanged(object sender, EventArgs e)
        {
            if (oldCellY != -1)
            {
                this.InvalidateRow(oldCellY);
            }
            oldCellY = this.CurrentCellAddress.Y;
        }

        private void HandleRowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            //DataGridViewElementStates state;  

            //state = e.State & DataGridViewElementStates.Selected;              

            //if (state == DataGridViewElementStates.Selected)                   
            //{  
            //    e.PaintParts &=                                                // prevent the grid from automatically   
            //   ~(                                                              // painting the selection background  
            //       DataGridViewPaintParts.Focus |  
            //       DataGridViewPaintParts.SelectionBackground  
            //    );  
            //}  
        }

        private void HandleRowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Rectangle rowRect;                                                 // a selection rectangle  
            DataGridViewElementStates state;

            state = e.State & DataGridViewElementStates.Selected;              // only paint on selected row  
            if (state == DataGridViewElementStates.Selected)
            {
                int iBorder = Convert.ToInt32(BORDER_WIDTH);                   // calculate columns width  
                int columnsWidth = this.Columns.GetColumnsWidth(DataGridViewElementStates.Visible);
                int xStart = this.RowHeadersWidth;

                // need do calculate the clipping rectangle, because you can't use e.RowBounds  
                rowRect =
                   new Rectangle
                   (
                       xStart,                                                 // start after the row header  
                       e.RowBounds.Top + iBorder - 1,                          // at the top of the row  
                       columnsWidth - this.HorizontalScrollingOffset + 1,  // get the visible part of the row  
                       e.RowBounds.Height - iBorder                            // get the row's height  
                    );

                // draw the border  
                using (Pen pen = new Pen(this.DefaultCellStyle.SelectionBackColor, BORDER_WIDTH))
                {
                    e.Graphics.DrawRectangle(pen, rowRect);                    // you can't use e.RowBounds here!  
                }
            }
        }


        private void musicGridView_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = (DataGridView)sender;
            // run this piece of code only for the selected row
            if (dgv.Rows[e.RowIndex].Selected)
            {
                int width = this.Width;
                Rectangle r = dgv.GetRowDisplayRectangle(e.RowIndex, false);
                var rect = new Rectangle(r.X, r.Y, width - 1, r.Height - 1);//new Rectangle(r.X, r.Y, width – 1, r.Height – 1);
                // draw the border around the selected row using the highlight color and using a border width of 2
                ControlPaint.DrawBorder(e.Graphics, rect,
                    SystemColors.Highlight, 1, ButtonBorderStyle.Solid,
                    SystemColors.Highlight, 1, ButtonBorderStyle.Solid,
                    SystemColors.Highlight, 1, ButtonBorderStyle.Solid,
                    SystemColors.Highlight, 1, ButtonBorderStyle.Solid);
            }
        }

        // Forces the control to repaint itself when the user 
        // manually changes the width of a column.
        void DataGridView_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            this.Invalidate();
        }

        //// Forces the row to repaint itself when the user changes the 
        //// current cell. This is necessary to refresh the focus rectangle.
        //void dataGridView1_CurrentCellChanged(object sender, EventArgs e)
        //{
        //    if (oldRowIndex != -1)
        //    {
        //        this.dataGridView1.InvalidateRow(oldRowIndex);
        //    }
        //    oldRowIndex = this.dataGridView1.CurrentCellAddress.Y;
        //}


        // Paints the custom selection background for selected rows.
        void DataGridView_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            // Do not automatically paint the focus rectangle.
            e.PaintParts &= ~DataGridViewPaintParts.Focus;

            // Determine whether the cell should be painted
            // with the custom selection background.
            if ((e.State & DataGridViewElementStates.Selected) == DataGridViewElementStates.Selected)
            {
                // Calculate the bounds of the row.
                Rectangle rowBounds = new Rectangle(
                    this.RowHeadersWidth, e.RowBounds.Top,
                    this.Columns.GetColumnsWidth(
                        DataGridViewElementStates.Visible) -
                    this.HorizontalScrollingOffset + 1, e.RowBounds.Height);

                // Paint the custom selection background.
                using (Brush backbrush =
                    new System.Drawing.Drawing2D.LinearGradientBrush(rowBounds,
                        this.DefaultCellStyle.SelectionBackColor,
                        e.InheritedRowStyle.ForeColor,
                        System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
                {
                    e.Graphics.FillRectangle(backbrush, rowBounds);
                }
            }
        }


        // Paints the content that spans multiple columns and the focus rectangle.
        void DataGridView_RowPostPaint(object sender,
            DataGridViewRowPostPaintEventArgs e)
        {
            // Calculate the bounds of the row.
            Rectangle rowBounds = new Rectangle(
                this.RowHeadersWidth, e.RowBounds.Top,
                this.Columns.GetColumnsWidth(
                    DataGridViewElementStates.Visible) -
                this.HorizontalScrollingOffset + 1,
                e.RowBounds.Height);

            SolidBrush forebrush = null;
            try
            {
                // Determine the foreground color.
                if ((e.State & DataGridViewElementStates.Selected) ==
                    DataGridViewElementStates.Selected)
                {
                    forebrush = new SolidBrush(e.InheritedRowStyle.SelectionForeColor);
                }
                else
                {
                    forebrush = new SolidBrush(e.InheritedRowStyle.ForeColor);
                }

                // Get the content that spans multiple columns.
                object recipe =
                    this.Rows.SharedRow(e.RowIndex).Cells[XLib.MagicNumber.Index2].Value;

                if (recipe != null)
                {
                    String text = recipe.ToString();

                    // Calculate the bounds for the content that spans multiple 
                    // columns, adjusting for the horizontal scrolling position 
                    // and the current row height, and displaying only whole
                    // lines of text.
                    Rectangle textArea = rowBounds;
                    textArea.X -= this.HorizontalScrollingOffset;
                    textArea.Width += this.HorizontalScrollingOffset;
                    textArea.Y += rowBounds.Height - e.InheritedRowStyle.Padding.Bottom;
                    textArea.Height -= rowBounds.Height -
                        e.InheritedRowStyle.Padding.Bottom;
                    textArea.Height = (textArea.Height / e.InheritedRowStyle.Font.Height) *
                        e.InheritedRowStyle.Font.Height;

                    // Calculate the portion of the text area that needs painting.
                    RectangleF clip = textArea;
                    clip.Width -= this.RowHeadersWidth + 1 - clip.X;
                    clip.X = this.RowHeadersWidth + 1;
                    RectangleF oldClip = e.Graphics.ClipBounds;
                    e.Graphics.SetClip(clip);

                    // Draw the content that spans multiple columns.
                    e.Graphics.DrawString(
                        text, e.InheritedRowStyle.Font, forebrush, textArea);

                    e.Graphics.SetClip(oldClip);
                }
            }
            finally
            {
                forebrush.Dispose();
            }

            if (this.CurrentCellAddress.Y == e.RowIndex)
            {
                // Paint the focus rectangle.
                e.DrawFocus(rowBounds, true);
            }
        }

        //private void dataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        //{
        //    this.Invalidate();
        //}

        //private void dataGridView_CellLeave(object sender, DataGridViewCellEventArgs e)
        //{
        //    this.CurrentRow.DefaultCellStyle.BackColor = Color.White;
        //    //this.Invalidate();
        //}

        private void dataGridView_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            //datagridview fullrowselect and rectangle on focused cell
            //https://social.msdn.microsoft.com/Forums/en-US/bfdde33c-77eb-4fe3-a02f-4cd62cda5739/change-the-border-of-a-full-selected-row?forum=winformsdatacontrols

            //            if( this.CurrentCell.RowIndex == e.RowIndex)
            //{
            //e.AdvancedBorderStyle.Top =
            //DataGridViewAdvancedCellBorderStyle.OutsetDouble;
            //e.AdvancedBorderStyle.Bottom=
            //DataGridViewAdvancedCellBorderStyle.OutsetDouble;
            //}
            //            return;

            //           if (e.ColumnIndex == this.CurrentCell.ColumnIndex
            //              && e.RowIndex == this.CurrentCell.RowIndex)
            //           {
            //               e.Paint(e.CellBounds, DataGridViewPaintParts.All
            //& ~DataGridViewPaintParts.Border);
            //               using (Pen p = new Pen(Color.Black, 4))
            //               {
            //                   Rectangle rect = e.CellBounds;
            //                   rect.Width -= 2;
            //                   rect.Height -= 2;
            //                   e.Graphics.DrawRectangle(p, rect);
            //               }
            //               e.Handled = true;
            //           }


            //e.Paint(e.CellBounds, DataGridViewPaintParts.All & ~DataGridViewPaintParts.Focus);
            //e.Handled = true;

            //var dgv = (DataGridView)sender;
            //// run this piece of code only for the selected row
            //if (dgv.Rows[e.RowIndex].Selected)
            //{
            //    int width = this.Width;
            //    Rectangle r = dgv.GetRowDisplayRectangle(e.RowIndex, false);
            //    var rect = new Rectangle(r.X, r.Y, width - 1, r.Height - 1);//new Rectangle(r.X, r.Y, width – 1, r.Height – 1);
            //    // draw the border around the selected row using the highlight color and using a border width of 2
            //    ControlPaint.DrawBorder(e.Graphics, rect,
            //        SystemColors.Highlight, 4, ButtonBorderStyle.Solid,
            //        SystemColors.Highlight, 4, ButtonBorderStyle.Solid,
            //        SystemColors.Highlight, 4, ButtonBorderStyle.Solid,
            //        SystemColors.Highlight, 4, ButtonBorderStyle.Solid);
            //}

            if (e.RowIndex >= XLib.MagicNumber.IntZero && e.ColumnIndex >= XLib.MagicNumber.IntZero)
            {
                if (this.Rows[e.RowIndex].Cells[e.ColumnIndex].Selected == true)
                {
                    //this.Rows[e.RowIndex].Selected = true;
                    row = this.Rows[e.RowIndex].Cells[e.ColumnIndex].OwningRow;
                    //this.CurrentRow.DefaultCellStyle.BackColor = Color.Lavender;

                    e.Paint(e.CellBounds, DataGridViewPaintParts.All & ~DataGridViewPaintParts.Border);
                    using (Pen p = new Pen(Color.Blue, 1))
                    {
                        Rectangle rect = e.CellBounds;
                        rect.Width -= 2;
                        rect.Height -= 2;
                        e.Graphics.DrawRectangle(p, rect);
                    }
                    //for (int i = 0; i < this.Columns - 1; i++)
                    //{
                    //    //this.Rows[e.RowIndex].Cells[i].back
                    //}
                    e.Handled = true;
                }
            }
            //else
            //{
            //    this.CurrentRow.DefaultCellStyle.BackColor = Color.White;
            //}
        }

    }
}
