﻿using System.ComponentModel;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XLinkLabel : LinkLabel, IXLeakable
    {
        #region IXLeakable Members
        [Browsable(false)]
        public EventCollection EventCollection { get; set; } = new EventCollection();

        public virtual void Subscribe() { }
        #endregion

        #region IDisposable
        protected override void Dispose(bool disposing)
        {
            //this.ToolTip?.Dispose();
            this.EventCollection?.Dispose();
            base.Dispose(disposing);
        }
        #endregion
    }
}
