﻿using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XGroupBox : GroupBox
    {
        #region Property Overrides
        //do not use this on panels with objectlistviews!!!!!
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                if (!DesignMode)
                {
                    //WS_EX_COMPOSITED. Prevents flickering, with BUG on ObjectListview
                    cp.ExStyle |= 0x02000000;
                }
                return cp;
            }
        }
        #endregion
    }
}
