﻿
using ProjectX.General;

//TODO: add summary...

namespace ProjectX.UiControls.Windows.Forms
{
    public static class WinFormXToolStripExtensions
    {
        #region Methods: Toolstrip
        internal static void SetDefaults(this XToolStrip control, string name, FriendlyTextType text, FriendlyToolTipType toolTip, bool enabled = true)
        {
            control.Name = name;
            control.Enabled = enabled;
            control.XControlText = text;
            control.XToolTipText = toolTip;
            if (text != FriendlyTextType.None)
            {
                control.Text = text.ToString();
            }
            //TODO
            control.ImageScalingSize = new System.Drawing.Size(48, 48);
            control.SetRenderer();
        }

        /// <summary>
        /// BUGFIX: Bypass the toolstrip's "System" renderer.
        /// As a result, no line will be drawn underneath the toolstrip.
        /// </summary>
        private static void SetRenderer(this XToolStrip control)
        {
            control.Renderer = new XToolStripCustomRenderer();
        }

        //public static void EnableToolStripButtons(this ToolStrip toolStrip, params ToolStripButton[] buttonsToEnable)
        //{
        //    foreach (ToolStripItem item in toolStrip.Items)
        //    {
        //        item.Enabled = false;
        //    }
        //    if (buttonsToEnable != null)
        //    {
        //        toolStrip.Enabled = true;
        //        foreach (ToolStripButton button in buttonsToEnable)
        //        {
        //            button.Enabled = true;
        //        }
        //    }
        //}

        //public static void EnableToolStripButtons(this ToolStrip toolStrip, object entity, XMdiChildDetail detail, params ToolStripButton[] buttonsToEnable)
        //{
        //    if (detail.ActionIdentifier == MenuGroupAction.Edit)
        //    {
        //        bool bEnable = true;
        //        if (entity == null)
        //        {
        //            bEnable = false;
        //        }
        //        foreach (ToolStripItem item in toolStrip.Items)
        //        {
        //            item.Enabled = bEnable;
        //        }
        //    }
        //    else if (detail.ActionIdentifier == MenuGroupAction.New)
        //    {
        //        foreach (ToolStripItem item in toolStrip.Items)
        //        {
        //            item.Enabled = false;
        //        }
        //        if (buttonsToEnable != null)
        //        {
        //            foreach (ToolStripButton button in buttonsToEnable)
        //            {
        //                button.Enabled = true;
        //            }
        //        }
        //    }
        //}
        #endregion
    }
}
