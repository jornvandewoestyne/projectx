﻿using ProjectX.General;
using System.Drawing;
using System.Windows.Forms;

#region Source
//https://stackoverflow.com/questions/1918247/how-to-disable-the-line-under-tool-strip-in-winform-c
//https://docs.microsoft.com/en-us/dotnet/api/system.windows.forms.toolstripitem?view=netframework-4.8
//https://docs.microsoft.com/en-us/dotnet/api/system.windows.forms.toolstriprenderer?redirectedfrom=MSDN&view=netframework-4.8
#endregion

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// Resolve a bug in the toolstrip's "System" renderer.
    /// As a result, no line will be drawn underneath the toolstrip.
    /// </summary>
    internal class XToolStripCustomRenderer : ToolStripSystemRenderer
    {
        #region Overrides
        protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e)
        {
            if (e.ToolStrip is XToolStrip)
            {
                // skip render border
            }
            else
            {
                // do render border
                base.OnRenderToolStripBorder(e);
            }
        }

        protected override void OnRenderButtonBackground(ToolStripItemRenderEventArgs e)
        {
            var item = e.Item as XToolStripButton;
            if (item != null && item.Selected)
            {
                var g = e.Graphics;
                var bounds = new Rectangle(0, 0, item.Width - 1, item.Height - 1);
                var colors = ColorPreference.GetAccentColor(item.Work);
                FillBackground(g, bounds, colors);
            }
        }

        //protected override void OnRenderButtonBackground(ToolStripItemRenderEventArgs e)
        //{
        //    ////Disable higlighting the button
        //    ////base.OnRenderButtonBackground(e);
        //    ////return;
        //    //// If the item is in the rollover state, 
        //    //// draw a border at the bottom.
        //    //var g = e.Graphics;
        //    //var item = e.Item as XToolStripButton;
        //    //if (item != null && item.Enabled && item.Rollover)
        //    //{
        //    //    //FillBackground(g, new Rectangle(Point.Empty, item.Size), Color.AliceBlue);
        //    //    var rectangle = item.ContentRectangle;
        //    //    var height = 1;
        //    //    var width = rectangle.Width;
        //    //    var x = rectangle.Left;
        //    //    var y = rectangle.Bottom - height;
        //    //    var bounds = new Rectangle(x, y, width, height);
        //    //    using (Brush backBrush = new SolidBrush(Color.LightSlateGray))
        //    //    {
        //    //        g.FillRectangle(backBrush, bounds);
        //    //    }
        //    //}
        //}

        ////Gray out the image, except on hover
        //protected override void OnRenderItemImage(ToolStripItemImageRenderEventArgs e)
        //{
        //    base.OnRenderItemImage(e);
        //    //return;
        //    var item = e.Item as XToolStripButton;
        //    if (item != null && item.Enabled) // && !item.Rollover)
        //    {
        //        //if (item.Rollover) { item.Image = ProjectX.Infrastructure.Properties.Resources.Maximize; }
        //        //else
        //        //{
        //        //    item.Image = ProjectX.Infrastructure.Properties.Resources.Minimize;
        //        //}
        //        //Graphics g = e.Graphics;
        //        //using (Bitmap bmp = new Bitmap(e.Image))
        //        //{

        //        //    // Set the image attribute's color mappings
        //        //    ColorMap[] colorMap = new ColorMap[XLib.Number.Index1];
        //        //    colorMap[XLib.Number.Index0] = new ColorMap();
        //        //    colorMap[XLib.Number.Index0].OldColor = Color.FromArgb(89, 89, 89); // Color.Black;
        //        //    colorMap[XLib.Number.Index0].NewColor = Color.Blue;
        //        //    ImageAttributes attr = new ImageAttributes();
        //        //    attr.SetRemapTable(colorMap, ColorAdjustType.Bitmap);
        //        //    // Draw using the color map
        //        //    Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
        //        //    g.DrawImage(bmp, rect, 0, 0, rect.Width, rect.Height, GraphicsUnit.Pixel, attr);
        //        //}

        //        //using (Graphics g = this.CreateGraphics())
        //        //{
        //        //    Bitmap bmp =
        //        //    this.GetGrayImage(this.ImageList.Images[b.ImageIndex]);
        //        //    g.DrawImage(bmp, b.Rectangle.X + 4,
        //        //    b.Rectangle.Y + 3);
        //        //}

        //        //Image image = e.Image;
        //        //image = CreateDisabledImage(image);
        //        //image.Dispose();

        //        //var bitmap = new Bitmap(e.Image);// new Size(12, 12));
        //        ////this.GetGrayImage(bitmap);
        //        //ControlPaint.DrawImageDisabled(
        //        //        e.Graphics,
        //        //        bitmap,
        //        //        e.ImageRectangle.X,
        //        //        e.ImageRectangle.Y,
        //        //        item.BackColor);
        //        //bitmap.Dispose();
        //    }
        //    //else
        //    //{
        //    //    item.Image = ProjectX.Infrastructure.Properties.Resources.Minimize;
        //    //}
        //}
        #endregion

        #region Helpers
        /// <summary>
        ///  Fill the item's background as bounded by the rectangle
        /// </summary>
        //TODO: Make this one general
        private static void FillBackground(Graphics g, Rectangle bounds, ColorPreference colorPreference)
        {
            using (var brush = new SolidBrush(colorPreference.BackgroundColor))
            {
                g.FillRectangle(brush, bounds);
            }
            using (var pen = new Pen(colorPreference.BorderColor))
            {
                g.DrawRectangle(pen, bounds);
            }
        }

        //TODO: Make this one general, to preserve consequent coloring!
        //private static ColorPreference GetColorPreference(Work work)
        //{
        //    switch (work)
        //    {
        //        case Work.Cancel:
        //        case Work.Close:
        //        case Work.Delete:
        //            return ColorScheme.AlertAccent;
        //        default:
        //            return ColorScheme.DefaultAccent;
        //    }
        //}
        #endregion
    }
}
