﻿using ProjectX.General;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XToolStripButtonMinimize : XToolStripButton
    {
        public XToolStripButtonMinimize()
        {
            this.Name = "minimizeToolStripButton";
            this.Work = Work.Minimize;
            this.XControlText = FriendlyTextType.Minimize;
            this.XToolTipText = FriendlyToolTipType.Minimize;
            this.Text = this.XControlText.ToString();
            this.Image = ProjectX.Infrastructure.Properties.Resources.Minimize; //.ToBitmap(new Size(10, 10));
            this.AutoSize = false;
            this.ImageScaling = ToolStripItemImageScaling.None;
            this.Size = new Size(30, 30);

            this.Click += new EventHandler(Execute);
        }

        private void Execute(object sender, EventArgs e)
        {
            var form = Form.ActiveForm;
            if (form != null)
            {
                NativeMethods.SendMessage(form.Handle, (int)WindowMessages.WM_SYSCOMMAND, (int)SystemCommands.SC_MINIMIZE, 0);
                form.WindowState = FormWindowState.Minimized;
            }
        }
    }
}
