﻿using ProjectX.General;
using ProjectX.Infrastructure;
using System.Drawing;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XToolStripButtonEdit : XToolStripButton
    {
        public XToolStripButtonEdit()
        {
            this.Name = "editToolStripButton";
            this.Work = Work.Edit;
            this.XControlText = FriendlyTextType.Edit;
            this.XToolTipText = FriendlyToolTipType.Edit;
            this.Text = this.XControlText.ToString();
            this.Image = ProjectX.Infrastructure.Properties.Resources.Edit.ToBitmap(new Size(20, 20));
            this.AutoSize = false;
            this.ImageScaling = ToolStripItemImageScaling.None;
            this.Size = new Size(30, 30);
        }
    }
}
