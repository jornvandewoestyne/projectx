﻿using ProjectX.General;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XToolStripButtonClose : XToolStripButton
    {
        public override void Subscribe()
        {
            this.Subscribe(Event.Click, this.Execute, this);
            base.Subscribe();
        }


        public XToolStripButtonClose()
        {
            this.Name = "closeToolStripButton";
            this.Work = Work.Close;
            this.XControlText = FriendlyTextType.Close;
            this.XToolTipText = FriendlyToolTipType.Close;
            this.Text = this.XControlText.ToString();
            this.Image = ProjectX.Infrastructure.Properties.Resources.Close; //.ToBitmap(new Size(6, 6));
            this.AutoSize = true;
            this.ImageScaling = ToolStripItemImageScaling.None;
            this.Size = new Size(30, 30);
            //TODO:
            this.Subscribe();
        }

        private void Execute(object sender, EventArgs e)
        {
            var form = Form.ActiveForm;
            if (form != null)
            {
                //NativeMethods.SendMessage(form.Handle, (int)WindowMessages.WM_SYSCOMMAND, (int)SystemCommands.SC_CLOSE, 0);
                //form.WindowState = FormWindowState.Minimized;
                form.Close();
            }
        }
    }
}
