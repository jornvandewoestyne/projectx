﻿using ProjectX.General;
using ProjectX.Infrastructure;
using System.Drawing;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XToolStripButtonDelete : XToolStripButton
    {
        public XToolStripButtonDelete()
        {
            this.Name = "deleteToolStripButton";
            this.Work = Work.Delete;
            this.XControlText = FriendlyTextType.Delete;
            this.XToolTipText = FriendlyToolTipType.Delete;
            this.Text = this.XControlText.ToString();
            this.Image = ProjectX.Infrastructure.Properties.Resources.Delete.ToBitmap(new Size(20, 20));
            this.AutoSize = false;
            this.ImageScaling = ToolStripItemImageScaling.None;
            this.Size = new Size(30, 30);
        }
    }
}
