﻿using ProjectX.General;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XToolStripButtonMaximize : XToolStripButton
    {
        public XToolStripButtonMaximize()
        {
            this.Name = "maximizeToolStripButton";
            this.Work = Work.Maximize;
            this.XControlText = FriendlyTextType.Maximize;
            this.XToolTipText = FriendlyToolTipType.Maximize;
            this.Text = this.XControlText.ToString();
            this.Image = ProjectX.Infrastructure.Properties.Resources.Maximize; //.ToBitmap(new Size(10, 10));
            this.AutoSize = false;
            this.ImageScaling = ToolStripItemImageScaling.None;
            this.Size = new Size(30, 30);

            //this.Click += new EventHandler(Execute);
        }

        //protected override void OnPaint(PaintEventArgs e)
        //{
        //    var form = Form.ActiveForm;
        //    if (form.WindowState == FormWindowState.Maximized)
        //    {
        //        this.Image = ProjectX.Infrastructure.Properties.Resources.Restore.ToBitmap(new Size(20, 20));
        //    }
        //    else if (form.WindowState == FormWindowState.Normal)
        //    {
        //        this.Image = ProjectX.Infrastructure.Properties.Resources.Maximize.ToBitmap(new Size(20, 20));
        //    }
        //    base.OnPaint(e);
        //}

        protected override void OnClick(EventArgs e)
        {
            //base.OnClick(e);
            this.Execute();
        }

        //public void OnMaximizedState()
        //{
        //    OnNormalState();
        //    ////var form = Form.ActiveForm;
        //    ////if (form != null)
        //    ////{
        //    ////    if (!IsOnScreen(form.Location, form.Size, 1))
        //    ////    {
        //    //        this.Image = ProjectX.Infrastructure.Properties.Resources.Restore.ToBitmap(new Size(20, 20));
        //    //        this.XControlText = FriendlyTextType.Restore;
        //    //        this.XToolTipText = FriendlyToolTipType.Restore;
        //    ////    }
        //    ////    else
        //    ////    {
        //    ////        OnNormalState();
        //    ////    }
        //    ////}
        //}

        //public void OnNormalState()
        //{
        //    return;

        //    Update();
        //    return;

        //    //var form = Form.ActiveForm;
        //    //if (form != null)
        //    //{
        //    //    if (IsOnScreen(form.Location, form.Size, 1))
        //    //    {
        //    //        this.Image = ProjectX.Infrastructure.Properties.Resources.Maximize.ToBitmap(new Size(20, 20));
        //    //        this.XControlText = FriendlyTextType.Maximize;
        //    //        this.XToolTipText = FriendlyToolTipType.Maximize;
        //    //    }
        //    //    else if(GetIsSnapped() || form.WindowState == FormWindowState.Maximized)
        //    //    {
        //    //        //OnMaximizedState();

        //    //        this.Image = ProjectX.Infrastructure.Properties.Resources.Restore.ToBitmap(new Size(20, 20));
        //    //        this.XControlText = FriendlyTextType.Restore;
        //    //        this.XToolTipText = FriendlyToolTipType.Restore;
        //    //    }
        //    //}
        //}

        public void UpdateButton()
        {
            var form = Form.ActiveForm;
            //if (form != null && IsOnScreen(form.Location, form.Size, 1) && !GetIsSnapped() && form.WindowState != FormWindowState.Maximized)
            //{
            //    this.Image = ProjectX.Infrastructure.Properties.Resources.Maximize; //.ToBitmap(new Size(12, 12));
            //    this.XControlText = FriendlyTextType.Maximize;
            //    this.XToolTipText = FriendlyToolTipType.Maximize;
            //}
            //else
            //{
            //    this.Image = ProjectX.Infrastructure.Properties.Resources.Restore; //.ToBitmap(new Size(12, 12));
            //    this.XControlText = FriendlyTextType.Restore;
            //    this.XToolTipText = FriendlyToolTipType.Restore;WindowState = FormWindowState.Maximized
            //}
            if (form != null && form.WindowState == FormWindowState.Maximized)
            {
                this.Image = ProjectX.Infrastructure.Properties.Resources.Restore; //.ToBitmap(new Size(12, 12));
                this.XControlText = FriendlyTextType.Restore;
                this.XToolTipText = FriendlyToolTipType.Restore;
            }
            else
            {
                this.Image = ProjectX.Infrastructure.Properties.Resources.Maximize; //.ToBitmap(new Size(12, 12));
                this.XControlText = FriendlyTextType.Maximize;
                this.XToolTipText = FriendlyToolTipType.Maximize;
            }
        }


        //TODO: restore image on move to normal

        public void Execute(bool isDragging = false) //(object sender, EventArgs e)
        {
            var form = Form.ActiveForm;
            if (form != null)
            {
                var validHeight = form.Size.Height >= (Screen.FromHandle(form.Handle).WorkingArea.Height) * 0.95;
                if (form.WindowState == FormWindowState.Maximized) // || validHeight)
                {
                    this.UpdateButton();
                    //form.WindowState = FormWindowState.Normal;
                    //form.FormBorderStyle = FormBorderStyle.None;
                    if (isDragging)
                    {
                        NativeMethods.SendMessage(form.Handle, (int)WindowMessages.WM_SYSCOMMAND, (int)SystemCommands.SC_RESTORE2, 0);
                    }
                    else
                    {
                        //this.OnMaximizedState();
                        form.WindowState = FormWindowState.Normal;
                        form.FormBorderStyle = FormBorderStyle.None;
                        NativeMethods.SendMessage(form.Handle, (int)WindowMessages.WM_SYSCOMMAND, (int)SystemCommands.SC_RESTORE, 0);
                    }
                }
                else if (form.WindowState == FormWindowState.Normal)
                {
                    this.UpdateButton();
                    if (!IsOnScreen(form.Location, form.Size, 1) || GetIsSnapped())
                    {
                        form.CenterToScreen();
                        this.UpdateButton();
                        //TODO: test/keep?
                        form.Invalidate();
                        form.Update();
                    }
                    //else
                    //{    
                    form.WindowState = FormWindowState.Maximized;
                    //var borderlessForm = form as XBorderlessForm;
                    //borderlessForm.SetMaximizedBounds();
                    ////TODO: remove?
                    //form.FormBorderStyle = FormBorderStyle.None;
                    //form.SuspendLayout();
                    NativeMethods.SendMessage(form.Handle, (int)WindowMessages.WM_SYSCOMMAND, (int)SystemCommands.SC_MAXIMIZE, 0);
                    //form.ResumeLayout();
                    //}
                }
            }
        }

        public bool IsOnScreen(System.Drawing.Point RecLocation, System.Drawing.Size RecSize, double MinPercentOnScreen = 0.2)
        {
            var PixelsVisible = XLib.MagicNumber.DoubleZero;
            System.Drawing.Rectangle Rec = new System.Drawing.Rectangle(RecLocation, RecSize);

            foreach (Screen Scrn in Screen.AllScreens)
            {
                System.Drawing.Rectangle r = System.Drawing.Rectangle.Intersect(Rec, Scrn.WorkingArea);
                // intersect rectangle with screen
                if (r.Width != XLib.MagicNumber.IntZero & r.Height != XLib.MagicNumber.IntZero)
                {
                    PixelsVisible += (r.Width * r.Height);
                    // tally visible pixels
                }
            }
            return PixelsVisible >= (Rec.Width * Rec.Height) * MinPercentOnScreen;
        }

        [DllImport("user32.dll")]
        public static extern IntPtr MonitorFromWindow(IntPtr hwnd, uint dwFlags);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool GetMonitorInfo(HandleRef hmonitor, [In, Out] MONITORINFOEX info);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto, Pack = 4)]
        public class MONITORINFOEX
        {
            public int cbSize = Marshal.SizeOf(typeof(MONITORINFOEX));
            public RECT rcMonitor = new RECT();
            public RECT rcWork = new RECT();
            public int dwFlags = XLib.MagicNumber.IntZero;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public char[] szDevice = new char[32];
        }

        private const int MONITOR_DEFAULTTONEAREST = 2;


        public bool GetIsSnapped()
        {
            var form = Form.ActiveForm;
            if (form != null)
            {
                //var screen = Screen.FromPoint(new Point(Cursor.Position.X, Cursor.Position.Y));
                //var bounds = screen.Bounds;
                //var area = screen.Primary ? screen.WorkingArea : bounds;
                //return form.Height >= area.Height; // && form.WindowState != FormWindowState.Maximized;

                var monitorHandle = MonitorFromWindow(form.Handle, MONITOR_DEFAULTTONEAREST);
                var monitorInfo = new MONITORINFOEX();
                GetMonitorInfo(new HandleRef(null, monitorHandle), monitorInfo);
                var point = new Point(monitorInfo.rcWork.left, monitorInfo.rcWork.top);

                var screen = Screen.FromPoint(point); //new Point(Cursor.Position.X, Cursor.Position.Y)); //Screen.FromControl(this);
                var bounds = screen.Bounds;
                var area = screen.Primary ? screen.WorkingArea : bounds;
                return form.Height >= area.Height - 1;

                return form.WindowState == FormWindowState.Normal
                    && (form.Left != area.Left ||
                    form.Top != area.Top ||
                    form.Right != area.Right ||
                    form.Bottom != area.Bottom ||
                    form.Height >= area.Height);
            }
            return false;
        }
    }
}
