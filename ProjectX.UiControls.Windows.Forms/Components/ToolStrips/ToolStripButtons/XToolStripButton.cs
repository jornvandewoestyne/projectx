﻿using ProjectX.General;
using System.ComponentModel;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XToolStripButton : ToolStripButton, IXLocalizableComponent, IXLockable
    {
        #region Properties
        ////https://docs.microsoft.com/en-us/dotnet/api/system.windows.forms.toolstripitem?view=netframework-4.8
        //// This property returns true if the mouse is 
        //// inside the client rectangle.
        //public bool Rollover { get; private set; }
        //public Color[] AccentColors { get; set; } = new Color[] { Color.AliceBlue, Color.LightSteelBlue };
        #endregion

        #region Constructor
        public XToolStripButton()
        {
            this.Enabled = false;
            this.DisplayStyle = ToolStripItemDisplayStyle.Image;
        }
        #endregion

        #region Events
        //// This method defines the behavior of the MouseEnter event.
        //// It sets the state of the rolloverValue field to true and
        //// tells the control to repaint.
        //protected override void OnMouseEnter(EventArgs e)
        //{
        //    base.OnMouseEnter(e);
        //    this.Rollover = true;
        //    this.Invalidate();
        //}

        //// This method defines the behavior of the MouseLeave event.
        //// It sets the state of the rolloverValue field to false and
        //// tells the control to repaint.
        //protected override void OnMouseLeave(EventArgs e)
        //{
        //    base.OnMouseLeave(e);
        //    this.Rollover = false;
        //    this.Invalidate();
        //}

        //// This method defines the behavior of the OnClick event.
        //// It sets the state of the rolloverValue field to false and
        //// tells the control to repaint.
        //protected override void OnClick(EventArgs e)
        //{
        //    base.OnClick(e);
        //    this.Rollover = false;
        //    this.Invalidate();
        //}
        #endregion

        #region IXLockable Members
        public Work Work { get; set; }

        public virtual void Validate(IXLockable lockable, IXConditionStrategy strategy)
        {
            strategy?.Validate(this, lockable);
        }
        #endregion

        #region ILocalizableControl Members
        [Browsable(false)]
        public ToolTip ToolTip { get; set; }

        [Browsable(false)]
        public FriendlyTextType XControlText { get; set; }

        [Browsable(false)]
        public FriendlyToolTipType XToolTipText { get; set; }

        public void Localize()
        {
            this.TryLocalize();
        }
        #endregion

        #region IXLeakable Members
        [Browsable(false)]
        public EventCollection EventCollection { get; set; } = new EventCollection();

        public virtual void Subscribe() { }
        #endregion

        #region Methods
        protected override void Dispose(bool disposing)
        {
            // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
            // TODO: set large fields to null.
            this.ToolTip?.Dispose();
            this.EventCollection?.Dispose();
            this.Image?.Dispose();
            this.Image = null;
            base.Dispose(disposing);
        }
        #endregion
    }
}
