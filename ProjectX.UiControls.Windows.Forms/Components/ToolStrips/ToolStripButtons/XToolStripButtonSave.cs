﻿using ProjectX.General;
using ProjectX.Infrastructure;
using System.Drawing;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XToolStripButtonSave : XToolStripButton
    {
        public XToolStripButtonSave()
        {
            this.Name = "saveToolStripButton";
            this.Work = Work.Save;
            this.XControlText = FriendlyTextType.Save;
            this.XToolTipText = FriendlyToolTipType.Save;
            this.Text = this.XControlText.ToString();
            this.Image = ProjectX.Infrastructure.Properties.Resources.Save.ToBitmap(new Size(20, 20));
            this.AutoSize = false;
            this.ImageScaling = ToolStripItemImageScaling.None;
            this.Size = new Size(30, 30);
        }
    }
}
