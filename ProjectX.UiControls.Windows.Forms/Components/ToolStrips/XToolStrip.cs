﻿using ProjectX.General;
using System.ComponentModel;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XToolStrip : ToolStrip, IXLocalizableControl, IXLockable, IXFreezable
    {
        #region Overrides
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                if (!DesignMode)
                {
                    //WS_EX_COMPOSITED. Prevents flickering, with BUG on ObjectListview
                    cp.ExStyle |= 0x02000000;
                }
                return cp;
            }
        }
        #endregion

        #region Constructor
        public XToolStrip() => this.SetDefaults(nameof(XToolStrip), FriendlyTextType.None, FriendlyToolTipType.None, true);
        #endregion

        #region IXFreezable Members
        [Browsable(false), DefaultValue(false)]
        public bool BlockInput { get; set; }

        [Browsable(false), DefaultValue(true)]
        public bool CanBlockInput { get; set; } = true;

        public virtual void Validate(IXFreezable freezable, IXConditionStrategy strategy)
        {
            strategy?.Validate(this, freezable);
        }

        public override Cursor Cursor
        {
            get
            {
                if (this.CanBlockInput)
                {
                    return base.Cursor;
                }
                return Cursors.Default;
            }
            set
            {
                base.Cursor = value;
            }
        }

        protected override void WndProc(ref Message m)
        {
            var block = this.BlockInput(ref m);
            if (!block)
            {
                base.WndProc(ref m);
            }
        }
        #endregion

        #region IXLockable Members
        public Work Work { get; set; } = Work.None;

        public virtual void Validate(IXLockable lockable, IXConditionStrategy strategy)
        {
            strategy?.Validate(this, lockable);
            foreach (var item in this.Items)
            {
                if (item is IXLockable lockableItem)
                {
                    lockableItem.Validate(lockableItem, strategy);
                }
            }
        }
        #endregion

        #region ILocalizableControl Members
        [Browsable(false)]
        public ToolTip ToolTip { get; set; }

        [Browsable(false)]
        public FriendlyTextType XControlText { get; set; }

        [Browsable(false)]
        public FriendlyToolTipType XToolTipText { get; set; }

        public void Localize()
        {
            this.TryLocalize();
            //Do not forget to localize the underlying ToolStripItems, which are Components not Controls!
            foreach (var item in this.Items)
            {
                if (item is IXLocalizableComponent component)
                {
                    component.Localize();
                }
            }
        }
        #endregion

        #region IXLeakable Members
        [Browsable(false)]
        public EventCollection EventCollection { get; set; } = new EventCollection();

        public virtual void Subscribe() { }
        #endregion

        #region IDisposable
        protected override void Dispose(bool disposing)
        {
            this.ToolTip?.Dispose();
            this.EventCollection?.Dispose();
            base.Dispose(disposing);
        }
        #endregion
    }
}
