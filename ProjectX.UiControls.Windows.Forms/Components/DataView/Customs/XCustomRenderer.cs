﻿using BrightIdeasSoftware;
using System.Drawing;

//TODO: add renderers accordinly, when needed...

namespace ProjectX.UiControls.Windows.Forms
{
    internal class XCustomHighlightTextRenderer : HighlightTextRenderer
    {
        public override Color GetBackgroundColor()
        {
            var color = base.GetBackgroundColor();
            return this.GetDisabledBackColor(color);
        }
    }

    internal class XCustomCheckStateRenderer : CheckStateRenderer
    {
        public override Color GetBackgroundColor()
        {
            var color = base.GetBackgroundColor();
            return this.GetDisabledBackColor(color);
        }
    }
}
