﻿using BrightIdeasSoftware;
using System.Drawing;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XCustomEmptyListMsgOverlay : TextOverlay
    {
        public XCustomEmptyListMsgOverlay()
        {
            this.Alignment = ContentAlignment.MiddleCenter;
            this.TextColor = SystemColors.ControlDarkDark;
            this.BackColor = Color.White;
            this.BorderColor = SystemColors.ActiveBorder;
            this.BorderWidth = 2.0f;
        }
    }
}
