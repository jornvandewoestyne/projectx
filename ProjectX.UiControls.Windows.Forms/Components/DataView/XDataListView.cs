﻿using BrightIdeasSoftware;
using ProjectX.General;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

//TODO: summary

namespace ProjectX.UiControls.Windows.Forms
{
    public class XDataListView : FastDataListView, IXDataView, IXLeakable //TODO: IXLockable, IXFreezableUnSafe
    {
        public override void CopySelectionToClipboard()
        {
            var formats = this.RemoveFormatStrings();
            base.CopySelectionToClipboard();
            this.ResetFormatStrings(formats);
        }

        public override void CopyObjectsToClipboard(IList objectsToCopy)
        {
            var formats = this.RemoveFormatStrings();
            base.CopyObjectsToClipboard(objectsToCopy);
            this.ResetFormatStrings(formats);
        }

        private string[] RemoveFormatStrings()
        {
            var columns = this.AllColumns;
            var formats = columns.Select(x => x.AspectToStringFormat).ToArray();
            foreach (var column in columns)
            {
                column.AspectToStringFormat = String.Empty;
            }
            return formats;
        }

        private void ResetFormatStrings(string[] formats)
        {
            var columns = this.AllColumns;
            var length = columns.Count;
            for (int i = 0; i < length; i++)
            {
                columns[i].AspectToStringFormat = formats[i];
            }
        }

        #region Properties
        public Color DisabledBackColor { get; }
        #endregion

        int originalExStyle = -1;

        #region Property Overrides
        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        if (originalExStyle == -1)
        //            originalExStyle = base.CreateParams.ExStyle;

        //        CreateParams cp = base.CreateParams;
        //        if (!DesignMode)
        //        {
        //            //WS_EX_COMPOSITED. Prevents flickering, with BUG on ObjectListview
        //            cp.ExStyle = originalExStyle;
        //        }
        //        return cp;
        //    }
        //}
        #endregion

        #region Constructors
        public XDataListView()
        {
            //this.DoubleBuffered = false;
            this.SetHeaderFormatStyle();
            this.DisabledBackColor = this.BackColor;
            this.OverrideDefaultRenderer();
            //this.ResizeRedraw = true;
            //this.VirtualMode = false;
        }
        #endregion
        //protected override void OnDrawColumnHeader(DrawListViewColumnHeaderEventArgs e)
        //{
        //    MessageBox.Show("Test");
        //    //base.OnDrawColumnHeader(e);
        //    var g = e.Graphics;
        //    var bounds = e.Bounds; // 0, 0, e.Bounds.Width - 1, e.Bounds.Height - 1);
        //    var color = Color.CornflowerBlue;
        //    using (var brush = new SolidBrush(color))
        //    {
        //        g.FillRectangle(brush, bounds);
        //    }
        //    using (var pen = new Pen(color))
        //    {
        //        g.DrawRectangle(pen, bounds);
        //    }
        //}

        private void SetHeaderFormatStyle()
        {
            //TODO: make this better
            var normalStyle = new HeaderStateStyle()
            {
                //FrameWidth = 1.0f,
                //FrameColor = Color.WhiteSmoke,
                BackColor = Color.GhostWhite,
                Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0))),
                ForeColor = SystemColors.HotTrack,
            };

            var hotStyle = new HeaderStateStyle()
            {
                //FrameWidth = 1.0f,
                //FrameColor = Color.WhiteSmoke,
                //BackColor = Color.WhiteSmoke,
                Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0))),
                ForeColor = SystemColors.HotTrack,
            };

            this.HeaderFormatStyle = new HeaderFormatStyle();
            this.HeaderFormatStyle.Normal = normalStyle;
            this.HeaderFormatStyle.Hot = hotStyle;
            this.HeaderFormatStyle.Pressed = hotStyle;
        }


        #region Methods
        public bool Populate<TEntity>(ICollectionPage<TEntity> collection, EntityKeyName entityKeyName)
            where TEntity : BaseEntity
        {
            return this.FillDataGrid(collection, entityKeyName);
        }

        public async Task<bool> PopulateAsync<TEntity>(ICollectionPage<TEntity> collection, EntityKeyName entityKeyName, CancellationToken cancellationToken)
            where TEntity : BaseEntity
        {
            return await Task.Run(() => this.FillDataGrid(collection, entityKeyName), cancellationToken);
        }

        public EntityKey GetSelectedKeyValue(EntityKeyName entityKeyName, bool showMessage = true)
        {
            return this.GetSelectedKey(entityKeyName, showMessage);
        }

        public override void Reset()
        {
            //this.UIInvoke(x => x.Freeze());
            //this.UIInvoke(x => x.ClearObjects());
            //this.UIInvoke(x => x.Items.Clear());
            //this.UIInvoke(x => x.Unsort());
            //this.UIInvoke(x => x.AllColumns.Clear());
            //this.UIInvoke(x => x.RebuildColumns());
            //this.UIInvoke(x => x.Unfreeze());
            this.UIInvoke(x => x.EmptyListMsg = XLib.Text.LoadDataAdviceText);
            this.UIInvoke(x => x.ClearObjects());
            this.UIInvoke(x => x.Objects = null);
            //this.EmptyListMsg = XLib.Text.LoadDataAdvice;
            //TODO: remove, TEST:
            //this.DataSource = ProjectX.General.AppLibrary.Translations.ToList();
            //this.AutoResizeColumns();
            //this.BuildList();
            //this.UIInvoke(x => x.Unfreeze());
        }
        #endregion

        #region Overrides
        protected override void Dispose(bool disposing)
        {
            this.UIInvoke(x => x.ClearObjects());
            this.UIInvoke(x => x.Objects = null);
            this.EventCollection?.Dispose();
            base.Dispose(disposing);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            ControlPaint.DrawBorder(e.Graphics, ClientRectangle,
                Color.Transparent, 0, ButtonBorderStyle.None,
                Color.Yellow, 3, ButtonBorderStyle.Solid,
                Color.Transparent, 0, ButtonBorderStyle.None,
                Color.Transparent, 0, ButtonBorderStyle.None);
        }

        //protected override void WndProc(ref Message m)
        //{
        //    if (IsDisposed) return;
        //    if (DesignMode)
        //    {
        //        base.WndProc(ref m);
        //        return;
        //    }
        //    switch (m.Msg)
        //    {

        //        case (int)WindowMessages.WM_SIZE:
        //            this.WmSize(ref m);
        //            break;
        //        default:
        //            base.WndProc(ref m);
        //            break;
        //    }
        //}


        //private void WmSize(ref Message m)
        //{
        //    // If this is an MDI parent, don't pass WM_SIZE to the default
        //    // window proc. We handle resizing the MDIClient window ourselves
        //    // (using ControlDock.FILL).
        //    //
        //    //if (ctlClient == null)
        //    //{
        //    //    base.WndProc(ref m);
        //    //    if (MdiControlStrip == null && MdiParentInternal != null && MdiParentInternal.ActiveMdiChildInternal == this)
        //    //    {
        //    //        int wParam = m.WParam.ToInt32();
        //    //        MdiParentInternal.UpdateMdiControlStrip(wParam == NativeMethods.SIZE_MAXIMIZED);
        //    //    }
        //    //}
        //}

        //protected override void OnResize(EventArgs e)
        //{
        //    this.VirtualMode = false;
        //    //base.OnResize(e);
        //    //MessageBox.Show("OnResize XDataListView");
        //}


        protected override void InitializeEmptyListMsgOverlay()
        {
            this.OverrideEmptyListMsgOverlay();
        }

        /// <summary>
        /// Override to avoid greyed-out listview backcolor on populate.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnEnabledChanged(EventArgs e)
        {
            this.OverrideDisabledBackColor();
        }

        /// <summary>
        /// Override to avoid greyed-out subitem backcolor on populate.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDrawSubItem(DrawListViewSubItemEventArgs e)
        {
            this.OverrideOnDrawSubItem(e);
        }
        #endregion

        #region IXLeakable Members
        [Browsable(false)]
        public EventCollection EventCollection { get; set; } = new EventCollection();

        public virtual void Subscribe() { }
        #endregion
    }
}
