﻿using ProjectX.General;
using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

//TODO: summary

namespace ProjectX.UiControls.Windows.Forms
{
    public partial class XDataPageViewer : UserControl, IXDataPageViewer, IXLockable, IXFreezable, IXLeakable //NOT XBaseUserControl
    {
        #region Properties
        [Browsable(false)]
        public IXDataView DataView => this.mainDataView;
        #endregion

        #region Constructors
        public XDataPageViewer()
        {
            InitializeComponent();
        }
        #endregion

        #region Events
        [Category("Action"), Description("Occurs when the DataView is double-clicked.")]
        public event EventHandler DataViewDoubleClick;

        private void DataView_DoubleClick(object sender, EventArgs e)
        {
            //bubble the event up to the parent
            this.DataViewDoubleClick?.Invoke(this, e);
        }


        //protected override void OnResize(EventArgs e)
        //{
        //    base.OnResize(e);
        //}
        #endregion

        #region Methods
        public bool Populate<TEntity>(ICollectionPage<TEntity> collection, EntityKeyName entityKeyName)
            where TEntity : BaseEntity
        {
            return this.DataView.Populate<TEntity>(collection, entityKeyName);
        }

        public async Task<bool> PopulateAsync<TEntity>(ICollectionPage<TEntity> collection, EntityKeyName entityKeyName, CancellationToken cancellationToken)
            where TEntity : BaseEntity
        {
            return await this.DataView.PopulateAsync<TEntity>(collection, entityKeyName, cancellationToken);
        }

        public EntityKey GetSelectedKeyValue(EntityKeyName entityKeyName, bool showMessage = true)
        {
            return this.DataView.GetSelectedKeyValue(entityKeyName, showMessage);
        }
        #endregion

        #region IXFreezable Members
        [Browsable(false), DefaultValue(false)]
        public bool BlockInput { get; set; }

        [Browsable(false), DefaultValue(true)]
        public bool CanBlockInput { get; set; } = true;

        public virtual void Validate(IXFreezable freezable, IXConditionStrategy strategy)
        {
            strategy?.Validate(this, freezable);
        }

        public override Cursor Cursor
        {
            get
            {
                if (this.CanBlockInput)
                {
                    return base.Cursor;
                }
                return Cursors.Default;
            }
            set
            {
                base.Cursor = value;
            }
        }

        protected override void WndProc(ref Message m)
        {
            var block = this.BlockInput(ref m);
            if (!block)
            {
                base.WndProc(ref m);
            }
        }
        #endregion

        #region IXLockable Members
        public Work Work { get; set; }

        public virtual void Validate(IXLockable lockable, IXConditionStrategy strategy)
        {
            strategy?.Validate(this, lockable);
        }
        #endregion

        #region IDisposable
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            //TODO:
            this.mainDataView.UIInvoke(x => x.Dispose());
            this.EventCollection?.Dispose();
            base.Dispose(disposing);
        }
        #endregion

        #region IXLeakable Members
        [Browsable(false)]
        public EventCollection EventCollection { get; set; } = new EventCollection();

        public virtual void Subscribe()
        {
            this.Subscribe(Event.DoubleClick, this.DataView_DoubleClick, this.DataView);
        }
        #endregion
    }
}
