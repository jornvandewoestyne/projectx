﻿namespace ProjectX.UiControls.Windows.Forms
{
    partial class XDataPageViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainDataView = new ProjectX.UiControls.Windows.Forms.XDataListView();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataView)).BeginInit();
            this.SuspendLayout();
            // 
            // mainDataView
            // 
            this.mainDataView.BackColor = System.Drawing.Color.White;
            this.mainDataView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.mainDataView.CellEditUseWholeCell = false;
            this.mainDataView.CheckedAspectName = "";
            this.mainDataView.DataSource = null;
            this.mainDataView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDataView.FullRowSelect = true;
            this.mainDataView.GridLines = true;
            this.mainDataView.HeaderFont = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainDataView.HeaderMaximumHeight = 25;
            this.mainDataView.HeaderMinimumHeight = 25;
            this.mainDataView.HideSelection = false;
            this.mainDataView.IncludeColumnHeadersInCopy = true;
            this.mainDataView.Location = new System.Drawing.Point(0, 2);
            this.mainDataView.Margin = new System.Windows.Forms.Padding(0);
            this.mainDataView.Name = "mainDataView";
            this.mainDataView.RowHeight = 25;
            this.mainDataView.ShowGroups = false;
            this.mainDataView.ShowImagesOnSubItems = true;
            this.mainDataView.ShowItemCountOnGroups = true;
            this.mainDataView.ShowItemToolTips = true;
            this.mainDataView.Size = new System.Drawing.Size(150, 148);
            this.mainDataView.TabIndex = 0;
            this.mainDataView.TintSortColumn = true;
            this.mainDataView.UnfocusedSelectedBackColor = System.Drawing.SystemColors.Control;
            this.mainDataView.UnfocusedSelectedForeColor = System.Drawing.SystemColors.ControlText;
            this.mainDataView.UseCompatibleStateImageBehavior = false;
            this.mainDataView.UseHotItem = true;
            this.mainDataView.UseSubItemCheckBoxes = true;
            this.mainDataView.View = System.Windows.Forms.View.Details;
            this.mainDataView.VirtualMode = true;
            // 
            // XDataPageViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Controls.Add(this.mainDataView);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "XDataPageViewer";
            this.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            ((System.ComponentModel.ISupportInitialize)(this.mainDataView)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion

        protected XDataListView mainDataView;
    }
}
