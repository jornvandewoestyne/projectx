﻿using BrightIdeasSoftware;
using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public static class WinFormXDataListViewExtensions
    {
        //TODO: Keep default's here?
        #region Fields: Constants
        private const bool DefaultIncludeObsoleteOrOptionalProperties = false;
        private const bool DefaultFullRowSelect = true;
        private const ColumnHeaderAutoResizeStyle DefaultAutoSizeColumnsMode = ColumnHeaderAutoResizeStyle.ColumnContent;
        #endregion

        #region Methods: DataView
        /// <summary>
        /// Fills the given datagridview with data.
        /// <para>Returns false when no rows are returned.</para>
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="control"></param>
        /// <param name="collection"></param>
        /// <param name="entityKeyName"></param>
        /// <param name="includeObsoleteOrOptionalProperties"></param>
        /// <param name="autoSizeColumnsMode"></param>
        /// <param name="fullRowSelect"></param>
        /// <returns></returns>
        public static bool FillDataGrid<TEntity>(this XDataListView control, ICollectionPage<TEntity> collection, EntityKeyName entityKeyName, bool includeObsoleteOrOptionalProperties = DefaultIncludeObsoleteOrOptionalProperties, ColumnHeaderAutoResizeStyle autoSizeColumnsMode = DefaultAutoSizeColumnsMode, bool fullRowSelect = DefaultFullRowSelect)
            where TEntity : BaseEntity
        {
            var source = collection?.Items;
            if (collection != null && source != null)
            {
                var count = collection.TotalItems;
                var itemsPerPage = collection.ItemsPerPage;
                var notifications = collection.Notifications;
                var propertySchemas = collection.EntityPropertySchemas;
                control.UIInvoke(x => x.BeginUpdate());
                control.UIInvoke(x => x.ResetAll()); //TODO: enable but first test if columns need to be refreshed!
                control.UIInvoke(x => x.SetHeaders(count, notifications, propertySchemas));
                control.UIInvoke(x => x.TranslateColumnHeaders());
                control.UIInvoke(x => x.HideIdColumn(entityKeyName));
                //control.UIInvoke(x => x.DataSource = source); //don't use!
                //control.SetObjects(source, false);
                control.UIInvoke(x => x.SetObjects(source));
                //if (count > XLib.Number.IntZero && autoSizeColumnsMode != ColumnHeaderAutoResizeStyle.None && itemsPerPage <= XLib.Limit.MaxItemsPerPage)
                //{
                //    control.UIInvoke(x => x.AutoResizeColumns(autoSizeColumnsMode));
                //}
                control.UIInvoke(x => x.EndUpdate());
                if (count != XLib.MagicNumber.IntZero) { return true; }
            }
            return false;
        }

        private static bool ResetAll(this XDataListView control)
        {
            var initialLoad = control.Columns?.Count == XLib.MagicNumber.IntZero;
            control.UIInvoke(x => x.Freeze());
            control.UIInvoke(x => x.ClearObjects());
            control.UIInvoke(x => x.Objects = null);
            control.UIInvoke(x => x.Items.Clear());
            control.UIInvoke(x => x.Unsort());
            control.UIInvoke(x => x.AllColumns.Clear());
            control.UIInvoke(x => x.RebuildColumns());
            control.UIInvoke(x => x.Unfreeze());
            return initialLoad;
        }

        /// <summary>
        /// Returns the entity key of the datagridview's selected row.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="entityKeyName"></param>
        /// <param name="showMessage"></param>
        /// <returns></returns>
        public static EntityKey GetSelectedKey(this XDataListView control, EntityKeyName entityKeyName, bool showMessage = true)
        {
            //https://stackoverflow.com/questions/25038495/save-and-restore-selection-in-objectlistview

            try
            {
                //TODO: multiselect, (multi)checked , doubleclick???
                //Get the current row:
                var currentRow = control.SelectedItem;
                var checkedObject = control.CheckedObject;
                if (checkedObject != null)
                {
                    currentRow = new OLVListItem(checkedObject);
                }
                //When no item is selected:
                if (currentRow == null)
                {
                    //TODO: showMessage = showisopen!
                    XMessenger.ShowInfo(ApplicationMessage.DataGridViewWithNoSelectionAdvice());
                    return XLib.Key.NotExcistingEntityKey;
                }
                ////Get identifier:
                var entityKey = control.GetRowKey(currentRow, entityKeyName);
                //TODO: TEST "NullEntityException": -> should be ok!
                //entityKey = new object[XLib.Number.Index1] { 99999999 }.AsEntityKey();
                return entityKey;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Hides all the key column(s) of the given datagridview.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="entityKeyName"></param>
        public static bool HideIdColumn(this XDataListView control, EntityKeyName entityKeyName)
        {
            if (entityKeyName != null && entityKeyName.Members != null && control.Columns.Count > XLib.MagicNumber.IntZero)
            {
                int length = entityKeyName.Members.Length;
                for (int i = 0; i < length; i++)
                {
                    string column = entityKeyName.Members[i];
                    foreach (OLVColumn col in control.AllColumns)
                    {
                        if (col.AspectName.Equals(column))
                        {
                            col.IsVisible = false;
                            control.UIInvoke(x => x.RebuildColumns()); //Required!
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Returns the entity id of the datagridview's selected row.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="row"></param>
        /// <param name="entityKeyName"></param>
        /// <returns></returns>
        private static EntityKey GetRowKey(this XDataListView control, OLVListItem row, EntityKeyName entityKeyName)
        {
            if (entityKeyName != null && entityKeyName.Members != null && control.Columns.Count > XLib.MagicNumber.IntZero)
            {
                int length = entityKeyName.Members.Length;
                var keys = new object[length];
                var missingColumns = new List<string>();
                for (int i = 0; i < length; i++)
                {
                    var column = entityKeyName.Members[i];
                    var type = row.RowObject.GetType();
                    var property = type.GetProperty(column);
                    if (property != null)
                    {
                        keys[i] = property.GetValue(row.RowObject, null);
                    }
                    else
                    {
                        missingColumns.Add(column);
                    }
                }
                if (missingColumns.Count > XLib.MagicNumber.IntZero)
                {
                    var parameters = entityKeyName.ToString();
                    var missing = missingColumns.ToArray().TryArrayToString<string>();
                    //TODO: "!" -> gets a space
                    var message = StringHelper.Sentence(AppLibrary.ExceptionMessages.MissingKeyValueException, parameters, "!", missing);
                    throw new MissingKeyValueException(message);
                }
                return keys.AsEntityKey();
            }
            return null;
        }
        #endregion

        #region Headers
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="control"></param>
        /// <param name="count"></param>
        /// <param name="notifications"></param>
        /// <param name="propertySchemas"></param>
        private static void SetHeaders(this XDataListView control, int count, IList<Notification> notifications, IList<PropertySchema> propertySchemas)
        {
            //control.UIInvoke(x => x.Freeze());
            ////control.UIInvoke(x => x.ClearObjects());
            ////control.UIInvoke(x => x.Items.Clear());
            ////control.UIInvoke(x => x.Unsort());
            ////control.UIInvoke(x => x.AllColumns.Clear());
            ////control.UIInvoke(x => x.RebuildColumns());
            if (count == XLib.MagicNumber.IntZero)
            {
                //TODO: notifications
                if (notifications != null && notifications.Count > XLib.MagicNumber.IntZero)
                {
                    control.UIInvoke(x => x.EmptyListMsg = notifications[XLib.MagicNumber.Index0].Message);
                }
                else
                {
                    control.UIInvoke(x => x.EmptyListMsg = XLib.Text.NoDataFoundText);
                }
                //Add columns
                //if (entityProperties != null && control.AllColumns != null && control.AllColumns.Count == XLib.Number.IntZero)
                //{
                //    foreach (var item in entityProperties)
                //    {
                //        var column = new OLVColumn(item.Name, item.Name);
                //        //TODO: find a better way!
                //        if (item.PropertyType == typeof(bool))
                //        {
                //            continue;
                //            column.CheckBoxes = true;
                //            column.TextAlign = HorizontalAlignment.Center;
                //            //TODO: hard-coded!
                //            column.IsEditable = false;
                //        }
                //        control.AllColumns.Add(column);
                //    }
                //}
                //control.AllColumns.Add(new OLVColumn("", "Test"));
                //TODO: Make defaultWidth
                //control.Columns[XLib.Number.Index0].Width = control.Bounds.Width - SystemInformation.VerticalScrollBarWidth - control.RowHeadersWidth;
                //control.AutoGenerateColumns = true;
            }
            //else
            //{
            //    //TODO: doesn't reset the columns!
            //    //control.UIInvoke(x => x.ClearObjects());
            //    //control.UIInvoke(x => x.Items.Clear());
            //    //control.UIInvoke(x => x.Columns.Clear());
            //    //control.UIInvoke(x => x.RebuildColumns());
            //    control.AutoGenerateColumns = true;
            //}
            //control.UISafe(delegate
            //{
            //    control.RebuildColumns();
            //});

            if (propertySchemas != null && control.AllColumns != null && control.AllColumns.Count == XLib.MagicNumber.IntZero)
            {
                foreach (var item in propertySchemas)
                {
                    if (!item.IsObsolete)
                    {
                        var column = new OLVColumn(item.Name, item.Name)
                        {
                            Width = item.Width,
                            MinimumWidth = item.MinimumWidth,
                            MaximumWidth = item.MaximumWidth,
                            AspectToStringFormat = item.Format,
                            FillsFreeSpace = item.FillsFreeSpace,
                            FreeSpaceProportion = item.FreeSpaceProportion,
                            IsVisible = item.IsVisible,
                            IsEditable = item.IsEditable,
                            CheckBoxes = item.CheckBoxes,
                            TriStateCheckBoxes = item.TriStateCheckBoxes,
                            Hyperlink = item.Hyperlink,
                            TextAlign = (HorizontalAlignment)item.Alignment,
                            HeaderTextAlign = HorizontalAlignment.Left
                            //doesn't seem to work: WordWrap = true
                        };
                        control.AllColumns.Add(column);
                    }
                }
                control.SetFillColumn();
            }
            //else
            //{
            //    control.AutoGenerateColumns = true;
            //}

            control.UIInvoke(x => x.RebuildColumns());
            //control.UIInvoke(x => x.Unfreeze());
        }

        private static void SetFillColumn(this XDataListView control)
        {
            var style = control.HeaderFormatStyle.Normal;
            var headerFormatStyle = new HeaderFormatStyle { Normal = style, Hot = style, Pressed = style };
            var fillcolumn = new OLVColumn("", "FillColumn")
            {
                FillsFreeSpace = true,
                Sortable = false,
                HeaderFormatStyle = headerFormatStyle
            };
            control.AllColumns.Add(fillcolumn);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="control"></param>
        private static void TranslateColumnHeaders(this XDataListView control)
        {
            if (control != null)
            {
                var columns = control.AllColumns;
                var length = columns.Count;
                for (int i = 0; i < length; i++)
                {
                    columns[i].Text = columns[i].ToolTipText = StringHelper.Translation(columns[i].Text.RemoveAllWhiteSpace());
                }
            }
        }
        #endregion

        #region BackColor
        /// <summary>
        /// Override the default backcolor when the listview is disabled.
        /// <para>-> Prevents returning "SystemColors.Control".</para>
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        internal static IRenderer OverrideDefaultRenderer(this XDataListView control)
        {
            var renderer = control.DefaultRenderer;
            return renderer = renderer.GetCustomRenderer();
        }

        /// <summary>
        /// Override the default backcolor when the listview is disabled.
        /// <para>-> Prevents returning "SystemColors.Control".</para>
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        internal static Color OverrideDisabledBackColor(this XDataListView control)
        {
            if (!control.Enabled)
            {
                //control.UISafe(delegate { control.BackColor = control.DisabledBackColor; });
                control.UIInvoke(x => x.BackColor = x.DisabledBackColor);
            }
            return control.BackColor;
        }

        /// <summary>
        /// Override to avoid greyed-out subitem backcolor on populate.
        /// <para>ATTENTION! Original code is used, adding "renderer = renderer.GetCustomRenderer();"</para>
        /// </summary>
        /// <param name="e"></param>
        internal static void OverrideOnDrawSubItem(this XDataListView control, DrawListViewSubItemEventArgs e)
        {
            // Don't try to do owner drawing at design time
            if (control.IsDesignMode)
            {
                e.DrawDefault = true;
                return;
            }
            var rowObject = ((OLVListItem)e.Item).RowObject;
            // Calculate where the subitem should be drawn
            var r = e.Bounds;
            // Get the special renderer for this column. If there isn't one, use the default draw mechanism.
            var column = control.GetColumn(e.ColumnIndex);
            var renderer = control.GetCellRenderer(rowObject, column);

            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //Override to avoid greyed-out subitem backcolor on disabled and mousemove.
            renderer = renderer.GetCustomRenderer();
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // Get a graphics context for the renderer to use.
            // But we have more complications. Virtual lists have a nasty habit of drawing column 0
            // whenever there is any mouse move events over a row, and doing it in an un-double-buffered manner,
            // which results in nasty flickers! There are also some unbuffered draw when a mouse is first
            // hovered over column 0 of a normal row. So, to avoid all complications,
            // we always manually double-buffer the drawing.
            // Except with Mono, which doesn't seem to handle double buffering at all :-(
            var buffer = BufferedGraphicsManager.Current.Allocate(e.Graphics, r);
            var g = buffer.Graphics;
            g.TextRenderingHint = ObjectListView.TextRenderingHint;
            g.SmoothingMode = ObjectListView.SmoothingMode;
            // Finally, give the renderer a chance to draw something
            e.DrawDefault = !renderer.RenderSubItem(e, g, r, rowObject);
            if (!e.DrawDefault)
                buffer.Render();
            buffer.Dispose();
        }
        #endregion

        #region Overlays
        internal static IOverlay OverrideEmptyListMsgOverlay(this XDataListView control)
        {
            return control.EmptyListMsgOverlay = new XCustomEmptyListMsgOverlay();
        }
        #endregion
    }
}
