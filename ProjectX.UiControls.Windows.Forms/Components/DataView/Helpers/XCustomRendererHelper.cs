﻿using BrightIdeasSoftware;
using System.Drawing;

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// Hack methods concerning the ObjectListView.
    /// </summary>
    internal static class XCustomRendererHelper
    {
        /// <summary>
        /// Override the default backcolor when the listview is disabled.
        /// <para>-> Prevents returning "SystemColors.Control".</para>
        /// </summary>
        /// <param name="renderer"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public static Color GetDisabledBackColor(this BaseRenderer renderer, Color color)
        {
            var listview = renderer.ListView;
            return !listview.Enabled ? listview.BackColor : color;
        }

        /// <summary>
        /// Override the renderer with a custom one (when available).
        /// </summary>
        /// <param name="renderer"></param>
        /// <returns></returns>
        public static IRenderer GetCustomRenderer(this IRenderer renderer)
        {
            switch (renderer)
            {
                case HighlightTextRenderer highlightText:
                    return new XCustomHighlightTextRenderer();
                case CheckStateRenderer checkState:
                    return new XCustomCheckStateRenderer();
                default:
                    return renderer;
            }
        }
    }
}
