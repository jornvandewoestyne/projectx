﻿using System.ComponentModel;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XComboBox : ComboBox, IXLeakable
    {
        #region Overrides
        protected override void OnDropDown(System.EventArgs e)
        {
            base.OnDropDown(e);
            this.AdjustDropDownWidth();
        }
        #endregion

        #region IXLeakable Members
        [Browsable(false)]
        public EventCollection EventCollection { get; set; } = new EventCollection();

        public virtual void Subscribe() { }
        #endregion

        #region IDisposable
        protected override void Dispose(bool disposing)
        {
            //this.ToolTip?.Dispose();
            this.EventCollection?.Dispose();
            base.Dispose(disposing);
        }
        #endregion

        #region Helpers
        /// <summary>
        /// Automatically adjusts DropDownWidth to the longest value.
        /// </summary>
        private void AdjustDropDownWidth()
        {
            int width = 0;
            int max = 400;
            foreach (var obj in this.Items)
            {
                int temp = TextRenderer.MeasureText(this.GetItemText(obj), this.Font).Width;
                if (temp > width)
                {
                    width = temp;
                }
                if (width > max)
                {
                    width = max;
                    break;
                }
            }
            this.DropDownWidth = width + SystemInformation.VerticalScrollBarWidth;
        }
        #endregion
    }
}
