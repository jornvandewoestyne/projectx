﻿using PostSharp.Aspects;
using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using ProjectX.Infrastructure.LogAttributes;
using System;
using System.Data.SqlClient;
using System.Windows.Forms;

//check: https://theburningmonk.com/2012/11/aop-using-postsharp-attributes-with-async-tasktaskt-methods/
//https://doc.postsharp.net/method-interception

namespace ProjectX.UiControls.Windows.Forms
{
    [Serializable]
    public class HandleUIExceptionAttribute : LogAttribute
    {
        #region Properties
        public bool Async { get; }
        #endregion

        #region Constructors
        public HandleUIExceptionAttribute(bool async, bool logOnEntry = false, bool logOnSuccess = false, bool logOnExit = false, bool showMessage = true)
            : base(logOnEntry, logOnSuccess, logOnExit, showMessage)
        {
            this.Async = async;
        }
        #endregion

        #region Overrides
        public override void OnException(MethodExecutionArgs args)
        {
            base.OnException(args);
            //args.FlowBehavior = FlowBehavior.RethrowException;
            this.HandleException(args);
            //args.ReturnValue = -1;
            //System.Windows.Forms.MessageBox.Show("HandleUIExceptionAttribute");
        }
        #endregion

        #region Helpers
        private void HandleException(MethodExecutionArgs args)
        {
            var exception = args.Exception;
            switch (exception)
            {
                case SqlException _:
                    //if (this.Async)
                    //{
                    //    await this.TestConnectionAsync(cancellationToken);
                    //}
                    if (this.RunExit(WinFormXHelper.CreateInstanceConnection()))
                    {
                        throw new CloseApplicationException();
                    }
                    break;
                default:
                    break;
            }
        }

        private bool RunExit(Form form)
        {
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                //TODO: message!
                var message = ApplicationMessage.CloseApplicationQuestion();
                return XMessenger.ConfirmWarning(message);
            }
            return false;
        }
        #endregion
    }
}
