﻿using PostSharp.Aspects;
using ProjectX.General;
using ProjectX.Infrastructure.ExceptionHandlers;
using ProjectX.Infrastructure.Localization;
using ProjectX.Infrastructure.LogAttributes;
using System;
using System.Data.Entity.Core;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    [Serializable]
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property, AllowMultiple = false)]
    public class XInterceptAttribute : InterceptExceptionAspect
    {
        #region Method Overrides
        public override IExceptionInfo HandleDefaultStrategy(MethodInterceptionArgs args, Exception exception, bool showMessage)
        {
            return AsyncHelper.RunSync<IExceptionInfo>
                (() => this.HandleDefaultStrategyAsync(args, exception, showMessage, CancellationToken.None));
        }

        public override async Task<IExceptionInfo> HandleDefaultStrategyAsync(MethodInterceptionArgs args, Exception exception, bool showMessage, CancellationToken cancellationToken)
        {
            switch (exception)
            {
                case CloseApplicationException _:
                    await this.HandleExitStrategyAsync(args, exception, showMessage, false, cancellationToken);
                    break;
                default:
                    await base.HandleDefaultStrategyAsync(args, exception, showMessage, cancellationToken);
                    break;
            }
            return this.ExceptionInfo;
        }

        public override IExceptionInfo HandleExitStrategy(MethodInterceptionArgs args, Exception exception, bool showMessage, bool isFatal)
        {
            return AsyncHelper.RunSync<IExceptionInfo>
                (() => this.HandleExitStrategyAsync(args, exception, showMessage, isFatal, CancellationToken.None));
        }

        public override async Task<IExceptionInfo> HandleExitStrategyAsync(MethodInterceptionArgs args, Exception exception, bool showMessage, bool isFatal, CancellationToken cancellationToken)
        {
            this.ExceptionInfo = await base.HandleDefaultStrategyAsync(args, exception, false, cancellationToken);
            if (isFatal)
            {
                await base.HandleDefaultStrategyAsync(args, ApplicationError.FatalException(exception), true, cancellationToken);
            }
            this.DoExit = XApplication.Exit();
            return this.ExceptionInfo;
        }

        public override IExceptionInfo HandleDataStrategy(MethodInterceptionArgs args, Exception exception, bool showMessage)
        {
            switch (exception)
            {
                case SqlException _:
                    this.HandleConnection(args, exception, showMessage, CancellationToken.None);
                    break;
                case EntityException _:
                    this.TestConnection(args, exception, false, CancellationToken.None);
                    break;
                default:
                    this.HandleDefaultStrategy(args, exception, showMessage);
                    break;
            }
            return this.ExceptionInfo;
        }

        public override async Task<IExceptionInfo> HandleDataStrategyAsync(MethodInterceptionArgs args, Exception exception, bool showMessage, CancellationToken cancellationToken)
        {
            switch (exception)
            {
                case SqlException _:
                    await this.HandleConnectionAsync(args, exception, showMessage, cancellationToken);
                    break;
                case EntityException _:
                    await this.TestConnectionAsync(args, exception, false, cancellationToken);
                    break;
                default:
                    await this.HandleDefaultStrategyAsync(args, exception, showMessage, cancellationToken);
                    break;
            }
            return this.ExceptionInfo;
        }
        #endregion

        #region Helpers
        private bool TestConnection(MethodInterceptionArgs args, Exception exception, bool showMessage, CancellationToken cancellationToken)
        {
            this.ExceptionInfo = this.HandleDefaultStrategy(args, exception, showMessage);
            using (var dependency = new XDI().Get<XDataConnector>())
            {
                dependency.Instance.TestConnection();
            }
            return this.IsHandled = true;
        }

        private async Task<bool> TestConnectionAsync(MethodInterceptionArgs args, Exception exception, bool showMessage, CancellationToken cancellationToken)
        {
            this.ExceptionInfo = await this.HandleDefaultStrategyAsync(args, exception, showMessage, cancellationToken);
            cancellationToken.ThrowIfCancellationRequested();
            using (var dependency = new XDI().Get<XDataConnector>())
            {
                await dependency.Instance.TestConnectionAsync(cancellationToken);
            }
            return this.IsHandled = true;
        }

        private bool HandleConnection(MethodInterceptionArgs args, Exception exception, bool showMessage, CancellationToken cancellationToken)
        {
            this.ExceptionInfo = this.HandleDefaultStrategy(args, exception, showMessage);
            return ResolveConnection(cancellationToken);
        }

        private async Task<bool> HandleConnectionAsync(MethodInterceptionArgs args, Exception exception, bool showMessage, CancellationToken cancellationToken)
        {
            this.ExceptionInfo = await this.HandleDefaultStrategyAsync(args, exception, showMessage, cancellationToken);
            return ResolveConnection(cancellationToken);
        }

        private bool ResolveConnection(CancellationToken cancellationToken)
        {
            var open = false;
            var retry = false;
            using (var form = WinFormXHelper.CreateInstanceConnection())
            {
                cancellationToken.ThrowIfCancellationRequested();
                switch (form.DialogResult)
                {
                    case DialogResult.Yes:
                    case DialogResult.Retry:
                    case DialogResult.OK:
                        retry = true;
                        break;
                    case DialogResult.No:
                    case DialogResult.None:
                    case DialogResult.Abort:
                    case DialogResult.Ignore:
                    case DialogResult.Cancel:
                        open = XApplication.IsOpen;
                        if (open)
                        {
                            retry = XMessenger.ConfirmWarning(ApplicationMessage.RetryQuestion());
                        }
                        else
                        {
                            retry = !XMessenger.ConfirmWarning(ApplicationMessage.CloseApplicationQuestion());
                        }
                        if (!retry)
                        {
                            if (!open) { this.DoExit = XApplication.Exit(); }
                            throw new OperationCanceledException();
                        }
                        break;
                }
            }
            this.Retry = retry;
            return this.IsHandled = true;
        }
        #endregion
    }
}
