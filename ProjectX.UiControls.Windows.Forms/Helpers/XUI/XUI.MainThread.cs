﻿using System;

namespace ProjectX.UiControls.Windows.Forms
{
    public static class XMainThread
    {
        #region Properties
        public static int MainThreadId { get; private set; }
        #endregion

        #region Methods:
        public static void Initialize(int mainThreadId) => MainThreadId = mainThreadId;

        public static bool IsOnMainThread => Environment.CurrentManagedThreadId == MainThreadId;
        #endregion
    }
}
