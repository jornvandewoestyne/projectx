﻿using System.Drawing;
using System.Drawing.Drawing2D;

namespace ProjectX.UiControls.Windows.Forms
{
    public static class XDrawing
    {
        //https://www.extensionmethod.net/csharp/draw/fill-draw-roundedrectangle
        //TRY: https://www.codeproject.com/Articles/5649/Extended-Graphics-An-implementation-of-Rounded-Rec
        public static void FillRoundedRectangle(this Graphics g, Pen pen, Brush brush, int x, int y, int width, int height, int radius)
        {
            Rectangle corner = new Rectangle(x, y, radius, radius);
            GraphicsPath path = new GraphicsPath();
            path.AddArc(corner, 180, 90);
            corner.X = x + width - radius;
            path.AddArc(corner, 270, 90);
            corner.Y = y + height - radius;
            path.AddArc(corner, 0, 90);
            corner.X = x;
            path.AddArc(corner, 90, 90);
            path.CloseFigure();

            g.FillPath(brush, path);

            if (pen != null)
            {
                g.DrawPath(pen, path);
            }
        }
    }
}
