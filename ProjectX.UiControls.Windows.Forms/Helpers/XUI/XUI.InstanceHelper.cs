﻿using ProjectX.General;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

//TODO: make interfaces for the given controls parameters

namespace ProjectX.UiControls.Windows.Forms
{
    public static class XInstanceHelper
    {
        #region Source
        //SOURCE: http://www.codeproject.com/Questions/331389/Csharp-Reflection-Creating-Instance-from-string
        #endregion

        #region Methods
        /// <summary>
        /// Opens a form dynamically without use of the "new" keyword.
        /// <para>Returns the success result and tries to reload the parent form data when false.</para>
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public static async Task<bool> CreateNewInstance<TEntity, TControl>(this TEntity entity, XChildSearchForm form, XMdiChildDetail detail, string title, string fullTitle)
            where TEntity : BaseEntity
            where TControl : XMenuUserControl
        {
            bool success = true;
            if (entity != null)
            {
                //Get the instance:
                var instance = XInstanceHelper.GetInstanceFormName(detail);
                //Open the new form (and keep the application responsive while doing so)
                var newForm = await Task.Run(() => XInstanceHelper.CreateNewInstance(form, instance, detail, entity));
                //Reset Menu Control: Do it before showing the form!
                await Task.Run(() => newForm.AddToOpenChild<TControl>(form, form.MdiChildDetail, title, detail));
                newForm.ShowMdiChildForm(form.ParentForm, fullTitle);
            }
            else
            {
                bool retry = XMessenger.ConfirmRetryReload();
                if (retry)
                {
                    //TODO: return result SearchAsync??
                    //TEST
                    //form.TextBox.Text = "flgisfhgjidfhgklfjhdfjkghdfjkghdf";
                    //TODO: add parameter initialLoad????
                    return await form.SearchAsync(true);
                    //success = !retry;
                }
            }
            return success;
        }

        /// <summary>
        /// Opens a form dynamically without use of the "new" keyword.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="detail"></param>
        /// <returns></returns>
        public static XChildForm CreateNewInstance(XMenuUserControl control, string instance, XMdiChildDetail detail)
        {
            return XInstanceHelper.CreateNewInstance(control.ParentForm, instance, detail, null);
        }

        /// <summary>
        /// Opens a form dynamically without use of the "new" keyword.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="instance"></param>
        /// <param name="detail"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static XChildForm CreateNewInstance(Form control, string instance, XMdiChildDetail detail, IBaseEntity entity)
        {
            try
            {
                var length = XLib.MagicNumber.IntOne;
                object result = null;
                for (int i = 0; i < length; i++)
                {
                    result = null;
                    var controlType = control.GetType();
                    var assembly = Assembly.GetAssembly(controlType);
                    var instanceTypeFullName = StringHelper.QualifiedName(controlType.Namespace, instance);
                    var type = assembly.GetType(instanceTypeFullName, true);
                    if (detail.ActionIdentifier == MenuGroupAction.Unknown)
                    {
                        //Use the default constructor:
                        result = type.CreateInstance<XChildForm>();
                    }
                    else
                    {
                        if (entity == null)
                        {
                            //Use the custom constructor:
                            result = type.CreateInstance<XChildForm>(detail);
                        }
                        else
                        {
                            //Use the extended constructor:
                            result = type.CreateInstance<XChildForm>(detail, entity);
                        }
                    }

                    //IMPORTANT: Subscribe all controls:
                    var leakable = result as Control;
                    leakable?.Subscribe<IXLeakable>();

                    //TODO: why do we do this?
                    if (i < length - XLib.MagicNumber.IntOne)
                    {
                        ((XChildForm)result)?.Dispose();
                    }
                }
                return (XChildForm)result;
            }
            catch (MissingMethodException) //Constructor with parameter "MdiChildDetail" is not found!
            {
                throw;
            }
            catch (TypeLoadException) //Type is not found!
            {
                throw;
            }
        }

        /// <summary>
        /// Returns the type of a given instance.
        /// </summary>
        /// <param name="formName"></param>
        /// <returns></returns>
        public static Type GetInstanceType(XMenuUserControl control, string instance)
        {
            try
            {
                var type = control.ParentForm.GetType();
                var assembly = Assembly.Load(type.Assembly.GetName());
                var instanceTypeFullName = StringHelper.QualifiedName(type.Namespace, instance);
                return assembly.GetType(instanceTypeFullName, true);
            }
            catch (TypeLoadException) //Type is not found!
            {
                return null;
            }
        }

        /// <summary>
        /// Returns/Builds an instance form name.
        /// </summary>
        /// <param name="detail"></param>
        /// <returns></returns>
        public static string GetInstanceFormName(XMdiChildDetail detail)
        {
            var name = detail.FormIdentifier;
            var action = detail.ActionIdentifier;
            var suffix = KeyWord.Form;
            return StringHelper.Concat(name, action, suffix);
        }

        /// <summary>
        /// Returns/Builds an instance form name.
        /// </summary>
        /// <param name="actionIdentifier"></param>
        /// <returns></returns>
        public static string GetInstanceFormName(XMenuUserControl control, string actionIdentifier)
        {
            var name = control.FormIdentifier.ToString();
            var suffix = KeyWord.Form;
            var instance = StringHelper.Concat(name, actionIdentifier, suffix);
            if (XInstanceHelper.GetInstanceType(control, instance) == null)
            {
                name = StringHelper.Concat(control.ModuleBloc.ToString(), name);
            }
            return StringHelper.Concat(name, actionIdentifier, suffix);
        }
        #endregion
    }
}
