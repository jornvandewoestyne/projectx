﻿using System;
using System.ComponentModel;

#region SOURCE
//https://www.codeproject.com/Articles/52752/Updating-Your-Form-from-Another-Thread-without-Cre
#endregion

namespace ProjectX.UiControls.Windows.Forms
{
    public static class XThread
    {
        public static void UIInvoke<T>(this T control, params Action<T>[] actions)
           where T : ISynchronizeInvoke
        {
            if (actions != null)
            {
                for (int i = 0; i < actions.Length; i++)
                {
                    control?.UIInvoke<T>(actions[i]);
                }
            }
        }

        private static void UIInvoke<T>(this T control, Action<T> action)
            where T : ISynchronizeInvoke
        {
            if (control != null)
            {
                if (control.InvokeRequired)
                    control.BeginInvoke(action, new object[] { control });
                else
                    action(control);
            }
        }
    }
}
