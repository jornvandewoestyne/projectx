﻿using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    internal static class XWndProc
    {
        public static bool BlockInput(this IXFreezable freezable, ref Message m)
        {
            if (freezable.BlockInput && freezable.CanBlockInput)
            {
                switch (m.Msg)
                {
                    case (int)WindowMessages.WM_LBUTTONDOWN:
                    case (int)WindowMessages.WM_LBUTTONUP:
                    case (int)WindowMessages.WM_LBUTTONDBLCLK:
                    case (int)WindowMessages.WM_RBUTTONDOWN:
                    case (int)WindowMessages.WM_RBUTTONUP:
                    case (int)WindowMessages.WM_RBUTTONDBLCLK:
                        {
                            return true;
                        }
                }
            }
            return false;
        }
    }
}
