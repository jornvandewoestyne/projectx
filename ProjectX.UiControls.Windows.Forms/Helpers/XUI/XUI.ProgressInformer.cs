﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class XProgressInformer : DisposableItem
    {
        #region Properties
        public bool Success { get; private set; }
        public bool Initial { get; private set; }
        public Work Work { get; private set; }
        public IList<WeakReference<IXLockable>> Controls { get; private set; }
        public XWaitCursor XWaitCursor { get; private set; } //don't use a static!
        public Stopwatch Stopwatch { get; private set; }

        private WeakReference<XChildSearchForm> Reference { get; set; }
        public XChildSearchForm Container => this.Reference.TryGet();
        #endregion

        #region Constructors
        public XProgressInformer(XChildSearchForm container, Work work, bool initialLoad = false)
        {
            this.Initialize(container, work, initialLoad);
        }
        #endregion

        #region Methods
        private void Initialize(XChildSearchForm container, Work work, bool initialLoad)
        {
            //Set Values:
            this.Stopwatch = Stopwatch.StartNew();
            this.Reference = container.AsWeakReference<XChildSearchForm>();
            this.XWaitCursor = new XWaitCursor(container);
            this.Work = work;
            this.Initial = initialLoad;
            this.SetControls();
            this.SetCancellationToken();
            //Do Work:
            this.StartAnimation();
            this.SetSearchString(XLib.Preference.CleanInput);
        }

        private void SetCancellationToken()
        {
            this.Container.CancellationTokenSource = new CancellationTokenSource();
            this.Container.CancellationToken = this.Container.CancellationTokenSource.Token;
        }

        private void SetControls()
        {
            this.Controls = this.Container.GetControls<IXLockable>().ToList();
            this.SetControls(false);
        }

        private void SetControls(bool bResult)
        {
            this.Container.SetLockable(new XBooleanStrategy(bResult), this.Controls);
        }

        private IXConditionStrategy SetStrategy()
        {
            IXConditionStrategy strategy = new XOnSearchSucceededStrategy();
            if (this.Work == Work.Search && !this.Success)
            {
                strategy = new XOnSearchOnlyOrFailedStrategy();
            }
            return strategy;
        }

        private void ResetControls(bool bResult)
        {
            var strategy = this.SetStrategy();
            if (strategy != null)
            {
                this.Container.SetLockable(strategy, this.Controls);
            }
            else
            {
                this.SetControls(bResult);
            }
        }

        private void SetSearchString(bool cleanInput)
        {
            if (this.Work == Work.Search)
            {
                this.Container.UIInvoke(x => x.SearchString = x.TextBox.CleanText(cleanInput));
            }
        }

        private void ResetText(bool selectAll)
        {
            if (this.Work == Work.Search)
            {
                this.Container.UIInvoke(x => x.TextBox.SelectText(selectAll));
            }
            this.Container.Informer.UIInvoke(x => x.ResetInfo(ApplicationProgressInfo.CompletedTask(), this.Stopwatch));
        }

        private void StartAnimation()
        {
            this.Container.UIInvoke(x => x.PictureBox.Image = ProjectX.Infrastructure.Properties.Resources.Animation);
        }

        /// <summary>
        /// Ensures responsiveness while performing non thread-safe actions.
        /// </summary>
        public void StopAnimation()
        {
            var image = new Bitmap(this.Container.Picture, new Size(48, 48));
            this.Container.UIInvoke(x => x.PictureBox.Image = image);
        }

        public bool SetSuccess(bool bResult, bool stopAnimation = false)
        {
            if (stopAnimation)
            {
                this.StopAnimation();
            }
            return this.Success = bResult;
        }

        public void ResetProgress()
        {
            this.Container.Informer.UIInvoke(x => x.ResetInfo());
        }

        public void ShowProgress(FriendlyMessage message)
        {
            this.Container.Informer.UIInvoke(x => x.ShowInfo(message));
        }

        public void ShowTimeOut(FriendlyMessage message = null)
        {
            this.Container.Informer.UIInvoke(x => x.ShowTimeOut(message));
        }

        public void ShowWarning(FriendlyMessage message)
        {
            this.Container.Informer.UIInvoke(x => x.ShowWarning(message));
        }

        public void ShowException(Exception exception)
        {
            this.Container.Informer.UIInvoke(x => x.ShowException(exception));
        }
        #endregion       

        #region DisposableItem
        protected override void Finish()
        {
            this.StopAnimation();
            this.Stopwatch.Stop();
            this.ResetText(XLib.Preference.SelectAllText);
            this.ResetControls(true);
        }

        protected override void Unsubscribe()
        {
            //No actions needed
        }

        protected override void Unbind()
        {
            this.Stopwatch = null;
            this?.Controls.Clear();
            this.Controls = null;
            this.Container.CancellationTokenSource.Dispose();
            this?.XWaitCursor.Dispose();
            this.Reference = null;
        }
        #endregion
    }
}
