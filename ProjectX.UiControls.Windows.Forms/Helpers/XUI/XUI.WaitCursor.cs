﻿using ProjectX.General;
using System;
using System.Windows.Forms;

//https://www.monitis.com/blog/improving-net-application-performance-part-5-finalize-and-dispose/

namespace ProjectX.UiControls.Windows.Forms
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class XWaitCursor : DisposableItem
    {
        #region Properties
        private WeakReference<Control> Reference { get; set; }
        public Control Container => this.Reference.TryGet();

        public bool Enabled
        {
            get { return this.Container.UseWaitCursor; }
            set
            {
                if (value == this.Container.UseWaitCursor) { return; }
                this.Container.UIInvoke(x => x.UseWaitCursor = value);
                // Send WM_SETCURSOR
                if (this.Container != null)
                {
                    SendMessage(this.Container.Handle, 0x20, this.Container.Handle, (IntPtr)1);
                }
            }
        }
        #endregion

        #region Constructors
        public XWaitCursor(Control container)
        {
            if (container == null)
            {
                this.Reference = Form.ActiveForm.AsWeakReference<Control>();
            }
            else
            {
                this.Reference = container.AsWeakReference<Control>();
            }
            this.Enabled = true;
        }
        #endregion

        #region Methods
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);
        #endregion

        #region DisposableItem
        protected override void Finish()
        {
            this.Enabled = false;
        }

        protected override void Unsubscribe()
        {
            //No actions needed
        }

        protected override void Unbind()
        {
            this.Reference = null;
        }
        #endregion
    }

}
