﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using ProjectX.Infrastructure.LogAttributes;
using System;
using System.Threading;
using System.Threading.Tasks;

//TODO: XChildSearchForm -> interface?
//TODO: XChildSearchForm -> methods

namespace ProjectX.UiControls.Windows.Forms
{
    public static class XSearchViewHelper
    {
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        //[Log] //TODO: check the logattribute -> possible bug on returnvalue when attribute is enabled
        //[XIntercept(ExceptionStrategy.Data)]
        public static async Task<bool> RunSearchAsync(this XChildSearchForm view, bool initialLoad)
        {
            using (var informer = new XProgressInformer(view, Work.Search, initialLoad))
            {
                try
                {
                    informer.ShowProgress(ApplicationProgressInfo.SearchingTask());
                    informer.SetSuccess(await view.GetDataAsync(informer, view.CancellationToken, initialLoad));
                    informer.ShowProgress(ApplicationProgressInfo.LoadingTask());
                    informer.SetSuccess(await view.Presenter.PopulateDataAsync(view, view.CancellationToken));
                    return informer.Success;
                }
                //TODO: catch System.Data.Entity.Core.EntityCommandExecutionException: An error occurred while reading from the store provider's data reader.
                catch (OperationCanceledException)
                {
                    informer.ShowWarning(ApplicationMessage.GenericMessage("Geannuleerd!"));
                    informer.SetSuccess(false);
                    view.DataViewer.DataView.Reset();
                    //throw;
                    return informer.Success;
                }
                //TODO: this is a test!
                catch (Exception exception)
                {
                    informer.ShowException(exception);
                    informer.SetSuccess(false);
                    view.DataViewer.DataView.Reset();
                    throw;
                    return informer.Success;
                }
                //TODO: catch ArgumentNullPropertyException and show Notification
                finally
                {
                    //TODO: Ensure buffering is correct! -> make method (interface?)
                    var container = view.ParentForm;
                    container.UIInvoke(x => x.MaximizeBox = false);
                }
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="view"></param>
        /// <param name="showIsOpen"></param>
        /// <returns></returns>
        /// <remarks>PostSharp LogAttribute is required -> Forces to reload the data, when the entity is not found!</remarks>
        [Log]
        public static async Task<bool> RunShowEntityAsync(this XChildSearchForm view, bool showNoSelection, bool showIsOpen)
        {
            if (view.ViewButton.Enabled)
            {
                using (var informer = new XProgressInformer(view, Work.View))
                {
                    try
                    {
                        informer.ShowProgress(ApplicationProgressInfo.OpeningTask());
                        var entityKey = view.Presenter.GetSelectedKey(view, showNoSelection);
                        var invalid = ArrayHelper.Compare<object>(entityKey.ResolveNull().Keys, XLib.Key.NotExcistingItem);
                        if (!invalid)
                        {
                            //Create a new instance:
                            var form = view.InstanceIsOpen(entityKey);
                            if (form == null)
                            {
                                informer.SetSuccess(await view.Presenter.SetEntityAsync(entityKey, informer, view.CancellationToken));
                                informer.SetSuccess(await view.Presenter.ShowEntityAsync(view, informer, view.CancellationToken));
                            }
                            else
                            {
                                form.ActivateMdiChildForm();
                                XMessenger.ShowMessageIsOpen(showIsOpen);
                                informer.SetSuccess(true);
                            }
                        }
                        return informer.Success;
                    }
                    catch (Exception exception)
                    {
                        informer.ShowException(exception);
                        informer.SetSuccess(false);
                        throw;
                    }
                    finally
                    {
                        informer.ResetProgress();
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="view"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        //[Log]
        private static async Task<bool> GetDataAsync(this XChildSearchForm view, XProgressInformer informer, CancellationToken cancellationToken, bool initialLoad)
        {
            #region Source
            //https://stackoverflow.com/questions/10490307/retry-a-task-multiple-times-based-on-user-input-in-case-of-an-exception-in-task
            #endregion

            //return await view.Presenter.GetDataAsync(view, cancellationToken, initialLoad);

            var success = false;
            var proceed = true;
            var cancel = false;
            var retryCount = XLib.Limit.RetryLimit;

            //TEST
            //view.Ordering = view.Presenter.GetOrdering(new OrderingSetting("XFullName")); //new Ordering(view.Presenter.DataType, new OrderingSetting("XFullName"));

            while (proceed)
            {
                try
                {
                    //await NinjectCompositionRoot.Get<IService>().TestConnectionAsync(cancellationToken);
                    //TODO: throw error when !ordering.IsValid (get best solution)
                    if (view.Ordering != null && !view.Ordering.IsValid)
                    {
                        throw new ArgumentNullPropertyException(MethodCommand.OrderBy, view.Presenter.DataType, "XFullName", null);
                    }
                    success = await view.Presenter.GetDataAsync(view, informer, cancellationToken, initialLoad);
                    proceed = false;
                }
                catch (ArgumentNullPropertyException)
                {
                    if (retryCount == XLib.MagicNumber.IntZero) { throw; }
                    //TODO: get default ordering
                    //Retry without OrderBy:
                    view.Ordering = view.Presenter.GetOrdering(); //new OrderingSetting("FullName", OrderingDirection.Descending)); //new Ordering(view.Presenter.DataType, new OrderingSetting("FullName"));
                    retryCount--;
                }
                catch
                {
                    proceed = false;
                    cancel = true;
                    //XMessenger.ShowInfo(ApplicationMessage.GenericMessage(exception.Message));
                    //TODO: test with XIntercept -> throw is disabled for now
                    //throw;
                }
            }
            //Inform the user!
            //TODO: do not show message again -> user
            try
            {
                if (cancel)
                {
                    view.CancellationTokenSource.Cancel();
                }
                //TODO: catch combo invalid and nosettings
                //TODO: use notifications to inform user (no messagebox)
                if (retryCount < XLib.Limit.RetryLimit)
                {
                    throw new ArgumentNullPropertyException(MethodCommand.OrderBy, view.Presenter.DataType, "XFullName", null);
                }
                if (view.Ordering != null && !view.Ordering.HasDefaultSettings)
                {
                    //use different exception! -> text is not good
                    throw new ArgumentNullPropertyException(MethodCommand.OrderBy, view.Presenter.DataType, "no settings", null);
                }
                return success;
            }
            //catch (ArgumentNullPropertyException)
            //{
            //    throw;
            //}
            catch
            {
                //XMessenger.ShowInfo(ApplicationMessage.GenericMessage("Test is geslaagd!"));
                throw;
            }
        }
    }
}
