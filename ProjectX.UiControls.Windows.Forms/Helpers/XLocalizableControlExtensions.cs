﻿using ProjectX.General;
using System.Linq;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    internal static class XLocalizableControlExtensions
    {
        #region Methods:
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="control"></param>
        public static void TryLocalize(this IXLocalizableControl control)
        {
            control.TryLocalizeText();
            control.TryLocalizeToolTip();
            control.TryLocalizeForm();
        }
        #endregion

        #region Private Methods
        private static void TryLocalizeText(this IXLocalizableControl control)
        {
            if (control.XControlText > FriendlyTextType.None)
            {
                control.Text = StringHelper.Translation(control.XControlText);
            }
        }

        private static void TryLocalizeToolTip(this IXLocalizableControl control)
        {
            if (control.XToolTipText > FriendlyToolTipType.None)
            {
                var caption = StringHelper.Translation(control.XToolTipText);
                if (control is IXLocalizableComponent component)
                {
                    component.ToolTipText = caption;
                }
                else
                {
                    var toolTip = control.ToolTip = new ToolTip();
                    toolTip.SetToolTip((Control)control, caption);
                }
            }
        }

        private static void TryLocalizeForm(this IXLocalizableControl control)
        {
            if (control is Form)
            {
                var baseControl = control as Control;
                var collection = baseControl.GetControls<IXLocalizableControl>().ToList();
                foreach (var item in collection)
                {
                    item.TryGet().Localize();
                }
            }
        }
        #endregion
    }
}
