﻿using ProjectX.General;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public static class XControlExtensions
    {
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="container"></param>
        /// <param name="strategy"></param>
        /// <returns></returns>
        public static IEnumerable<WeakReference<T>> GetControls<T>(this Control container, IXConditionStrategy strategy = null)
            where T : class
        {
            if (container != null)
            {
                var stack = new Stack<Control>();
                stack.Push(container);
                while (stack.Count > XLib.MagicNumber.IntZero)
                {
                    container = stack.Pop();
                    foreach (Control control in container.Controls)
                    {
                        stack.Push(control);
                        if (control is T result)
                        {
                            if (strategy != null)
                            {
                                if (strategy.GetResult(control) == true)
                                {
                                    yield return result.AsWeakReference<T>();
                                }
                            }
                            else
                            {
                                yield return result.AsWeakReference<T>();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Validate each <see cref="IXFreezable"/> control in the given container.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="container"></param>
        /// <param name="strategy"></param>
        /// <param name="controls"></param>
        public static void SetFreezable<T>(this Control container, IXConditionStrategy strategy, IEnumerable<WeakReference<T>> controls = null)
            where T : class, IXFreezable
        {
            if (controls == null)
            {
                controls = container.GetControls<T>();
            }
            if (controls != null)
            {
                foreach (var control in controls)
                {
                    var item = control.TryGet();
                    container.UIInvoke(x => item.Validate(item, strategy));
                }
            }
        }

        /// <summary>
        /// Validate each <see cref="IXLockable"/> control in the given container.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="container"></param>
        /// <param name="strategy"></param>
        /// <param name="controls"></param>
        public static void SetLockable<T>(this Control container, IXConditionStrategy strategy, IEnumerable<WeakReference<T>> controls = null)
            where T : class, IXLockable
        {
            if (controls == null)
            {
                controls = container.GetControls<T>();
            }
            if (controls != null)
            {
                foreach (var control in controls)
                {
                    var item = control.TryGet();
                    container.UIInvoke(x => item.Validate(item, strategy));
                }
            }
        }

        /// <summary>
        /// Subscribe each <see cref="IXLeakable"/> control in the given container.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="container"></param>
        /// <param name="controls"></param>
        public static void Subscribe<T>(this Control container, IEnumerable<WeakReference<T>> controls = null)
            where T : class, IXLeakable
        {
            var parent = container as T;
            container.UIInvoke(x => parent?.Subscribe());
            if (controls == null)
            {
                controls = container.GetControls<T>();
            }
            if (controls != null)
            {
                foreach (var control in controls)
                {
                    var item = control.TryGet();
                    container.UIInvoke(x => item.Subscribe());
                }
            }
        }
    }
}
