﻿using ProjectX.General;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.UiControls.Windows.Forms
{
    public sealed class XTaskHelper
    {
        #region Methods:
        [XIntercept(Strategy = ExceptionStrategy.Data)]
        public static void Run(Action Run)
        {
            while (true)
            {

                Run();
                break;
            }
        }

        [XIntercept(Strategy = ExceptionStrategy.Data)]
        public static async Task RunAsync(XProgressInformer informer, CancellationToken cancellationToken, Func<CancellationToken, Task> Run)
        {
            while (true)
            {
                await Run(cancellationToken).RunWithTimeoutAsync(informer);
                break;
            }
        }

        [XIntercept(Strategy = ExceptionStrategy.Data)]
        public static T Run<T>(SearchString searchString, Ordering ordering, Paging paging, Func<SearchString, Ordering, Paging, T> Run)
        {
            while (true)
            {
                return Run(searchString, ordering, paging);
            }
        }

        [XIntercept(Strategy = ExceptionStrategy.Data)]
        public static async Task<T> RunAsync<T>(SearchString searchString, Ordering ordering, Paging paging, XProgressInformer informer, CancellationToken cancellationToken, Func<SearchString, Ordering, Paging, CancellationToken, Task<T>> RunAsync)
        {
            while (true)
            {
                return await RunAsync(searchString, ordering, paging, cancellationToken).RunWithTimeoutAsync<T>(informer);
            }
        }
        #endregion
    }
}
