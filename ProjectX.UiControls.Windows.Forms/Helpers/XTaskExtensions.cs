﻿using ProjectX.Infrastructure.Localization;
using System;
using System.Threading;
using System.Threading.Tasks;

#region Source
//https://bettereducation.com.au/forum/it.aspx?g=posts&t=7753
#endregion

namespace ProjectX.UiControls.Windows.Forms
{
    internal static class XTaskExtensions
    {
        #region TimeOut
        //[Log] => don't implement LogAttribute, exceptions need to be thrown to get catched on the correct method
        /// <summary>
        /// USAGE: return await Task.Run(() => task, [cancellationToken]).RunWithTimeoutAsync(...)
        /// which ensures responsiveness in the UI at each moment
        /// </summary>
        /// <param name="task"></param>
        /// <param name="informer"></param>
        /// <param name="timeout"></param>
        /// <param name="proceed"></param>
        /// <returns></returns>
        public static async Task RunWithTimeoutAsync(this Task task, XProgressInformer informer = null, double timeout = XLib.Preference.DefaultTaskTimeOut, bool proceed = true)
        {
            try
            {
                using (var timeoutCancellationTokenSource = new CancellationTokenSource())
                {
                    var completedTask = await Task.WhenAny(task, Task.Delay(TimeSpan.FromMilliseconds(timeout), timeoutCancellationTokenSource.Token));
                    if (completedTask == task)
                    {
                        // Task completed within timeout.
                        // Consider that the task may have faulted or been canceled.
                        // We re-await the task so that any exception/cancellation is rethrown.
                        timeoutCancellationTokenSource.Cancel();
                        await task;
                    }
                    else
                    {
                        using (var splasher = Splasher.Instance)
                        {
                            if (!XApplication.HasOpenDialogForm())
                            {
                                splasher.Show(ApplicationMessage.TimeOutInfo());
                            }
                            // Timeout/Cancellation logic
                            if (proceed)
                            {
                                informer?.ShowTimeOut();
                                await task;
                            }
                            else
                            {
                                //TODO: set proper message
                                throw new TimeoutException();
                            }
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                Splasher.Instance.Dispose();
            }
        }

        //[Log] => don't implement LogAttribute, exceptions need to be thrown to get catched on the correct method
        /// <summary>
        /// USAGE: return await Task.Run(() => task, [cancellationToken]).RunWithTimeoutAsync(...)
        /// which ensures responsiveness in the UI at each moment
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="task"></param>
        /// <param name="informer"></param>
        /// <param name="timeout"></param>
        /// <param name="proceed"></param>
        /// <returns></returns>
        public static async Task<TResult> RunWithTimeoutAsync<TResult>(this Task<TResult> task, XProgressInformer informer = null, double timeout = XLib.Preference.DefaultTaskTimeOut, bool proceed = true)
        {
            try
            {
                using (var timeoutCancellationTokenSource = new CancellationTokenSource())
                {
                    var completedTask = await Task.WhenAny(task, Task.Delay(TimeSpan.FromMilliseconds(timeout), timeoutCancellationTokenSource.Token));
                    if (completedTask == task)
                    {
                        // Task completed within timeout.
                        // Consider that the task may have faulted or been canceled.
                        // We re-await the task so that any exception/cancellation is rethrown.
                        timeoutCancellationTokenSource.Cancel();
                        return await task;
                    }
                    else
                    {
                        using (var splasher = Splasher.Instance)
                        {
                            if (!XApplication.HasOpenDialogForm())
                            {
                                splasher.Show(ApplicationMessage.TimeOutInfo());
                            }
                            // Timeout/Cancellation logic
                            if (proceed)
                            {
                                informer?.ShowTimeOut();
                                return await task;
                            }
                            else
                            {
                                //TODO: set proper message
                                throw new TimeoutException();
                            }
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                Splasher.Instance.Dispose();
            }
        }
        #endregion
    }
}
