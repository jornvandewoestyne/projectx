﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using ProjectX.Infrastructure.LogAttributes;
using System;
using System.Drawing;
using System.Windows.Forms;

//TODO: remove
//TODO: Subscribe

namespace ProjectX.UiControls.Windows.Forms
{
    [Obsolete]
    public partial class XMainHeaderUserControl : XBaseUserControl
    {
        #region Fields
        private string _title;
        private Point _mouseOffset;
        #endregion

        #region Properties
        public string Title
        {
            get { return this._title; }
            set
            {
                this._title = value;
                this.SetTitle();
            }
        }

        public bool ConfirmToClose { get; set; }

        private Rectangle RestoreBounds { get; set; }

        private Point MouseOffset
        {
            get { return this._mouseOffset; }
            set { this._mouseOffset = value; }
        }
        #endregion

        #region Properties: Controls
        private XLabel Label
        {
            get { return this.titleLabel; }
        }
        #endregion

        #region Constructors
        public XMainHeaderUserControl()
        {
            InitializeComponent();
            this.minimizeButton.XToolTipText = FriendlyToolTipType.Minimize;
            this.maximizeButton.XToolTipText = FriendlyToolTipType.Maximize;
            this.closeButton.XToolTipText = FriendlyToolTipType.CloseApplication;
        }
        #endregion

        #region Events
        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            this.ParentForm.FormClosing += new FormClosingEventHandler(ParentForm_FormClosing);
        }

        [Log]
        void ParentForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ////TODO: disable or remove control, solved with XApplicationContext!
            //return;

            bool close = CloseParentForm();
            e.Cancel = !close;
            if (close)
            {
                throw new CloseApplicationException();
            }
        }

        private void Minimize(object sender, EventArgs e)
        {
            MinimizeParentForm();
        }

        private void Maximize(object sender, EventArgs e)
        {
            MaximizeParentForm();
        }

        private void Close(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        #region Methods: Initialize
        private void SetTitle()
        {
            this.Label.Text = this.Title;
        }
        #endregion

        #region Methods: Resize Form
        private void MinimizeParentForm()
        {
            Form parent = (Form)this.ParentForm;
            parent.WindowState = FormWindowState.Minimized;
        }

        private void MaximizeParentForm()
        {
            //TODO: Maximize...
            Form parent = (Form)this.ParentForm;
            Rectangle maximizedBounds = Screen.PrimaryScreen.WorkingArea;
            if (parent.Bounds == maximizedBounds)
            {
                parent.Bounds = this.RestoreBounds;
                parent.CenterToScreen();
            }
            else
            {
                this.RestoreBounds = parent.Bounds;
                parent.Bounds = maximizedBounds;
            }
        }
        #endregion

        #region Methods: Close
        private void Close()
        {
            return;
            Form parent = (Form)this.Parent;
            parent.Close();
        }

        private bool CloseParentForm()
        {
            if (this.ConfirmToClose)
            {
                var message = ApplicationMessage.CloseApplicationQuestion();
                return XMessenger.ConfirmWarning(message);
            }
            return true;
        }
        #endregion

        #region Methods: Events -> Move Form With "FormBorderStyle.None;"
        ///QUESTION: With "FormBorderStyle.None;", how can I move the form by mouse?
        ///SOURCE:https://social.msdn.microsoft.com/Forums/vstudio/en-US/44530727-42ec-461b-9bea-36026a449f05/with-formborderstylenone-how-can-i-move-the-form-by-mouse?forum=csharpgeneral

        private void GetMousePosition(object sender, MouseEventArgs e)
        {
            Form parent = (Form)this.ParentForm;
            if (parent.FormBorderStyle == FormBorderStyle.None)
            {
                this.MouseOffset = new Point(-e.X, -e.Y);
            }
        }

        private void SetMousePosition(object sender, MouseEventArgs e)
        {
            Form parent = (Form)this.ParentForm;
            if (parent.FormBorderStyle == FormBorderStyle.None)
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    Point mousePos = Control.MousePosition;
                    mousePos.Offset(this.MouseOffset.X, this.MouseOffset.Y);
                    this.Parent.Location = mousePos; //move the form to the desired location
                }
            }
        }
        #endregion
    }
}
