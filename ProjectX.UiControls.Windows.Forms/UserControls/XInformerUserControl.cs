﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


//TODO: "duurt langer" not allways shown! -> CHECK!

#region Source
//Source: https://mdavies.net/2013/04/27/safely-wake-exit-sleeping-thread-without-thread-abort/
#endregion

namespace ProjectX.UiControls.Windows.Forms
{
    public partial class XInformerUserControl : UserControl, IXLeakable
    {
        public XInformerUserControl()
        {
            InitializeComponent();
            this.ShowAbort(false);
        }

        #region Properties
        private XLabel Label => this.messageLabel;
        private XLinkLabel LinkLabel => this.abortLinkLabel;

        private double Duration { get; set; } = XLib.Preference.DefaultShowInfoDuration;
        private CancellationTokenSource CancellationTokenSource { get; set; }
        private ManualResetEvent ResetEvent { get; set; }

        public bool Success { get; private set; }
        public string Message => StringHelper.Join(XLib.Text.Separator, ProgressMessage?.Message, this.ElapsedTime, WarningMessage?.Message);
        public string ElapsedTime { get; private set; }
        public FriendlyMessage ProgressMessage { get; private set; }
        public FriendlyMessage WarningMessage { get; private set; }
        public Exception Exception { get; private set; }
        #endregion

        #region Methods
        public void ShowInfo(FriendlyMessage progress, double duration = XLib.Preference.DefaultShowInfoDuration)
        {
            this.UIInvoke(x => x.SetInfo(progress, null, null, Color.White, duration));
        }

        public void ShowTimeOut(FriendlyMessage warning = null, double duration = XLib.Preference.DefaultShowWarningDuration)
        {
            if (warning == null) { warning = ApplicationMessage.TimeOutInfo(); }
            this.UIInvoke(x => x.SetInfo(this.ProgressMessage, warning, null, Color.MistyRose, duration, true, true));
        }

        public void ShowWarning(FriendlyMessage warning, double duration = XLib.Preference.DefaultShowWarningDuration)
        {
            this.UIInvoke(x => x.SetInfo(this.ProgressMessage, warning, null, Color.MistyRose, duration, false, false));
        }

        public void ShowException(Exception exception, double duration = XLib.Preference.DefaultShowExceptionDuration)
        {
            this.UIInvoke(x => x.SetInfo(this.ProgressMessage, ApplicationMessage.GeneralAlert(), exception, Color.MistyRose, duration, false, false));
        }

        private void SetInfo(FriendlyMessage progress, FriendlyMessage warning, Exception exception, Color color, double duration, bool success = true, bool abort = false)
        {
            this.ElapsedTime = null;
            this.Success = success;
            this.Duration = duration;
            this.ProgressMessage = progress;
            this.WarningMessage = warning;
            this.Exception = exception;
            this.UIInvoke(
                x => x.WakeUp(),
                x => x.ShowAbort(abort),
                x => x.SetColor(color),
                x => x.SetText(),
                x => x.Show(),
                x => x.Refresh());
        }

        public void ResetInfo(FriendlyMessage message, Stopwatch stopwatch)
        {
            if (this.Success)
            {
                this.ProgressMessage = message;
            }
            this.Success = true;
            this.ElapsedTime = StringHelper.Sentence(stopwatch.Elapsed.ToString("s\\.ff"), XLib.Text.Seconds);
            this.UIInvoke(
                x => x.ShowAbort(false),
                x => x.SetText(),
                x => x.ResetInfo());
        }

        public void ResetInfo()
        {
            this.UIInvoke(
                x => x.DoWork(
                    this.Duration,
                    action => x.SetColor(Color.White),
                    action => x.Hide()));
        }
        #endregion

        #region Helpers
        private void SetText()
        {
            this.UIInvoke(x => x.Label.Text = this.Message);
        }

        private void SetColor(Color color)
        {
            if (!color.IsEmpty)
            {
                this.UIInvoke(
                    x => x.BackColor = color,
                    x => x.Label.BackColor = color,
                    x => x.LinkLabel.BackColor = color);
            }
        }

        private void ShowAbort(bool visible)
        {
            this.UIInvoke(x => x.LinkLabel.Visible = visible);
        }

        private void WakeUp()
        {
            if (this.CancellationTokenSource != null && this.ResetEvent != null)
            {
                // First set the cancellation token to Cancelled
                this.CancellationTokenSource.Cancel();
                // Now wake up the Start() thread if it is currently sleeping
                this.ResetEvent.Set();
            }
        }

        private void DoWork(double duration, params Action<XInformerUserControl>[] actions)
        {
            this.CancellationTokenSource = new CancellationTokenSource();
            // Set up a signal object in a default false state, to be used for waking up the Start() thread if sleeping
            this.ResetEvent = new ManualResetEvent(false);
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    this.ResetEvent.WaitOne(TimeSpan.FromMilliseconds(duration));
                    if (this.CancellationTokenSource.Token.IsCancellationRequested)
                    {
                        return;
                    }
                    //Do work:
                    this.UIInvoke(actions);
                }
            }, this.CancellationTokenSource.Token);
        }
        #endregion

        #region IXLeakable Members
        [Browsable(false)]
        public EventCollection EventCollection { get; set; } = new EventCollection();

        public virtual void Subscribe()
        {
            this.Subscribe(Event.Click, this.Abort, this.LinkLabel);
        }
        #endregion

        private void Abort(object sender, EventArgs e)
        {
            if (this.ParentForm is XChildForm parent)
            {
                //TODO: use Cancel form searchForm
                parent.Cancel();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            this.LinkLabel?.Dispose();
            this.CancellationTokenSource?.Dispose();
            this.CancellationTokenSource = null;
            this.ResetEvent?.Dispose();
            this.ResetEvent = null;
            base.Dispose(disposing);
        }
    }
}
