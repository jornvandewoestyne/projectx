﻿using ProjectX.General;
using ProjectX.Infrastructure.Localization;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public partial class XChildHeaderUserControl : XBaseUserControl, IXLockable, IXFreezable
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public override void Subscribe()
        {
            this.Subscribe(Event.Click, this.CloseForm, this.Label, this.Button);
            this.Subscribe(Event.MouseEnter, this.SetBackColor, this.Label, this.Button);
            this.Subscribe(Event.MouseLeave, this.ResetBackColor, this.Label, this.Button);
            base.Subscribe();
        }

        #region Fields
        private string _title;
        #endregion

        #region IXFreezable Members
        [Browsable(false), DefaultValue(false)]
        public bool BlockInput { get; set; }

        [Browsable(false), DefaultValue(true)]
        public bool CanBlockInput { get; set; } = true;

        public virtual void Validate(IXFreezable freezable, IXConditionStrategy strategy)
        {
            strategy?.Validate(this, freezable);
        }

        public override Cursor Cursor
        {
            get
            {
                if (this.CanBlockInput)
                {
                    return base.Cursor;
                }
                return Cursors.Default;
            }
            set
            {
                base.Cursor = value;
            }
        }

        protected override void WndProc(ref Message m)
        {
            var block = this.BlockInput(ref m);
            if (!block)
            {
                base.WndProc(ref m);
            }
        }
        #endregion

        #region IXLockable Members
        public Work Work { get; set; } = Work.None;

        public virtual void Validate(IXLockable lockable, IXConditionStrategy strategy)
        {
            strategy?.Validate(this, lockable);
        }
        #endregion

        #region Properties
        public string Title
        {
            get { return this._title; }
            set
            {
                this._title = value;
                this.SetTitle();
            }
        }

        [Browsable(false), DefaultValue(true)]
        public bool ConfirmToClose { get; set; }
        private Color DefaultColor { get; } = AppColor.DefaultColor;
        private Color ActiveColor { get; } = AppColor.ActiveColor;
        #endregion

        #region Properties: Controls
        private XLabel Label
        {
            get { return this.titleLabel; }
        }

        private XButtonClose Button
        {
            get { return this.closeButton; }
        }
        #endregion

        #region Constructors
        public XChildHeaderUserControl()
        {
            InitializeComponent();
        }
        #endregion

        #region Events
        private void CloseForm(object sender, EventArgs e)
        {
            CloseChildForm();
        }

        private void SetBackColor(object sender, EventArgs e)
        {
            this.BackColor = this.Button.FlatAppearance.BorderColor = this.ActiveColor;
        }

        private void ResetBackColor(object sender, EventArgs e)
        {
            this.BackColor = this.Button.FlatAppearance.BorderColor = this.DefaultColor;
        }
        #endregion

        #region Methods
        private void SetTitle()
        {
            this.Label.Text = this.Title;
        }

        private void CloseChildForm()
        {
            bool close = true;
            var parent = this.ParentForm;
            if (this.ConfirmToClose)
            {
                var message = ApplicationMessage.CloseCurrentFormQuestion(this.Title);
                close = XMessenger.ConfirmQuestion(message);
            }
            if (close)
            {
                //TODO: non-MDI
                if (parent.IsMdiContainer)
                {
                    ((IXMainForm)parent.MdiParent).CloseChildForm();
                }
                else
                {
                    ((IXMainForm)parent.ParentForm).CloseChildForm();
                }
            }
        }
        #endregion
    }
}
