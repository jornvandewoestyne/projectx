﻿namespace ProjectX.UiControls.Windows.Forms
{
    partial class XMainHeaderUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XMainHeaderUserControl));
            this.controlBoxImageList = new System.Windows.Forms.ImageList(this.components);
            this.titleLabel = new XLabel();
            this.minimizeButton = new XButton();
            this.maximizeButton = new XButton();
            this.closeButton = new XButton();
            this.SuspendLayout();
            // 
            // controlBoxImageList
            // 
            this.controlBoxImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("controlBoxImageList.ImageStream")));
            this.controlBoxImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.controlBoxImageList.Images.SetKeyName(0, "Minimize.ico");
            this.controlBoxImageList.Images.SetKeyName(1, "Minimize_Transparent.ico");
            this.controlBoxImageList.Images.SetKeyName(2, "Maximize.ico");
            this.controlBoxImageList.Images.SetKeyName(3, "Maximize_Transparent.ico");
            this.controlBoxImageList.Images.SetKeyName(4, "Close.ico");
            this.controlBoxImageList.Images.SetKeyName(5, "Close_Transparent.ico");
            // 
            // titleLabel
            // 
            this.titleLabel.BackColor = System.Drawing.Color.White;
            this.titleLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.ForeColor = System.Drawing.Color.Black;
            this.titleLabel.Image = ((System.Drawing.Image)(resources.GetObject("titleLabel.Image")));
            this.titleLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.titleLabel.Location = new System.Drawing.Point(0, 0);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.titleLabel.Size = new System.Drawing.Size(225, 25);
            this.titleLabel.TabIndex = 5;
            this.titleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.titleLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GetMousePosition);
            this.titleLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SetMousePosition);
            // 
            // minimizeButton
            // 
            this.minimizeButton.BackColor = System.Drawing.Color.White;
            this.minimizeButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.minimizeButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.minimizeButton.FlatAppearance.BorderSize = 0;
            this.minimizeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.minimizeButton.ForeColor = System.Drawing.Color.White;
            this.minimizeButton.ImageKey = "Minimize.ico";
            this.minimizeButton.ImageList = this.controlBoxImageList;
            this.minimizeButton.Location = new System.Drawing.Point(225, 0);
            this.minimizeButton.Name = "minimizeButton";
            this.minimizeButton.Size = new System.Drawing.Size(25, 25);
            this.minimizeButton.TabIndex = 6;
            this.minimizeButton.Tag = "";
            this.minimizeButton.UseVisualStyleBackColor = false;
            this.minimizeButton.Click += new System.EventHandler(this.Minimize);
            // 
            // maximizeButton
            // 
            this.maximizeButton.BackColor = System.Drawing.Color.White;
            this.maximizeButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.maximizeButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.maximizeButton.FlatAppearance.BorderSize = 0;
            this.maximizeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.maximizeButton.ForeColor = System.Drawing.Color.White;
            this.maximizeButton.ImageKey = "Maximize.ico";
            this.maximizeButton.ImageList = this.controlBoxImageList;
            this.maximizeButton.Location = new System.Drawing.Point(250, 0);
            this.maximizeButton.Name = "maximizeButton";
            this.maximizeButton.Size = new System.Drawing.Size(25, 25);
            this.maximizeButton.TabIndex = 7;
            this.maximizeButton.Tag = "";
            this.maximizeButton.UseVisualStyleBackColor = false;
            this.maximizeButton.Click += new System.EventHandler(this.Maximize);
            // 
            // closeButton
            // 
            this.closeButton.BackColor = System.Drawing.Color.White;
            this.closeButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.closeButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.closeButton.FlatAppearance.BorderSize = 0;
            this.closeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closeButton.ForeColor = System.Drawing.Color.White;
            this.closeButton.ImageKey = "Close.ico";
            this.closeButton.ImageList = this.controlBoxImageList;
            this.closeButton.Location = new System.Drawing.Point(275, 0);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(25, 25);
            this.closeButton.TabIndex = 8;
            this.closeButton.Tag = "";
            this.closeButton.UseVisualStyleBackColor = false;
            this.closeButton.Click += new System.EventHandler(this.Close);
            // 
            // MainHeaderUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.minimizeButton);
            this.Controls.Add(this.maximizeButton);
            this.Controls.Add(this.closeButton);
            this.Name = "MainHeaderUserControl";
            this.Size = new System.Drawing.Size(300, 25);
            this.ResumeLayout(false);

        }

        #endregion

        private XLabel titleLabel;
        private XButton minimizeButton;
        private System.Windows.Forms.ImageList controlBoxImageList;
        private XButton maximizeButton;
        private XButton closeButton;


    }
}
