﻿using ProjectX.Domain.Models;
using ProjectX.General;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

//TODO: Highligth activecontrol

namespace ProjectX.UiControls.Windows.Forms
{
    public partial class XModuleUserControl : XBaseUserControl
    {
        #region IXLeakable Members
        public override void Subscribe()
        {
            this.Subscribe(Event.Click, this.ShowModule, this.Button);
            base.Subscribe();
        }
        #endregion

        #region Fields
        private readonly Color _defaultColor = AppColor.DefaultSystemColor;
        private readonly Color _activeColor = AppColor.ActiveColor;
        #endregion

        #region Properties
        public int MinimumHeight { get; private set; }
        private List<XMenuUserControl> UserControls { get; set; }
        private Module Module { get; set; }

        public ModuleBloc FormIdentifier
        {
            //TODO: exception handling when Module is not declared at enum...
            get { return (ModuleBloc)this.Module.ModuleID; }
        }
        public string Title
        {
            get { return this.Module.Name; }
        }
        public string Image
        {
            //TODO: check icon to show + how to make icons available in the whole project?
            get { return this.Module.Name; } //this.Module.Icon.Name; }
        }
        private Color DefaultColor { get { return _defaultColor; } }
        private Color ActiveColor { get { return _activeColor; } }
        #endregion

        #region Properties: Controls
        private XButton Button
        {
            get { return this.moduleButton; }
        }
        #endregion

        #region Constructors
        public XModuleUserControl(Module module)
        {
            InitializeComponent();
            this.Module = module;
            this.MinimumHeight = this.Button.Height;
            this.UserControls = new List<XMenuUserControl>();
            SetButton(this.Title, this.Image);
        }
        #endregion

        #region Events
        private void ShowModule(object sender, EventArgs e)
        {
            ShowModule();
        }
        #endregion

        #region Methods
        private void ShowModule()
        {
            ActivateStartScreen();
            foreach (Control control in this.Parent.Controls)
            {
                if (control is XMenuUserControl)
                {
                    control.Visible = false;
                }
                if (control is XModuleUserControl)
                {
                    foreach (Control item in control.Controls)
                    {
                        if (item is Button)
                        {
                            item.BackColor = this.DefaultColor;
                        }
                    }
                }
                this.Button.BackColor = this.ActiveColor;
            }
            foreach (var item in this.UserControls)
            {
                item.Visible = true;
            }
        }
        #endregion

        #region Methods: Initialize
        public void SetButton(string text, string image)
        {
            this.Button.Text = text;
            this.Button.ImageKey = image;
        }

        public void AddUserControls(XMenuUserControl item)
        {
            this.UserControls.Add(item);
        }
        #endregion

        #region Methods: Create/Show StartScreen
        //TODO: Show dashboard per Module?
        private Form ActivateStartScreen()
        {
            return this.ParentForm.ActivateStartScreen();
        }
        #endregion
    }
}
