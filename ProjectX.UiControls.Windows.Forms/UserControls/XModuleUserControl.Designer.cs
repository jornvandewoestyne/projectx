﻿namespace ProjectX.UiControls.Windows.Forms
{
    partial class XModuleUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.moduleButton = new XButton();
            this.SuspendLayout();
            // 
            // moduleButton
            // 
            this.moduleButton.AutoSize = true;
            this.moduleButton.BackColor = System.Drawing.SystemColors.Control;
            this.moduleButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.moduleButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.moduleButton.FlatAppearance.BorderColor = System.Drawing.Color.Lavender;
            this.moduleButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Lavender;
            this.moduleButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lavender;
            this.moduleButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.moduleButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.moduleButton.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.moduleButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.moduleButton.ImageKey = "(none)";
            this.moduleButton.Location = new System.Drawing.Point(0, 0);
            this.moduleButton.Name = "moduleButton";
            this.moduleButton.Padding = new System.Windows.Forms.Padding(2, 0, 5, 0);
            this.moduleButton.Size = new System.Drawing.Size(150, 38);
            this.moduleButton.TabIndex = 1;
            this.moduleButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.moduleButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.moduleButton.UseVisualStyleBackColor = false;
            // 
            // ModuleUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.moduleButton);
            this.Name = "ModuleUserControl";
            this.Size = new System.Drawing.Size(150, 38);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private XButton moduleButton;
    }
}
