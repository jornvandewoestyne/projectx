﻿namespace ProjectX.UiControls.Windows.Forms
{
    partial class XFormFrame
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.viewPanel = new System.Windows.Forms.Panel();
            this.formHeader = new ProjectX.UiControls.Windows.Forms.UserControls.XFormHeader();
            this.xMenuFrame1 = new ProjectX.UiControls.Windows.Forms.UserControls.XMenuFrame();
            this.SuspendLayout();
            // 
            // viewPanel
            // 
            this.viewPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewPanel.Location = new System.Drawing.Point(150, 66);
            this.viewPanel.Name = "viewPanel";
            this.viewPanel.Padding = new System.Windows.Forms.Padding(2);
            this.viewPanel.Size = new System.Drawing.Size(450, 84);
            this.viewPanel.TabIndex = 2;
            // 
            // formHeader
            // 
            this.formHeader.BackColor = System.Drawing.Color.White;
            this.formHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.formHeader.Location = new System.Drawing.Point(150, 0);
            this.formHeader.Name = "formHeader";
            this.formHeader.Padding = new System.Windows.Forms.Padding(2);
            this.formHeader.Size = new System.Drawing.Size(450, 66);
            this.formHeader.TabIndex = 1;
            // 
            // xMenuFrame1
            // 
            this.xMenuFrame1.AutoScroll = true;
            this.xMenuFrame1.BackColor = System.Drawing.Color.White;
            this.xMenuFrame1.Dock = System.Windows.Forms.DockStyle.Left;
            this.xMenuFrame1.Location = new System.Drawing.Point(0, 0);
            this.xMenuFrame1.Name = "xMenuFrame1";
            this.xMenuFrame1.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.xMenuFrame1.Size = new System.Drawing.Size(150, 150);
            this.xMenuFrame1.TabIndex = 3;
            // 
            // XFormFrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.viewPanel);
            this.Controls.Add(this.formHeader);
            this.Controls.Add(this.xMenuFrame1);
            this.Name = "XFormFrame";
            this.Size = new System.Drawing.Size(600, 150);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel viewPanel;
        public UserControls.XFormHeader formHeader;
        public UserControls.XMenuFrame xMenuFrame1;
    }
}
