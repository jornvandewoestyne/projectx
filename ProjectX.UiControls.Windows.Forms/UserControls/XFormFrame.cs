﻿using System.Windows.Forms;

//TODO: Subscribe

namespace ProjectX.UiControls.Windows.Forms
{
    public partial class XFormFrame : XBaseUserControl
    {
        public XFormFrame()
        {
            InitializeComponent();
        }

        private void DragForm(object sender, MouseEventArgs e)
        {
            var form = this.ParentForm;
            if (e.Button == MouseButtons.Left && form != null)
            {
                if ((e.Clicks == 1))
                {
                    NativeMethods.ReleaseCapture();
                    NativeMethods.SendMessage(form.Handle, (int)WindowMessages.WM_NCLBUTTONDOWN, (int)HitTestValues.HTCAPTION, 0);
                }
            }
        }

        private void ToggleMaximize(object sender, MouseEventArgs e)
        {
            var form = this.ParentForm;
            if (e.Button == MouseButtons.Left && form != null)
            {
                form.WindowState = form.WindowState == FormWindowState.Maximized ? FormWindowState.Normal : FormWindowState.Maximized;
            }
        }
    }
}
