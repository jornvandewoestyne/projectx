﻿using ProjectX.General;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

//https://stackoverflow.com/questions/778095/windows-forms-using-backgroundimage-slows-down-drawing-of-the-forms-controls


//check: http://www.vbforums.com/showthread.php?568015-Move-and-Resize-a-Control-or-a-Borderless-Form-using-window-messages-(smooth!)

namespace ProjectX.UiControls.Windows.Forms.UserControls
{
    public partial class XFormHeader : XBaseUserControl, IXLocalizableControl//, IXLockable
    {
        #region Properties
        private bool IsMaximized { get; set; }
        private bool MouseButtonDown { get; set; }
        //private Form Form => this.ParentForm;
        #endregion

        public XLabel Title { get; set; }

        #region Constructors
        public XFormHeader()
        {
            //this.ResizeRedraw = true;
            InitializeComponent();

            //Ensure input is allways secured:
            this.SetFreezable<IXFreezable>(new XCanBlockInputStrategy(false));
            this.SetLockable<IXLockable>(new XBooleanStrategy(true));
        }
        #endregion

        #region Methods
        public void UpdateButtons()
        {
            this.maximizeToolStripButton?.UpdateButton();
        }
        #endregion

        #region Helpers
        private void StartDrag(object sender, MouseEventArgs e)
        {
            this.IsMaximized = false;
            this.MouseButtonDown = true;
            var form = this.ParentForm;
            if (form != null && e.Button == MouseButtons.Left)
            {
                if ((e.Clicks == 1))
                {
                    if (form.WindowState != FormWindowState.Maximized)
                    {
                        var borderlessForm = form as XBorderlessForm;
                        if (borderlessForm != null)
                        {
                            borderlessForm.IsDragging = true;
                        }
                        NativeMethods.ReleaseCapture();
                        NativeMethods.SendMessage(form.Handle, (int)WindowMessages.WM_NCLBUTTONDOWN, (int)HitTestValues.HTCAPTION, 0);
                        form.FormBorderStyle = FormBorderStyle.None;
                    }
                    else { this.IsMaximized = true; }
                }
            }
        }

        private void Drag(object sender, MouseEventArgs e)
        {
            var form = this.ParentForm;
            //var borderlessForm = form as XBorderlessForm;
            if (form != null && this.MouseButtonDown)
            {
                //if (borderlessForm != null)
                //{
                //    borderlessForm.IsDragging = true;
                //}
                if (this.IsMaximized)
                {
                    //This is a small hack:
                    //We need to enable aerosnap in every situation (formborderstyle doesn't update correctly without it)
                    //after dragging from maximized state to normal, aerosnap should also be available               
                    this.maximizeToolStripButton.Execute(true);
                    //Continue dragging
                    NativeMethods.ReleaseCapture();
                    NativeMethods.SendMessage(form.Handle, (int)WindowMessages.WM_NCLBUTTONDOWN, (int)HitTestValues.HTCAPTION, 0);
                    form.FormBorderStyle = FormBorderStyle.None;
                }
            }
            this.IsMaximized = false;
            //if (borderlessForm != null)
            //{
            //    borderlessForm.IsDragging = false;
            //}
        }

        private void StopDrag(object sender, MouseEventArgs e)
        {
            var form = this.ParentForm;
            if (form != null && form.Location.Y < XLib.MagicNumber.IntZero && this.MouseButtonDown)
            {
                form.Location = new Point(form.Location.X, 10);
            }
            this.IsMaximized = false;
            this.MouseButtonDown = false;
            var borderlessForm = form as XBorderlessForm;
            if (borderlessForm != null)
            {
                borderlessForm.IsDragging = false;
            }
        }

        private void ToggleMaximize(object sender, MouseEventArgs e)
        {
            var borderlessForm = this.ParentForm as XBorderlessForm;
            if (borderlessForm != null)
            {
                borderlessForm.IsDragging = false;
            }
            this.MouseButtonDown = false;
            this.maximizeToolStripButton.Execute();
        }
        #endregion

        #region ILocalizableControl Members
        [Browsable(false)]
        public ToolTip ToolTip { get; set; }

        [Browsable(false)]
        public FriendlyTextType XControlText { get; set; }

        [Browsable(false)]
        public FriendlyToolTipType XToolTipText { get; set; }

        public void Localize()
        {
            this.TryLocalize();
        }
        #endregion

        #region IXLeakable Members
        public override void Subscribe()
        {
            var targets = new IXLeakable[] { this.headerTitle, this.headerSubTitle, this.mainPictureBox, this.commandPanel };
            this.Subscribe(MouseEvent.MouseDown, this.StartDrag, targets);
            this.Subscribe(MouseEvent.MouseMove, this.Drag, targets);
            this.Subscribe(MouseEvent.MouseUp, this.StopDrag, targets);
            this.Subscribe(MouseEvent.MouseDoubleClick, this.ToggleMaximize, targets);
            base.Subscribe();
        }
        #endregion
    }
}
