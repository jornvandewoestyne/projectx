﻿namespace ProjectX.UiControls.Windows.Forms
{
    partial class XChildHeaderUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XChildHeaderUserControl));
            this.titleLabel = new XLabel();
            this.closeButton = new XButtonClose();
            this.controlBoxImageList = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            this.titleLabel.BackColor = System.Drawing.Color.Transparent;
            this.titleLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.titleLabel.Location = new System.Drawing.Point(0, 0);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(125, 25);
            this.titleLabel.TabIndex = 1;
            this.titleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // closeButton
            // 
            this.closeButton.BackColor = System.Drawing.Color.Transparent;
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.closeButton.FlatAppearance.BorderColor = System.Drawing.Color.Lavender;
            this.closeButton.FlatAppearance.BorderSize = 0;
            this.closeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closeButton.ForeColor = System.Drawing.Color.Transparent;
            this.closeButton.ImageKey = "Close.ico";
            this.closeButton.ImageList = this.controlBoxImageList;
            this.closeButton.Location = new System.Drawing.Point(125, 0);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(25, 25);
            this.closeButton.TabIndex = 4;
            this.closeButton.Tag = "";
            this.closeButton.UseVisualStyleBackColor = false;
            // 
            // controlBoxImageList
            // 
            this.controlBoxImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("controlBoxImageList.ImageStream")));
            this.controlBoxImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.controlBoxImageList.Images.SetKeyName(0, "Minimize.ico");
            this.controlBoxImageList.Images.SetKeyName(1, "Minimize_Transparent.ico");
            this.controlBoxImageList.Images.SetKeyName(2, "Maximize.ico");
            this.controlBoxImageList.Images.SetKeyName(3, "Maximize_Transparent.ico");
            this.controlBoxImageList.Images.SetKeyName(4, "Close.ico");
            this.controlBoxImageList.Images.SetKeyName(5, "Close_Transparent.ico");
            // 
            // XChildHeaderUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.closeButton);
            this.Name = "XChildHeaderUserControl";
            this.Size = new System.Drawing.Size(150, 25);
            this.ResumeLayout(false);

        }

        #endregion

        private XButtonClose closeButton;
        private System.Windows.Forms.ImageList controlBoxImageList;
        private XLabel titleLabel;
    }
}
