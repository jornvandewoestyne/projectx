﻿using ProjectX.Domain.Models;
using ProjectX.General;
using ProjectX.Infrastructure;
using ProjectX.Infrastructure.Localization;
using ProjectX.Infrastructure.LogAttributes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using Models = ProjectX.Domain.Models;

namespace ProjectX.UiControls.Windows.Forms
{
    public partial class XMenuUserControl : XBaseUserControl
    {
        #region IXLeakable Members
        public override void Subscribe()
        {
            this.Subscribe(Event.Click, this.ShowMenu, this.Button);
            this.Subscribe(Event.Click, this.RunAction, this.ListView);
            this.Subscribe(Event.SelectionChangeCommitted, this.SelectInstance, this.ComboBox);
            base.Subscribe();
        }
        #endregion

        #region Fields
        private readonly int _spacing = 10;
        private List<XMdiChildDetail> _openChilds;
        #endregion

        #region Properties
        //public IService Service => NinjectCompositionRoot.Get<IService>();
        public int MinimumHeight { get; private set; }
        private Models.Menu Menu { get; set; }

        public ModuleBloc ModuleBloc
        {
            //TODO: exception handling when Menu is not declared at enum...
            get
            {
                if (this.Menu != null)
                {
                    return (ModuleBloc)this.Menu.ModuleID;
                }
                return ModuleBloc.None;
            }
        }
        public MenuBloc FormIdentifier
        {
            //TODO: exception handling when Menu is not declared at enum...
            get
            {
                if (this.Menu != null)
                {
                    return (MenuBloc)this.Menu.MemberID;
                }
                return MenuBloc.None;
            }
        }
        public string Title
        {
            //get { return this.Menu.Member.LocalName; }
            get
            {
                if (this.Menu != null)
                {
                    return this.Menu.Member.Name;
                }
                return this.FormIdentifier.ToString();
            }
        }
        public string Image
        {
            //TODO: check icon to show + how to make icons available in the whole project?
            get
            {
                return this.FormIdentifier.ToString();
            } //this.Menu.Member.Icon.Name; }
        }
        private List<MenuActivity> Activities
        {
            get
            {
                if (this.Menu != null)
                {
                    return this.Menu.MenuActivities.ToList();
                }
                return null;
            }
        }
        public List<XMdiChildDetail> OpenChilds
        {
            get { return this._openChilds; }
            private set { this._openChilds = value; }
        }
        #endregion

        #region Properties: Controls
        private XButton Button
        {
            get { return this.menuButton; }
        }

        private XComboBox ComboBox
        {
            get { return this.menuComboBox; }
        }
        public XListView ListView
        {
            get { return this.menuListView; }
        }
        private XPanel Panel
        {
            get { return this.menuPanel; }
        }
        #endregion

        #region Constructors
        protected XMenuUserControl()
            : this(null)
        { }

        public XMenuUserControl(ProjectX.Domain.Models.Menu menu)
        {
            InitializeComponent();
            this.Menu = menu;
            this.MinimumHeight = this.Button.Height;
            this.OpenChilds = new List<XMdiChildDetail>();
            SetButton(this.Title, this.Image);
            FillActivityListview(this.ListView, this.Activities);
        }
        #endregion

        #region Events
        private void ShowMenu(object sender, EventArgs e)
        {
            Panel parentPanel = (Panel)this.Parent;
            CollapseAllPanels(parentPanel, this);
            ResizeControl(this, this.Panel, this.ListView, this.MinimumHeight, this._spacing);
            ActivateComboBoxSelectedValue();
        }

        private void SelectInstance(object sender, EventArgs e)
        {
            ActivateComboBoxSelectedValue();
        }

        private void RunAction(object sender, EventArgs e)
        {
            RunListViewItem(this.ListView);
        }
        #endregion

        #region Methods: Initialize
        public void SetButton(string text, string image)
        {
            this.Button.Text = text;
            this.Button.Image = image.ToBitmap(new Size(24, 24));

            //var obj = ProjectX.Infrastructure.Properties.Resources.ResourceManager.GetObject(image);
            //if (obj != null)
            //{
            //    var type = obj.GetType();
            //    if (type == typeof(System.Drawing.Icon))
            //    {
            //        var icon = new System.Drawing.Icon((System.Drawing.Icon)obj, 24, 24);
            //        this.Button.Image = icon.ToBitmap();
            //    }
            //    else
            //    {
            //        var bitmap = new System.Drawing.Bitmap((System.Drawing.Bitmap)obj, 24, 24);
            //        this.Button.Image = bitmap;
            //    }
            //}
            //else
            //{
            //    this.Button.ImageKey = image;
            //}
        }
        #endregion

        #region Methods: Service Helpers
        private XMdiChildDetail GetMdiChildDetail(Activity activity)
        {
            using (var dependency = new XDI().Get<XEntityPresenter<ActivityGroup>>())
            {
                var activityGroup = dependency.Instance.Get(activity.ActivityGroupID);
                return new XMdiChildDetail(activity, activityGroup, this.FormIdentifier);
            }
        }

        private XMdiChildDetail GetMdiChildDetail(MenuGroupAction action)
        {
            using (var dependency = new XDI().Get<XEntityPresenter<Activity>>())
            {
                var activity = dependency.Instance.Get(action);
                return GetMdiChildDetail(activity);
            }
        }

        private XMdiChildDetail GetMdiChildDetail(XMdiChildDetail detail, XChildForm form = null, string title = null)
        {
            Activity activity;
            bool showHeader = XLib.Preference.ShowChildHeaderDefault;
            bool confirmToClose = XLib.Preference.ConfirmToCloseDefault;
            if (detail == null)
            {
                using (var dependency = new XDI().Get<XEntityPresenter<Activity>>())
                {
                    activity = dependency.Instance.Get(MenuGroupAction.Unknown);
                }
            }
            else
            {
                activity = detail.Activity;
                showHeader = detail.ShowHeader;
                confirmToClose = detail.ConfirmToClose;
            }
            using (var dependency = new XDI().Get<XEntityPresenter<ActivityGroup>>())
            {
                var activityGroup = dependency.Instance.Get(activity.ActivityGroupID);
                return new XMdiChildDetail(form, title, activity, activityGroup, this.FormIdentifier, showHeader, confirmToClose);
            }
        }

        ////TODO: make helper method
        //private MdiChildDetail SetMdiChildBehavior(MdiChildDetail detail, bool confirmToClose, bool showHeader)
        //{

        //}
        #endregion

        #region Methods: Listview
        private void FillActivityListview(XListView listview, List<MenuActivity> list)
        {
            if (list != null)
            {
                //TODO: Make listview (helper) class for reusability?
                listview.Items.Clear();
                using (var dependency = new XDI().Get<XEntityPresenter<ActivityGroup>>())
                {
                    foreach (var item in list)
                    {
                        var activity = item.Activity;
                        var activityGroup = dependency.Instance.Get(activity.ActivityGroupID);
                        bool bFound = false;
                        ListViewGroup group = null;
                        foreach (ListViewGroup newGroup in listview.Groups)
                        {
                            if (newGroup.Header.Equals(activityGroup.Name))
                            {
                                group = newGroup;
                                bFound = true;
                                break;
                            }
                        }
                        if (!bFound)
                        {
                            group = new ListViewGroup(activityGroup.Name);
                            listview.Groups.Add(group);
                        }
                        var listItem = new ListViewItem(activity.Name, (ListViewGroup)group);
                        //var listItem = new BrightIdeasSoftware.OLVListItem(activity, activity.Name, null);
                        listItem.Tag = activity;
                        listview.Items.Add(listItem);
                    }
                }
            }
        }
        #endregion

        #region Methods: MenuItem
        private void RunListViewItem(XListView listview)
        {
            foreach (ListViewItem item in listview.SelectedItems)
            {
                XMdiChildDetail detail = GetMdiChildDetail((Activity)item.Tag);
                if (item.Group.ToString() == detail.ActivityGroupName)
                {
                    if (detail.ActionIdentifier == MenuGroupAction.Edit)
                    {
                        using (var dependency = new XDI().Get<XEntityPresenter<Activity>>())
                        {
                            var action = dependency.Instance.Get(MenuGroupAction.Search).Name;
                            var message = ApplicationMessage.SelectItemAdvice(this.Title.ToLower(), action);
                            XMessenger.ShowInfo(message);
                        }
                    }
                    else
                    {
                        detail.ShowHeader = XLib.Preference.ShowChildHeaderDefault;
                        string formName;
                        if (detail.ActionIdentifier == MenuGroupAction.New)
                        {
                            detail.ConfirmToClose = XLib.Preference.ConfirmToCloseOnEditForm;
                            formName = GetFormName(MenuGroupAction.Edit.ToString());
                        }
                        else
                        {
                            detail.ConfirmToClose = XLib.Preference.ConfirmToCloseOnSearchForm;
                            formName = GetFormName(detail.ActionIdentifier.ToString());
                        }
                        ShowSingleForm(formName, detail);
                    }
                }
            }
        }

        protected virtual string GetFormName(string actionIdentifier)
        {
            //TODO: ErrorHandling -> FatalException???
            throw new NotImplementedException();
            //return UI.GetInstanceFormName(this, actionIdentifier);
        }

        private string GetStartScreen()
        {
            //non-MDI
            var parent = this.ParentForm;
            if (parent.IsMdiContainer)
            {
                foreach (Form form in this.ParentForm.MdiChildren)
                {
                    if (form is StartScreenForm)
                    {
                        return form.Name;
                    }
                }
            }
            else
            {
                var container = parent as XMainForm;
                foreach (Form form in container.childContainer.Controls)
                {
                    if (form is StartScreenForm)
                    {
                        return form.Name;
                    }
                }
            }
            return null;
        }
        #endregion

        #region Methods: Show/Create New Form Instance
        [Log]
        private void ShowSingleForm(string formName, XMdiChildDetail detail)
        {
            try
            {
                //non-MDI
                if (!detail.MultipleInstances)
                {
                    //Check if form is already opened:
                    var bFound = false;
                    var parent = this.ParentForm;
                    if (parent.IsMdiContainer)
                    {
                        foreach (Form form in this.ParentForm.MdiChildren)
                        {
                            if (form.GetType() == GetInstanceType(formName))
                            {
                                form.ActivateMdiChildForm();
                                SetComboBoxSelectedValue(form);
                                bFound = true;
                            }
                            //else { form.Hide(); } //TODO: remove line?
                        }
                    }
                    else
                    {
                        var container = parent as XMainForm;
                        foreach (Form form in container.childContainer.Controls)
                        {
                            if (form.GetType() == GetInstanceType(formName))
                            {
                                form.ActivateMdiChildForm();
                                SetComboBoxSelectedValue(form);
                                bFound = true;
                            }
                            //else { form.Hide(); } //TODO: remove line?
                        }
                    }
                    //If found, exit procedure:
                    if (bFound) { return; }
                }
                //If not found, create new instance:
                var newForm = CreateNewInstance(formName, detail);
                if (newForm != null)
                {
                    newForm.ShowMdiChildForm(this.ParentForm);
                }
                else
                {
                    //WARNING: endless loop when newForm = null (CAUSE: type = null) -> force it via XString.Base.SetArguments()
                    //possible solution: get rid of this recursive method -> try to create proper looping to avoid endless loop!
                    //TODO: CustomException? -> to avoid messageloop
                    //RECURSIVE METHOD!!!!!
                    ShowSingleForm(GetStartScreen(), GetMdiChildDetail(MenuGroupAction.Unknown));
                    var message = ApplicationMessage.ItemNotAvailableAlert();
                    XMessenger.ShowWarning(message);
                }

            }
            catch (TypeLoadException)
            {
                //TODO: CustomException? -> to avoid messageloop (when throw is enabled, which ensures logging)
                var message = ApplicationMessage.ItemNotAvailableAlert();
                XMessenger.ShowWarning(message);
                //throw;
            }
        }

        private Form CreateNewInstance(string formName, XMdiChildDetail detail)
        {
            try
            {
                var title = detail.ActivityName;
                var newForm = CreateNewInstanceUsingFormName(formName, detail);
                if (detail.MultipleInstances)
                {
                    ResetListViewItemCount(newForm, detail, true);
                    // Reset Title:
                    var count = CountMdiFormsGroup(detail, true);
                    title = StringHelper.TryFormat("{0} {1,2}", title, EditFormTitleCount(title, count));
                }
                newForm.Text = StringHelper.Title(this.Title, title);
                AddToOpenChilds(newForm, title, detail);
                return newForm;
            }
            catch (MissingMethodException) //Constructor with parameter "MdiChildDetail" is not found!
            {
                return null;
            }
            catch (TypeLoadException) //Type is not found! (which means: form class doesn't exist, yet) or the returned type is null
            {
                throw; //throw in order to prevent an endless loop (-> recurcive ShowSingleForm())
            }
        }

        //TODO: make private again?
        protected virtual XChildForm CreateNewInstanceUsingFormName(string instance, XMdiChildDetail detail = null)
        {
            throw new NotImplementedException();
            //return UI.CreateNewInstance(this, instance, detail);
        }

        protected virtual Type GetInstanceType(string instance)
        {
            throw new NotImplementedException();
            //return UI.GetInstanceType(this, instance);         
        }
        #endregion      

        #region Methods: Handle ComboBox
        public void SetComboBoxSelectedValue(Form form)
        {
            if (form != null)
            {
                this.ComboBox.SelectedValue = form;
            }
            else
            {
                ShowSingleForm(GetStartScreen(), GetMdiChildDetail(MenuGroupAction.Unknown));
            }
        }

        private void ActivateComboBoxSelectedValue(Form form = null)
        {
            if (form == null)
            {
                form = (Form)this.ComboBox.SelectedValue;
            }
            form.ActivateMdiChildForm();
            HideAllMdiForms(form);
        }

        private void SetComboBoxBindingSource()
        {
            SortOpenChilds();
            this.ComboBox.DataSource = this.OpenChilds;
            this.ComboBox.ValueMember = "Key";
            this.ComboBox.DisplayMember = "Value";
        }
        #endregion

        #region Methods: Handle OpenChilds (public, partially)
        public void AddToOpenChilds(XChildForm form, string title, XMdiChildDetail detail = null)
        {
            if (form != null)
            {
                this.OpenChilds.Add(GetMdiChildDetail(detail: detail, form: form, title: title));
                SetComboBoxBindingSource();
                SetComboBoxSelectedValue(form);
            }
        }

        public void RemoveFromOpenChilds(Form form, XMdiChildDetail detail = null)
        {
            //BUGFIX: on attempt closing StartScreen
            if (GetFormDetails(form) != null)
            {
                using (var item = this.OpenChilds.Single(x => x.OpenForm == form))
                {
                    this.OpenChilds.Remove(item);
                    SetComboBoxBindingSource();
                    var newForm = GetFirstActiveChild(form, detail);
                    SetComboBoxSelectedValue(newForm);
                    ActivateComboBoxSelectedValue(newForm);
                    ResetListViewItemCount(form, detail, false);
                    //Clear resources on close (keep this for last to presurve necessary data):
                    item.Dispose();
                    if (detail == null) { form.Dispose(); }
                }
            }
        }

        public void ResetOpenChilds(XChildForm form, string newTitle, XMdiChildDetail detail)
        {
            foreach (XMdiChildDetail child in this.OpenChilds)
            {
                if (child.OpenForm == form)
                {
                    // Delete from OpenChilds:
                    //CHECK: not needed
                    //RemoveFromOpenChilds(form, detail);
                    // Reset FormText:
                    form.Text = newTitle;
                    // Add to OpenChilds:
                    //CHECK: not needed
                    //AddToOpenChilds(form, newTitle, detail);
                    // Reset count:
                    ResetListViewItemCount(form, GetMdiChildDetail(MenuGroupAction.Unknown), false);
                    break;
                }
            }
        }

        public XChildForm ChildIsOpen(EntityKey entityKey)
        {
            if (entityKey != null && entityKey.Keys != null)
            {
                foreach (XMdiChildDetail item in this.OpenChilds)
                {
                    IXMdiChildFormObject iX = (IXMdiChildFormObject)item.OpenForm;
                    if (ArrayHelper.Compare<object>(iX.EntityKey.ResolveNull().Keys, entityKey.Keys))
                    {
                        SetComboBoxSelectedValue(item.OpenForm);
                        return item.OpenForm;
                    }
                }
            }
            return null;
        }

        public Form GetFirstActiveChild(Form excludeForm, XMdiChildDetail detail)
        {
            //TODO
            var container = this.ParentForm as XMainForm;
            var controls = container.childContainer.Controls;
            var iForm = (IXMdiChildFormObject)controls[XLib.MagicNumber.Index0];
            if (excludeForm != null && detail == null)
            {
                //Foreach is not needed = TEST
                foreach (ContainerControl control in controls)
                {
                    if (control == excludeForm)
                    {
                        continue;
                    }
                    else //if (control.Visible && control is Form)
                    {
                        iForm = control as IXMdiChildFormObject;
                        break;
                    }
                }
            }

            //MessageBox.Show("CONTROL " + iForm?.ToString() + " *** " + Form.ActiveForm?.Name);
            //IXMdiChildFormObject iForm = (IXMdiChildFormObject)container.childContainer.Controls[XLib.Number.Index0];
            if (this.ParentForm.IsMdiContainer)
            {
                iForm = (IXMdiChildFormObject)this.ParentForm.ActiveMdiChild;
            }
            if (iForm.MdiChildDetail != null)
            {
                if (iForm.MdiChildDetail.FormIdentifier != this.FormIdentifier)
                {
                    foreach (XMdiChildDetail item in this.OpenChilds)
                    {
                        SetComboBoxSelectedValue(item.OpenForm);
                        ActivateComboBoxSelectedValue(item.OpenForm);
                        return item.OpenForm;
                    }
                }
            }
            //TODO: test
            //else
            //{
            //    MessageBox.Show("no mdidetail!!!!!");
            //}
            return (Form)iForm;
        }

        private object[] GetFormDetails(Form form)
        {
            if (form != null)
            {
                var oldDetail = (form as IXMdiChildFormObject)?.MdiChildDetail;
                if (oldDetail != null)
                {
                    var activity = oldDetail.Activity;
                    var actionIdentifier = oldDetail.ActionIdentifier;
                    return new object[] { actionIdentifier, activity };
                }
            }
            return null;
        }

        private void ResetListViewItemCount(Form form, XMdiChildDetail detail, bool isNew)
        {
            if (form != null)
            {
                var details = GetFormDetails(form);
                if (details != null)
                {
                    var actionIdentifier = (MenuGroupAction)details?[XLib.MagicNumber.Index0];
                    var activity = (Activity)details?[XLib.MagicNumber.Index1];
                    ResetListViewItemCount(actionIdentifier, activity, detail, isNew);
                }
            }
        }

        private void ResetListViewItemCount(MenuGroupAction actionIdentifier, Activity activity, XMdiChildDetail detail, bool isNew)
        {
            int count = CountMdiFormsGroup(actionIdentifier, isNew);
            int index = (int)actionIdentifier;
            EditListViewItemCount(this.ListView, actionIdentifier, count);
            // Reset to new ActionIdentifier (necessary when ActionIdentifier changes!):
            if (detail != null)
            {
                if (detail.ActionIdentifier != MenuGroupAction.Unknown)
                {
                    activity = detail.Activity;
                }
            }
        }

        private void SortOpenChilds()
        {
            this.OpenChilds = this.OpenChilds.OrderBy(x => x.SequenceNumber).ThenBy(x => x.SortName).ThenBy(x => x.Title).ToList();
        }
        #endregion

        #region Methods: Count/Edit OpenChilds
        private int CountMdiFormsGroup(XMdiChildDetail detail, bool isNew)
        {
            return CountMdiFormsGroup(detail.ActionIdentifier, isNew);
        }

        private int CountMdiFormsGroup(MenuGroupAction actionIdentifier, bool isNew)
        {
            var count = XLib.MagicNumber.IntZero;
            foreach (XMdiChildDetail item in this.OpenChilds)
            {
                IXMdiChildFormObject iX = (IXMdiChildFormObject)item.OpenForm;
                if (iX.MdiChildDetail != null)
                {
                    if (iX.MdiChildDetail.ActionIdentifier == actionIdentifier)
                    {
                        count++;
                    }
                }
            }
            if (isNew) { count++; }
            return count;
        }

        private int EditFormTitleCount(string title, int count)
        {
            int result = count;
            foreach (XMdiChildDetail item in this.OpenChilds)
            {
                if (item.Title.Contains(title))
                {
                    bool x = Int32.TryParse(item.Title.Split(XLib.Token.Space).Last(), out result);
                    result++;
                }
            }
            return result;
        }

        private void EditListViewItemCount(XListView listview, MenuGroupAction action, int count)
        {
            if (action == MenuGroupAction.Unknown) { return; }
            using (var dependency = new XDI().Get<XEntityPresenter<Activity>>())
            {
                foreach (ListViewItem item in listview.Items)
                {
                    if (((Activity)item.Tag).ActivityID == (int)action)
                    {
                        item.Text = dependency.Instance.Get(action).Name;
                        if (count != XLib.MagicNumber.IntZero) item.Text += StringHelper.Concat(new SurroundFormat(" (", ")"), count);
                        break;
                    }
                }
            }
        }
        #endregion

        #region Methods: Hide ChildForms
        private void HideAllMdiForms(Form currentForm)
        {
            //non-MDI
            var parent = this.ParentForm;
            if (parent.IsMdiContainer)
            {
                foreach (Form form in this.ParentForm.MdiChildren)
                {
                    if (form != currentForm)
                    {
                        form.Hide();
                    }
                }
            }
            else
            {
                var container = parent as XMainForm;
                foreach (Form form in container.childContainer.Controls)
                {
                    if (form != currentForm)
                    {
                        form.Hide();
                    }
                }
            }

        }
        #endregion

        #region Methods: Expand/Collapse MenuBloc
        private void CollapseAllPanels(Panel parentPanel, XMenuUserControl excludePanel, bool hideAll = false)
        {
            WinFormXMenuUserControlHelper.CollapseUserControlPanels<XMenuUserControl>(parentPanel, excludePanel, hideAll);
        }

        private void ResizeControl(XMenuUserControl control, Panel panel, XListView listview, int minimumHeight, int spacing)
        {
            control.ResizeHeight(panel, listview, minimumHeight, spacing);
        }
        #endregion
    }
}
