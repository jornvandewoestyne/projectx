﻿//TODO: add summary...

namespace ProjectX.UiControls.Windows.Forms
{
    public static class WinFormXChildHeaderUserControlExtensions
    {
        #region Methods: XChildHeaderUserControl
        public static void SetUserControlValues(this XChildHeaderUserControl header, string title, XMdiChildDetail detail)
        {
            header.Title = title;
            header.ConfirmToClose = !XLib.Preference.ConfirmToCloseDefault;
            header.Visible = !XLib.Preference.ShowChildHeaderDefault;
            if (detail != null)
            {
                header.ConfirmToClose = detail.ConfirmToClose;
                header.Visible = detail.ShowHeader;
            }
        }

        public static void SetUserControlValues(this XChildHeaderUserControl header, string title, bool confirmToClose, bool showHeader)
        {
            header.Title = title;
            header.ConfirmToClose = confirmToClose;
            header.Visible = showHeader;
        }

        public static void SetUserControlValues(this XMainHeaderUserControl header, string title, bool confirmToClose)
        {
            header.Title = title;
            header.ConfirmToClose = confirmToClose;
        }
        #endregion
    }
}
