﻿using System.Windows.Forms;

//TODO: add summary...

namespace ProjectX.UiControls.Windows.Forms
{
    public static class WinFormXMenuUserControlExtensions
    {
        #region Methods: XMenuUserControl
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="newForm"></param>
        /// <param name="form"></param>
        /// <param name="detail"></param>
        /// <param name="newTitle"></param>
        /// <param name="newDetail"></param>
        public static void AddToOpenChild<T>(this XChildForm newForm, Form form, XMdiChildDetail detail, string newTitle, XMdiChildDetail newDetail)
            where T : XMenuUserControl
        {
            WinFormXMenuUserControlHelper.AddToOpenChild<T>(form, detail, newForm, newTitle, newDetail);
        }

        public static void ResizeHeight(this XMenuUserControl control, Panel panel, XListView listview, int minimumHeight, int spacing)
        {
            //Resize the UserControl:
            int size = minimumHeight;
            if (control.Height == size)
            {
                //Get resize height of the listview:
                size = listview.GetResizeHeight(minimumHeight, spacing);
            }
            //Set the result:
            control.Height = size;
            panel.Height = size;
        }
        #endregion
    }
}
