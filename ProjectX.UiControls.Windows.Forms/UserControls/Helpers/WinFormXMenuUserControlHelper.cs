﻿using ProjectX.General;
using System.Windows.Forms;

//TODO: add summary...
//TODO: MainForm as BaseMainForm???

namespace ProjectX.UiControls.Windows.Forms
{
    public static class WinFormXMenuUserControlHelper
    {
        #region Methods: XMenuUserControl
        public static void CollapseUserControlPanels<T>(Panel groupPanel, object excludePanel = null, bool hideAll = true)
            where T : XMenuUserControl
        {
            foreach (Control control in groupPanel.Controls)
            {
                if (control is T)
                {
                    if (control != excludePanel)
                    {
                        var userControl = (T)control;
                        control.Height = userControl.MinimumHeight;
                    }
                    if (hideAll)
                    {
                        control.Visible = false;
                    }
                }
            }
        }

        public static void AddToOpenChild<T>(Form form, XMdiChildDetail detail, XChildForm newForm, string newTitle, XMdiChildDetail newDetail)
            where T : XMenuUserControl
        {
            if (form.IsMdiChild && form.MdiParent is IXMainForm)
            {
                var userControls = ((IXMainForm)form.MdiParent).MenuControls;
                foreach (var userControl in userControls)
                {
                    if (userControl.FormIdentifier == detail.FormIdentifier)
                    {
                        userControl.AddToOpenChilds(newForm, newTitle, newDetail);
                        userControl.ResetOpenChilds(newForm, newTitle, newDetail);
                        break;
                    }
                }
            }
            else
            {
                var container = form.ParentForm as XMainForm;
                var userControls = ((IXMainForm)container).MenuControls;
                foreach (var userControl in userControls)
                {
                    if (userControl.FormIdentifier == detail.FormIdentifier)
                    {
                        userControl.AddToOpenChilds(newForm, newTitle, newDetail);
                        userControl.ResetOpenChilds(newForm, newTitle, newDetail);
                        break;
                    }
                }
            }
        }

        public static void CloseAndRemoveFromOpenChilds<T>(Form form) where T : XMenuUserControl
        {
            //TODO: only non-MDI, MDI is disabled = TODO!
            //if (form.IsMdiChild && form.MdiParent is IXMainForm)
            //{
            Form parent = form.ParentForm as XMainForm; //form.MdiParent;
                                                        //if (form.MdiParent.MdiChildren.Length > XLib.Number.IntZero)
                                                        //{
            bool isFound = false;
            var userControls = ((IXMainForm)parent).MenuControls;
            foreach (var userControl in userControls)
            {
                foreach (XMdiChildDetail child in userControl.OpenChilds)
                {
                    if (child.OpenForm == form)
                    {
                        //TODO: correct?
                        //form.Close();
                        //form.Dispose();
                        userControl.RemoveFromOpenChilds((Form)child.OpenForm);
                        //form.Dispose();
                        isFound = true;
                        break;
                    }
                }
                if (isFound) { break; }
            }
            //}
            //TODO: non-MDI
            //At least, show the startscreen:
            if (parent.IsMdiContainer && parent.ActiveMdiChild == null)
            {
                parent.ActivateStartScreen();
            }
            //}
        }

        public static void ResetOpenChild<T>(XChildForm form, XMdiChildDetail detail, string newTitle)
            where T : XMenuUserControl
        {
            //TODO: non-MDI
            if (form.IsMdiChild && form.MdiParent is IXMainForm)
            {
                var userControls = ((IXMainForm)form.MdiParent).MenuControls;
                foreach (var userControl in userControls)
                {
                    if (userControl.FormIdentifier == detail.FormIdentifier)
                    {
                        userControl.ResetOpenChilds(form, newTitle, detail);
                        break;
                    }
                }
            }
            else
            {
                var container = form.ParentForm as XMainForm;
                var userControls = ((IXMainForm)container).MenuControls;
                foreach (var userControl in userControls)
                {
                    if (userControl.FormIdentifier == detail.FormIdentifier)
                    {
                        userControl.ResetOpenChilds(form, newTitle, detail);
                        break;
                    }
                }
            }
        }

        public static Form InstanceIsOpen<T>(Form form, XMdiChildDetail detail, EntityKey entityKey)
            where T : XMenuUserControl
        {
            //TODO: non-MDI
            if (form.IsMdiChild && form.MdiParent is IXMainForm)
            {
                var userControls = ((IXMainForm)form.MdiParent).MenuControls;
                foreach (var userControl in userControls)
                {
                    if (userControl.FormIdentifier == detail.FormIdentifier)
                    {
                        return userControl.ChildIsOpen(entityKey);
                    }
                }
            }
            else
            {
                var container = form.ParentForm as XMainForm;
                var userControls = ((IXMainForm)container).MenuControls;
                foreach (var userControl in userControls)
                {
                    if (userControl.FormIdentifier == detail.FormIdentifier)
                    {
                        return userControl.ChildIsOpen(entityKey);
                    }
                }
            }
            return null;
        }
        #endregion
    }
}
