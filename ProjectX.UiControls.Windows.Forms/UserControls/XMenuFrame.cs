﻿using System.Windows.Forms;

//TODO: Subscribe

namespace ProjectX.UiControls.Windows.Forms.UserControls
{
    public partial class XMenuFrame : XBaseUserControl
    {
        private bool _mRestoreIfMove;
        private bool _mIsDragging;

        public XMenuFrame()
        {
            InitializeComponent();

            //this.DoubleBuffered = true;
            //this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw | ControlStyles.AllPaintingInWmPaint, true);

            this.imagePanel.MouseDown += this.DragForm;
            this.imagePanel.MouseDoubleClick += this.ToggleMaximize;
        }

        private void DragForm(object sender, MouseEventArgs e)
        {
            var form = this.ParentForm;
            if (e.Button == MouseButtons.Left && form != null)
            {
                //var state = form.WindowState;
                if ((e.Clicks == 1))
                {
                    NativeMethods.ReleaseCapture();
                    NativeMethods.SendMessage(form.Handle, (int)WindowMessages.WM_NCLBUTTONDOWN, (int)HitTestValues.HTCAPTION, 0);
                    form.FormBorderStyle = FormBorderStyle.None;
                }
            }
        }

        private void ToggleMaximize(object sender, MouseEventArgs e)
        {
            var form = this.ParentForm;
            if (e.Button == MouseButtons.Left && form != null)
            {
                //form.WindowState = form.WindowState == FormWindowState.Maximized ? FormWindowState.Normal : FormWindowState.Maximized;
                var validHeight = form.Size.Height >= (Screen.FromHandle(form.Handle).WorkingArea.Height) * 0.95;
                if (form.WindowState == FormWindowState.Maximized || validHeight)
                {
                    form.WindowState = FormWindowState.Normal;
                    form.FormBorderStyle = FormBorderStyle.None;
                    NativeMethods.SendMessage(form.Handle, (int)WindowMessages.WM_SYSCOMMAND, (int)SystemCommands.SC_RESTORE, 0);

                }
                else if (form.WindowState == FormWindowState.Normal)
                {
                    form.WindowState = FormWindowState.Maximized;
                    //form.FormBorderStyle = FormBorderStyle.None;
                    NativeMethods.SendMessage(form.Handle, (int)WindowMessages.WM_SYSCOMMAND, (int)SystemCommands.SC_MAXIMIZE, 0);
                }
            }
        }

        //protected override void OnPaint(PaintEventArgs e)
        //{
        //    base.OnPaint(e);
        //    ControlPaint.DrawBorder(e.Graphics, ClientRectangle,
        //        Color.Transparent, 0, ButtonBorderStyle.None,
        //        Color.Transparent, 0, ButtonBorderStyle.None,
        //        Color.LightSlateGray, 3, ButtonBorderStyle.Dashed,
        //        Color.Transparent, 0, ButtonBorderStyle.None);
        //}
    }
}
