﻿namespace ProjectX.UiControls.Windows.Forms
{
    partial class XInformerUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.messageLabel = new XLabel();
            this.abortLinkLabel = new XLinkLabel();
            this.SuspendLayout();
            // 
            // messageLabel
            // 
            this.messageLabel.BackColor = System.Drawing.Color.White;
            this.messageLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.messageLabel.Image = null;
            this.messageLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.messageLabel.Location = new System.Drawing.Point(103, 3);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.messageLabel.Size = new System.Drawing.Size(394, 17);
            this.messageLabel.TabIndex = 2;
            this.messageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // abortLinkLabel
            // 
            this.abortLinkLabel.BackColor = System.Drawing.Color.White;
            this.abortLinkLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.abortLinkLabel.Location = new System.Drawing.Point(3, 3);
            this.abortLinkLabel.Name = "abortLinkLabel";
            this.abortLinkLabel.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.abortLinkLabel.Size = new System.Drawing.Size(100, 17);
            this.abortLinkLabel.TabIndex = 3;
            this.abortLinkLabel.TabStop = true;
            this.abortLinkLabel.Text = "Onderbreken";
            this.abortLinkLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // XInformerUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.abortLinkLabel);
            this.Name = "XInformerUserControl";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.Size = new System.Drawing.Size(500, 23);
            this.ResumeLayout(false);

        }

        #endregion

        private XLabel messageLabel;
        private XLinkLabel abortLinkLabel;
    }
}
