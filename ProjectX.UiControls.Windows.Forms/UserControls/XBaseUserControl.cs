﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace ProjectX.UiControls.Windows.Forms
{
    public class XBaseUserControl : UserControl, IXLeakable
    {
        public XBaseUserControl()
        {
            this.ResizeRedraw = true;
            //InitializeComponent();
        }

        #region Property Overrides
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                //if (!DesignMode)
                //{
                //    //WS_EX_COMPOSITED.Prevents flickering, with BUG on ObjectListview
                //    cp.ExStyle |= 0x02000000;
                //}
                return cp;
            }
        }
        #endregion

        #region IXLeakable Members
        [Browsable(false)]
        public EventCollection EventCollection { get; set; } = new EventCollection();

        public virtual void Subscribe() { }
        #endregion

        protected override void OnResize(EventArgs e)
        {
            //avoid flicker!
            if (this.Visible && this.Cursor == Cursors.Default) { this.Refresh(); }
            base.OnResize(e);
        }
    }
}
