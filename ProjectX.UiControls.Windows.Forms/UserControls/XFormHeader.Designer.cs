﻿namespace ProjectX.UiControls.Windows.Forms.UserControls
{
    partial class XFormHeader
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XFormHeader));
            this.mainPictureBox = new XPictureBox();
            this.headerTitle = new XLabel();
            this.headerSubTitle = new XLabel();
            this.commandPanel = new XPanelBuffered();
            this.mainToolStrip = new XToolStrip();
            this.closeToolStripButton = new XToolStripButtonClose();
            this.maximizeToolStripButton = new XToolStripButtonMaximize();
            this.minimizeToolStripButton = new XToolStripButtonMinimize();
            this.imagePanel = new XPanelBuffered();
            ((System.ComponentModel.ISupportInitialize)(this.mainPictureBox)).BeginInit();
            this.commandPanel.SuspendLayout();
            this.mainToolStrip.SuspendLayout();
            this.imagePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainPictureBox
            // 
            this.mainPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            //TODO:
            this.mainPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("mainPictureBox.Image")));
            this.mainPictureBox.Location = new System.Drawing.Point(8, 8);
            this.mainPictureBox.Name = "mainPictureBox";
            this.mainPictureBox.Size = new System.Drawing.Size(50, 50);
            this.mainPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.mainPictureBox.TabIndex = 0;
            this.mainPictureBox.TabStop = false;
            // 
            // headerTitle
            // 
            this.headerTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerTitle.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headerTitle.ForeColor = System.Drawing.Color.Blue;
            this.headerTitle.Image = null;
            this.headerTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.headerTitle.Location = new System.Drawing.Point(68, 2);
            this.headerTitle.Name = "headerTitle";
            this.headerTitle.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.headerTitle.Size = new System.Drawing.Size(375, 35);
            this.headerTitle.TabIndex = 1;
            this.headerTitle.Text = "Title";
            this.headerTitle.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // headerSubTitle
            // 
            this.headerSubTitle.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.headerSubTitle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headerSubTitle.ForeColor = System.Drawing.Color.DimGray;
            this.headerSubTitle.Image = null;
            this.headerSubTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.headerSubTitle.Location = new System.Drawing.Point(68, 43);
            this.headerSubTitle.Name = "headerSubTitle";
            this.headerSubTitle.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.headerSubTitle.Size = new System.Drawing.Size(375, 25);
            this.headerSubTitle.TabIndex = 2;
            this.headerSubTitle.Text = "Subtitle";
            // 
            // commandPanel
            // 
            this.commandPanel.Controls.Add(this.mainToolStrip);
            this.commandPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.commandPanel.Location = new System.Drawing.Point(443, 2);
            this.commandPanel.Name = "commandPanel";
            this.commandPanel.Padding = new System.Windows.Forms.Padding(0, 0, 0, 36);
            this.commandPanel.Size = new System.Drawing.Size(155, 66);
            this.commandPanel.TabIndex = 3;
            this.commandPanel.Work = ProjectX.General.Work.None;
            this.commandPanel.XControlText = ProjectX.General.FriendlyTextType.None;
            this.commandPanel.XToolTipText = ProjectX.General.FriendlyToolTipType.None;
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.AutoSize = false;
            this.mainToolStrip.BackColor = System.Drawing.Color.White;
            this.mainToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.mainToolStrip.ImageScalingSize = new System.Drawing.Size(12, 12);
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripButton,
            this.maximizeToolStripButton,
            this.minimizeToolStripButton});
            this.mainToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.mainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Padding = new System.Windows.Forms.Padding(0);
            this.mainToolStrip.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.mainToolStrip.Size = new System.Drawing.Size(155, 32);
            this.mainToolStrip.TabIndex = 2;
            this.mainToolStrip.TabStop = true;
            this.mainToolStrip.Work = ProjectX.General.Work.None;
            this.mainToolStrip.XControlText = ProjectX.General.FriendlyTextType.None;
            this.mainToolStrip.XToolTipText = ProjectX.General.FriendlyToolTipType.None;
            // 
            // closeToolStripButton
            // 
            this.closeToolStripButton.AutoSize = false;
            this.closeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.closeToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.closeToolStripButton.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.closeToolStripButton.Name = "closeToolStripButton";
            this.closeToolStripButton.Size = new System.Drawing.Size(30, 30);
            this.closeToolStripButton.Text = "Close";
            this.closeToolStripButton.Work = ProjectX.General.Work.Close;
            this.closeToolStripButton.XControlText = ProjectX.General.FriendlyTextType.Close;
            this.closeToolStripButton.XToolTipText = ProjectX.General.FriendlyToolTipType.Close;
            // 
            // maximizeToolStripButton
            // 
            this.maximizeToolStripButton.AutoSize = false;
            this.maximizeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.maximizeToolStripButton.Enabled = false;
            this.maximizeToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.maximizeToolStripButton.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.maximizeToolStripButton.Name = "maximizeToolStripButton";
            this.maximizeToolStripButton.Size = new System.Drawing.Size(30, 30);
            this.maximizeToolStripButton.Text = "Maximize";
            this.maximizeToolStripButton.Work = ProjectX.General.Work.Maximize;
            this.maximizeToolStripButton.XControlText = ProjectX.General.FriendlyTextType.Maximize;
            this.maximizeToolStripButton.XToolTipText = ProjectX.General.FriendlyToolTipType.Maximize;
            // 
            // minimizeToolStripButton
            // 
            this.minimizeToolStripButton.AutoSize = false;
            this.minimizeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.minimizeToolStripButton.Enabled = false;
            this.minimizeToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.minimizeToolStripButton.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.minimizeToolStripButton.Name = "minimizeToolStripButton";
            this.minimizeToolStripButton.Size = new System.Drawing.Size(30, 30);
            this.minimizeToolStripButton.Text = "Minimize";
            this.minimizeToolStripButton.Work = ProjectX.General.Work.Minimize;
            this.minimizeToolStripButton.XControlText = ProjectX.General.FriendlyTextType.Minimize;
            this.minimizeToolStripButton.XToolTipText = ProjectX.General.FriendlyToolTipType.Minimize;
            // 
            // imagePanel
            // 
            this.imagePanel.Controls.Add(this.mainPictureBox);
            this.imagePanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.imagePanel.Location = new System.Drawing.Point(2, 2);
            this.imagePanel.Name = "imagePanel";
            this.imagePanel.Padding = new System.Windows.Forms.Padding(8);
            this.imagePanel.Size = new System.Drawing.Size(66, 66);
            this.imagePanel.TabIndex = 4;
            this.imagePanel.Work = ProjectX.General.Work.None;
            this.imagePanel.XControlText = ProjectX.General.FriendlyTextType.None;
            this.imagePanel.XToolTipText = ProjectX.General.FriendlyToolTipType.None;
            // 
            // XFormHeader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.headerSubTitle);
            this.Controls.Add(this.headerTitle);
            this.Controls.Add(this.commandPanel);
            this.Controls.Add(this.imagePanel);
            this.Name = "XFormHeader";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.Size = new System.Drawing.Size(600, 70);
            ((System.ComponentModel.ISupportInitialize)(this.mainPictureBox)).EndInit();
            this.commandPanel.ResumeLayout(false);
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.imagePanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private XPictureBox mainPictureBox;
        private XLabel headerTitle;
        private XLabel headerSubTitle;
        private XPanelBuffered commandPanel;
        private XPanelBuffered imagePanel;
        private XToolStrip mainToolStrip;
        private XToolStripButtonMinimize minimizeToolStripButton;
        private XToolStripButtonMaximize maximizeToolStripButton;
        private XToolStripButtonClose closeToolStripButton;
    }
}
