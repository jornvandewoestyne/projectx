﻿namespace ProjectX.UiControls.Windows.Forms
{
    partial class XMenuUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuPanel = new XPanel();
            this.menuListView = new XListView();
            this.menuComboBoxPanel = new XPanel();
            this.menuComboBox = new XComboBox();
            this.menuButton = new XButton();
            this.menuPanel.SuspendLayout();
            this.menuComboBoxPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuPanel
            // 
            this.menuPanel.Controls.Add(this.menuListView);
            this.menuPanel.Controls.Add(this.menuComboBoxPanel);
            this.menuPanel.Controls.Add(this.menuButton);
            this.menuPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.menuPanel.Location = new System.Drawing.Point(0, 0);
            this.menuPanel.Name = "menuPanel";
            this.menuPanel.Size = new System.Drawing.Size(150, 150);
            this.menuPanel.TabIndex = 5;
            // 
            // menuListView
            // 
            this.menuListView.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.menuListView.BackColor = System.Drawing.Color.White;
            this.menuListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.menuListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.menuListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.menuListView.HideSelection = false;
            this.menuListView.LabelWrap = false;
            this.menuListView.Location = new System.Drawing.Point(0, 59);
            this.menuListView.MultiSelect = false;
            this.menuListView.Name = "menuListView";
            this.menuListView.ShowItemToolTips = true;
            this.menuListView.Size = new System.Drawing.Size(150, 91);
            this.menuListView.TabIndex = 1;
            this.menuListView.UseCompatibleStateImageBehavior = false;
            this.menuListView.View = System.Windows.Forms.View.SmallIcon;
            this.menuListView.Work = ProjectX.General.Work.None;
            // 
            // menuComboBoxPanel
            // 
            this.menuComboBoxPanel.BackColor = System.Drawing.Color.White;
            this.menuComboBoxPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuComboBoxPanel.Controls.Add(this.menuComboBox);
            this.menuComboBoxPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.menuComboBoxPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.menuComboBoxPanel.Location = new System.Drawing.Point(0, 34);
            this.menuComboBoxPanel.Name = "menuComboBoxPanel";
            this.menuComboBoxPanel.Padding = new System.Windows.Forms.Padding(9, 2, 9, 0);
            this.menuComboBoxPanel.Size = new System.Drawing.Size(150, 25);
            this.menuComboBoxPanel.TabIndex = 3;
            // 
            // menuComboBox
            // 
            this.menuComboBox.BackColor = System.Drawing.Color.White;
            this.menuComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuComboBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.menuComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.menuComboBox.DropDownWidth = 250;
            this.menuComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.menuComboBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.menuComboBox.FormattingEnabled = true;
            this.menuComboBox.Location = new System.Drawing.Point(9, 2);
            this.menuComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.menuComboBox.Name = "menuComboBox";
            this.menuComboBox.Size = new System.Drawing.Size(132, 21);
            this.menuComboBox.TabIndex = 2;
            this.menuComboBox.TabStop = false;
            // 
            // menuButton
            // 
            this.menuButton.AutoSize = true;
            this.menuButton.BackColor = System.Drawing.Color.White;
            this.menuButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.menuButton.FlatAppearance.BorderSize = 0;
            this.menuButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.AliceBlue;
            this.menuButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.menuButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.menuButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuButton.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.menuButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.menuButton.ImageKey = "(none)";
            this.menuButton.Location = new System.Drawing.Point(0, 0);
            this.menuButton.Name = "menuButton";
            this.menuButton.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.menuButton.Size = new System.Drawing.Size(150, 34);
            this.menuButton.TabIndex = 0;
            this.menuButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.menuButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.menuButton.UseVisualStyleBackColor = false;
            // 
            // XMenuUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.menuPanel);
            this.Name = "XMenuUserControl";
            this.menuPanel.ResumeLayout(false);
            this.menuPanel.PerformLayout();
            this.menuComboBoxPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private XPanel menuPanel;
        private XListView menuListView;
        private XComboBox menuComboBox;
        private XButton menuButton;
        private XPanel menuComboBoxPanel;

    }
}
