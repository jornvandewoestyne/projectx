﻿using ProjectX.Data;
using ProjectX.Domain.Models;
using ProjectX.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

//TODO: summary...
//TODO: translate keywords...

//TODO: TryLength, TryCount

namespace ProjectX.Business.Helpers
{
    internal static class ServiceHelper
    {
        #region Methods: CollectionPage
        private static ICollectionPage<TEntity> GetDataPage<TEntity>(this IBaseService service, Ordering ordering, Paging paging)
            where TEntity : BaseEntity
        {
            return service.GetRepository<TEntity>().GetDataPage(ordering, paging);
        }

        private static async Task<ICollectionPage<TEntity>> GetDataPageAsync<TEntity>(this IBaseService service, Ordering ordering, Paging paging, CancellationToken cancellationToken)
            where TEntity : BaseEntity
        {
            return await service.GetRepository<TEntity>().GetDataPageAsync(ordering, paging, cancellationToken);
        }

        private static ICollectionPage<TData> GetDataPage<TEntity, TData>(this IBaseService service, IKeySet<int> keySet, Ordering ordering, Paging paging)
            where TEntity : BaseCommonEntity
            where TData : BaseEntity
        {
            if (keySet != null)
            {
                var entityKeyName = service.GetRepository<TEntity>().GetKeyName();
                return service.GetRepository<TData>().GetDataPage(keySet, entityKeyName, ordering, paging);
            }
            return service.GetDataPage<TData>(ordering, paging);
        }

        public static ICollectionPage<TData> GetDataPage<TEntity, TSearch, TData>(this IBaseService service, SearchString searchString, Ordering ordering, Paging paging)
            where TEntity : BaseCommonEntity
            where TSearch : BaseSearchEntity
            where TData : BaseEntity
        {
            var keySet = service.RunMultiSearch<TSearch>(searchString, paging);
            return service.GetDataPage<TEntity, TData>(keySet, ordering, paging);
        }

        private static async Task<ICollectionPage<TData>> GetDataPageAsync<TEntity, TData>(this IBaseService service, IKeySet<int> keySet, Ordering ordering, Paging paging, CancellationToken cancellationToken)
            where TEntity : BaseCommonEntity
            where TData : BaseEntity
        {
            if (keySet != null)
            {
                var entityKeyName = service.GetRepository<TEntity>().GetKeyName();
                return await service.GetRepository<TData>().GetDataPageAsync(keySet, entityKeyName, ordering, paging, cancellationToken);
            }
            return await service.GetDataPageAsync<TData>(ordering, paging, cancellationToken);
        }

        public static async Task<ICollectionPage<TData>> GetDataPageAsync<TEntity, TSearch, TData>(this IBaseService service, SearchString searchString, Ordering ordering, Paging paging, CancellationToken cancellationToken)
            where TEntity : BaseCommonEntity
            where TSearch : BaseSearchEntity
            where TData : BaseEntity
        {
            var keySet = await service.RunMultiSearchAsync<TSearch>(searchString, paging, cancellationToken);
            return await service.GetDataPageAsync<TEntity, TData>(keySet, ordering, paging, cancellationToken);
        }
        #endregion

        #region Methods: MultiSearch
        /// <summary>
        /// Returns a <see cref="IKeySet&lt;int&gt;"/> synchronously 
        /// as a result from a global search method which enables multi search.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="service"></param>
        /// <param name="searchString"></param>
        /// <param name="paging"></param>
        /// <returns></returns>
        private static IKeySet<int> RunMultiSearch<TEntity>(this IBaseService service, SearchString searchString, Paging paging)
            where TEntity : BaseSearchEntity
        {
            return AsyncHelper.RunSync<IKeySet<int>>
                (() => service.RunMultiSearchAsync<TEntity>(searchString, paging, CancellationToken.None));
        }

        /// <summary>
        /// Returns a <see cref="IKeySet&lt;int&gt;"/> asynchronously 
        /// as a result from a global search method which enables multi search.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="service"></param>
        /// <param name="searchString"></param>
        /// <param name="paging"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private static async Task<IKeySet<int>> RunMultiSearchAsync<TEntity>(this IBaseService service, SearchString searchString, Paging paging, CancellationToken cancellationToken)
            where TEntity : BaseSearchEntity
        {
            if (searchString != null && !searchString.IsEmpty && paging != null && !paging.SkipAll)
            {
                //Initialize some variables:
                var repository = service.GetRepository<TEntity>();
                var input = searchString.SearchValue;
                var delimiters = searchString.Delimeters;
                var notifications = new List<Notification>();
                //Run:
                if (!input.IsEmptyString())
                {
                    //Make an array of both included and excluded keywords:
                    var array = searchString.BreakUp.Array;
                    if (array != null)
                    {
                        //Initialize some variables:
                        var includedKeywords = searchString.BreakUp.Array_IncludedKeyWords;
                        //Split the input string to check if multiple string search is applied:
                        var keywords = searchString.BreakUp.KeyWords;
                        //Initialize the output variable:
                        HashSet<int> result = null;
                        //Find all matches:
                        if (keywords.Count == 1 && !includedKeywords.IsEmptyString())
                        {
                            return await repository.RunSearchAsync(keywords, result, cancellationToken);
                        }
                        else //-> Multi search!
                        {
                            //Initialize some variables:
                            result = new HashSet<int>();
                            HashSet<int> tempKeys = null;
                            IEnumerable<int> matches = null;
                            var length = searchString.BreakUp.Array.Length;
                            //Run through all keywords (included and excluded):
                            for (int i = 0; i < length; i++)
                            {
                                //Initialize temporary set of results:
                                var tempResult = new HashSet<int>();
                                if (!array[i].IsEmptyString())
                                {
                                    if (i == (int)BreakUpIndex.Excluded && result.Count > XLib.MagicNumber.IntZero) //-> excluded keywords
                                    {
                                        //Narrow the temporary result for optimal performance:
                                        tempKeys = new HashSet<int>(result);
                                    }
                                    //Split the string further apart (char = "or" variable):
                                    var keywordsOr = array[i].Split(XLib.Token.Or, true).ToList();
                                    foreach (var keywordOr in keywordsOr)
                                    {
                                        if (!keywordOr.IsEmptyString())
                                        {
                                            //Split the string further apart (char = "and" variable):
                                            var keywordsAnd = keywordOr.Split(XLib.Token.And, true).ToList();
                                            if (keywordsAnd.Count > XLib.MagicNumber.IntZero)
                                            {
                                                //Get all keys based on the splitted input string:
                                                var keySet = await repository.RunSearchAsync(keywordsAnd, tempKeys, cancellationToken);
                                                if (keySet != null)
                                                {
                                                    matches = keySet.Items;
                                                    if (keySet.HasNotifications)
                                                    {
                                                        notifications = keySet.Notifications.ToList();
                                                        return new List<int>().AsKeySet<int>(notifications);
                                                    }
                                                    //Cumulate all found matches:
                                                    tempResult.UnionWith(matches);
                                                }
                                            }
                                        }
                                    }
                                    //Cumulate all found keys:
                                    result.UnionWith(tempResult);
                                }
                                //Remove all found matches in case of excluded keywords are given:
                                if (i == (int)BreakUpIndex.Excluded && !array[i].IsEmptyString())
                                {
                                    //Set keys when no include keywords are given:
                                    if (includedKeywords.IsEmptyString())
                                    {
                                        var allKeys = await repository.GetDataAsync(cancellationToken);
                                        result = new HashSet<int>(allKeys.Select(x => x.ID));
                                    }
                                    //Remove all found keys:
                                    result.RemoveWhere(x => tempResult.Contains(x));
                                }
                            }
                        }
                        //return the unique keys:
                        return result.AsKeySet<int>(notifications);
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="keywords"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        private static async Task<IKeySet<int>> RunSearchAsync<TEntity>(this IRepository<TEntity> repository, List<string> keywords, HashSet<int> keys, CancellationToken cancellationToken)
            where TEntity : BaseSearchEntity
        {
            var initialSearch = false;
            var keySet = keys.AsKeySet<int>(null);
            if (!keySet.ResolveNull().HasItems)
            {
                initialSearch = true;
            }
            if (keywords != null)
            {
                var entityKeyName = new string[] { repository.NameOf((TEntity master) => master.ID) }.AsEntityKeyName();
                //Run through the keywords:
                foreach (var keyword in keywords)
                {
                    if (!keyword.IsEmptyString())
                    {
                        Expression<Func<TEntity, bool>> simpleFilter = q => q.SearchString.Contains(keyword);
                        Expression<Func<TEntity, bool>> combinedFilter = q => keySet.Items.Contains(q.ID) && q.SearchString.Contains(keyword);
                        keySet = await repository.GetDataAsync<TEntity>(simpleFilter, combinedFilter, keySet, entityKeyName, keyword, initialSearch, null, cancellationToken);
                        if (!keySet.ResolveNull().HasItems || keySet.ResolveNull().HasNotifications)
                        {
                            //Break the loop early:
                            return keySet;
                        }
                        if (initialSearch)
                        {
                            initialSearch = false;
                        }
                    }
                }
            }
            return keySet;
        }
        #endregion
    }
}