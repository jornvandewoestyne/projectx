﻿using ProjectX.Business;
using ProjectX.Domain.Models;
using ProjectX.General;

namespace ProjectX.DTO
{
    public sealed class BaseEntitySummaryDTO<TEntity> : BaseClass where TEntity : ICommonEntity //, IDisposable
    {
        #region Properties
        private IService Service { get; }
        //private ICommonEntity Entity { get; }
        private EntityKey EntityKey { get; }
        private MenuBloc Action { get; }

        public string Title => this.GetTitle();
        public string Identifier => this.GetIdentifier();
        #endregion

        #region Constructors
        internal BaseEntitySummaryDTO(IService service, EntityKey entityKey, MenuBloc identifier)
        {
            this.Service = service;
            this.EntityKey = entityKey;
            this.Action = identifier;
        }
        #endregion

        #region Methods
        private string GetTitle()
        {
            return null; // StringHelper.Title(this.Entity?.TitleProperties);
        }

        private string GetIdentifier()
        {
            var entityKey = this.Action.AsEntityKey();
            return this.Service.GetEntity<Menu>(entityKey, null)?.Member?.Name;
        }

        public override string ToString()
        {
            return StringHelper.Title(this.Identifier, this.Title);
        }
        #endregion
    }
}
