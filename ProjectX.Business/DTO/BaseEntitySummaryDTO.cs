﻿using ProjectX.Business;
using ProjectX.Domain.Models;
using ProjectX.General;

//TODO: implement IDisposable

namespace ProjectX.DTO
{
    public sealed class BaseEntitySummaryDTO : DisposableObject
    {
        #region Properties
        private IService Service { get; set; }
        private ICommonEntity Entity { get; set; }
        private MenuBloc Action { get; set; }
        public string Title => this.GetTitle();
        public string Identifier => this.GetIdentifier();
        #endregion

        #region Constructors
        internal BaseEntitySummaryDTO(IService service, ICommonEntity entity, MenuBloc identifier)
        {
            this.Service = service;
            this.Entity = entity;
            this.Action = identifier;
        }
        #endregion

        #region Methods
        private string GetTitle()
        {
            return StringHelper.Title(this.Entity?.TitleProperties);
        }

        private string GetIdentifier()
        {
            var entityKey = this.Action.AsEntityKey();
            return this.Service.GetEntity<Menu>(entityKey, null)?.Member?.Name;
        }

        public override string ToString()
        {
            return StringHelper.Title(this.Identifier, this.Title);
        }
        #endregion

        #region Methods: DisposableObject
        public override void Dispose(bool disposing)
        {
            if (disposing && !IsDisposed)
            {
                this.Entity = null;
                this.Service.Dispose();
                this.Service = null;
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
