﻿using ProjectX.Domain.Models;
using ProjectX.General;

namespace ProjectX.DTO
{
    public sealed class MenuSummaryDTO : BaseClass
    {
        #region Properties
        public string Identifier { get; }
        #endregion

        #region Constructors
        internal MenuSummaryDTO(Menu menu)
        {
            //TODO:
            this.Identifier = menu.Member.Name;
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return this.Identifier;
        }
        #endregion
    }
}
