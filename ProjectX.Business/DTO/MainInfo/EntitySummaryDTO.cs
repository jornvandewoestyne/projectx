﻿using ProjectX.Domain.Models;
using ProjectX.General;

namespace ProjectX.DTO
{
    public sealed class EntitySummaryDTO : BaseEntitySummary
    {
        #region Properties      
        public override string Title { get; }
        public override string SubTitle { get; }
        public override string FullName { get; }
        public override string SortName { get; }

        //TODO
        public string Identifier { get; }
        #endregion

        #region Constructors
        internal EntitySummaryDTO(BaseEntitySummary summary)
        {
            if (summary != null)
            {
                this.Title = summary.Title;
                this.SubTitle = summary.SubTitle;
                this.SortName = summary.SortName;
            }
        }

        internal EntitySummaryDTO(BaseEntitySummary summary, Menu menu)
        {
            if (summary != null && menu != null)
            {
                this.Title = summary.Title;
                this.SubTitle = summary.SubTitle;
                this.SortName = summary.SortName;
                //TODO:
                this.Identifier = menu.Member.Name;
            }
        }
        #endregion
    }
}
