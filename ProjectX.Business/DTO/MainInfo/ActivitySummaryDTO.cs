﻿using ProjectX.Domain.Models;
using ProjectX.General;

namespace ProjectX.DTO
{
    public sealed class ActivitySummaryDTO : BaseClass
    {
        #region Properties
        public int ID { get; }
        public string Name { get; }
        public string GroupName { get; }
        public bool MultipleInstancesAllowed { get; }
        public int SequenceNumber => this.ID;
        public MenuGroupAction ActionIdentifier => (MenuGroupAction)this.ID;
        #endregion

        #region Constructors
        internal ActivitySummaryDTO(Activity activity)
        {
            //TODO:
            this.ID = activity.ActivityID;
            this.Name = activity.Name;
            this.GroupName = activity.ActivityGroup.Name;
            this.MultipleInstancesAllowed = activity.MultipleInstancesAllowed;
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return this.Name;
        }
        #endregion
    }
}

