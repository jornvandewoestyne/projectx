﻿namespace ProjectX.DTO
{
    public sealed class MainInfoDTO
    {
        #region Properties
        public EntitySummaryDTO EntityDTO { get; }
        public MenuSummaryDTO MenuDTO { get; }
        public ActivitySummaryDTO ActivityDTO { get; }
        #endregion

        #region Constructors
        public MainInfoDTO(EntitySummaryDTO entityDTO, MenuSummaryDTO menuDTO, ActivitySummaryDTO activityDTO)
        {
            this.EntityDTO = entityDTO;
            this.MenuDTO = menuDTO;
            this.ActivityDTO = activityDTO;
        }
        #endregion
    }
}
