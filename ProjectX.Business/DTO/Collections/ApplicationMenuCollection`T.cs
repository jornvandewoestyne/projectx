﻿using ProjectX.Business;
using ProjectX.Domain.Models;
using ProjectX.General;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.DTO.Collections
{
    public abstract class ApplicationMenuCollection<Key, Value> : IKeyValueCollection<Key, Value>
        where Key : Module
        where Value : Menu
    {
        #region Properties
        protected IService Service { get; }
        public Dictionary<Key, IEnumerable<Value>> Items { get; private set; } = new Dictionary<Key, IEnumerable<Value>>();
        #endregion

        #region Constructors
        public ApplicationMenuCollection(IService service)
        {
            this.Service = service;
        }
        #endregion

        #region Methods
        protected async Task<ApplicationMenuCollection<Key, Value>> GetDataAsync(CancellationToken cancellationToken)
        {
            using (var service = this.Service as IBaseService)
            {
                var modules = await this.GetActiveApplicationMenuAsync(service, cancellationToken);
                foreach (var module in modules)
                {
                    var menuItems = await this.GetMenuItemsAsync(module, cancellationToken);
                    this.Items.Add(module, menuItems);
                }
                return this;
            }
        }
        #endregion

        #region Methods: Helpers
        private async Task<IEnumerable<Key>> GetActiveApplicationMenuAsync(IBaseService service, CancellationToken cancellationToken)
        {
            //Get/Sort active Application Modules:
            var list = await service.GetRepository<Key>().GetDataAsync(
                q => q.IsBlocked == false
                , new Ordering<Key>().OrderBy
                , new Including<Key>(service.GetRepository<Key>().GetNavigationProperties(), x => x.Menus)
                , cancellationToken);
            //Get/Sort active Application Members:
            foreach (var item in list)
            {
                //Get active Menu items:
                item.Menus = item.Menus
                    .Where(x => x.IsBlocked == false)
                    .ToList();
                //Get/Sort active Members:
                item.Menus = item.Menus
                    .Where(x => x.Member.IsBlocked == false)
                    .OrderBy(x => x.SortKey).ThenBy(x => x.Member.SortKey).ThenBy(x => x.Member.Name)
                    .ToList();
                //Get active Activity items:
                foreach (var menu in item.Menus)
                {
                    menu.MenuActivities = menu.MenuActivities
                        .Where(y => y.IsBlocked == false)
                        .OrderBy(y => y.SortKey).ThenBy(y => y.Activity.SortKey).ThenBy(y => y.Activity.Name)
                        .ToList();
                }
            }
            return list
                .Where(x => x.Menus.Count() > XLib.MagicNumber.IntZero);
        }

        private async Task<IEnumerable<Value>> GetMenuItemsAsync(Module entity, CancellationToken cancellationToken)
        {
            return (IEnumerable<Value>)await Task.Run(() => entity.Menus, cancellationToken);
        }
        #endregion
    }
}
