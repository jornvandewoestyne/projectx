﻿using ProjectX.Business;
using ProjectX.Domain.Models;
using ProjectX.DTO.Collections;
using ProjectX.General;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.DTO
{
    public sealed class ApplicationMenuDTO : ApplicationMenuCollection<Module, Menu>, IKeyValueDTO
    {
        #region Constructors
        public ApplicationMenuDTO(IService service)
            : base(service)
        { }
        #endregion

        #region Methods:
        internal ApplicationMenuDTO GetData()
        {
            return AsyncHelper.RunSync(() => this.GetDataAsync(CancellationToken.None));
        }

        internal new async Task<ApplicationMenuDTO> GetDataAsync(CancellationToken cancellationToken)
        {
            await base.GetDataAsync(cancellationToken);
            return this;
        }
        #endregion
    }
}
