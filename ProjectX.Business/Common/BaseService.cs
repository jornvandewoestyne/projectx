﻿using ProjectX.Data;
using ProjectX.General;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.Business.Common
{
    public abstract class BaseService : IBaseService
    {
        #region Properties
        private IUnitOfWork UnitOfWork { get; }
        #endregion

        #region Constructors
        protected BaseService(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
            this.Disposed = false;
        }
        #endregion

        #region Methods
        public void TryToConnect()
        {
            this.UnitOfWork.TryToConnect();
        }

        public async Task TryToConnectAsync(CancellationToken cancellationToken)
        {
            await this.UnitOfWork.TryToConnectAsync(cancellationToken);
        }

        /// <summary>
        /// Returns the requested repository.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity
        {
            return this.UnitOfWork.GetRepository<TEntity>();
        }

        /// <summary>
        /// Used to save the changes to the underlying data store.
        /// </summary>
        public void Save()
        {
            this.UnitOfWork.Save();
        }
        #endregion

        #region Methods: -> interface IDisposable
        protected bool Disposed { get; private set; }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.Disposed)
            {
                if (disposing)
                {
                    this.UnitOfWork.Dispose();
                }
                this.Disposed = true;
            }
        }
        #endregion
    }
}