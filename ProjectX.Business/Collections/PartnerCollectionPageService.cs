﻿using ProjectX.Domain.Models;

//REMINDER: Add CollectionPageServices accordingly...

namespace ProjectX.Business
{
    internal sealed class PartnerGeneralDataCollectionPageService : BaseCollectionPageService<Partner, PartnerSearch, PartnerGeneralData>
    {
        public PartnerGeneralDataCollectionPageService(IService service) : base(service) { }
    }

    internal sealed class CustomerGeneralDataCollectionPageService : BaseCollectionPageService<Partner, CustomerPartnerSearch, CustomerGeneralData>
    {
        public CustomerGeneralDataCollectionPageService(IService service) : base(service) { }
    }
}
