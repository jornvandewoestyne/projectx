﻿using ProjectX.Business.Helpers;
using ProjectX.Domain.Models;
using ProjectX.General;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.Business
{
    /// <summary>
    /// ...
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TSearch"></typeparam>
    /// <typeparam name="TData"></typeparam>
    internal abstract class BaseCollectionPageService<TEntity, TSearch, TData> : DisposableObject, ICollectionPage, ICollectionPageService<TEntity, TSearch, TData>
            where TEntity : BaseCommonEntity
            where TSearch : BaseSearchEntity
            where TData : BaseEntity
    {
        #region Properties
        protected IService Service { get; private set; }
        public object CollectionPage { get; protected set; }
        #endregion

        #region Constructors
        public BaseCollectionPageService(IService service)
        {
            this.Service = service;
        }
        #endregion

        #region Methods => ICollectionPageService
        public virtual ICollectionPage GetDataPage(SearchString searchString, Ordering ordering, Paging paging)
        {
            return AsyncHelper.RunSync(() => this.GetDataPageAsync(searchString, ordering, paging, CancellationToken.None));
        }

        public virtual async Task<ICollectionPage> GetDataPageAsync(SearchString searchString, Ordering ordering, Paging paging, CancellationToken cancellationToken)
        {
            var service = this.Service as IBaseService;
            this.CollectionPage = await service?.GetDataPageAsync<TEntity, TSearch, TData>(searchString, ordering, paging, cancellationToken);
            return this;
        }
        #endregion

        #region Methods: DisposableObject
        public override void Dispose(bool disposing)
        {
            if (disposing && !IsDisposed)
            {
                this.Service?.Dispose();
                this.Service = null;
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
