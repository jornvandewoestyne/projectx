﻿using ProjectX.Domain.Models;
using ProjectX.General;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.Business
{
    /// <summary>
    /// ...
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TSearch"></typeparam>
    /// <typeparam name="TData"></typeparam>
    public interface ICollectionPageService<TEntity, TSearch, TData>
            where TEntity : BaseCommonEntity
            where TSearch : BaseSearchEntity
            where TData : BaseEntity
    {
        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="ordering"></param>
        /// <param name="paging"></param>
        /// <returns></returns>
        ICollectionPage GetDataPage(SearchString searchString, Ordering ordering, Paging paging);


        /// <summary>
        /// ...
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="ordering"></param>
        /// <param name="paging"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<ICollectionPage> GetDataPageAsync(SearchString searchString, Ordering ordering, Paging paging, CancellationToken cancellationToken);
        #endregion
    }
}
