﻿using ProjectX.General;
using System.Collections.Generic;

namespace ProjectX.DTO
{
    public interface IKeyValueCollection<Key, Value>
        where Key : BaseEntity
        where Value : BaseEntity
    {
        #region Properties
        Dictionary<Key, IEnumerable<Value>> Items { get; }
        #endregion
    }
}
