﻿using ProjectX.Domain.Models;
using ProjectX.DTO;
using ProjectX.General;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.Business
{
    public interface IService : IDisposable
    {
        //TODO: Remove test method...
        int Test(string name);

        #region Methods: Test Connection
        /// <summary>
        /// Tests the current connection.
        /// </summary>
        void TestConnection();

        /// <summary>
        /// Tests the current connection asynchronously.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task TestConnectionAsync(CancellationToken cancellationToken);
        #endregion

        #region Methods: Entity NavigationProperties
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        IEnumerable<System.Reflection.PropertyInfo> GetNavigationProperties<TEntity>() where TEntity : BaseEntity;
        #endregion

        #region Methods: EntityKeys
        /// <summary>
        /// Returns the primary key(s) of a given entity.
        /// <para>ATTENTION!</para>
        /// <para>Don't implement the PostSharp [Log] attribute in the UI layer which gives an unexpected InvalidCastException!</para>
        /// </summary>
        EntityKey GetEntityKey<TEntity>(TEntity entity) where TEntity : BaseEntity;

        /// <summary>
        /// Returns the names of the primary key(s) of a given entity.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        EntityKeyName GetEntityKeyName<TEntity>() where TEntity : BaseEntity;
        #endregion

        #region Methods: Entities
        /// <summary>
        /// Returns an entity based on its id.
        /// </summary>
        TEntity GetEntity<TEntity>(EntityKey entityKey, Including<TEntity> including) where TEntity : BaseEntity;

        /// <summary>
        /// Returns an entity based on its id asynchronously.
        /// </summary>
        Task<TEntity> GetEntityAsync<TEntity>(EntityKey entityKey, Including<TEntity> including, CancellationToken cancellationToken) where TEntity : BaseEntity;

        /// <summary>
        /// Returns the fullname of the given entity. (format: [fullname])
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        string GetFullName<TEntity>(TEntity entity) where TEntity : BaseCommonEntity;

        MainInfoDTO GetInstanceSummary(ICommonEntity entity, MenuGroupAction menuGroupAction, MenuBloc menuBloc);

        EntitySummaryDTO GetSummary(ICommonEntity entity, MenuBloc menuBloc);

        Task<EntitySummaryDTO> GetSummaryAsync(ICommonEntity entity, MenuBloc menuBloc, CancellationToken cancellationToken);

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entityKey"></param>
        /// <param name="identifier"></param>
        /// <returns></returns>
        BaseEntitySummary GetSummary<TEntity>(EntityKey entityKey, MenuBloc identifier) where TEntity : BaseCommonEntity;

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="identifier"></param>
        /// <returns></returns>
        BaseEntitySummaryDTO GetSummary<TEntity>(TEntity entity, MenuBloc identifier) where TEntity : ICommonEntity;
        #endregion

        #region Methods:
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        List<GroupValue> GetEntityValues<TEntity>(TEntity entity) where TEntity : BaseEntity;
        #endregion

        #region Methods: DTO
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetDTO<T>() where T : IKeyValueDTO;

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<T> GetDTOAsync<T>(CancellationToken cancellationToken) where T : IKeyValueDTO;
        #endregion

        #region CollectionPage
        /// <summary>
        /// Returns requested general data synchronously (read-only).
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="searchString"></param>
        /// <param name="ordering"></param>
        /// <param name="paging"></param>
        /// <returns></returns>
        ICollectionPage GetDataPage<TEntity>(SearchString searchString, Ordering ordering, Paging paging)
            where TEntity : BaseEntity;

        /// <summary>
        /// Returns requested general data asynchronously (read-only).
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="searchString"></param>
        /// <param name="ordering"></param>
        /// <param name="paging"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<ICollectionPage> GetDataPageAsync<TEntity>(SearchString searchString, Ordering ordering, Paging paging, CancellationToken cancellationToken)
            where TEntity : BaseEntity;
        #endregion
    }
}
