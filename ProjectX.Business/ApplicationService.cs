﻿using ProjectX.Business.Common;
using ProjectX.Data;
using ProjectX.Domain.Models;
using ProjectX.DTO;
using ProjectX.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectX.Business
{
    public sealed class ApplicationService : BaseService, IService
    {
        public int Test(string name) //TODO: return object[]!!
        {
            try
            {
                //throw new NotImplementedException();
                //Update a person:
                //Person person = this.Person.GetById(16);
                //person.FirstName = "TESTING " + DateTime.Now.ToString();
                //this.Person.Update(person);
                //this.UnitOfWork.Save();
                int id = this.GetRepository<Partner>().GetData().Max(x => x.PartnerID) + 1; //TODO: GetEntityKey!
                //Insert a partner:           
                Company company = new Company();
                company.CompanyName = (name.RemoveWhiteSpace().ToUpper() + " " + id).TryTrim();
                company.CallName = company.CompanyName.RemoveAllWhiteSpace();
                company = this.GetRepository<Company>().InsertAndSave(company);
                Contact contact = new Contact();
                contact.CompanyID = company.CompanyID;
                contact = this.GetRepository<Contact>().InsertAndSave(contact);
                Partner partner = new Partner();
                partner.MasterContactID = contact.ContactID;
                partner.SalesInvoiceContactID = contact.ContactID;
                partner.SalesDeliveryContactID = contact.ContactID;
                partner.PartnerCode = "X" + contact.ContactID;
                partner.LanguageID = 1;
                partner = this.GetRepository<Partner>().InsertAndSave(partner);
                //company.PartnerID = partner.PartnerID;
                this.GetRepository<Company>().Update(company);
                contact.PartnerID = partner.PartnerID;
                this.GetRepository<Contact>().Update(contact);

                int[] key = new int[] { 2, 4, 5, 6, 7 };
                for (int i = 0; i < key.Length; i++)
                {
                    ContactRole role = new ContactRole();
                    role.ContactID = contact.ContactID;
                    role.ContactRoleTypeID = key[i];
                    this.GetRepository<ContactRole>().InsertAndSave(role);
                }
                PartnerSearch search = new PartnerSearch();
                search.ID = partner.PartnerID;
                search.SearchString = company.CompanyName;
                search.DateCreated = DateTime.Now;
                this.GetRepository<PartnerSearch>().InsertAndSave(search);
                PartnerSearch search1 = new PartnerSearch();
                search1.ID = partner.PartnerID;
                search1.SearchString = company.CallName;
                search1.DateCreated = DateTime.Now;
                this.GetRepository<PartnerSearch>().InsertAndSave(search1);
                PartnerSearch search2 = new PartnerSearch();
                search2.ID = partner.PartnerID;
                search2.SearchString = partner.PartnerCode;
                search2.DateCreated = DateTime.Now;
                this.GetRepository<PartnerSearch>().InsertAndSave(search2);
                this.Save();
                return id;
            }
            catch (Exception)
            {
                return Convert.ToInt32(XLib.Key.NotExcistingItem);  //TODO: will give Exception!!!!???
            }
        }

        #region Properties
        private IStoredProcedure StoredProcedure { get; set; }
        #endregion

        #region Constructors
        public ApplicationService(IUnitOfWork unitOfWork, IStoredProcedure storedProcedure)
            : base(unitOfWork)
        {
            this.StoredProcedure = storedProcedure;
        }
        #endregion

        #region Methods: Test Connection
        public void TestConnection()
        {
            this.TryToConnect();
        }

        public async Task TestConnectionAsync(CancellationToken cancellationToken)
        {
            await this.TryToConnectAsync(cancellationToken);
        }
        #endregion

        #region Methods: Entity NavigationProperties
        public IEnumerable<System.Reflection.PropertyInfo> GetNavigationProperties<TEntity>() where TEntity : BaseEntity
        {
            return this.GetRepository<TEntity>().GetNavigationProperties();
        }
        #endregion

        #region Methods: EntityKeys
        public EntityKey GetEntityKey<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            return this.GetRepository<TEntity>().GetKey(entity);
        }

        public EntityKeyName GetEntityKeyName<TEntity>() where TEntity : BaseEntity
        {
            return this.GetRepository<TEntity>().GetKeyName();
        }
        #endregion

        #region Methods: Entities
        public TEntity GetEntity<TEntity>(EntityKey entityKey, Including<TEntity> including) where TEntity : BaseEntity
        {
            return this.GetRepository<TEntity>().GetEntity(entityKey, including);
        }

        public async Task<TEntity> GetEntityAsync<TEntity>(EntityKey entityKey, Including<TEntity> including, CancellationToken cancellationToken) where TEntity : BaseEntity
        {
            return await this.GetRepository<TEntity>().GetEntityAsync(entityKey, including, cancellationToken);
        }

        public string GetFullName<TEntity>(TEntity entity) where TEntity : BaseCommonEntity
        {
            var type = typeof(TEntity);
            switch (type.Name)
            {
                case nameof(Partner):
                    return this.GetPartnerFullName(entity as Partner);
                default:
                    throw new NotImplementedException(type.ToString());
            }
        }

        public MainInfoDTO GetInstanceSummary(ICommonEntity entity, MenuGroupAction menuGroupAction, MenuBloc menuBloc)
        {
            var entityDTO = new EntitySummaryDTO(entity.EntitySummary);
            var menuDTO = new MenuSummaryDTO(this.GetRepository<Menu>().GetEntity(menuBloc.AsEntityKey(), null));
            var activityDTO = new ActivitySummaryDTO(this.GetRepository<Activity>().GetEntity(menuGroupAction.AsEntityKey(), null));
            return new MainInfoDTO(entityDTO, menuDTO, activityDTO);
        }

        public EntitySummaryDTO GetSummary(ICommonEntity entity, MenuBloc menuBloc)
        {
            if (entity != null)
            {
                var summary = entity.EntitySummary;
                var menu = this.GetRepository<Menu>().GetEntity(menuBloc.AsEntityKey(), null);
                return new EntitySummaryDTO(summary, menu);
            }
            return new EntitySummaryDTO(null);
        }

        public async Task<EntitySummaryDTO> GetSummaryAsync(ICommonEntity entity, MenuBloc menuBloc, CancellationToken cancellationToken)
        {
            var summary = entity?.EntitySummary;
            var menu = await this.GetRepository<Menu>().GetEntityAsync(menuBloc.AsEntityKey(), null, cancellationToken);
            return new EntitySummaryDTO(summary, menu);
        }

        public BaseEntitySummary GetSummary<TEntity>(EntityKey entityKey, MenuBloc identifier) where TEntity : BaseCommonEntity
        {
            var entity = this.GetEntity<TEntity>(entityKey, null) as ICommonEntity;
            return entity.EntitySummary;
        }

        public BaseEntitySummaryDTO GetSummary<TEntity>(TEntity entity, MenuBloc identifier) where TEntity : ICommonEntity
        {
            using (var summary = new BaseEntitySummaryDTO(this, entity, identifier))
            {
                return summary;
            }
        }

        #endregion

        public List<GroupValue> GetEntityValues<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            return entity.GetGroupValues<TEntity>();
        }

        #region Methods: DTO
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetDTO<T>() where T : IKeyValueDTO
        {
            return AsyncHelper.RunSync(() => this.GetDTOAsync<T>(CancellationToken.None));
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<T> GetDTOAsync<T>(CancellationToken cancellationToken)
            where T : IKeyValueDTO
        {
            var type = typeof(T);
            IKeyValueDTO result = null;
            switch (type.Name)
            {
                case nameof(ApplicationMenuDTO):
                    result = await new ApplicationMenuDTO(this).GetDataAsync(cancellationToken);
                    return (T)result;
                default:
                    throw new System.NotImplementedException(type.ToString());
            }
        }
        #endregion

        #region Methods: Partners
        private string GetPartnerFullName(Partner entity)
        {
            //TODO: DisposedException
            if (entity != null)
            {
                return entity?.MasterContact?.FullName;
            }
            return null;
        }
        #endregion

        //REMINDER: Add CollectionPageServices accordingly...
        #region CollectionPage
        public ICollectionPage GetDataPage<TEntity>(SearchString searchString, Ordering ordering, Paging paging)
            where TEntity : BaseEntity
        {
            return AsyncHelper.RunSync<ICollectionPage>
                (() => this.GetDataPageAsync<TEntity>(searchString, ordering, paging, CancellationToken.None));
        }

        public async Task<ICollectionPage> GetDataPageAsync<TEntity>(SearchString searchString, Ordering ordering, Paging paging, CancellationToken cancellationToken)
            where TEntity : BaseEntity
        {
            var type = typeof(TEntity);
            switch (type.Name)
            {
                case nameof(PartnerGeneralData):
                    return await new PartnerGeneralDataCollectionPageService(this)
                        .GetDataPageAsync(searchString, ordering, paging, cancellationToken);
                case nameof(CustomerGeneralData):
                    return await new CustomerGeneralDataCollectionPageService(this)
                        .GetDataPageAsync(searchString, ordering, paging, cancellationToken);
                default:
                    throw new NotImplementedException(type.ToString());
            }
        }
        #endregion
    }
}
