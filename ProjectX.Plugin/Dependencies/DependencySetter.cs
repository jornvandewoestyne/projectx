﻿using ProjectX.Business;
using ProjectX.Data;
using ProjectX.Data.Models;
using ProjectX.Data.StoredProcedures;
using ProjectX.Infrastructure.DependencyInjection;
using ProjectX.Infrastructure.ExceptionHandlers;
using ProjectX.Infrastructure.Localization;
using ProjectX.Infrastructure.LogService;
using ProjectX.Infrastructure.MessageService;

namespace ProjectX.Plugin.Dependencies
{
    public sealed class DependencySetter
    {
        /// <summary>
        /// ...
        /// </summary>
        public ApplicationDependencies UseWinForms(MessageBoxType messageBoxType)
        {
            var root = new CompositionRootType<NinjectCompositionRoot>().DependencyType;
            var module = new ModuleType<NinjectUiWindowsFormsModule>().DependencyType;
            var context = new ContextType<EfDbContext>().DependencyType;
            var handler = new ExceptionHandlerType<ExceptionHandler>().DependencyType;
            var interceptor = new InterceptorType<NinjectExceptionInterceptor>().DependencyType;
            var logger = new LoggerType<LogToFile>().DependencyType;
            var messenger = new MessengerType<WindowsFormsMessenger>(messageBoxType).DependencyType;
            var service = new ServiceType<ApplicationService>().DependencyType;
            var storedProcedure = new StoredProcedureType<EfStoredProcedure>().DependencyType;
            var translator = new TranslatorType<ResourceXml>().DependencyType;
            var unitOfWork = new UnitOfWorkType<EfUnitOfWork>().DependencyType;
            return new ApplicationDependencies(root, module, context, handler, interceptor, logger, messenger, service, storedProcedure, translator, unitOfWork);
        }
    }
}
