﻿using System;

//TODO: add summary...
//TODO: Globals for messages?
//TODO: ShowMessage property!!!!

//https://msdn.microsoft.com/en-us/library/ms229005.aspx

namespace ProjectX.General
{
    #region Custom Exceptions

    /// <summary>
    /// (Custom exception)
    /// <para>...</para>
    /// </summary>
    [Serializable]
    public class LogSuccessException : Exception
    {
        #region Constructors
        public LogSuccessException()
            : base("Logging method execution when the conditions have been met successfully.")
        { }

        public LogSuccessException(string message)
            : base(message)
        { }
        #endregion
    }

    /// <summary>
    /// (Custom exception)
    /// <para>...</para>
    /// </summary>
    [Serializable]
    public class LogNullException : Exception
    {
        #region Constructors
        public LogNullException()
            : base("Logging (no exception occurred).")
        { }

        public LogNullException(string message)
            : base(message)
        { }

        public LogNullException(string message, Exception innerException)
            : base(message, innerException)
        { }
        #endregion
    }

    /// <summary>
    /// (Custom exception)
    /// <para>...</para>
    /// </summary>
    [Serializable]
    public class NullEntityException : Exception
    {
        #region Constructors
        public NullEntityException()
            : base("The requested entity doesn't exist!")
        { }

        public NullEntityException(string message)
            : base(message)
        { }

        public NullEntityException(string message, Exception innerException)
            : base(message, innerException)
        { }
        #endregion
    }

    /// <summary>
    /// (Custom exception)
    /// <para>...</para>
    /// </summary>
    [Serializable]
    public class MissingMethodParameterException : Exception
    {
        //TODO: check this code -> NOT OK YET!
        #region Properties
        public string PropertyName { get; }
        #endregion

        #region Constructors
        public MissingMethodParameterException(string propertyName)
            : base("The requested parameter is missing!")
        { this.PropertyName = propertyName; }

        public MissingMethodParameterException(string message, string propertyName)
            : base(message)
        { this.PropertyName = propertyName; }

        public MissingMethodParameterException(string message, Exception innerException, string propertyName)
            : base(message, innerException)
        { this.PropertyName = propertyName; }
        #endregion
    }

    /// <summary>
    /// (Custom exception)
    /// <para>...</para>
    /// </summary>
    [Serializable]
    public class MissingKeyValueException : Exception
    {
        #region Constructors
        public MissingKeyValueException()
            : base("The entitykey is incomplete!")
        { }

        public MissingKeyValueException(string message)
            : base(message)
        { }

        public MissingKeyValueException(string message, Exception innerException)
            : base(message, innerException)
        { }
        #endregion
    }

    /// <summary>
    /// (Custom exception)
    /// <para>...</para>
    /// </summary>
    [Serializable]
    public class MissingOrderingValueException : Exception
    {
        #region Properties
        public Type Type { get; }
        #endregion

        #region Constructors
        public MissingOrderingValueException()
            : base("The ordering value is not set!")
        { }

        public MissingOrderingValueException(string message, Type type)
            : base(message)
        {
            this.Type = type;
        }
        #endregion
    }

    /// <summary>
    /// (Custom exception)
    /// <para>...</para>
    /// </summary>
    [Serializable]
    public class LogBugException : Exception
    {
        //TODO: handle!
        #region Constructors
        public LogBugException(Exception innerException, string message = "BUG! Bugfix needed!")
            : base(message, innerException)
        { }
        #endregion
    }

    /// <summary>
    /// (Custom exception)
    /// <para>...</para>
    /// </summary>
    [Serializable]
    public class LoadingResourcesException : Exception
    {
        //TODO: handle!
        #region Constructors
        public LoadingResourcesException(Exception innerException, string message = "Unable to load the requested resources.\nPlease contact the administrator!")
            : base(message, innerException)
        { }
        #endregion
    }

    /// <summary>
    /// ...
    /// </summary>
    [Serializable]
    public class ArgumentNullPropertyException : Exception
    {
        #region Fields
        const string _message = AppLibrary.ExceptionMessages.NullPropertyException;
        #endregion

        #region Properties
        //TODO: change to FriendlyMessage?
        public string CustomMessage => _message;
        public MethodCommand Command { get; }
        public Type EntityType { get; }
        public string PropertyName { get; }
        #endregion

        #region Constructors
        public ArgumentNullPropertyException()
            : base(_message)
        { }

        public ArgumentNullPropertyException(Type type, string propertyName)
            : base(StringHelper.Concat(type, _message, propertyName))
        {
            this.PropertyName = propertyName;
        }

        public ArgumentNullPropertyException(MethodCommand command, Type entityType, string propertyName, Exception innerException)
            : base(StringHelper.TryFormat("{0}{1}{2} ({3})", EnumHelper.GetName(command), _message, propertyName, entityType), innerException)
        {
            this.Command = command;
            this.EntityType = entityType;
            this.PropertyName = propertyName;
        }
        #endregion
    }

    /// <summary>
    /// (Custom exception)
    /// <para>...</para>
    /// </summary>
    [Serializable]
    public class StartApplicationException : Exception
    {
        #region Constructors
        public StartApplicationException()
            : base("The application has started successfully.")
        { }

        public StartApplicationException(string message)
            : base(message)
        { }
        #endregion
    }

    /// <summary>
    /// (Custom exception)
    /// <para>...</para>
    /// </summary>
    [Serializable]
    public class CloseApplicationException : Exception
    {
        #region Constructors
        public CloseApplicationException()
            : base("Closing the application on demand of the user.")
        { }

        public CloseApplicationException(string message)
            : base(message)
        { }
        #endregion
    }

    /// <summary>
    /// (Custom exception)
    /// <para>...</para>
    /// </summary>
    [Serializable]
    public class FatalException : Exception
    {
        #region Constructors
        public FatalException(string message, Exception exception)
            : base(message, exception?.InnerException == null ? exception : exception.InnerException)
        { }

        //public FatalException(string message, Exception innerException)
        //    : base(message, innerException)
        //{ }
        #endregion
    }

    /// <summary>
    /// (Custom exception)
    /// <para>...</para>
    /// </summary>
    [Serializable]
    public class FatalSqlException : Exception
    {
        #region Constructors
        public FatalSqlException(string message, Exception innerException)
            : base(message, innerException)
        { }
        #endregion
    }

    /// <summary>
    /// (Custom exception)
    /// <para>...</para>
    /// </summary>
    [Serializable]
    public class RetriesExceededException : Exception
    {
        #region Properties
        public int Attempts { get; }
        #endregion

        #region Constructors
        public RetriesExceededException(int attempts)
            : base(StringHelper.Sentence("The maximum number of retries has been exceeded! Attempts: ", attempts - 1, "."))
        {
            this.Attempts = attempts - 1;
        }
        #endregion
    }
    #endregion
}
