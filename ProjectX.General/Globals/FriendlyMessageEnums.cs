﻿namespace ProjectX.General
{
    #region Words
    public enum KeyWord
    {
        Address,
        Assembly,
        Company,
        Contact,
        Details,
        Exception,
        Form,
        id,
        MethodInfo,
        Person
    }
    #endregion

    #region Enums
    public enum FriendlyTextType
    {
        None,
        Abort,
        Cancel,
        Close,
        ConnectionManagement,
        Delete,
        Edit,
        Maximize,
        Minimize,
        OK,
        Restore,
        Save,
        Search,
        View
    }

    /// <summary>
    /// ...
    /// </summary>
    public enum FriendlyToolTipType
    {
        None,
        Abort,
        Cancel,
        Close,
        CloseApplication,
        Delete,
        Edit,
        Maximize,
        Minimize,
        OK,
        Restore,
        Save,
        Search,
        View
    }

    public enum CustomCultureInfo
    {
        CustomNumberDecimalSeparator,
        CustomNumberGroupSeparator,
        CustomCurrencyDecimalSeparator,
        CustomCurrencyGroupSeparator,
        CustomDateSeparator
    }

    public enum FriendlyFormatType
    {
        AddressBoxNumberFormat = 77000,
        TranslationFormat = 77001,
        BracketsFormat = 77002
    }

    /// <summary>
    /// ...
    /// </summary>
    public enum FriendlyProgressType
    {
        ConnectingToDatabaseTask = 88000,
        InitializingTask = 88001,
        OpeningTask = 88002,
        LoadingModuleTask = 88003,
        LoadingResourceTask = 88004,
        SearchingTask = 88005,
        LoadingTask = 88006,
        CompletedTask = 88007,
        InitializingModuleTask = 88008,
        StartUpTask = 88009
    }

    /// <summary>
    /// ...
    /// </summary>
    public enum FriendlyMessageType
    {
        CloseApplicationAlert = 99000,
        CloseApplicationQuestion = 99001,
        CloseCurrentFormQuestion = 99002,
        ContactAdministratorAdvice = 99003,
        ContactNetworkAdministratorAdvice = 99004,
        DatabaseConnectionAlert = 99005,
        DatabaseConnectionRestoreAdvice = 99006,
        DatabaseUnavailableAlert = 99007,
        DataGridViewWithNoSelectionAdvice = 99008,
        EntityNotFoundAlert = 99009,
        EntityNotFoundCause = 99010,
        FatalErrorAlert = 99011,
        FormAlreadyOpenedAlert = 99012,
        GeneralAlert = 99013,
        ItemNotAvailableAlert = 99014,
        MissingKeyValueAlert = 99015,
        MissingKeyValueCause = 99016,
        PropertyNotFoundAlert = 99017,
        RetryAdvice = 99018,
        RetryAlert = 99019,
        RetryReloadAdvice = 99020,
        RetryReloadQuestion = 99021,
        SettingsAlert = 99022,
        SortingFailedAdvice = 99023,
        SortingFailedAlert = 99024,
        UnknownErrorAlert = 99025,
        MissingKeyValueInfo = 99026,
        DatabaseUnavailableInfo = 99027,
        DatabaseLoginFailedInfo = 99028,
        EntityNotFoundInfo = 99029,
        UnhandledBugInfo = 99030,
        PropertyNotFoundInfo = 99031,
        UnhandledErrorAlert = 99032,
        SelectItemAdvice = 99033,
        GenericMessage = 99034,
        PropertyNotSupportedAlert = 99035,
        MissingMethodParameterAlert = 99036,
        LoadDataAdvice = 99037,
        TimeOutAlert = 99038,
        PatiencePleaseInfo = 99039,
        RetryQuestion = 99040,
        RetriesExceededInfo = 99041,
        TooManyResults = 99042
    }
    #endregion
}
