﻿using System.ComponentModel;

//TODO: safely use enums https://stackoverflow.com/questions/39661087/enum-not-default-to-0-when-not-defined
//TODO: add summary...

namespace ProjectX.General
{
    #region Struct
    public struct DisplayFormatSetting
    {
        #region Properties
        public string DisplayFormat { get; }
        public string TrimDigits { get; }
        public bool LogResolvedException { get; }
        #endregion

        #region Constructor
        public DisplayFormatSetting(string displayFormat)
            : this(displayFormat, null, true)
        { }

        public DisplayFormatSetting(string displayFormat, string trimDigits, bool logResolvedException)
        {
            this.DisplayFormat = displayFormat;
            this.TrimDigits = trimDigits;
            this.LogResolvedException = logResolvedException;
        }
        #endregion
    }

    public struct ConnectionDetail
    {
        public string Name { get; set; }
        public string ProviderName { get; set; }
        public string ConnectionString { get; set; }
    }
    #endregion

    #region Enums
    public enum ExceptionStrategy
    {
        Default,
        Exit,
        Data
    }

    public enum NumberConstant
    {
        None = 0,
        RetryLimit = 3,
        MediumRetryLimit = 5,
        MaxRetryLimit = 10
    }

    /// <summary>
    /// ...
    /// </summary>
    public enum MethodCommand
    {
        Contains,
        OrderBy,
        OrderByDescending
    }

    public enum SearchStringCommand
    {
        Default,
        ReturnEmptyList
    }

    /// <summary>
    /// ...
    /// </summary>
    public enum OrderingDirection
    {
        Ascending = 0,
        Descending = 1
    }

    /// <summary>
    /// ...
    /// </summary>
    public enum Alignment
    {
        //
        // Summary:
        //     The object or text is aligned on the left of the control element.
        Left = 0,
        //
        // Summary:
        //     The object or text is aligned on the right of the control element.
        Right = 1,
        //
        // Summary:
        //     The object or text is aligned in the center of the control element.
        Center = 2
    }

    public enum SupportedExtension
    {
        resxml = 1
    }

    /// <summary>
    /// ...
    /// </summary>
    [DefaultValue(SupportedLanguage.NL)]
    public enum SupportedLanguage
    {
        //REMINDER: add SupportedLanguages accordingly...
        NL = 1,
        FR = 2,
        EN = 3,
        DE = 4
    }

    public enum CustomResource
    {
        Languages = 1,
        Preferences = 2
    }

    /// <summary>
    /// ...
    /// </summary>
    public enum ModuleBloc
    {
        //REMINDER: add Modules accordingly...
        None = 0,
        General = 1,
        Production = 2,
        Partner = 3,
        Purchase = 4,
        Sales = 5,
        Stock = 6,
        Accountancy = 7,
        Management = 8
    }

    /// <summary>
    /// ...
    /// </summary>
    public enum MenuBloc
    {
        //REMINDER: add Menu Members accordingly...
        None = 0,
        Product = 1,
        Customer = 2,
        Supplier = 3,
        Offer = 4,
        Order = 5,
        Invoice = 6
    }

    /// <summary>
    /// ...
    /// </summary>
    public enum MenuGroup
    {
        //REMINDER: add Activity Groups accordingly...
        Unknown = 0,
        Action = 1
    }

    /// <summary>
    /// ...
    /// </summary>
    public enum MenuGroupAction
    {
        //REMINDER: add Activities accordingly...
        Unknown = 0,
        Search = 1,
        New = 2,
        Edit = 3,
    }

    /// <summary>
    /// ...
    /// </summary>
    public enum PartnerRole
    {
        //REMINDER: add PartnerRoles accordingly...
        Owner = 1,
        Customer = 2,
        Supplier = 3,
        Contact = 4,
        DeliveryAddress = 5,
        ServiceAddress = 6,
        InvoiceAddress = 7,
        MailingAddress = 8,
        Employee = 9
    }

    /// <summary>
    /// Type of work to be done.
    /// </summary>
    public enum Work
    {
        None = 0,
        Search = 1,
        View = 2,
        Batch = 3,
        Cancel = 4,
        Confirm = 5,
        Delete = 6,
        Edit = 7,
        Save = 8,
        Close = 9,
        Minimize = 10,
        Maximize = 11,
        Abort = 12
    }

    public enum Event
    {
        Click,
        Closed,
        DoubleClick,
        Load,
        MouseEnter,
        MouseHover,
        MouseLeave,
        SelectionChangeCommitted
    }

    public enum CancelEvent
    {
        Closing
    }

    public enum MouseEvent
    {
        MouseDoubleClick,
        MouseDown,
        MouseMove,
        MouseUp
    }

    public enum ListViewEvent
    {
        ItemSelectionChanged
    }
    #endregion  
}
