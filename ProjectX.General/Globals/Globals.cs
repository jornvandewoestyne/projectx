﻿using System.Collections.Generic;

namespace ProjectX.General
{
    public static class AppLibrary
    {
        public static ITranslator Translator { get; set; }

        #region Translations
        //TODO:
        private static Dictionary<string, string> _databaseTranslations;
        public static Dictionary<string, string> DatabaseTranslations
        {
            get
            {
                if (_databaseTranslations == null)
                {
                    //TODO: throw CustomException!
                    //throw new Exception("test");
                    _databaseTranslations = new Dictionary<string, string>();
                    _databaseTranslations.Add("Belgium", "België");
                    _databaseTranslations.Add("United Arab Emirates", "Verenigde Arabische Emiraten");
                    _databaseTranslations.Add("Antigua and Barbuda", "Antigua en Barbuda");
                    _databaseTranslations.Add("Albania", "Albanië");
                    _databaseTranslations.Add("Armania", "Armenië");
                    _databaseTranslations.Add("Argentina", "Argentinië");
                    _databaseTranslations.Add("American Samoa", "Amerikaans-Samoa");
                    _databaseTranslations.Add("Austria", "Ooestenrijk");
                    _databaseTranslations.Add("Australia", "Australië");
                    _databaseTranslations.Add("Åland Islands", "Ålandeilanden");
                    _databaseTranslations.Add("Azerbaijan", "Azerbeidzjan");
                    _databaseTranslations.Add("Bosnia and Herzegovina", "Bosnië en Herzegovina");
                    _databaseTranslations.Add("Bulgaria", "Bulgarije");
                    _databaseTranslations.Add("Bahrain", "Bahrein");
                    _databaseTranslations.Add("Bolivia", "Bolivië");
                    _databaseTranslations.Add("Bonaire, Saint Eustatius and Saba", "Bonaire, Sint-Eustatius en Saba");
                    _databaseTranslations.Add("Brazil", "Brazilië");
                    _databaseTranslations.Add("Dominican Republic", "Dominicaanse Republiek");
                    _databaseTranslations.Add("South Georgia and the South Sandwich Islands", "Zuid-Georgië en de Zuid-Sandwicheilanden");
                    _databaseTranslations.Add("United States", "Verenigde Staten");
                }
                return _databaseTranslations;
            }
        }

        private static Dictionary<string, string> _translations;
        public static Dictionary<string, string> Translations
        {
            get
            {
                //try/catch in TryTranslate() slows down application!!! -> Notify developer in other ways!!!
                //if (_translations == null)
                //{
                //    //TODO: throw CustomException!
                //    throw new Exception("test");
                //}
                return _translations;
            }
            set
            {
                var test = new Dictionary<string, string>();
                test = value;
                _databaseTranslations = new Dictionary<string, string>();
                if (test != null)
                {
                    //test.Add("Belgium", "België");
                    test.Add("United Arab Emirates", "Verenigde Arabische Emiraten");
                    test.Add("Antigua and Barbuda", "Antigua en Barbuda");
                    test.Add("Albania", "Albanië");
                    test.Add("Armania", "Armenië");
                    test.Add("Argentina", "Argentinië");
                    test.Add("American Samoa", "Amerikaans-Samoa");
                    test.Add("Austria", "Oostenrijk");
                    test.Add("Australia", "Australië");
                    test.Add("Åland Islands", "Ålandeilanden");
                    test.Add("Azerbaijan", "Azerbeidzjan");
                    test.Add("Bosnia and Herzegovina", "Bosnië en Herzegovina");
                    test.Add("Bulgaria", "Bulgarije");
                    test.Add("Bahrain", "Bahrein");
                    test.Add("Bolivia", "Bolivië");
                    test.Add("Bonaire, Saint Eustatius and Saba", "Bonaire, Sint-Eustatius en Saba");
                    test.Add("Brazil", "Brazilië");
                    test.Add("Dominican Republic", "Dominicaanse Republiek");
                    test.Add("South Georgia and the South Sandwich Islands", "Zuid-Georgië en de Zuid-Sandwicheilanden");
                    test.Add("United States", "Verenigde Staten");
                    test.Add("Italy", "Italië");
                    //test.Add("France", "Frankrijk");
                    _translations = test;
                }
            }
        }
        #endregion

        #region Constants -> MOVED
        //public static class Constants
        //{
        //    #region Constants
        //    private const string _ANONYMOUSSELECTOR = "new ({0})";
        //    private const string _JOINPLUSOPERATOR = " + ";
        //    #endregion

        //    #region Properties
        //    public static string AnonymousSelector { get { return _ANONYMOUSSELECTOR; } }
        //    public static string JoinPlusOperator { get { return _JOINPLUSOPERATOR; } }
        //    #endregion
        //}
        #endregion

        #region Attributes -> MOVED
        //public static class Attributes
        //{
        //    //REMINDER: Add Attributes when available...
        //    #region Constants
        //    public const string DefaultGroupName = "Details";
        //    public const string EmptyName = "";

        //    public const string AddressName = "Address";
        //    public const string AssemblyName = "Assembly";
        //    public const string CompanyName = "Company";
        //    public const string ContactName = "Contact";
        //    public const string ExceptionName = "Exception";
        //    public const string MethodInfoName = "MethodInfo";
        //    public const string PersonName = "Person";
        //    #endregion
        //}
        #endregion

        #region Preferences -> MOVED
        //public static class Preferences
        //{
        //    #region Fields
        //    private static string _currentLanguage = "NL"; //TODO: set default to ""
        //    private static bool _translateDatabaseValues = true;
        //    private static bool _splitBySpace = true;
        //    private const bool _showChildHeaderDefault = true;
        //    private const bool _confirmToCloseDefault = true;
        //    private static bool _confirmToCloseOnEditForm = true;
        //    private static bool _confirmToCloseOnSearchForm = false;
        //    private static bool _logResolvedExceptions = false;
        //    private static bool _tryResolveExceptions = true; //TODO: test when false!!
        //    #endregion

        //    #region Properties
        //    public static bool TranslateDatabaseValues { get { return _translateDatabaseValues; } }
        //    public static bool ShowChildHeaderDefault { get { return _showChildHeaderDefault; } }
        //    public static bool ConfirmToCloseDefault { get { return _confirmToCloseDefault; } }
        //    public static bool TryResolveExceptions { get { return _tryResolveExceptions; } }
        //    public static bool LogResolvedExceptions { get { return _logResolvedExceptions; } }

        //    public static string CurrentLanguage
        //    {
        //        get { return _currentLanguage; }
        //        set { _currentLanguage = value; }
        //    }

        //    public static bool ConfirmToCloseOnEditForm 
        //    { 
        //        get { return _confirmToCloseOnEditForm; }
        //        set { _confirmToCloseOnEditForm = value; }
        //    }

        //    public static bool ConfirmToCloseOnSearchForm 
        //    { 
        //        get { return _confirmToCloseOnSearchForm; }
        //        set { _confirmToCloseOnSearchForm = value; }
        //    }

        //    public static bool SplitBySpace
        //    {
        //        get { return _splitBySpace; }
        //        set { _splitBySpace = value; }
        //    }
        //    #endregion
        //}
        #endregion

        #region Keys -> MOVED
        //public static class Keys
        //{
        //    #region Fields
        //    private static object _notExcistingItemValue = null;
        //    #endregion

        //    #region Properties
        //    public static object NotExcistingItemValue { get { return _notExcistingItemValue; } }

        //    public static object[] NotExcistingItem { get { return _notExcistingItemValue.TryConvertToArray(); } }
        //    #endregion
        //}
        #endregion

        #region Limits -> MOVED
        //public static class Limits
        //{
        //    #region Fields
        //    private static int _maxItemsPerPage = 250;
        //    private static int _maxForOptimization = 2100; //TODO: test to get the most optimal value
        //    private static int _maxItemsOnSearch = 1000000;
        //    private static int _maxItemsToBeReturned = 0;
        //    private static int _minLengthKeyWordForOptimization = 3;
        //    #endregion

        //    #region Properties
        //    public static int MaxItemsPerPage { get { return _maxItemsPerPage; } }
        //    public static int MaxForOptimization { get { return _maxForOptimization; } }
        //    public static int MaxItemsOnSearch { get { return _maxItemsOnSearch; } }
        //    public static int MaxItemsToBeReturned { get { return _maxItemsToBeReturned; } }
        //    public static int MinLengthKeyWordForOptimization { get { return _minLengthKeyWordForOptimization; } }
        //    #endregion
        //}
        #endregion

        #region Strings -> MOVED
        //public static class Strings
        //{
        //    #region Constants
        //    internal const string _DEFAULTSEPARATOR = ",";
        //    #endregion

        //    #region Fields
        //    private static string _applicationTitle = "Furnitures";
        //    private static string _winFormSuffix = "Form";
        //    private static string _applicationVersion = "";
        //    private static string _defaultInfo = "...";
        //    private static string _info = "Info";
        //    private static string _confirm = "Confirm";
        //    private static string _notSpecified = "{N/A}";
        //    private static string _error = "{ERROR!}";
        //    private static string _horizontalEnumSuffix = "=";
        //    private static string _horizontalEnumSeparator = ", ";
        //    private static string _verticalEnumSuffix = ": ";
        //    private static string _verticalEnumSeparator = Environment.NewLine;
        //    private static string _titleSeparator = " - ";
        //    private static string _arrowSeparator = " -> ";
        //    private static string _noDataFound = "NoDataFound";
        //    private static string _boxNumber = "BoxNumber";
        //    #endregion

        //    #region Properties
        //    public static string Dot { get { return XLib.Token.Dot.ToString(); } }
        //    public static string Space { get { return XLib.Token.Space.ToString(); } }
        //    public static string DefaultSeparator { get { return _DEFAULTSEPARATOR; } }

        //    [Obsolete]
        //    public static string ApplicationTitle 
        //    { 
        //        get {return _applicationTitle; }
        //        set { _applicationTitle = value; }
        //    }

        //    public static string WinFormSuffix
        //    {
        //        get { return _winFormSuffix; }
        //    }

        //    [Obsolete]
        //    public static string ApplicationVersion
        //    {
        //        get { return _applicationVersion; }
        //        set { _applicationVersion = value; }
        //    }

        //    [Obsolete]
        //    public static string ApplicationFullName
        //    {
        //        get { return StringHelper.Sentence(ApplicationTitle, ApplicationVersion); }
        //    }

        //    public static string BoxNumber { get { return new Translation(_boxNumber).ToString(); } }
        //    public static string DefaultInfo { get { return new Translation(_defaultInfo).ToString(); } }
        //    public static string Info { get { return new Translation(_info).ToString(); } }
        //    public static string Confirm { get { return new Translation(_confirm).ToString(); } }
        //    public static string NotSpecified { get { return _notSpecified; } }
        //    public static string NotSpecifiedText { get { return new Translation(_notSpecified).ToString(); } }
        //    public static string NoDataFound { get { return _noDataFound; } }
        //    public static string NoDataFoundText { get { return new Translation(_noDataFound).ToString(); } }
        //    public static string Error { get { return new Translation(_error).ToString(); } }

        //    public static string HorizontalEnumSuffix { get { return _horizontalEnumSuffix; } }
        //    public static string HorizontalEnumSeparator { get { return _horizontalEnumSeparator; } }
        //    public static string VerticalEnumSuffix { get { return _verticalEnumSuffix; } }
        //    public static string VerticalEnumSeparator { get { return _verticalEnumSeparator; } }
        //    public static string TitleSeparator { get { return _titleSeparator; } }
        //    public static string ArrowSeparator { get { return _arrowSeparator; } }
        //    #endregion
        //}
        #endregion

        #region Regex -> MOVED
        //public static class Regex
        //{
        //    #region Fields
        //    private const string _NUMBERPATTERN = @"\d+";
        //    private const string _PLACEHOLDERPATTERN = @"(?<!\{)(?>\{\{)*\{\d(.*?)";
        //    #endregion

        //    #region Properties
        //    public static string NumberPattern { get { return _NUMBERPATTERN; } }
        //    public static string PlaceHolderPattern { get { return _PLACEHOLDERPATTERN; } }
        //    #endregion
        //}
        #endregion

        #region Chars -> MOVED
        //public static class Chars
        //{
        //    #region Fields
        //    private const char _DOT = '.';
        //    private const char _DELIMETER = ',';
        //    private const char _SPACE = ' ';
        //    private const char _AND = ',';
        //    private const char _OR = '+';
        //    private const char _NOT = '!';
        //    private const char _OPTIONALSTART = '[';
        //    private const char _OPTIONALEND = ']';
        //    private static char[] _delimeters = { _AND, _OR, _NOT };
        //    private static char[] _digits = { _AND, _OR, _NOT, _SPACE };
        //    private static char[] _digitsWithoutNot = { _AND, _OR, _SPACE };
        //    private static char[] _optionalIndicators = { _OPTIONALSTART, _OPTIONALEND };
        //    #endregion

        //    #region Properties
        //    public static char Dot { get { return _DOT; } }
        //    public static char Delimeter { get { return _DELIMETER; } }
        //    public static char Space { get { return _SPACE; } }
        //    public static char And { get { return _AND; } }
        //    public static char Or { get { return _OR; } }
        //    public static char Not { get { return _NOT; } }
        //    public static char OptionalStart { get { return _OPTIONALSTART; } }
        //    public static char OptionalEnd { get { return _OPTIONALEND; } }
        //    public static char[] Delimeters { get { return _delimeters; } }
        //    public static char[] Digits { get { return _digits; } }
        //    public static char[] DigitsWithoutNot { get { return _digitsWithoutNot; } }
        //    public static char[] OptionalIndicators { get { return _optionalIndicators; } }
        //    #endregion
        //}
        #endregion

        #region ExceptionMessages
        //TODO: ExceptionMessages...
        public static class ExceptionMessages
        {
            #region Fields
            public const string NullEntityException = ": The requested entity doesn't exist! --> parameters: ";
            public const string NullPropertyException = ": The requested property is not found! --> member: ";
            public const string MissingKeyValueException = "The entitykey is incomplete! --> parameters (requested/missing): ";
            #endregion
        }
        #endregion

        #region Orientation -> MOVED
        //public static class Orientation
        //{
        //    #region Properties
        //    public static OrientationFormat HorizontalFormat
        //    {
        //        get { return new OrientationFormat(XLib.Text.HorizontalEnumSuffix, XLib.Text.HorizontalEnumSeparator); }
        //    }

        //    public static OrientationFormat VerticalFormat
        //    {
        //        get { return new OrientationFormat(XLib.Text.VerticalEnumSuffix, XLib.Text.VerticalEnumSeparator); }
        //    }
        //    #endregion
        //}
        #endregion

        #region Colors -> MOVED
        //public static class Colors
        //{
        //    #region Fields
        //    private static Color _defaultColor = Color.White;
        //    private static Color _activeColor = Color.Lavender;
        //    private static Color _defaultSystemColor = SystemColors.Control;

        //    #endregion

        //    #region Properties
        //    public static Color DefaultColor
        //    {
        //        get { return _defaultColor; }
        //        set { _defaultColor = value; }
        //    }

        //    public static Color ActiveColor
        //    {
        //        get { return _activeColor; }
        //        set { _activeColor = value; }
        //    }

        //    public static Color DefaultSystemColor
        //    {
        //        get { return _defaultSystemColor; }
        //        set { _defaultSystemColor = value; }
        //    }
        //    #endregion
        //}
        #endregion
    }
}
