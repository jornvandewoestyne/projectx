﻿namespace ProjectX.General
{
    #region Enums
    public enum LogLevel
    {
        Bug = 0,
        All = 1,
        Debug = 2,
        Info = 3,
        Warning = 4,
        Error = 5,
        Fatal = 6,
        Question = 32,
        Off = 500,
        Unknown = 1000,
        OnEntry = 2000,
        OnSuccess = 3000,
        OnExit = 4000
    };
    #endregion
}
