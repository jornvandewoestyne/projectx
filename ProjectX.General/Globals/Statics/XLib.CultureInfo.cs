﻿using ProjectX.General;
using System.Threading;

//TODO: summary

namespace XLib
{
    /// <summary>
    /// ...
    /// </summary>
    public static class CultureInfo
    {
        #region Properties
        public static string NumberDecimalSeparator => GetValue(CustomCultureInfo.CustomNumberDecimalSeparator);
        public static string NumberGroupSeparator => GetValue(CustomCultureInfo.CustomNumberGroupSeparator);
        public static string CurrencyDecimalSeparator => GetValue(CustomCultureInfo.CustomCurrencyDecimalSeparator);
        public static string CurrencyGroupSeparator => GetValue(CustomCultureInfo.CustomCurrencyGroupSeparator);
        public static string DateSeparator => GetValue(CustomCultureInfo.CustomDateSeparator);
        #endregion

        #region Methods
        private static string GetValue(CustomCultureInfo key)
        {
            var result = StringHelper.Translation(key, XLib.Preference.CurrentLanguage);
            if (result.Equals(key.ToString()))
            {
                var culture = Thread.CurrentThread.CurrentCulture;
                switch (key)
                {
                    case CustomCultureInfo.CustomNumberDecimalSeparator:
                        return culture.NumberFormat.NumberDecimalSeparator;
                    case CustomCultureInfo.CustomNumberGroupSeparator:
                        return culture.NumberFormat.NumberGroupSeparator;
                    case CustomCultureInfo.CustomCurrencyDecimalSeparator:
                        return culture.NumberFormat.CurrencyDecimalSeparator;
                    case CustomCultureInfo.CustomCurrencyGroupSeparator:
                        return culture.NumberFormat.CurrencyGroupSeparator;
                    case CustomCultureInfo.CustomDateSeparator:
                        return culture.DateTimeFormat.DateSeparator;
                    default:
                        return string.Empty;
                }
            }
            return result;
        }
        #endregion
    }
}
