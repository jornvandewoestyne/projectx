﻿//TODO: summary

namespace XLib
{
    /// <summary>
    /// ...
    /// </summary>
    public static class Limit
    {
        #region Properties
        public static int MaxItemsPerPage { get; } = 250;
        public static int MaxForOptimization { get; } = 2100;
        public static int MaxItemsOnSearch { get; } = 1000000;
        public static int MaxItemsToBeReturned { get; } = 10000;
        public static int MinLengthKeyWordForOptimization { get; } = 3;
        public static int RetryLimit { get; } = 5;
        #endregion
    }
}
