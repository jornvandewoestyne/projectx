﻿//TODO: summary

namespace XLib
{
    /// <summary>
    /// ...
    /// </summary>
    public static class Token
    {
        #region Fields
        private const char _SEPARATOR = '|';
        private const char _DOT = '.';
        private const char _DELIMETER = ',';
        private const char _SPACE = ' ';
        private const char _AND = ',';
        private const char _OR = '+';
        private const char _NOT = '!';
        private const char _OPTIONALSTART = '[';
        private const char _OPTIONALEND = ']';
        private readonly static char[] _delimeters = { _AND, _OR, _NOT };
        private readonly static char[] _digits = { _AND, _OR, _NOT, _SPACE };
        private readonly static char[] _digitsWithoutNot = { _AND, _OR, _SPACE };
        private readonly static char[] _optionalIndicators = { _OPTIONALSTART, _OPTIONALEND };
        #endregion

        #region Properties
        public static char Separator => _SEPARATOR;
        public static char Dot => _DOT;
        public static char Delimeter => _DELIMETER;
        public static char Space => _SPACE;
        public static char And => _AND;
        public static char Or => _OR;
        public static char Not => _NOT;
        public static char OptionalStart => _OPTIONALSTART;
        public static char OptionalEnd => _OPTIONALEND;
        public static char[] Delimeters => _delimeters;
        public static char[] Digits => _digits;
        public static char[] DigitsWithoutNot => _digitsWithoutNot;
        public static char[] OptionalIndicators => _optionalIndicators;
        #endregion
    }
}
