﻿//TODO: summary

namespace XLib
{
    /// <summary>
    /// ...
    /// </summary>
    public static class Constant
    {
        //REMINDER: Add Constants when available...
        #region Annotation Constants
        //public const string _DEFAULTGROUPNAME = "Details";
        public const string EmptyName = "";
        //public const string _ADDRESSNAME = "Address";
        //public const string _ASSEMBLYNAME = "Assembly";
        //public const string _COMPANYNAME = "Company";
        //public const string _CONTACTNAME = "Contact";
        //public const string _EXCEPTIONNAME = "Exception";
        //public const string _METHODINFONAME = "MethodInfo";
        //public const string _PERSONNAME = "Person";
        #endregion

        #region Constants
        public const string DefaultSeparator = ",";
        public const string AnonymousSelector = "new ({0})";
        public const string JoinPlusOperator = " + ";
        #endregion
    }
}
