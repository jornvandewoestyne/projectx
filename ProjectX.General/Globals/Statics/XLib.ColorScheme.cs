﻿using ProjectX.General;
using System.Drawing;

namespace XLib
{
    internal static class ColorScheme
    {
        #region Properties
        public static ColorPreference DefaultAccent => new ColorPreference(Color.AliceBlue, Color.LightSteelBlue);
        public static ColorPreference AlertAccent => new ColorPreference(Color.AntiqueWhite, Color.Red);
        #endregion
    }
}
