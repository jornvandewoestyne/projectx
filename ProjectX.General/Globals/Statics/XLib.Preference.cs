﻿
using ProjectX.General;

//TODO: summary

namespace XLib
{
    /// <summary>
    /// ...
    /// </summary>
    public static class Preference
    {
        #region Constants
        private const bool _SHOWCHILDHEADERDEFAULT = true;
        private const bool _CONFIRMTOCLOSEDEFAULT = true;
        public const SupportedLanguage DefaultLanguage = SupportedLanguage.EN;
        public const bool TranslateArguments = false;
        public const bool TranslateEntityArguments = false;
        public const int DefaultTaskTimeOut = 3000;
        public const int DefaultSplasherDelay = 50;
        public const int DefaultDelay = 100;
        public const int DefaultShowClosingDuration = 1000;
        public const int DefaultShowInfoDuration = 2000;
        public const int DefaultShowWarningDuration = 5000;
        public const int DefaultShowExceptionDuration = 10000;
        #endregion

        #region Properties
        public static SupportedLanguage CurrentLanguage { get; set; } = SupportedLanguage.NL;
        public static bool TranslateDatabaseValues { get; } = true;
        public static bool ShowChildHeaderDefault => _SHOWCHILDHEADERDEFAULT;
        public static bool ConfirmToCloseDefault => _CONFIRMTOCLOSEDEFAULT;
        public static bool TryResolveExceptions { get; } = true;
        public static bool LogResolvedExceptions { get; } = false;
        public static bool ConfirmToCloseOnEditForm { get; set; } = true;
        public static bool ConfirmToCloseOnSearchForm { get; set; } = false;
        public static bool ShowNoSelection { get; set; } = true;
        public static bool ShowInstanceIsOpen { get; set; } = false;
        public static bool SplitBySpace { get; set; } = true;
        public static bool CleanInput { get; set; } = true;
        public static bool SelectAllText { get; set; } = true;
        public static bool UseDisplayFormat { get; set; } = true;
        #endregion
    }
}
