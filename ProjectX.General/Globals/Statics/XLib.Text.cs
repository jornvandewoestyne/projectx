﻿using ProjectX.General;
using System;

//TODO: summary

namespace XLib
{
    /// <summary>
    /// ...
    /// </summary>
    public static class Text
    {
        #region Properties
        public const string Separator = " | ";
        public const string JoinSeparator = " - ";
        public static string Dot => XLib.Token.Dot.ToString();
        public static string Space => XLib.Token.Space.ToString();
        public static string DefaultSeparator => XLib.Constant.DefaultSeparator;

        [Obsolete]
        public static string BoxNumber => StringHelper.Translation(nameof(BoxNumber));

        public static string Seconds => StringHelper.Translation(nameof(Seconds)).ToLower();
        public static string DefaultInfo => StringHelper.Translation("...");
        public static string Info => StringHelper.Translation(nameof(Info));
        public static string Bug => StringHelper.Translation(nameof(Bug));
        public static string Error => StringHelper.Translation(nameof(Error));
        public static string FatalError => StringHelper.Translation(nameof(FatalError));
        public static string Confirm => StringHelper.Translation(nameof(Confirm));
        public static string NotSpecified { get; } = "{N/A}";
        public static string NotSpecifiedText => StringHelper.Translation(NotSpecified);
        public static string NoDataFound { get; } = nameof(NoDataFound);
        public static string NoDataFoundText => StringHelper.Translation(NoDataFound);
        public static string LoadDataAdvice => nameof(LoadDataAdvice);
        public static string LoadDataAdviceText => StringHelper.Translation(LoadDataAdvice);
        public static string ArgumentError => StringHelper.Translation("{ERROR!}");
        public static string HorizontalEnumSuffix { get; } = "=";
        public static string HorizontalEnumSeparator { get; } = ", ";
        public static string VerticalEnumSuffix { get; } = ": ";
        public static string VerticalEnumSeparator { get; } = Environment.NewLine;
        public static string TitleSeparator { get; } = " - ";
        public static string ArrowSeparator { get; } = " -> ";
        public static string AnonymousSelector => XLib.Constant.AnonymousSelector;
        public static string JoinPlusOperator => XLib.Constant.JoinPlusOperator;
        #endregion
    }
}
