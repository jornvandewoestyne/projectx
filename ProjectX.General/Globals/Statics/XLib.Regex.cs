﻿//TODO: summary

namespace XLib
{
    /// <summary>
    /// ...
    /// </summary>
    public static class Regex
    {
        #region Fields
        private const string _NUMBERPATTERN = @"\d+";
        private const string _PLACEHOLDERPATTERN = @"(?<!\{)(?>\{\{)*\{\d(.*?)";
        #endregion

        #region Properties
        public static string NumberPattern => _NUMBERPATTERN;
        public static string PlaceHolderPattern => _PLACEHOLDERPATTERN;
        #endregion
    }
}
