﻿using ProjectX.General;

//TODO: summary

namespace XLib
{
    /// <summary>
    /// ...
    /// </summary>
    public static class Key
    {
        #region Fields
        private const object _NOTEXCISTINGITEMVALUE = null;
        #endregion

        #region Properties
        public static object NotExcistingItemValue => _NOTEXCISTINGITEMVALUE;
        public static object[] NotExcistingItem => NotExcistingItemValue.TryConvertToArray();
        public static EntityKey NotExcistingEntityKey => NotExcistingItem.AsEntityKey();
        #endregion
    }
}
