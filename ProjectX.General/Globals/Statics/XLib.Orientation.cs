﻿using ProjectX.General;

//TODO: summary

namespace XLib
{
    /// <summary>
    /// ...
    /// </summary>
    public static class Orientation
    {
        #region Properties
        public static OrientationFormat HorizontalFormat
            => new OrientationFormat(XLib.Text.HorizontalEnumSuffix, XLib.Text.HorizontalEnumSeparator);

        public static OrientationFormat VerticalFormat
            => new OrientationFormat(XLib.Text.VerticalEnumSuffix, XLib.Text.VerticalEnumSeparator);
        #endregion
    }
}
