﻿namespace XLib
{
    public struct MagicNumber
    {
        #region Constants
        public const int IntZero = 0;
        public const int IntOne = 1;
        public const int IntTwo = 2;
        public const double DoubleZero = 0.0d;

        public const int Index0 = IntZero;
        public const int Index1 = IntOne;
        public const int Index2 = IntTwo;
        #endregion
    }
}
