﻿using System;

//TODO: summary...

namespace ProjectX.General.Attributes
{
    /// <summary>
    /// ...
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class DefaultOrderingPropertyAttribute : Attribute
    {
        //TODO: Ascending or Descending

        public int Order { get; set; }

        public DefaultOrderingPropertyAttribute()
        { }

        public int? GetOrder()
        {
            return this.Order;
        }
    }
}
