﻿using System;

//TODO: summary...

//TODO: make class, which can be used not only as attribute!

namespace ProjectX.General.Attributes
{
    /// <summary>
    /// ...
    /// </summary>
    public enum DisplayPreference
    {
        Short = 0,
        Medium = 1,
        Long = 2,
        Memo = 3,
        Boolean = 4,
        Date = 5,
        Time = 6,
        DateAndTime = 7,
        Integer = 8,
        Code = 9,
        ExtraLong = 10
    }

    /// <summary>
    /// ...
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class DisplayPreferenceAttribute : Attribute
    {
        #region Properties
        public PropertyPreference Preference { get; }
        //public int Width { get; } = 100;
        //public int MaximumWidth { get; } = 250;
        //public int MinimumWidth { get; } = 50;
        #endregion

        #region Constructors
        //public DisplayPreferenceAttribute(int width, int maximumWidth, int minimumWidth, string format, Alignment alignment)
        //{
        //    this.Preference = new PropertyPreference(width, maximumWidth, minimumWidth, format, alignment);
        //}

        public DisplayPreferenceAttribute(DisplayPreference preference)
        {
            this.Preference = new PropertyPreference(preference);
            //switch (preference)
            //{
            //    case DisplayPreference.Short:
            //        break;
            //    case DisplayPreference.Medium:
            //        break;
            //    case DisplayPreference.Long:
            //        this.Width = 200;
            //        this.MaximumWidth = 400;
            //        this.MinimumWidth = 100;
            //        break;
            //    case DisplayPreference.Memo:
            //        break;
            //    default:
            //        break;
            //}
        }
        #endregion
    }
}
