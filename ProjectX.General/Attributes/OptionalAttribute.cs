﻿using System;

//TODO: summary...

namespace ProjectX.General.Attributes
{
    /// <summary>
    /// ...
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class OptionalAttribute : Attribute
    {
        public OptionalAttribute()
        { }
    }
}
