﻿using ProjectX.General.Attributes;
using ProjectX.General.LogService;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;

//TODO: summary
//TODO: check which methods should be public, internal, ...!!!!

namespace ProjectX.General
{
    //TODO: move out GroupValue???
    public sealed class GroupValue
    {
        #region Properties
        public String Group { get; set; }
        public String Title { get; set; }
        public String Data { get; set; }

        public String Warning { get; set; }
        #endregion
    }

    public static class ReflectionExtensions
    {
        #region Methods: Properties
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="property"></param>
        /// <returns></returns>
        public static bool TryGetProperty<T>(this string name, out PropertyInfo property)
             where T : class
        {
            var type = typeof(T);
            return type.TryGetProperty(name, out property);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <param name="property"></param>
        /// <returns></returns>
        public static bool TryGetProperty(this Type type, string name, out PropertyInfo property)
        {
            property = null;
            var result = false;
            if (type != null && !(name.TryTrim()).IsEmptyString())
            {
                property = type.GetProperty(name);
                if (property != null)
                {
                    result = true;
                }
            }
            return result;
        }

        ///// <summary>
        ///// Get an attribute for a type
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="type"></param>
        ///// <returns></returns>
        //public static T GetAttribute<T>(this Type type)
        //    where T : Attribute
        //{
        //    var attributes = type.GetCustomAttributes(typeof(T), false).FirstOrDefault();
        //    return (T)attributes;
        //}

        ///// <summary>
        ///// Get an attribute for a property
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="propertyinfo"></param>
        ///// <returns></returns>
        //public static T GetAttribute<T>(this PropertyInfo propertyinfo)
        //    where T : Attribute
        //{
        //    var attributes = propertyinfo.GetCustomAttributes(typeof(T), false).FirstOrDefault();
        //    return (T)attributes;
        //}

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        internal static PropertyInfo[] GetBaseDataProperties<T>()
            where T : class
        {
            var type = typeof(T);
            return type.GetBaseDataProperties();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal static PropertyInfo[] GetBaseDataProperties(this Type type)
        {
            var output = new List<PropertyInfo>();
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var property in properties)
            {
                if (property.CanWrite)
                {
                    output.Add(property);
                }
            }
            return output.ToArray();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        internal static string[] GetBaseDataPropertyNames<T>()
            where T : class
        {
            var type = typeof(T);
            return type.GetBaseDataPropertyNames();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal static string[] GetBaseDataPropertyNames(this Type type)
        {
            return type.GetBaseDataProperties().ToList().Select(x => x.Name).ToArray();
        }

        internal static PropertyInfo[] GetMetaDataProperties<T>(bool includeObsoleteOrOptionalProperties)
            where T : class
        {
            var type = typeof(T);
            return type.GetMetaDataProperties(includeObsoleteOrOptionalProperties);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="type"></param>
        /// <param name="includeObsoleteOrOptionalProperties"></param>
        /// <returns></returns>
        internal static PropertyInfo[] GetMetaDataProperties(this Type type, bool includeObsoleteOrOptionalProperties)
        {
            var output = new List<PropertyInfo>();
            var attribute = type.GetCustomAttribute<MetadataTypeAttribute>();
            if (attribute != null)
            {
                var metadata = attribute.MetadataClassType;
                if (metadata != null)
                {
                    type = metadata;
                    var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                    foreach (var property in properties)
                    {
                        if (includeObsoleteOrOptionalProperties)
                        {
                            if (property.CanRead)
                            {
                                output.Add(property);
                            }
                        }
                        else
                        {
                            if (property.CanRead && !(property.HasAttribute<ObsoleteAttribute>() || property.HasAttribute<OptionalAttribute>()))
                            {
                                output.Add(property);
                            }
                        }
                    }
                }
            }
            return output.ToArray();
        }

        internal static string[] GetMetaDataPropertyNames<T>(bool includeObsoleteOrOptionalProperties)
            where T : class
        {
            var type = typeof(T);
            return type.GetMetaDataPropertyNames(includeObsoleteOrOptionalProperties);
        }

        /// <summary>
        /// ...
        /// </summary>
        internal static string[] GetMetaDataPropertyNames(this Type type, bool includeObsoleteOrOptionalProperties)
        {
            return type.GetMetaDataProperties(includeObsoleteOrOptionalProperties).ToList().Select(x => x.Name).ToArray();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="includeObsoleteOrOptionalProperties"></param>
        /// <returns></returns>
        internal static PropertyInfo[] GetCollectionPageProperties<T>(bool includeObsoleteOrOptionalProperties)
            where T : class
        {
            var result = GetMetaDataProperties<T>(includeObsoleteOrOptionalProperties);
            if (result != null && result.Length == XLib.MagicNumber.IntZero)
            {
                return GetBaseDataProperties<T>();
            }
            return result;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="includeObsoleteOrOptionalProperties"></param>
        /// <returns></returns>
        internal static string[] GetCollectionPagePropertyNames<T>(bool includeObsoleteOrOptionalProperties)
            where T : class
        {
            return GetCollectionPageProperties<T>(includeObsoleteOrOptionalProperties).ToList()
                .Select(x => x.Name).ToArray();
        }

        internal static PropertySchema[] GetCollectionPagePropertySchemas<T>(bool includeObsoleteOrOptionalProperties)
            where T : class
        {
            var output = new List<PropertySchema>();
            var properties = GetCollectionPageProperties<T>(includeObsoleteOrOptionalProperties);
            if (properties != null)
            {
                for (int i = 0; i < properties.Length; i++)
                {
                    var schema = new PropertySchema(properties[i]);
                    output.Add(schema);
                }
            }
            return output.ToArray();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal static string[] GetDefaultOrderingPropertyNames(this Type type)
        {
            var output = new List<Tuple<string, int>>();
            var attribute = type.GetCustomAttribute<MetadataTypeAttribute>();
            if (attribute != null)
            {
                var metadata = attribute.MetadataClassType;
                if (metadata != null)
                {
                    type = metadata;
                    var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                    foreach (var property in properties)
                    {
                        if (property.CanRead && property.HasAttribute<DefaultOrderingPropertyAttribute>())
                        {
                            var item1 = property.Name;
                            var item2 = property.GetDefaultOrderingPropertyOrderAttribute();
                            output.Add(new Tuple<string, int>(item1, item2));
                        }
                    }
                }
            }
            return output.OrderBy(x => x.Item2).Select(x => x.Item1).ToArray();
        }
        #endregion

        #region Methods: ToFriendlyString
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="propertyNames"></param>
        /// <returns></returns>
        public static string ToFriendlyString<T>(this T obj, params string[] propertyNames)
            where T : class
        {
            var type = typeof(T);
            var format = XLib.Orientation.HorizontalFormat;
            var digits = format.Separator.ToCharArray();
            var stringBuilder = new StringBuilder();
            foreach (var name in propertyNames)
            {
                var property = type.GetProperty(name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                if (property != null)
                {
                    stringBuilder.Append(obj.GetValue<T>(property, true, format));
                }
                else
                {
                    var value = StringHelper.Concat(new SurroundFormat(name, format.Separator), format.Suffix, XLib.Text.ArgumentError);
                    stringBuilder.Append(value);
                    if (XLib.Preference.LogResolvedExceptions)
                    {
                        //TODO: interceptor
                        object interceptor = null;
                        var method = MethodBase.GetCurrentMethod() as MethodInfo;
                        var assembly = method.ReflectedType.Assembly;
                        var info = value;
                        var information = new LogInformationBase(interceptor, assembly, method, info);
                        var exception = new ArgumentNullPropertyException(type, name);
                        var entry = new LogEntryBase(LogLevel.Debug, information, exception);
                        LogToFile.Log(entry, true);
                    }
                }
            }
            return stringBuilder.ToString().TryTrim(digits);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="showPropertyName"></param>
        /// <returns></returns>
        internal static string ToFriendlyString<T>(this T obj, bool showPropertyName = true)
            where T : class
        {
            var format = XLib.Orientation.VerticalFormat;
            var digits = format.Separator.ToCharArray();
            return obj.GetValues<T>(showPropertyName, format)
                .ToString()
                .TryTrimDigits(digits);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="showPropertyName"></param>
        /// <returns></returns>
        internal static string ToFriendlyStringAsOneLine<T>(this T obj, bool showPropertyName = true)
            where T : class
        {
            var format = XLib.Orientation.HorizontalFormat;
            var digits = format.Separator.ToCharArray();
            return obj.GetValues<T>(showPropertyName, format)
                .ToString()
                .TryTrim(digits)
                .RemoveWhiteSpace();
        }
        #endregion

        #region Methods: GetValues
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="property"></param>
        /// <param name="showPropertyName"></param>
        /// <param name="orientationFormat"></param>
        /// <returns></returns>
        private static StringBuilder GetValue<T>(this T obj, PropertyInfo property, bool showPropertyName, OrientationFormat orientationFormat)
            where T : class
        {
            var stringBuilder = new StringBuilder();
            // Get PropertyName:
            if (showPropertyName)
            {
                var name = property.GetDisplayNameAttribute();
                var surround = new SurroundFormat(null, orientationFormat.Suffix);
                stringBuilder.Append(StringHelper.Concat(surround, name));
            }
            //TODO: obj == null???
            // Get PropertyValue:
            var displayFormat = property.GetDisplayFormatAttribute(obj);
            var stringFormat = StringHelper.Format("{{{0}{1}}}{2}", 0, displayFormat, orientationFormat.Separator);
            var value = property.GetValue(obj, null).TryArrayToString<object>();
            if (value.IsEmptyString())
            {
                stringBuilder.Append(stringFormat.TryFormat(XLib.Text.NotSpecifiedText));
            }
            else
            {
                stringBuilder.Append(stringFormat.TryFormat(value));
            }
            return stringBuilder;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="showPropertyName"></param>
        /// <param name="orientationFormat"></param>
        /// <returns></returns>
        private static StringBuilder GetValues<T>(this T obj, bool showPropertyName, OrientationFormat orientationFormat)
            where T : class
        {
            //TODO: obj == null???
            var type = typeof(T);
            var stringBuilder = new StringBuilder();
            var hasGroups = type.HasDisplayGroupAttributes();
            var properties = type.GetProperties();
            foreach (var property in properties)
            {
                if (property.CanRead)
                {
                    if (hasGroups)
                    {
                        string group = property.GetDisplayGroupAttribute();
                        if (group != null)
                        {
                            stringBuilder.Append(obj.GetValue(property, showPropertyName, orientationFormat));
                        }
                    }
                    else
                    {
                        stringBuilder.Append(obj.GetValue(property, showPropertyName, orientationFormat));
                    }
                }
            }
            return stringBuilder;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static List<GroupValue> GetGroupValues<T>(this T obj)
            where T : BaseEntity
        {
            var output = new List<GroupValue>();
            if (obj == null)
            {
                obj = typeof(T).CreateInstance<T>();
            }
            if (obj.GetType().IsClass)
            {
                var type = typeof(T);
                var attribute = type.GetCustomAttribute<MetadataTypeAttribute>();
                if (attribute != null)
                {
                    var metadata = attribute.MetadataClassType;
                    if (metadata != null)
                    {
                        var properties = metadata.GetProperties();
                        foreach (var property in properties)
                        {
                            var group = StringHelper.Translation(property.GetDisplayGroupAttribute());
                            if (group != null)
                            {
                                var values = new GroupValue();
                                // Get GroupName:
                                values.Group = group;
                                // Get PropertyName
                                values.Title = StringHelper.Translation(property.Name);
                                // Get PropertyValue:
                                var displayFormat = property.GetDisplayFormatAttribute(obj, true);
                                var stringFormat = StringHelper.Format("{{{0}{1}}}", 0, displayFormat);
                                values.Data = StringHelper.TryFormat(stringFormat, type.GetProperty(property.Name).GetValue(obj, null));
                                // Get WarningMessage:
                                //values.Warning = prop.GetCustomWarning(prop.GetValue(obj, null));
                                output.Add(values);
                            }
                        }
                        //if (output.Count > XLib.Number.IntZero) { return output; }
                    }
                }
            }
            return output;
            //return null;
        }
        #endregion

        #region Methods: Helpers
        //TODO: ?????
        private static string GetDisplayFormatAttribute(this PropertyInfo property, Object obj, bool formatBmi = false, bool formatOutput = true)
        {
            var attribute = property.GetCustomAttribute<DisplayFormatAttribute>();
            if (attribute != null)
            {
                var displayFormat = new StringBuilder(attribute.DataFormatString);
                if (formatOutput)
                {
                    displayFormat.Replace("{", "");
                    displayFormat.Replace("}", "");
                    displayFormat.Replace("0:", ":");
                }
                return displayFormat.ToString();
            }
            else { return null; }
        }

        private static string GetDisplayNameAttribute(this PropertyInfo property)
        {
            var attribute = property.GetCustomAttribute<DisplayAttribute>();
            if (attribute != null)
            {
                if (attribute.GetName() != null)
                {
                    return attribute.Name.TryTrim();
                }
            }
            return property.Name;
        }

        private static string GetDisplayGroupAttribute(this PropertyInfo property)
        {
            var attribute = property.GetCustomAttribute<DisplayAttribute>();
            if (attribute != null)
            {
                if (attribute.GetGroupName() != null)
                {
                    return attribute.GroupName;
                }
            }
            return null;
        }

        private static int GetDefaultOrderingPropertyOrderAttribute(this PropertyInfo property)
        {
            var attribute = property.GetCustomAttribute<DefaultOrderingPropertyAttribute>();
            if (attribute != null)
            {
                if (attribute.GetOrder() != null)
                {
                    return attribute.Order;
                }
            }
            return -1;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        /// <returns></returns>
        public static bool HasAttribute<T>(this PropertyInfo property) where T : Attribute
        {
            var attribute = property.GetCustomAttribute<T>();
            if (attribute != null)
            {
                return true;
            }
            return false;
        }

        private static bool HasDisplayGroupAttributes(this Type type)
        {
            var properties = type.GetProperties().Where(p => p.IsDefined(typeof(DisplayAttribute), false)).ToList();
            foreach (var property in properties)
            {
                if (property.GetDisplayGroupAttribute() != null)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion
    }
}
