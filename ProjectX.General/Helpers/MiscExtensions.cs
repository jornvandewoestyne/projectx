﻿using System;
using System.Linq.Expressions;

//TODO: summury
//TODO: ArgumentException message
//TODO: move it to the application or data layer???!!!!!!

#region Source
//https://stackoverflow.com/questions/671968/retrieving-property-name-from-lambda-expression
#endregion

namespace ProjectX.General
{
    public static class MiscExtensions
    {
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="object"></param>
        /// <param name="propertyExpression"></param>
        /// <returns></returns>
        public static string NameOf<TModel, TProperty>(this object @object, Expression<Func<TModel, TProperty>> propertyExpression)
        {
            var expression = propertyExpression.Body as MemberExpression;
            if (expression == null)
            {
                //TODO: string
                throw new ArgumentException("Expression is not a property.");
            }
            return expression.Member.Name;
        }
    }
}
