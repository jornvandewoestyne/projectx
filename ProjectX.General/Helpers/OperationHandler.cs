﻿using System;
using System.Threading;
using System.Threading.Tasks;

#region Source
// https://michaelscodingspot.com/multi-thread-timeout-challenges-c/
#endregion

namespace ProjectX.General
{
    public sealed class OperationHandler
    {
        #region Properties
        private Action Action { get; }
        private ManualResetEvent ManualResetEvent { get; } = new ManualResetEvent(false);
        #endregion

        #region Constructors
        public OperationHandler(Action action)
        {
            this.Action = action;
        }
        #endregion

        public OperationHandler RunWithTimeout(int timeOut = XLib.Preference.DefaultTaskTimeOut)
        {
            this.ManualResetEvent.Reset();
            Task.Factory.StartNew(() =>
            {
                bool wasStopped = this.ManualResetEvent.WaitOne(timeOut);
                if (!wasStopped) { this.Action(); }
            });
            return this;
        }

        public OperationHandler StopOperationIfNotStartedYet()
        {
            this.ManualResetEvent.Set();
            return this;
        }
    }
}
