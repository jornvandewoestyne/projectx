﻿using ProjectX.General.Localization;
using System;
using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;

namespace ProjectX.General
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class StringHelper
    {
        #region Concat
        /// <summary>
        /// Concatenates the elements of an object array.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// <para>NewLine arguments are accepted!</para>
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Concat(params object[] args)
        {
            return new XString.Concat(args).ToString();
        }

        /// <summary>
        /// Concatenates the elements of an object array, surrounding the result with the given surround prefix and/or suffix.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// <para>NewLine arguments are accepted!</para>
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Concat(SurroundFormat surround, params object[] args)
        {
            return new XString.Concat(surround, args).ToString();
        }
        #endregion

        #region Format
        /// <summary>
        /// Replaces the format item in a specified string with the string representation of a corresponding object in a specified array.
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Format(string format, params object[] args)
        {
            return String.Format(format, args);
        }

        /// <summary>
        /// Replaces the format item in a specified string with the string representation of a corresponding object in a specified array.
        /// <para>Resolves a FormatException or a ArgumentNullException by replacing missing parameters with a {N/A} or {index} string.</para>
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string TryFormat(string format, params object[] args)
        {
            return format.TryFormat(args);
        }
        #endregion

        #region Join
        /// <summary>
        /// Concatenates the elements of an object array, using the given separator between each element.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Join(string separator, params object[] args)
        {
            return new XString.Join(separator, args).ToString();
        }

        /// <summary>
        /// Concatenates the elements of an object array, using a ARROW separator between each element.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string ArrowStyle(params object[] args)
        {
            return new XString.ArrowStyle(args).ToString();
        }

        /// <summary>
        /// Concatenates the elements of an object array, using a HORIZONTAL separator between each element.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Enumeration(params object[] args)
        {
            return new XString.Enumeration(args).ToString();
        }

        /// <summary>
        /// Concatenates the elements of an object array, using a PLUS separator between each element.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string PlusStyle(params object[] args)
        {
            return new XString.PlusStyle(args).ToString();
        }

        /// <summary>
        /// Concatenates the elements of an object array, using a DOT separator between each element.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string QualifiedName(params object[] args)
        {
            return new XString.QualifiedName(args).ToString();
        }

        /// <summary>
        /// Concatenates the elements of an object array, using a SPACE separator between each element.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Sentence(params object[] args)
        {
            return new XString.Sentence(args).ToString();
        }

        /// <summary>
        /// Concatenates the elements of an object array, using a TITLE separator between each element.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Title(params object[] args)
        {
            return new XString.Title(args).ToString();
        }
        #endregion

        #region Translation
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="enumType"></param>
        /// <returns></returns>
        public static string Translation(Enum enumType)
        {
            return new Translation(enumType).Value;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="enumType"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public static string Translation(Enum enumType, SupportedLanguage language)
        {
            return new Translation(enumType, language).Value;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="enumType"></param>
        /// <param name="language"></param>
        /// <param name="translateArguments"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Translation(Enum enumType, SupportedLanguage language, bool translateArguments, params object[] args)
        {
            return new Translation(enumType, language, translateArguments, args).Value;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string Translation(string key)
        {
            return new Translation(key).Value;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="key"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public static string Translation(string key, SupportedLanguage language)
        {
            return new Translation(key, language).Value;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="key"></param>
        /// <param name="language"></param>
        /// <param name="translateArguments"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Translation(string key, SupportedLanguage language, bool translateArguments, params object[] args)
        {
            return new Translation(key, language, translateArguments, args).Value;
        }
        #endregion

        #region Words
        /// <summary>
        /// Get singular or plural from a given word
        /// <para>Supports only the English culture</para>
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public static string GetPluralOrSingular(string word)
        {
            var name = SupportedLanguage.EN.ToString().ToLower();
            var service = PluralizationService.CreateService(CultureInfo.GetCultureInfo(name));
            if (service.IsPlural(word))
            {
                return service.Singularize(word);
            }
            if (service.IsSingular(word))
            {
                return service.Pluralize(word);
            }
            return word;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="singular"></param>
        /// <param name="plural"></param>
        /// <returns></returns>
        public static PluralizationService IntializePluralization(string singular, string plural)
        {
            var service = PluralizationService.CreateService(CultureInfo.GetCultureInfo("en-us"));
            ((ICustomPluralizationMapping)service).AddWord(singular, plural);
            return service;
        }
        #endregion
    }
}
