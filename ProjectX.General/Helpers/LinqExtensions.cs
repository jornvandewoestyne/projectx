﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

//TODO: summary
//TODO: ExceptionHandling

#region Source
//Contains: http://forums.asp.net/t/1976544.aspx?Err+LINQ+to+Entities+does+not+recognize+the+method+Boolean+LikeString+System+String+System+String+Microsoft+VisualBasic+CompareMethod+method+and+this+method+cannot+be+translated+into+a+store+expression+
//Select: http://stackoverflow.com/questions/26367241/dynamically-linq-select-cast-to-ienumerable
// CHECK OUT: http://stackoverflow.com/questions/41244/dynamic-linq-orderby-on-ienumerablet
#endregion

namespace ProjectX.General
{
    public static class LinqExtensions
    {
        #region Methods: Select
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static IEnumerable<TResult> SelectDynamic<TEntity, TResult>(this IEnumerable<TEntity> source)
            where TEntity : BaseEntity
        {
            if (source != null)
            {
                var propertyNames = ReflectionExtensions.GetBaseDataPropertyNames<TEntity>();
                return source.SelectDynamic<TEntity, TResult>(propertyNames);
            }
            return null;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="source"></param>
        /// <param name="propertyNames"></param>
        /// <returns></returns>
        public static IEnumerable<object> SelectDynamic<TEntity>(this IEnumerable<TEntity> source, bool includeObsoleteOrOptionalProperties, params string[] propertyNames)
            where TEntity : BaseEntity
        {
            var result = source.SelectDynamic<TEntity, object>(propertyNames);
            if (result == null)
            {
                result = source.SelectDynamicMetaData(includeObsoleteOrOptionalProperties);
                if (result == null)
                {
                    result = source.SelectDynamicBaseData();
                }
            }
            return result;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        private static IEnumerable<object> SelectDynamicBaseData<TEntity>(this IEnumerable<TEntity> source)
            where TEntity : BaseEntity
        {
            return source.SelectDynamic<TEntity, object>();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="source"></param>
        /// <param name="includeObsoleteOrOptionalProperties"></param>
        /// <returns></returns>
        private static IEnumerable<object> SelectDynamicMetaData<TEntity>(this IEnumerable<TEntity> source, bool includeObsoleteOrOptionalProperties)
            where TEntity : BaseEntity
        {
            if (source != null)
            {
                var propertyNames = ReflectionExtensions.GetMetaDataPropertyNames<TEntity>(includeObsoleteOrOptionalProperties);
                return source.SelectDynamic<TEntity, object>(propertyNames);
            }
            return null;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="source"></param>
        /// <param name="propertyNames"></param>
        /// <returns></returns>
        private static IEnumerable<TResult> SelectDynamic<TEntity, TResult>(this IEnumerable<TEntity> source, params string[] propertyNames)
            where TEntity : BaseEntity
        {
            if (source != null)
            {
                var length = propertyNames.Length;
                if (length > XLib.MagicNumber.IntZero)
                {
                    //TODO: test
                    var properties = new List<string>();
                    for (int i = 0; i < length; i++)
                    {
                        var type = typeof(TEntity);
                        var proceed = type.TryGetProperty(propertyNames[i], out var property);
                        if (proceed)
                        {
                            properties.Add(property.Name);
                        }
                    }
                    var values = StringHelper.Enumeration(properties.ToArray());
                    var selector = XLib.Text.AnonymousSelector.TryFormat(values);
                    return source.Select(selector).Cast<TResult>();
                }
            }
            return null;
        }
        #endregion

        #region Methods: Where
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="source"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public static IQueryable<TEntity> TryWhere<TEntity>(this IQueryable<TEntity> source, Expression<Func<TEntity, bool>> filter)
           where TEntity : BaseEntity
        {
            if (source != null && filter != null)
            {
                return source.Where(filter);
            }
            return source;
        }

        public static IQueryable<TEntity> WhereDynamic<TEntity>(this IQueryable<TEntity> source, string predicate, params object[] values)
            where TEntity : BaseEntity
        {
            if (source != null)
            {
                return source.Where(predicate, values);
            }
            return null;
        }
        #endregion

        //REMINDER: Add methods accordingly when the method needs to be supplied for a specific type...
        #region Methods: Contains
        /// <summary>
        /// LINQ to Entities: q => keys.Contains(q.ID)
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="keys"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static Expression<Func<TEntity, bool>> Contains<TEntity>(this HashSet<int> keys, string propertyName)
            where TEntity : BaseEntity
        {
            return keys.Contains<TEntity, int>(propertyName);
        }

        /// <summary>
        /// LINQ to Entities: q => keys.Contains(q.ID)
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="keys"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static Expression<Func<TEntity, bool>> Contains<TEntity>(this HashSet<string> keys, string propertyName)
            where TEntity : BaseEntity
        {
            return keys.Contains<TEntity, string>(propertyName);
        }

        /// <summary>
        /// LINQ to Entities: q => keys.Contains(q.ID)
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="keys"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static Expression<Func<TEntity, bool>> Contains<TEntity>(this HashSet<Guid> keys, string propertyName)
            where TEntity : BaseEntity
        {
            return keys.Contains<TEntity, Guid>(propertyName);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="keys"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private static Expression<Func<TEntity, bool>> Contains<TEntity, TResult>(this HashSet<TResult> keys, string propertyName)
        {
            //TODO: ExceptionHandling
            //System.ArgumentException -> Exemplaareigenschap PartnerID,Code is niet gedefinieerd voor type ProjectX.Domain.Models.CustomerGeneralData
            var methodInfo = typeof(HashSet<TResult>).GetMethod("Contains", new Type[] { typeof(TResult) });
            var list = Expression.Constant(keys);
            var param = Expression.Parameter(typeof(TEntity), "q");
            var value = Expression.Property(param, propertyName);
            var body = Expression.Call(list, methodInfo, value);
            return Expression.Lambda<Func<TEntity, bool>>(body, param);
        }
        #endregion

        #region Methods: OrderBy
        /// <summary>
        /// Sorts the elements of a sequence in ascending or descending order according to a keystring.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="source"></param>
        /// <param name="ordering"></param>
        /// <returns></returns>
        public static IEnumerable<TEntity> TryOrderByDynamic<TEntity>(this IEnumerable<TEntity> source, Ordering ordering)
            where TEntity : BaseEntity
        {
            if (source != null && ordering != null && ordering.IsSafe)
            {
                return source.OrderBy(ordering.ToString());
            }
            return source;
        }

        public static IQueryable<TEntity> TryOrderByDynamic<TEntity>(this IQueryable<TEntity> source, Ordering ordering)
            where TEntity : BaseEntity
        {
            if (source != null && ordering != null && ordering.IsSafe)
            {
                return source.OrderBy(ordering.ToString());
            }
            return source;
        }
        #endregion

        #region Methods: Take
        public static IQueryable<TEntity> TryTake<TEntity>(this IQueryable<TEntity> source, Ordering ordering, Paging paging)
            where TEntity : BaseEntity
        {
            if (source != null)
            {
                if (paging != null)
                {
                    if (!paging.TakeAll)
                    {
                        if (paging.SkipAll)
                        {
                            //return nothing:
                            return source
                                .Take(XLib.MagicNumber.IntZero)
                                .Where("1 = 2");
                        }
                        else if (paging.Skip > XLib.MagicNumber.IntZero)
                        {
                            return source
                                .TryOrderByDynamic(ordering)
                                .Skip(paging.Skip)
                                .Take(paging.Take);
                        }
                        else
                        {
                            return source
                                .TryOrderByDynamic(ordering)
                                .Take(paging.Take);
                        }
                    }
                }
                return source
                    .TryOrderByDynamic(ordering);
            }
            return source;
        }
        #endregion

        #region Methods: Join
        public static IQueryable<TResult> TryJoin<TOuter, TInner, TKey, TResult>(this IQueryable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector)
            where TOuter : BaseEntity
            where TInner : struct
            where TKey : class
            where TResult : BaseEntity
        {
            //TODO: check nulls
            return outer
                .AsEnumerable<TOuter>()
                .Join(inner, outerKeySelector, innerKeySelector, resultSelector)
                .AsQueryable<TResult>();
        }
        #endregion

        #region Methods: ToListAsync
        /// <summary>
        /// Creates an <see cref="System.Collections.Generic.IList&lt;T&gt;"/> from an <see cref="System.Collections.Generic.IEnumarable&lt;T&gt;"/> by enumerating it asynchronously.
        /// <para>NOTE: Although the process is not really running asynchronously, this method still ensures that the UI is kept responsive during the process! In fact, the operation is temporarely blocked untill it has finished, but lukily the UI doesn't notice that. Unfortunately, a major consequence is that cancelling the process is only enforced AFTER the process has finished! So, keep in mind that long-running requests (with lots of data) can not be easily stopped.</para>
        /// </summary>
        public static async Task<IList<T>> AsToListAsync<T>(this IEnumerable<T> source, CancellationToken cancellationToken) where T : class
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }
            cancellationToken.ThrowIfCancellationRequested();
            return await Task.Run(() => source.ToList(), cancellationToken);
        }
        #endregion
    }
}
