﻿using System;
using System.Linq;
using System.Reflection;

namespace ProjectX.General
{
    public sealed class PropertyCache
    {
        #region Fields
        private static readonly LazyConcurrentDictionary<string, PropertyCache> FastCache
            = new LazyConcurrentDictionary<string, PropertyCache>();
        private static readonly LazyConcurrentDictionary<Type, PropertyCache[]> Cache
            = new LazyConcurrentDictionary<Type, PropertyCache[]>();
        private static readonly MethodInfo CallInnerDelegateMethod
            = typeof(PropertyCache).GetMethod(nameof(CallInnerDelegate), BindingFlags.NonPublic | BindingFlags.Static);
        #endregion

        #region Properties
        public string Name { get; private set; }
        public Func<object, object> Value { get; private set; }
        public BusinessRule.ComposedOfAttribute ComposedOf { get; private set; }
        #endregion

        #region Methods
        // Called via reflection.
        private static Func<object, object> CallInnerDelegate<TClass, TResult>(Func<TClass, TResult> deleg)
            => instance
            => deleg((TClass)instance);

        public static PropertyCache GetCachedProperty(Type type, string propertyName)
        {
            if (type == null) { throw new MissingMethodParameterException(nameof(type)); }
            if (propertyName.IsEmptyString()) { throw new MissingMethodParameterException(nameof(propertyName)); }
            if (!type.BaseType.IsAbstract) { type = type.BaseType; }
            var key = StringHelper.QualifiedName(type, propertyName);
            return FastCache.GetOrAdd(key, k => GetCachedProperties(type).FirstOrDefault(x => x.Name == propertyName));
        }

        public static PropertyCache[] GetCachedProperties(Type type)
        {
            return Cache
                .GetOrAdd(type, t => type
                    .GetProperties()
                    .Where(prop => prop.GetIndexParameters().Length == XLib.MagicNumber.IntZero)
                    .Select(property =>
                        {
                            var getMethod = property.GetMethod;
                            var declaringClass = property.DeclaringType;
                            var typeOfResult = property.PropertyType;
                            // Func<Type, TResult>
                            var getMethodDelegateType = typeof(Func<,>).MakeGenericType(declaringClass, typeOfResult);
                            // c => c.Data
                            var getMethodDelegate = getMethod.CreateDelegate(getMethodDelegateType);
                            // CallInnerDelegate<Type, TResult>
                            var callInnerGenericMethodWithTypes = CallInnerDelegateMethod.MakeGenericMethod(declaringClass, typeOfResult);
                            // Func<object, object>
                            var result = (Func<object, object>)callInnerGenericMethodWithTypes.Invoke(null, new[] { getMethodDelegate });
                            var propertyHelper = new PropertyCache
                            {
                                Name = property.Name,
                                Value = result
                            };
                            var composedOf = property.GetCustomAttribute<BusinessRule.ComposedOfAttribute>();
                            if (composedOf != null)
                            {
                                propertyHelper.ComposedOf = composedOf;
                            }
                            return propertyHelper;
                        })
                .ToArray());
        }
        #endregion
    }
}