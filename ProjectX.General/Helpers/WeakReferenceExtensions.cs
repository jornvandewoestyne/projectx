﻿using System;

namespace ProjectX.General
{
    public static class WeakReferenceExtensions
    {
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static WeakReference<T> AsWeakReference<T>(this T obj) where T : class
        {
            return new WeakReference<T>(obj);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reference"></param>
        /// <returns></returns>
        public static T TryGet<T>(this WeakReference<T> reference) where T : class
        {
            if (reference == null)
            {
                throw new NullReferenceException(nameof(reference));
            }
            if (reference.TryGetTarget(out T target))
            {
                return target;
            }
            //TODO: throw correct exception?
            throw new NullReferenceException(nameof(target));
        }
    }
}
