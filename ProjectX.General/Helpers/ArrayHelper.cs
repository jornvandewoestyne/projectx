﻿using ProjectX.General.LogService;
using System;
using System.Collections.Generic;
using System.Linq;

//TODO: summary
//TODO: disable logging?

//TODO: BUG DANGER -> EXCEPTION ARE THROWN BACK!

namespace ProjectX.General
{
    public static class ArrayHelper
    {
        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        public static T[] Combine<T>(params IEnumerable<T>[] items)
        {
            return items.SelectMany(i => i).Distinct().ToArray();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="array"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private static string TryGetValue(this string[] array, int index)
        {
            if (array != null)
            {
                if (index >= XLib.MagicNumber.IntZero && index < array.Length)
                {
                    return array[index];
                }
            }
            return null;
        }

        //TODO: what with incorrect index -> what to return?
        ///// <summary>
        ///// ...
        ///// </summary>
        ///// <param name="array"></param>
        ///// <param name="index"></param>
        ///// <param name="value"></param>
        ///// <returns></returns>
        //public static bool TryGetValue(this string[] array, int index, out string value)
        //{
        //    value = array.TryGetValue(index);
        //    if (value != null)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="array"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static bool HasValidValue(this string[] array, int index)
        {
            var value = array.TryGetValue(index);
            if (!value.IsEmptyString())
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Determines whether two sequences are equal by comparing the elements by using a customized equality comparer for their type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static bool Compare<T>(T[] first, T[] second) where T : class
        {
            try
            {
                if (first == null && second == null)
                {
                    return true;
                }
                return first.SequenceEqual<T>(second);
            }
            catch (System.Exception)
            {
                LogToFile.Log(null, false);
                return false;
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static string TryArrayToString<T>(this T[] array, string separator = XLib.Constant.DefaultSeparator) where T : class
        {
            try
            {
                if (array != null)
                {
                    return String.Join<T>(separator, array)
                        .TryTrimEnd(separator.TryConvertToCharArray(true));
                }
                return null;
            }
            catch (Exception)
            {
                LogToFile.Log(null, false);
                return null;
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static string TryArrayToString<T>(this T value, string separator = XLib.Constant.DefaultSeparator) where T : class
        {
            try
            {
                if (value != null)
                {
                    var type = value.GetType();
                    if (type.IsArray)
                    {
                        return value.TryConvertToArray().TryArrayToString<object>(separator);
                    }
                    return value.ToString();
                }
                return null;
            }
            catch (Exception)
            {
                LogToFile.Log(null, false);
                return null;
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static object[] TryConvertToArray(this Enum value)
        {
            try
            {
                if (value != null)
                {
                    return new object[] { Convert.ToInt32(value) };
                }
                return null;
            }
            catch (Exception)
            {
                LogToFile.Log(null, false);
                return null;
            }
        }

        public static object[] TryConvertToArray(this int value)
        {
            try
            {
                return new object[] { Convert.ToInt32(value) };
            }
            catch (Exception)
            {
                LogToFile.Log(null, false);
                return null;
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static object[] TryConvertToArray(this object value)
        {
            try
            {
                if (value != null)
                {
                    return ((object[])value).Select(obj => obj).ToArray();
                }
                return null;
            }
            catch (Exception)
            {
                LogToFile.Log(null, false);
                return null;
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IEnumerable<T> TryGetValidArguments<T>(this T[] args)
        {
            if (args != null)
            {
                var length = args.Length;
                for (int i = 0; i < length; i++)
                {
                    var value = args[i]?.ToString();
                    if (!value.IsEmptyString())
                    {
                        yield return args[i];
                    }
                }
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IEnumerable<T> TryGetValidArguments<T>(this T[] args, bool acceptNewLine)
        {
            if (args != null)
            {
                var length = args.Length;
                if (acceptNewLine)
                {
                    for (int i = 0; i < length; i++)
                    {
                        var value = args[i]?.ToString();
                        if (!value.IsEmptyString() || value == Environment.NewLine)
                        {
                            yield return args[i];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < length; i++)
                    {
                        var value = args[i]?.ToString();
                        if (!value.IsEmptyString())
                        {
                            yield return args[i];
                        }
                    }
                }
            }
        }
        #endregion
    }
}
