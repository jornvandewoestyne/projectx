﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//TODO: summary

namespace ProjectX.General
{
    public static class TypeExtensions
    {
        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="fast"></param>
        /// <returns></returns>
        public static T CreateInstance<T>(this Type type, bool fast = true) //where T : class
        {
            if (fast)
            {
                var expression = Expression.New(type);
                var lambda = Expression.Lambda<Func<object>>(expression);
                var compiled = lambda.Compile();
                return (T)compiled();
            }
            else
            {
                return (T)Activator.CreateInstance(type);
            }
        }

        public static T CreateInstance<T>(this Type type, params object[] args)
        {
            return (T)Activator.CreateInstance(type, args);
        }

        public static IEnumerable<Type> GetInheritanceHierarchy(this Type type)
        {
            for (var current = type; current != null; current = current.BaseType)
                yield return current;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static Type GetEnumeratedType<T>(this IEnumerable<T> source)
        {
            return typeof(T);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static Type GetEnumeratedType<TEntity>(this ICollectionPage<TEntity> source)
            where TEntity : BaseEntity
        {
            return typeof(TEntity);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static SearchString AsSearchString(this string input)
        {
            return new SearchString(input);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static SearchString AsSearchString(this StringBuilder input)
        {
            string value = null;
            if (input != null)
            {
                value = input.ToString();
            }
            return value.AsSearchString();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="languageID"></param>
        /// <returns></returns>
        public static SupportedLanguage AsSupportedLanguage(this Nullable<int> languageID)
        {
            return languageID == null ? XLib.Preference.CurrentLanguage : ((int)languageID).AsSupportedLanguage();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="languageID"></param>
        /// <returns></returns>
        public static SupportedLanguage AsSupportedLanguage(this int languageID)
        {
            return EnumHelper.IsDefined<SupportedLanguage>(languageID) ? (SupportedLanguage)languageID : XLib.Preference.CurrentLanguage;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static EntityKey AsEntityKey(this object[] array)
        {
            return new EntityKey(array);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static EntityKey AsEntityKey(this MenuBloc value)
        {
            return value.TryConvertToArray().AsEntityKey();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static EntityKey AsEntityKey(this MenuGroupAction value)
        {
            return value.TryConvertToArray().AsEntityKey();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static EntityKey AsEntityKey(this int value)
        {
            return value.TryConvertToArray().AsEntityKey();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static EntityKeyName AsEntityKeyName(this string[] array)
        {
            return new EntityKeyName(array);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static EntityKeyType AsEntityKeyType(this Type[] array)
        {
            return new EntityKeyType(array);
        }

        ///// <summary>
        ///// ...
        ///// </summary>
        ///// <param name="title"></param>
        ///// <returns></returns>
        //public static EntitySummary AsEntitySummary(string title)
        //{
        //    return new EntitySummary(title);
        //}

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="notifications"></param>
        /// <returns></returns>
        public static IKeySet<T> AsKeySet<T>(this IEnumerable<T> items, IList<Notification> notifications)
            where T : struct
        {
            if (items == null)
            {
                items = new List<T>();
            }
            return new KeySet<T>(items, notifications);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TSearchEntity"></typeparam>
        /// <param name="entities"></param>
        /// <param name="notifications"></param>
        /// <returns></returns>
        public static IKeySet<int> AsKeySet<TEntity, TSearchEntity>(this IEnumerable<TEntity> entities, IList<Notification> notifications)
            where TEntity : BaseEntity
            where TSearchEntity : ISearchEntity
        {
            if (entities != null)
            {
                var items = entities.Cast<TSearchEntity>().Select(x => x.ID);
                return items.AsKeySet<int>(notifications);
            }
            else
            {
                return new List<int>().AsKeySet<int>(notifications);
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="items"></param>
        /// <param name="paging"></param>
        /// <param name="totalItems"></param>
        /// <param name="keySet"></param>
        /// <returns></returns>
        public static async Task<ICollectionPage<TEntity>> AsCollectionPage<TEntity>(this IEnumerable<TEntity> items, Paging paging, int totalItems, IKeySet<int> keySet)
            where TEntity : BaseEntity
        {
            return await CollectionPage<TEntity>.CreateAsync(items, paging, totalItems, keySet.ResolveNull().Notifications, CancellationToken.None);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="items"></param>
        /// <param name="paging"></param>
        /// <param name="totalItems"></param>
        /// <param name="keySet"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async Task<ICollectionPage<TEntity>> AsCollectionPage<TEntity>(this IEnumerable<TEntity> items, Paging paging, int totalItems, IKeySet<int> keySet, CancellationToken cancellationToken)
            where TEntity : BaseEntity
        {
            return await CollectionPage<TEntity>.CreateAsync(items, paging, totalItems, keySet.ResolveNull().Notifications, cancellationToken);
        }
        #endregion
    }
}
