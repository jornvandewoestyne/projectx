﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

//TODO: Add Constraints: http://stackoverflow.com/questions/79126/create-generic-method-constraining-t-to-an-enum
//TODO: default value: https://jaliyaudagedara.blogspot.com/2015/07/setting-default-value-for-enums-in-c.html

//TODO: remake!!!!!

#region Source
//SOURCE: http://stackoverflow.com/questions/13099834/how-to-get-the-display-name-attribute-of-an-enum-member-via-mvc-razor-code
#endregion

namespace ProjectX.General
{
    public static class EnumHelper
    {
        //TODO: add summary
        #region Methods
        /// <summary>
        /// 
        /// </summary>
        public static IList<T> GetValues<T>(T value) where T : Enum
        {
            var enumValues = new List<T>();

            foreach (FieldInfo fi in value.GetType().GetFields(BindingFlags.Static | BindingFlags.Public))
            {
                enumValues.Add((T)Enum.Parse(value.GetType(), fi.Name, false));
            }
            return enumValues;
        }

        /// <summary>
        /// 
        /// </summary>
        public static T[] GetValues<T>() where T : Enum
        {
            return Enum.GetValues(typeof(T)) as T[];
        }


        public static T GetDefaultValue<T>() where T : Enum
        {
            var type = typeof(T);
            var attribute = type.GetCustomAttribute(typeof(DefaultValueAttribute), false);
            if (attribute != null && type.IsEnum)
            {
                return Parse<T>(attribute.ToString());
            }
            return default(T);
        }

        /// <summary>
        /// ...
        /// </summary>
        public static T Parse<T>(string value) where T : Enum
        {
            var type = typeof(T);
            if (value != null && type.IsEnum)
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            return GetDefaultValue<T>(); //default(T);
        }

        ////TODO: implement
        //public static T TryParse<T>(string value)
        //{
        //    throw new NotImplementedException();
        //}

        public static bool IsDefined<T>(object value) where T : Enum
        {
            if (value != null)
            {
                return Enum.IsDefined(typeof(T), value);
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public static IList<string> GetNames<T>(T value) where T : Enum
        {
            return value.GetType().GetFields(BindingFlags.Static | BindingFlags.Public).Select(fi => fi.Name).ToList();
        }

        public static IList<string> GetNames<T>() where T : Enum
        {
            return typeof(T).GetType().GetFields(BindingFlags.Static | BindingFlags.Public).Select(fi => fi.Name).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        public static IList<string> GetDisplayValues<T>(T value) where T : Enum
        {
            return GetNames(value).Select(obj => GetDisplayValue(Parse<T>(obj))).ToList();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetName<T>(T value) where T : Enum
        {
            return Enum.GetName(typeof(T), value);
        }

        /// <summary>
        /// ...
        /// </summary>
        public static string GetDisplayValue<T>(T value) where T : Enum
        {
            var fieldInfo = value.GetType().GetField(value.ToString());

            var descriptionAttributes = fieldInfo.GetCustomAttributes(
                typeof(DisplayAttribute), false) as DisplayAttribute[];

            if (descriptionAttributes == null || descriptionAttributes.Length == XLib.MagicNumber.IntZero)
            {
                throw new ArgumentNullException("Geen attribuut gevonden!");
                //return String.Empty; //TODO: remove line?
            }
            return (descriptionAttributes.Length > XLib.MagicNumber.IntZero) ? descriptionAttributes[XLib.MagicNumber.Index0].Name : value.ToString();
        }
        #endregion
    }
}
