﻿using ProjectX.General.LogService;
using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

//TODO: add summary

//CHECK OUT: http://www.codeproject.com/Articles/692603/Csharp-String-Extensions

#region Source
//ToTitleCase:
//Source: http://stackoverflow.com/questions/2004944/is-there-a-native-proper-case-string-function-in-c
//RemoveWhiteSpace:
//Source: using LINQ: http://stackoverflow.com/questions/6219454/efficient-way-to-remove-all-whitespace-from-string
#endregion

namespace ProjectX.General
{
    public static class StringExtensions
    {
        #region Methods: RemoveWhiteSpace
        /// <summary>
        /// Removes all whitespace from a given string.
        /// </summary>
        public static string RemoveAllWhiteSpace(this string input)
        {
            return new string(input.Where(c => !Char.IsWhiteSpace(c)).ToArray());
        }

        /// <summary>
        /// Remove/Cleans whitespace from a given string.
        /// </summary>
        public static string RemoveWhiteSpace(this string input)
        {
            if (input != null)
            {
                var stringBuilder = new StringBuilder();
                bool lastCharWs = false; // = last char is whitespace!
                foreach (char ch in input)
                {
                    if (char.IsWhiteSpace(ch))
                    {
                        if (lastCharWs) { continue; }
                        stringBuilder.Append(XLib.Token.Space);
                        lastCharWs = true;
                    }
                    else
                    {
                        stringBuilder.Append(ch);
                        lastCharWs = false;
                    }
                }
                return stringBuilder.ToString();
            }
            return null;
        }
        #endregion

        #region Methods: CheckValues
        /// <summary>
        /// Indicates whether a specified string is null, empty, or consists only of white-space characters.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsEmptyString(this string value)
        {
            return String.IsNullOrWhiteSpace(value);
        }
        #endregion

        #region Methods: Substring
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="digits"></param>
        /// <param name="digitToFind"></param>
        /// <param name="isExclusive"></param>
        /// <returns></returns>
        public static string SubstringExclusive(this string input, char[] digits, char digitToFind, bool isExclusive)
        {
            if (input != null)
            {
                var stringBuilder = new StringBuilder();
                bool isArrayOfDigits = false;
                foreach (char ch in input)
                {
                    foreach (char digit in digits)
                    {
                        if (ch.Equals(digitToFind))
                        {
                            isArrayOfDigits = true;
                            break;
                        }
                        else if (ch.Equals(digit))
                        {
                            if (isExclusive)
                            {
                                if (!isArrayOfDigits)
                                {
                                    isArrayOfDigits = false;
                                }
                            }
                            else
                            {
                                isArrayOfDigits = false;
                            }
                            break;
                        }
                    }
                    if (isArrayOfDigits)
                    {
                        stringBuilder.Append(ch);
                    }
                }
                return stringBuilder.ToString();
            }
            return null;
        }
        #endregion

        #region Methods: Split
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="input"></param>
        /// <param name="digit"></param>
        /// <param name="removeEmptyEntries"></param>
        /// <returns></returns>
        public static string[] Split(this string input, char digit, bool removeEmptyEntries)
        {
            var option = StringSplitOptions.None;
            if (removeEmptyEntries)
            {
                option = StringSplitOptions.RemoveEmptyEntries;
            }
            return input.Split(new[] { digit }, option);
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="input"></param>
        /// <param name="digits"></param>
        /// <param name="removeEmptyEntries"></param>
        /// <returns></returns>
        public static string[] Split(this string input, char[] digits, bool removeEmptyEntries)
        {
            var option = StringSplitOptions.None;
            if (removeEmptyEntries)
            {
                option = StringSplitOptions.RemoveEmptyEntries;
            }
            return input.Split(digits, option);
        }

        /// <summary>
        /// Returns a string array that contains the substrings in this string that are 
        /// delimited by elements of a specified Unicode character array without empty array elements. 
        /// The returned array contains only substrings for the extra specified character!
        /// </summary>
        public static string[] Split(this string input, char[] digits, char splitByDigit, bool isExclusive)
        {
            return input.SubstringExclusive(digits, splitByDigit, isExclusive)
                .Split(splitByDigit, true);
        }
        #endregion

        #region Methods: Replace Chars
        /// <summary>
        /// Remove/Cleans chars from a given string.
        /// </summary>
        public static string CleanSubStringOfDigits(this string input, char[] digits)
        {
            if (input != null)
            {
                var stringBuilder = new StringBuilder();
                bool isArrayOfDigits = false;
                foreach (char ch in input)
                {
                    var i = XLib.MagicNumber.IntZero;
                    foreach (char digit in digits)
                    {
                        i++;
                        if (ch.Equals(digit))
                        {
                            if (isArrayOfDigits) { break; }
                            stringBuilder.Append(ch);
                            isArrayOfDigits = true;
                            break;
                        }
                        if (char.IsWhiteSpace(ch)) { break; }
                        if (i == digits.Count()) { isArrayOfDigits = false; }
                    }
                    if (!isArrayOfDigits)
                    {
                        stringBuilder.Append(ch);
                    }
                }
                return stringBuilder.ToString().RemoveWhiteSpace().TryTrim();
            }
            return null;
        }

        /// <summary>
        /// Remove/Cleans chars from a given string.
        /// </summary>
        public static string RemoveCharsButOne(this string input, char digit, string replacement)
        {
            if (input != null)
            {
                var stringBuilder = new StringBuilder();
                bool isLastChar = false;
                foreach (char ch in input)
                {
                    if (ch.Equals(digit))
                    {
                        if (isLastChar) { continue; }
                        stringBuilder.Append(replacement);
                        isLastChar = true;
                    }
                    else
                    {
                        stringBuilder.Append(ch);
                        if (char.IsWhiteSpace(ch)) { continue; }
                        isLastChar = false;
                    }
                }
                return stringBuilder.ToString().RemoveWhiteSpace().TryTrim();
            }
            return null;
        }

        //TODO: Avoid use of Replace
        //https://stackoverflow.com/questions/1271567/how-do-i-replace-accents-german-in-net
        //https://german.stackexchange.com/questions/4992/conversion-table-for-diacritics-e-g-%C3%BC-%E2%86%92-ue

        public static string RemoveDiacritics(this string input)
        {
            if (input != null)
            {
                var normalizedString = input.Normalize(NormalizationForm.FormD);
                var stringBuilder = new StringBuilder();
                for (int i = 0; i < normalizedString.Length; i++)
                {
                    var ch = normalizedString[i];
                    if (CharUnicodeInfo.GetUnicodeCategory(ch) != UnicodeCategory.NonSpacingMark)
                    {
                        stringBuilder.Append(ch);
                    }
                }
                return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
            }
            return null;
        }
        #endregion

        #region Methods: Clean/Remove/Replace line breaks
        /// <summary>
        /// Replaces line breaks (\n) within a given string by Environment.NewLine.
        /// </summary>
        public static string WithCleanLineBreaks(this string input)
        {
            if (input != null)
            {
                input = input.Replace("\n", Environment.NewLine);
                input = input.Replace("\\n", Environment.NewLine);
                return input;
            }
            return null;
        }

        /// <summary>
        /// Removes line breaks (\r\n or \n) within a given string.
        /// </summary>
        public static string RemoveLineBreaks(this string input)
        {
            if (input != null)
            {
                input = input.WithCleanLineBreaks();
                return Regex.Replace(input, @"\r\n?|\n", "");
            }
            return null;
        }

        /// <summary>
        /// Replaces line breaks (\n\n) within a given string by a replacement string.
        /// </summary>
        public static string ReplaceLineBreaks(this string input, string replacement)
        {
            if (input != null)
            {
                input = input.WithCleanLineBreaks();
                return Regex.Replace(input, @"\r\n?|\n", replacement);
                //return input.Replace("\n\n", replacement);
            }
            return null;
        }
        #endregion

        #region Methods: String and Number
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        public static bool TryGetNumber(this string input, out int output)
        {
            output = XLib.MagicNumber.IntZero;
            var result = false;
            if (input != null)
            {
                var pattern = XLib.Regex.NumberPattern;
                var match = Regex.Match(input, pattern);
                if (match.Success)
                {
                    result = int.TryParse(match.Value, out output);
                }
            }
            return result;
        }
        #endregion

        #region Methods: TryConvertToCharArray
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static char[] TryConvertToCharArray(this string input, bool includeEmptyString)
        {
            var result = XLib.Text.Space;
            if (!input.IsEmptyString())
            {
                if (includeEmptyString)
                {
                    result = StringHelper.Concat(new SurroundFormat(result), input);
                }
                else
                {
                    result = input;
                }
            }
            return result.ToCharArray();
        }
        #endregion

        #region Methods: Try ToTitleCase
        /// <summary>
        /// Returns a string in ProperCase.
        /// </summary>
        // Use the current thread's culture info for conversion
        public static string TryToTitleCase(this string input)
        {
            if (input != null)
            {
                var cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                return cultureInfo.TextInfo.ToTitleCase(input.TryToLower());
            }
            return null;
        }

        /// <summary>
        /// Returns a string in ProperCase.
        /// </summary>
        // Overload which uses the culture info with the specified name.
        public static string TryToTitleCase(this string input, string cultureInfoName)
        {
            if (input != null)
            {
                try
                {
                    var cultureInfo = new CultureInfo(cultureInfoName);
                    return cultureInfo.TextInfo.ToTitleCase(input.TryToLower());
                }
                catch (ArgumentNullException)
                {
                    return input.TryToTitleCase();
                }
                catch (CultureNotFoundException)
                {
                    return input.TryToTitleCase();
                }
            }
            return null;
        }

        /// <summary>
        /// Returns a string in ProperCase.
        /// </summary>
        // Overload which uses the specified culture info.
        public static string TryToTitleCase(this string input, CultureInfo cultureInfo)
        {
            if (input != null)
            {
                if (cultureInfo != null)
                {
                    return cultureInfo.TextInfo.ToTitleCase(input.TryToLower());
                }
                else
                {
                    return input.TryToTitleCase();
                }
            }
            return null;
        }
        #endregion

        #region Methods: Try Upper/Lower Case
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string TryToLower(this string input)
        {
            if (input != null)
            {
                return input.Trim().ToLower();
            }
            return null;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string TryToUpper(this string input)
        {
            if (input != null)
            {
                return input.Trim().ToUpper();
            }
            return null;
        }
        #endregion

        #region Methods: TryTrim
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string TryTrim(this string input)
        {
            if (input != null)
            {
                return input.Trim();
            }
            return null;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="input"></param>
        /// <param name="trimChars"></param>
        /// <returns></returns>
        public static string TryTrim(this string input, params char[] trimChars)
        {
            if (input != null)
            {
                return input.Trim(trimChars);
            }
            return null;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="input"></param>
        /// <param name="trimChars"></param>
        /// <returns></returns>
        public static string TryTrimStart(this string input, params char[] trimChars)
        {
            if (input != null)
            {
                return input.TrimStart(trimChars);
            }
            return null;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="input"></param>
        /// <param name="trimChars"></param>
        /// <returns></returns>
        public static string TryTrimEnd(this string input, params char[] trimChars)
        {
            if (input != null)
            {
                return input.TrimEnd(trimChars);
            }
            return null;
        }

        /// <summary>
        /// Remove all leading and trailing occurrences of the specified set of characters.
        /// </summary>
        //[Obsolete]
        public static string TryTrimDigits(this string input, params char[] digits)
        {
            if (input != null)
            {
                //Add an Empty string to set of digits:
                int length = digits.Length;
                char[] trimChars = new char[length + 1];
                for (int i = 0; i < length; i++)
                {
                    trimChars[i] = digits[i];
                }
                trimChars[length] = XLib.Token.Space;
                //Execute Trim:
                input = input.TryTrim(trimChars);
                return input;
            }
            return null;
        }
        #endregion

        #region Methods: TryFormat
        /// <summary>
        /// Replaces the format item in a specified string with the string representation of a corresponding object in a specified array.
        /// <para>Resolves a FormatException or a ArgumentNullException by replacing missing parameters with a {N/A} or {index} string.</para>
        /// </summary>
        /// <param name="input"></param>
        /// <param name="argument0"></param>
        /// <returns></returns>
        public static string TryFormat(this string input, object argument0)
        {
            var args = new object[] { argument0 };
            return input.TryFormat(args);
        }

        /// <summary>
        /// Replaces the format item in a specified string with the string representation of a corresponding object in a specified array.
        /// <para>Resolves a FormatException or a ArgumentNullException by replacing missing parameters with a {N/A} or {index} string.</para>
        /// </summary>
        /// <param name="input"></param>
        /// <param name="argument0"></param>
        /// <param name="argument1"></param>
        /// <returns></returns>
        public static string TryFormat(this string input, object argument0, object argument1)
        {
            var args = new object[] { argument0, argument1 };
            return input.TryFormat(args);
        }

        /// <summary>
        /// Replaces the format item in a specified string with the string representation of a corresponding object in a specified array.
        /// <para>Resolves a FormatException or a ArgumentNullException by replacing missing parameters with a {N/A} or {index} string.</para>
        /// </summary>
        /// <param name="input"></param>
        /// <param name="argument0"></param>
        /// <param name="argument1"></param>
        /// <param name="argument2"></param>
        /// <returns></returns>
        public static string TryFormat(this string input, object argument0, object argument1, object argument2)
        {
            var args = new object[] { argument0, argument1, argument2 };
            return input.TryFormat(args);
        }

        /// <summary>
        /// Replaces the format item in a specified string with the string representation of a corresponding object in a specified array.
        /// <para>Resolves a FormatException or a ArgumentNullException by replacing missing parameters with a {N/A} or {index} string.</para>
        /// </summary>
        /// <param name="input"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string TryFormat(this string input, params object[] args)
        {
            return input.TryFormat(XLib.Preference.LogResolvedExceptions, args);
        }

        /// <summary>
        /// Replaces the format item in a specified string with the string representation of a corresponding object in a specified array.
        /// <para>Resolves a FormatException or a ArgumentNullException by replacing missing parameters with a {N/A} or {index} string.</para>
        /// </summary>
        /// <param name="input"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private static string TryFormat(this string input, bool logResolvedException, params object[] args)
        {
            if (!input.IsEmptyString())
            {
                try
                {
                    //TODO: gives no exception BUT: loop through args to detect null or empty values -> GlobalVar.Strings.NotSpecified
                    return StringHelper.Format(input, args).WithCleanLineBreaks();
                }
                catch (FormatException exception)
                {
                    return input.TryResolveFormatException(exception, logResolvedException, args);
                }
                catch (ArgumentNullException exception)
                {
                    return input.TryResolveFormatException(exception, logResolvedException, args);
                }
            }
            return null;
        }

        /// <summary>
        /// Replaces the format item in a specified string with the string representation of a corresponding object in a specified array.
        /// <para>Evaluates and optimizes the format string when optional indicators are found within the string. (optional: values between "[" and "]")</para>
        /// <para>Resolves a FormatException or a ArgumentNullException by replacing missing parameters with a {N/A} or {index} string.</para>
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string TryFormat(this BaseDisplayFormat format)
        {
            if (format != null && !format.DisplayFormat.IsEmptyString())
            {
                var result = format.DisplayFormat;
                //Evaluate optional values and remove empty placeholders to boost performance and return a clean result:
                var optionalValues = result.Split(XLib.Token.OptionalIndicators, XLib.Token.OptionalStart, false);
                if (optionalValues != null && optionalValues.Length > XLib.MagicNumber.IntZero)
                {
                    result = result
                        .Replace(XLib.Token.OptionalStart.ToString(), String.Empty)
                        .Replace(XLib.Token.OptionalEnd.ToString(), String.Empty);
                    var length = optionalValues.Length;
                    for (int i = 0; i < length; i++)
                    {
                        var isValid = false;
                        var optionalValue = optionalValues[i];
                        var pattern = XLib.Regex.PlaceHolderPattern;
                        var match = Regex.Match(optionalValue, pattern);
                        if (match.Success)
                        {
                            int index;
                            var value = match.Value;
                            var args = format.Arguments;
                            var hasIndex = value.TryGetNumber(out index);
                            if (hasIndex)
                            {
                                isValid = args.HasValidValue(index);
                            }
                        }
                        if (!isValid)
                        {
                            result = result.Replace(optionalValue, String.Empty);
                        }
                    }
                }
                //TryTrim the displayformat when optional values are included:
                if (format.TrimDigits != null && format.TrimDigits.Length > XLib.MagicNumber.IntZero)
                {
                    result = result.TryTrim(format.TrimDigits);
                }
                //TryFormat the final result:
                return result.TryFormat(format.LogResolvedException, format.Arguments);
            }
            return null;
        }

        /// <summary>
        /// Resolves a FormatException or a ArgumentNullException by replacing missing parameters with a {N/A} or {index} string.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="exception"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private static string TryResolveFormatException(this string input, Exception exception, bool logResolvedException, params object[] args)
        {
            var pattern = XLib.Regex.PlaceHolderPattern;
            var matches = Regex.Matches(input, pattern);
            var length = matches.OfType<Match>().Select(m => m.Value).Distinct().Count();
            var newArgs = new object[length];
            for (int i = 0; i < length; i++)
            {
                if (args == null || (args != null && args.Length <= i))
                {
                    newArgs[i] = new object();
                    newArgs[i] = XLib.Text.NotSpecifiedText;
                }
                else
                {
                    newArgs[i] = args[i];
                }
            }
            var originalInput = input;
            input = input.TryResolveFormatException(newArgs);
            Regex.Replace(input, pattern, XLib.Text.NotSpecifiedText);
            //Log the exception:
            if (logResolvedException)
            {
                var method = MethodBase.GetCurrentMethod() as MethodInfo;
                var assembly = method.ReflectedType.Assembly;
                var info = StringHelper.ArrowStyle(originalInput, input);
                //TODO: interceptor
                var information = new LogInformationBase(null, assembly, method, info);
                var entry = new LogEntryBase(LogLevel.Debug, information, exception);
                LogToFile.Log(entry, logResolvedException);
            }
            return input.WithCleanLineBreaks();
        }

        /// <summary>
        /// Try format a string.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private static string TryResolveFormatException(this string input, params object[] args)
        {
            try
            {
                input = StringHelper.Format(input, args);
                return input;
            }
            catch (FormatException)
            {
                return input;
            }
        }
        #endregion
    }
}
