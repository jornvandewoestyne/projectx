﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;

//TODO: fix hard-coded strings...

namespace ProjectX.General
{
    public static class SettingsHelper
    {
        #region Methods
        /// <summary>
        /// Sets the DbSettings of the application.
        /// </summary>
        public static bool UpdateDbSettings(string setting, string file)
        {
            var section = "appSettings";
            var attribute = "DbSettings";
            var key = "key";
            var value = "value";
            if (file == null) { return false; }
            var document = XDocument.Load(file);
            var elements = document.Descendants(section).Elements();
            var element = elements.First(x => x.Attribute(key).Value == attribute);
            if (element != null)
            {
                element.Attribute(value).Value = setting;
            }
            else { return false; }
            document.Save(file);
            ConfigurationManager.RefreshSection(section);
            return true;
        }

        /// <summary>
        /// Gets a list of all available Connectionstrings from the application config file.
        /// </summary>
        public static List<ConnectionStringSettings> GetConnectionStrings()
        {
            var list = new List<ConnectionStringSettings>();
            foreach (ConnectionStringSettings item in ConfigurationManager.ConnectionStrings)
            {
                list.Add(item);
            }
            return list;
        }
        #endregion
    }
}
