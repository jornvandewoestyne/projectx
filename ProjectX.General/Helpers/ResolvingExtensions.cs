﻿using System;
using System.Collections.Generic;

namespace ProjectX.General
{
    public static class ResolvingExtensions
    {
        #region Methods
        /// <summary>
        /// (IResolvable) Avoids a NullReferenceException when accessing the given class member(s).
        /// </summary>
        /// <typeparam name="T">IResolvable</typeparam>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static T ResolveNull<T>(this T instance) where T : IResolvable, new()
        {
            return instance == null ? new T() : instance;
        }

        /// <summary>
        /// Avoids a NullReferenceException when accessing the given class member(s). 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static string ResolveNull(this string instance)
        {
            return instance ?? String.Empty;
        }

        /// <summary>
        /// Avoids a NullReferenceException when accessing the given class member(s). 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static IEnumerable<T> ResolveNull<T>(this IEnumerable<T> instance) where T : struct
        {
            return instance ?? new List<T>();
        }

        /// <summary>
        /// Avoids a NullReferenceException when accessing the given class member(s). 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static IKeySet<T> ResolveNull<T>(this IKeySet<T> instance) where T : struct
        {
            return instance ?? new List<T>().AsKeySet<T>(null);
        }

        /// <summary>
        /// Avoids a NullReferenceException when accessing the given class member(s). 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static HashSet<T> ResolveNull<T>(this HashSet<T> instance) where T : struct
        {
            return instance ?? new HashSet<T>();
        }

        /// <summary>
        /// Avoids a NullReferenceException when accessing the given class member(s). 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static IList<T> ResolveNull<T>(this IList<T> instance) where T : class
        {
            return instance ?? new List<T>();
        }

        /// <summary>
        /// Avoids a NullReferenceException when accessing the given class member(s). 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static Progress<T> ResolveNull<T>(this Progress<T> instance) where T : class
        {
            return instance ?? new Progress<T>();
        }
        #endregion
    }
}
