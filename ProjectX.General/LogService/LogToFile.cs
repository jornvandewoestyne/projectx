﻿using System;
using System.IO;


//TODO: LogInformation!!!!
//TODO: name of logfile to settings!
//TODO: setting activate log?

namespace ProjectX.General.LogService
{
    internal static class LogToFile
    {
        #region Methods:
        public static void Log(LogEntryBase entry, bool log)
        {
            //TODO: enable
            if (XLib.Preference.LogResolvedExceptions && log)
            {
                Stream stream = null;
                try
                {
                    //TODO: test!
                    //var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    //TODO: XString
                    var file = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), AppTitle.FolderName, "Log", String.Concat("LogFile.", entry.FriendlyExceptionInfo.Type, ".log"));
                    Directory.CreateDirectory(Path.GetDirectoryName(file));
                    //using (var fileStream = new FileStream(String.Concat(path, "\\LogFile.", entry.Exception.GetType(), ".log"), FileMode.Append))
                    stream = new FileStream(file, FileMode.Append);
                    using (var writer = new StreamWriter(stream))
                    {
                        stream = null;
                        var value = StringHelper.Concat(new SurroundFormat(XLib.Text.VerticalEnumSeparator, null), entry);
                        writer.WriteLine(value);
                    }
                }
                //catch (Exception)
                //{
                //    //TODO: do some stuff...?
                //}
                finally
                {
                    if (stream != null)
                    {
                        stream.Dispose();
                    }
                }
            }
        }
        #endregion
    }
}
