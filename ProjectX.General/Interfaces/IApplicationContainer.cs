﻿namespace ProjectX.General
{
    public interface IApplicationContainer
    {
        #region Properties
        bool IsReady { get; set; }
        AppTitle AppTitle { get; }
        #endregion
    }
}
