﻿using ProjectX.General.Localization;
using System.Threading;
using System.Threading.Tasks;

//TODO: summary

namespace ProjectX.General
{
    /// <summary>
    /// ...
    /// </summary>
    public interface ITranslator
    {
        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="location"></param>
        /// <param name="resources"></param>
        void GetResources(string location, params CustomResource[] resources);

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="location"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="resources"></param>
        /// <returns></returns>
        Task GetResourcesAsync(string location, CancellationToken cancellationToken, params CustomResource[] resources);

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="key"></param>
        /// <param name="language"></param>
        /// <param name="translateArguments"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        Translation TryTanslate(string key, SupportedLanguage language, bool translateArguments, params object[] args);
        #endregion
    }
}
