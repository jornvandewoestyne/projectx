﻿namespace ProjectX.General
{
    public interface ISearchEntity : IBaseEntity
    {
        #region Properties
        int ID { get; set; }
        string SearchString { get; set; }
        #endregion
    }
}
