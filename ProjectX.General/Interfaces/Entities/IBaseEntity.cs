﻿namespace ProjectX.General
{
    public interface IBaseEntity
    {
        #region Properties
        object this[string propertyName] { get; }
        SupportedLanguage CurrentLanguage { get; }
        #endregion

        #region Methods
        object GetValueOf(string propertyName);
        #endregion
    }
}


//TODO: -> EntityState! https://blog.magnusmontin.net/2013/05/30/generic-dal-using-entity-framework/comment-page-2/