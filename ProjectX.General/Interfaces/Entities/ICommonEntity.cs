﻿

namespace ProjectX.General
{
    public interface ICommonEntity : IBaseEntity
    {
        object[] TitleProperties { get; }

        BaseEntitySummary EntitySummary { get; }
    }
}
