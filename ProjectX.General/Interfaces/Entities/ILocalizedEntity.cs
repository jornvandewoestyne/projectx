﻿namespace ProjectX.General
{
    public interface ILocalizedEntity : IBaseEntity //ICommonEntity
    {
        /// <summary>
        /// ...
        /// </summary>
        string KeyName { get; set; }

        /// <summary>
        /// Returns the localized name of the application's current language.
        /// </summary>
        /// <returns></returns>
        string Name { get; }

        /// <summary>
        /// Returns the localized name of the entity's current language.
        /// </summary>
        /// <returns></returns>
        string CurrentName { get; }
    }
}
