﻿
namespace ProjectX.General
{
    /// <summary>
    /// ATTENTION: ensure to make a public(!) default parameterless constructor, used for resolving the null value class!
    /// </summary>
    public interface IResolvable
    { }
}
