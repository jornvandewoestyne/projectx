﻿
namespace ProjectX.General
{
    /// <summary>
    /// Avoiding TypeLoadException on a generic <see cref="ICollectionPage{TEntity};"/>.
    /// <para>At the UI presentation level, cast the CollectionPage to the desired return type.</para>
    /// USAGE: result = <see cref="ICollectionPage.CollectionPage;"/> as <see cref="ICollectionPage{TEntity};"/>;
    /// </summary>
    public interface ICollectionPage
    {
        /// <summary>
        /// Please CAST me to a desired <see cref="ICollectionPage{TEntity};"/>!
        /// </summary>
        object CollectionPage { get; }
    }
}
