﻿using System.Collections.Generic;

namespace ProjectX.General
{
    public interface IKeySet<T> where T : struct
    {
        #region Properties
        /// <summary>
        /// A list of items.
        /// </summary>
        HashSet<T> Items { get; }

        /// <summary>
        /// A list of notifications.
        /// </summary>
        IList<Notification> Notifications { get; }

        /// <summary>
        /// ...
        /// </summary>
        bool HasNotifications { get; }

        /// <summary>
        /// ...
        /// </summary>
        bool HasItems { get; }
        #endregion    
    }
}
