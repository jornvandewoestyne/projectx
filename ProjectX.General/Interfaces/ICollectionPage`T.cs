﻿using System;
using System.Collections.Generic;

namespace ProjectX.General
{
    /// <summary>
    /// Holds relevant information related to a page of a collection of information.
    /// </summary>
    public interface ICollectionPage<TEntity> : IDisposable where TEntity : BaseEntity
    {
        #region Properties
        /// <summary>
        /// A page of items.
        /// </summary>
        IList<object> Items { get; }

        /// <summary>
        /// A page of items.
        /// </summary>
        IEnumerable<TEntity> EntityItems { get; }

        /// <summary>
        /// ...
        /// </summary>
        IList<PropertySchema> EntityPropertySchemas { get; }

        /// <summary>
        /// ...
        /// </summary>
        IList<Notification> Notifications { get; }

        /// <summary>
        /// Number of items returned, regardless of page.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Total number of items, regardless of page.
        /// </summary>
        int TotalItems { get; }

        /// <summary>
        /// The number of items that should be shown per page.
        /// </summary>
        int ItemsPerPage { get; }

        /// <summary>
        /// ...
        /// </summary>
        int TotalPages { get; }
        #endregion
    }
}
