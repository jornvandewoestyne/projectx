﻿namespace ProjectX.General
{
    public interface INotification
    {
        bool IsValid { get; }
        FriendlyMessageBase Information { get; }
    }
}
