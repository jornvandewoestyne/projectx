﻿//TODO: summary

namespace ProjectX.General
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class Notification : FriendlyMessageBase
    {
        #region Constructors
        public Notification(FriendlyMessageType enumType, bool translateArguments, params object[] args)
            : base(enumType, true, args)
        {
            this.SetMessage(translateArguments, args);
        }

        //internal Notification(FriendlyProgressType enumType, params object[] args)
        //    : base(enumType, args)
        //{
        //    this.SetMessage(args);
        //}
        #endregion

        #region Methods
        protected sealed override void SetMessage(bool translateArguments, object[] args)
        {
            this.Message = StringHelper.Translation(this.Name, XLib.Preference.CurrentLanguage, translateArguments, args);
        }
        #endregion
    }
}