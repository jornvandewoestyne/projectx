﻿namespace ProjectX.General
{
    public sealed class FullCompanyNameDisplayFormat : BaseDisplayFormat
    {
        #region Properties
        public string CompanyName { get; }
        public string CompanyCorporationTypeName { get; }
        #endregion

        #region Constructors
        public FullCompanyNameDisplayFormat(DisplayFormatSetting setting, string companyName, string companyCorporationTypeName)
            : base(setting)
        {
            this.CompanyName = companyName.TryTrim();
            this.CompanyCorporationTypeName = companyCorporationTypeName.TryTrim();
            this.SetArguments();
        }
        #endregion

        #region Methods
        protected sealed override void SetArguments()
        {
            this.Arguments = new string[] { this.CompanyName, this.CompanyCorporationTypeName };
        }
        #endregion
    }
}
