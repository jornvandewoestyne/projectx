﻿namespace ProjectX.General
{
    public sealed class FullPersonNameDisplayFormat : BaseDisplayFormat
    {
        #region Properties
        public string FirstName { get; }
        public string LastName { get; }
        #endregion

        #region Constructors
        public FullPersonNameDisplayFormat(DisplayFormatSetting setting, string firstName, string lastName)
            : base(setting)
        {
            this.FirstName = firstName.TryTrim();
            this.LastName = lastName.TryTrim();
            this.SetArguments();
        }
        #endregion

        #region Methods
        protected sealed override void SetArguments()
        {
            this.Arguments = new string[] { this.FirstName, this.LastName };
        }
        #endregion
    }
}

