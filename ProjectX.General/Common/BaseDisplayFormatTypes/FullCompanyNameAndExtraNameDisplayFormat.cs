﻿namespace ProjectX.General
{
    public sealed class FullCompanyNameAndExtraNameDisplayFormat : BaseDisplayFormat
    {
        #region Properties
        public string FullName { get; }
        public string ExtraName { get; }
        #endregion

        #region Constructors
        public FullCompanyNameAndExtraNameDisplayFormat(DisplayFormatSetting setting, string fullName, string extraName)
            : base(setting)
        {
            this.FullName = fullName.TryTrim();
            this.ExtraName = extraName.TryTrim();
            this.SetArguments();
        }
        #endregion

        #region Methods
        protected sealed override void SetArguments()
        {
            this.Arguments = new string[] { this.FullName, this.ExtraName };
        }
        #endregion
    }
}

