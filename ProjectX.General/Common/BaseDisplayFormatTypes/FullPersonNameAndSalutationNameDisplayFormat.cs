﻿namespace ProjectX.General
{
    public sealed class FullPersonNameAndSalutationNameDisplayFormat : BaseDisplayFormat
    {
        #region Properties
        public string SalutationName { get; }
        public string FirstName { get; }
        public string LastName { get; }
        #endregion

        #region Constructors
        public FullPersonNameAndSalutationNameDisplayFormat(DisplayFormatSetting setting, string salutationName, string firstName, string lastName)
            : base(setting)
        {
            this.SalutationName = salutationName.TryTrim();
            this.FirstName = firstName.TryTrim();
            this.LastName = lastName.TryTrim();
            this.SetArguments();
        }
        #endregion

        #region Methods
        protected sealed override void SetArguments()
        {
            this.Arguments = new string[] { this.SalutationName, this.FirstName, this.LastName };
        }
        #endregion
    }
}


