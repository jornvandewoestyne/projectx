﻿namespace ProjectX.General
{
    public sealed class PartnerTitleDisplayFormat : BaseDisplayFormat
    {
        #region Properties
        public string PartnerCode { get; }
        public string FullName { get; }
        #endregion

        #region Constructors
        public PartnerTitleDisplayFormat(DisplayFormatSetting setting, string partnerCode, string fullName)
            : base(setting)
        {
            this.PartnerCode = partnerCode.TryTrim();
            this.FullName = fullName.TryTrim();
            this.SetArguments();
        }
        #endregion

        #region Methods
        protected sealed override void SetArguments()
        {
            this.Arguments = new string[] { this.PartnerCode, this.FullName };
        }
        #endregion
    }
}


