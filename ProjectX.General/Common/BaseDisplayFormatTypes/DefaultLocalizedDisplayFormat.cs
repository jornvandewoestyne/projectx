﻿using ProjectX.General.Attributes;
using ProjectX.General.Localization;

namespace ProjectX.General
{
    public sealed class DefaultLocalizedDisplayFormat : BaseDisplayFormat
    {
        #region Properties
        public string Key { get; }

        [Optional]
        public string Value { get; private set; }
        #endregion

        #region Constructors
        public DefaultLocalizedDisplayFormat(DisplayFormatSetting setting, Translation localization)
            : base(setting)
        {
            this.Key = localization.Key;
            this.Value = localization.Value;
            this.SetArguments();
        }
        #endregion

        #region Methods
        protected sealed override void SetArguments()
        {
            if (this.Value == this.Key)
            {
                this.Value = null;
            }
            this.Arguments = new string[] { this.Key, this.Value };
        }
        #endregion
    }
}
