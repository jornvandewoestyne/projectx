﻿//TODO: attribute to mark as optional property

namespace ProjectX.General
{
    public sealed class FullAddressDisplayFormat : BaseDisplayFormat
    {
        #region Properties
        public string Street { get; }
        public string HouseNumber { get; }
        public string BoxNumber { get; private set; }
        #endregion

        #region Constructors
        public FullAddressDisplayFormat(DisplayFormatSetting addressSetting, DisplayFormatSetting boxNumberSetting, string street, string houseNumber, string boxNumber)
            : base(addressSetting)
        {
            this.Street = street.TryTrim();
            this.HouseNumber = houseNumber.TryTrim();
            this.SetBoxNumber(boxNumberSetting, boxNumber);
            this.SetArguments();
        }
        #endregion

        #region Methods
        private void SetBoxNumber(DisplayFormatSetting setting, string boxNumber)
        {
            if (!boxNumber.IsEmptyString())
            {
                this.BoxNumber = new FullBoxNumberDisplayFormat(setting, boxNumber).ToString();
            }
        }

        protected sealed override void SetArguments()
        {
            this.Arguments = new string[] { this.Street, this.HouseNumber, this.BoxNumber };
        }
        #endregion
    }
}


//try: http://stackoverflow.com/questions/6219454/efficient-way-to-remove-all-whitespace-from-string
//.RemoveWhiteSpace();

//TODO: http://stackoverflow.com/questions/356464/localization-of-displaynameattribute

//class LocalizedDisplayNameAttribute : DisplayNameAttribute
//{
//    private readonly string resourceName;
//    public LocalizedDisplayNameAttribute(string resourceName)
//        : base()
//    {
//        this.resourceName = resourceName;
//    }

//    public override string DisplayName
//    {
//        get
//        {
//            return Resources.ResourceManager.GetString(this.resourceName);
//        }
//    }
//}

//[LocalizedDisplayName(ResourceStrings.MyPropertyName)]
//public string MyProperty
//{
//  get
//  {
//    ...
//  }
//}

//public static class ResourceStrings
//{
//    public const string ForegroundColorDisplayName = "ForegroundColorDisplayName";
//    public const string FontSizeDisplayName = "FontSizeDisplayName";
//}

//When I try this approach, I get an error message saying "An attribute argument must be a constant expression, 
//typeof expression or array creation expression of an attribute parameter type. 
//However, passing the value to LocalizedDisplayName as a string works. Wish it would be strongly typed.