﻿using System.ComponentModel.DataAnnotations;

namespace ProjectX.General
{
    public abstract class BaseDisplayFormat : BaseClass
    {
        #region Properties
        [Required]
        public string DisplayFormat { get; }

        [Required]
        public char[] TrimDigits { get; }

        [Required]
        public bool LogResolvedException { get; }

        [Required]
        public string[] Arguments { get; protected set; }
        #endregion

        #region Constructors
        internal BaseDisplayFormat(DisplayFormatSetting setting)
        {
            this.DisplayFormat = setting.DisplayFormat;
            this.TrimDigits = setting.TrimDigits.TryConvertToCharArray(true);
            this.LogResolvedException = setting.LogResolvedException;
        }
        #endregion

        #region Methods
        protected abstract void SetArguments();

        public sealed override string ToString()
        {
            return this.TryFormat();
        }
        #endregion
    }
}