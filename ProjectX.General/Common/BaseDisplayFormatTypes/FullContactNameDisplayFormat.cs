﻿namespace ProjectX.General
{
    public sealed class FullContactNameDisplayFormat : BaseDisplayFormat
    {
        #region Properties
        public string CompanyName { get; }
        public string PersonName { get; }
        #endregion

        #region Constructors
        public FullContactNameDisplayFormat(DisplayFormatSetting setting, string companyName, string personName)
            : base(setting)
        {
            this.CompanyName = companyName.TryTrim();
            this.PersonName = personName.TryTrim();
            this.SetArguments();
        }
        #endregion

        #region Methods
        protected sealed override void SetArguments()
        {
            this.Arguments = new string[] { this.CompanyName, this.PersonName };
        }
        #endregion
    }
}
