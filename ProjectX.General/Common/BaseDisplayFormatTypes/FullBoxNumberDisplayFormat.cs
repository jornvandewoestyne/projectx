﻿namespace ProjectX.General
{
    public sealed class FullBoxNumberDisplayFormat : BaseDisplayFormat
    {
        #region Properties
        public string Prefix { get; private set; }
        public string BoxNumber { get; }
        #endregion

        #region Constructors
        public FullBoxNumberDisplayFormat(DisplayFormatSetting setting, string boxNumber)
            : base(setting)
        {
            this.BoxNumber = boxNumber.TryTrim();
            this.SetPrefix();
            this.SetArguments();
        }
        #endregion

        #region Methods
        private void SetPrefix()
        {
            if (!this.BoxNumber.IsEmptyString())
            {
                this.Prefix = XLib.Text.BoxNumber.TryToLower();
            }
        }
        protected sealed override void SetArguments()
        {
            this.Arguments = new string[] { this.Prefix, this.BoxNumber };
        }
        #endregion
    }
}
