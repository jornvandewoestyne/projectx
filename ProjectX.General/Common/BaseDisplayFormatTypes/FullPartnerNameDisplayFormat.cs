﻿namespace ProjectX.General
{
    public sealed class FullPartnerNameDisplayFormat : BaseDisplayFormat
    {
        #region Properties
        public string CompanyName { get; private set; }
        public string PersonName { get; private set; }
        #endregion

        #region Constructors
        public FullPartnerNameDisplayFormat(DisplayFormatSetting setting, string companyName, string personName)
            : base(setting)
        {
            this.CompanyName = companyName.TryTrim();
            this.PersonName = personName.TryTrim();
            this.SetArguments();
        }
        #endregion

        #region Methods
        protected override void SetArguments()
        {
            this.Arguments = new string[] { this.CompanyName, this.PersonName };
        }
        #endregion
    }
}

