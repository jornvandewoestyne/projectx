﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

//TODO: clean...

namespace ProjectX.General.BaseSearchStringTypes.Helpers
{
    /// <summary>
    /// ...
    /// </summary>
    internal static class SearchStringExtensions
    {
        /// <summary>
        /// Separates a given string into an array of included (index = 0) and excluded (index = 1) keywords.
        /// </summary>
        public static string[] SplitToIncludedAndExcluded(this string input, bool isExclusive = true)
        {
            if (input != null)
            {
                //Initialize some values:
                var or = XLib.Token.Or;
                var not = XLib.Token.Not;
                var delimiters = XLib.Token.Delimeters;
                //Split the input to get the excluded keywords:
                string exclude = input.SubstringExclusive(delimiters, not, isExclusive);
                //Remove the excluded keywords from the input string:
                if (!exclude.IsEmptyString())
                {
                    input = input.Replace(exclude, XLib.Text.Space);
                    exclude = exclude.TryTrim(not).Replace(not, or);
                }
                //Return an array of both included and excluded keywords:
                return new string[] { input.TryTrim(), exclude.TryTrim() };
            }
            return null;
        }

        /// <summary>
        /// Remove/Cleans chars from a given string (specific for MultiSearch strings).
        /// </summary>
        public static string ReplaceCharsToStringConventionsMultiSearch(this string input)
        {
            if (input != null)
            {
                //Clean up input:
                input = input
                        .RemoveLineBreaks()
                        .RemoveWhiteSpace()
                        .TryTrim(XLib.Token.DigitsWithoutNot)
                        .TryTrimEnd(XLib.Token.Digits)
                        .CleanSubStringOfExcludedDigits()
                        .CleanSubStringOfDigits(XLib.Token.Delimeters)
                        .FormatMultiSearch();
                return input;
            }
            return null;
        }

        /// <summary>
        /// Remove/Cleans chars from a given string and enables 'SplitBySpace' (specific for MultiSearch strings).
        /// </summary>
        public static string ReplaceCharsToStringConventionsMultiSearch(this string input, bool splitBySpace)
        {
            //Normalize the input:
            //input = input.ReplaceCharsToStringConventionsMultiSearch();
            //Use 'space' as delimeter when set as preference:
            if (input != null && splitBySpace)
            {
                //TODO: XString
                var stringBuilder = new StringBuilder(input);
                stringBuilder.Replace(StringHelper.Format(" {0} ", XLib.Token.Or), XLib.Token.Or.ToString());     //Or = '+'
                stringBuilder.Replace(StringHelper.Format(" {0} ", XLib.Token.Not), XLib.Token.Not.ToString());   //Not = '!'
                stringBuilder.Replace(XLib.Text.Space, XLib.Token.And.ToString());
                input = stringBuilder.ToString();
            }
            return input;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static string CleanSubStringOfExcludedDigits(this string input)
        {
            if (input != null)
            {
                var array = input.Split(XLib.Token.Not);
                var length = array.Length;
                for (int i = 0; i < length; i++)
                {
                    array[i] = array[i].TryTrimEnd(XLib.Token.DigitsWithoutNot);
                }
                return array.TryArrayToString<string>(XLib.Token.Not.ToString());
            }
            return null;
        }

        /// <summary>
        /// (private) Format string from a given string (specific for MultiSearch strings).
        /// </summary>
        private static string FormatMultiSearch(this string input)
        {
            var or = XLib.Token.Or.ToString();
            var and = XLib.Token.And.ToString();
            var not = XLib.Token.Not.ToString();
            var stringBuilder = new StringBuilder(input);
            stringBuilder.Replace(and, StringHelper.Format("{0} ", and));                               //And = ','
            stringBuilder.Replace(StringHelper.Format(" {0}", and), StringHelper.Format("{0} ", and));  //And = ','
            stringBuilder.Replace(or, StringHelper.Format(" {0} ", or));                                //Or = '+'
            stringBuilder.Replace(not, StringHelper.Format(" {0} ", not));                              //Not = '!'
            return stringBuilder.ToString().RemoveWhiteSpace().TryTrim();
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static string[] Optimize(this string[] array)
        {
            var length = array.Length;
            for (int i = 0; i < length; i++)
            {
                var keysOr = array[i].Split(XLib.Token.Or, true).ToList();
                var newKeysOr = new List<string>();
                foreach (var keyOr in keysOr)
                {
                    if (!keyOr.IsEmptyString())
                    {
                        var keysAnd = keyOr.Split(XLib.Token.And, true).Distinct().OrderBy(x => x.Length).ThenBy(x => x).ToList();
                        var newKeysAnd = new List<string>(keysAnd);
                        foreach (var keyAnd in keysAnd)
                        {
                            var filter = newKeysAnd.Where(x => x.Contains(keyAnd));
                            if (filter.Count() > XLib.MagicNumber.IntOne)
                            {
                                newKeysAnd.Remove(keyAnd);
                            }
                        }
                        //Reorder (descending) the keywords to optimize the search:
                        var item = newKeysAnd.OrderByDescending(x => x.Length).ThenBy(x => x).ToArray()
                            .TryArrayToString<string>(XLib.Token.And.ToString());
                        newKeysOr.Add(item);
                    }
                }
                array[i] = newKeysOr.Distinct().ToArray()
                    .TryArrayToString<string>(XLib.Token.Or.ToString());
            }
            return array;
        }
    }
}
