﻿using ProjectX.General.BaseSearchStringTypes.Helpers;
using System;

//TODO: add function for returning emptylist

namespace ProjectX.General
{
    public sealed class SearchString : BaseClass
    {
        #region Fields
        private readonly char _not = XLib.Token.Not;
        private readonly char[] _delimeters = XLib.Token.Delimeters;
        private readonly bool _splitBySpace = XLib.Preference.SplitBySpace;
        #endregion

        #region Properties
        public string Input { get; private set; }
        public string SearchValue { get; private set; }
        public BreakUp BreakUp { get; private set; }
        //TODO: make global?
        public bool IsEmpty => this.Input.IsEmptyString();
        public char[] Delimeters => this._delimeters;
        public bool SplitBySpace => this._splitBySpace;
        private char Not => this._not;
        #endregion

        #region Constructor
        internal SearchString(string input)
        {
            this.Initialize(input);
        }
        #endregion

        #region Methods
        private void Initialize(string input)
        {
            if (!input.IsEmptyString())
            {
                this.Input = input.ReplaceCharsToStringConventionsMultiSearch();
                this.BreakUp = new BreakUp(this);
                this.SearchValue = this.BreakUp.Array.TryArrayToString<string>(this.Not.ToString());
            }
        }

        public override string ToString()
        {
            if (this != null)
            {
                return this.Input;
            }
            return String.Empty;
        }
        #endregion
    }
}
