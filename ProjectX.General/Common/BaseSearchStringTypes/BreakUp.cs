﻿using ProjectX.General.BaseSearchStringTypes.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace ProjectX.General
{
    public sealed class BreakUp
    {
        #region Properties
        public string[] Array { get; private set; }
        public string Array_IncludedKeyWords { get; private set; }
        public string Array_ExcludedKeyWords { get; private set; }
        public List<string> KeyWords { get; private set; }
        #endregion

        #region Constructor
        internal BreakUp(SearchString searchString)
        {
            this.Initialize(searchString);
        }
        #endregion

        #region Methods
        private void Initialize(SearchString searchString)
        {
            if (searchString != null)
            {
                this.Array = searchString
                    .ToString()
                    .ReplaceCharsToStringConventionsMultiSearch(searchString.SplitBySpace)
                    .RemoveDiacritics()
                    .SplitToIncludedAndExcluded()
                    .Optimize();
                if (this.Array != null)
                {
                    this.Array_IncludedKeyWords = this.Array[(int)BreakUpIndex.Included];
                    this.Array_ExcludedKeyWords = this.Array[(int)BreakUpIndex.Excluded];
                    this.KeyWords = this.Array
                        .TryArrayToString<string>()
                        .Split(searchString.Delimeters, true)
                        .ToList();
                }
            }
        }
        #endregion
    }
}
