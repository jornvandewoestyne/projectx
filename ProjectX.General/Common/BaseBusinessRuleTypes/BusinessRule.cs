﻿using ProjectX.General.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

//TODO: summary...
//Use PropertyHelper replacing trygetproperty

//TODO: Language

namespace ProjectX.General
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class BusinessRule : BaseClass
    {
        #region Properties
        [Required]
        public string[] Arguments { get; private set; }
        #endregion

        #region Constructors
        public BusinessRule(IBaseEntity entity, string propertyName, SupportedLanguage language, bool translate)
        {
            this.Initialize(entity, propertyName, language, translate);
        }
        #endregion

        #region Methods
        private void Initialize(IBaseEntity entity, string propertyName, SupportedLanguage language, bool translate)
        {
            if (entity != null)
            {
                var type = entity.GetType();
                var cachedProperty = PropertyCache.GetCachedProperty(type, propertyName);
                if (cachedProperty != null)
                {
                    var components = cachedProperty?.ComposedOf?.ComponentNames;
                    if (components != null && components.Length > XLib.MagicNumber.IntZero)
                    {
                        var length = components.Length;
                        var args = new string[length];
                        var separator = components[XLib.MagicNumber.Index0].Separator;
                        for (int i = 0; i < length; i++)
                        {
                            var item = components[i];
                            var format = item.Format;
                            propertyName = item.Member;
                            var value = entity.GetValueOf(propertyName)?.ToString();
                            if (translate)
                            {
                                value = StringHelper.Translation(value, language);
                            }
                            args[i] = format != null ? format.Value(value, propertyName, language, translate) : value;
                        }
                        this.Arguments = new string[] { StringHelper.Join(separator, args) };
                    }
                }
            }
        }

        public override string ToString()
        {
            return StringHelper.Sentence(this.Arguments);
        }
        #endregion

        #region Attributes
        /// <summary>
        /// ...
        /// </summary>
        [AttributeUsage(AttributeTargets.Property)]
        public sealed class ReadOnlyOrderingKeyAttribute : Attribute
        { }

        /// <summary>
        /// Use the specified separator between each member when concatenating the given members.
        /// </summary>
        [AttributeUsage(AttributeTargets.Property)]
        public sealed class UseSeparatorAttribute : Attribute
        {
            public string Value { get; }

            public UseSeparatorAttribute(string separator = XLib.Text.Separator)
            {
                this.Value = separator;
            }
        }

        /// <summary>
        /// ...
        /// </summary>
        [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
        public sealed class FormatAttribute : Attribute
        {
            public string PropertyName { get; }
            public FriendlyFormatType Format { get; }

            public FormatAttribute(FriendlyFormatType format, string propertyName)
            {
                this.PropertyName = propertyName;
                this.Format = format;
            }

            //TODO: Language
            public string Value(string value, string propertyName, SupportedLanguage language, bool translate)
            {
                if (!value.IsEmptyString())
                {
                    var translation = new Translation(this.Format, language, translate, value);
                    if (translation.IsFound)
                    {
                        return translation.Value;
                    }
                    else
                    {
                        value = StringHelper.Sentence(propertyName.TryToLower(), value);
                    }
                }
                return value;
            }

            //TODO: implement??
            //public string Value(IBaseEntity entity, params string[] propertyNames)
            //{
            //    var hasValues = false;
            //    if (propertyNames != null && propertyNames.Length > XLib.Number.IntZero)
            //    {
            //        var length = propertyNames.Length;
            //        var args = new string[length];
            //        for (int i = 0; i < length; i++)
            //        {
            //            var propertyName = propertyNames[i];
            //            var value = entity.GetValueOf(propertyName)?.ToString();
            //            args[i] = value;
            //            if (!hasValues && !value.IsEmptyString())
            //            {
            //                hasValues = true;
            //            }
            //        }
            //        if (hasValues)
            //        {
            //            return StringHelper.Translation(this.Format, args);
            //        }
            //    }
            //    return String.Empty;
            //}

            //ToUpper, ToLower???
        }

        /// <summary>
        /// ...
        /// </summary>
        [AttributeUsage(AttributeTargets.Property)]
        public sealed class ComposedOfAttribute : Attribute
        {
            #region Properties
            public Type Type { get; private set; }
            public string Separator { get; private set; } = XLib.Text.Space;
            public string PropertyName { get; private set; }
            private string[] Members { get; set; }
            public string[] MemberNames { get; private set; }
            public string[] OrderingNames { get; private set; }
            public FormatAttribute[] Formats { get; private set; }
            public ComposefOfGroup[] ComponentNames { get; private set; }
            #endregion

            #region Constructors
            public ComposedOfAttribute(string propertyName, Type type, params string[] members)
            {
                this.Initialize(propertyName, type, members);
            }
            #endregion

            #region Methods
            private void Initialize(string propertyName, Type type, params string[] members)
            {
                if (type != null && members != null)
                {
                    this.Type = type;
                    this.PropertyName = propertyName;
                    this.SetSeparator(type, propertyName);
                    this.SetFormat(type, propertyName);
                    this.Members = members;
                    this.TryGetMemberNames();
                    this.TryGetNames();
                }
            }

            private void SetSeparator(Type type, string propertyName)
            {
                if (type != null)
                {
                    var proceed = type.TryGetProperty(propertyName, out var property);
                    if (proceed)
                    {
                        var attribute = property.GetCustomAttribute<UseSeparatorAttribute>();
                        if (attribute != null)
                        {
                            this.Separator = attribute.Value;
                        }
                    }
                }
            }

            private void SetFormat(Type type, string propertyName)
            {
                if (type != null)
                {
                    var proceed = type.TryGetProperty(propertyName, out var property);
                    if (proceed)
                    {
                        var attributes = property.GetCustomAttributes<FormatAttribute>();
                        if (attributes != null)
                        {
                            this.Formats = attributes.ToArray();
                        }
                    }
                }
            }

            /// <summary>
            /// ...
            /// </summary>
            /// <returns></returns>
            private void TryGetMemberNames()
            {
                var names = new List<string>();
                if (this.Type != null && this.Members != null)
                {
                    var type = this.Type;
                    var members = this.Members;
                    var length = members.Length;
                    for (int i = 0; i < length; i++)
                    {
                        var member = members[i];
                        var proceed = type.TryGetProperty(member, out var property);
                        if (proceed)
                        {
                            names.Add(property.Name);
                        }
                    }
                }
                this.MemberNames = names.ToArray();
            }

            /// <summary>
            /// ...
            /// </summary>
            /// <returns></returns>
            private void TryGetNames()
            {
                var names = new List<string>();
                var components = new List<ComposefOfGroup>();
                if (this.Type != null && this.Members != null)
                {
                    var members = this.Members;
                    var length = members.Length;
                    for (int i = 0; i < length; i++)
                    {
                        var member = members[i];
                        var validOrderingNames = this.TryGetAllOrderingNames(member).Reverse<string>();
                        var validComponents = this.TryGetAllComponents(member).Reverse<ComposefOfGroup>();
                        names.AddRange(validOrderingNames);
                        components.AddRange(validComponents);
                    }
                }
                this.OrderingNames = names.ToArray();
                this.ComponentNames = components.ToArray();
            }

            private IEnumerable<ComposefOfGroup> TryGetAllComponents(string member)
            {
                var type = this.Type;
                var separator = this.Separator;
                var format = this.Formats.ToList().FirstOrDefault(x => x.PropertyName == member);
                var stack = new Stack<Tuple<Type, ComposefOfGroup>>();
                stack.Push(Tuple.Create(type, new ComposefOfGroup(member, separator, format)));
                while (stack.Count > XLib.MagicNumber.IntZero)
                {
                    var current = stack.Pop();
                    type = current.Item1;
                    member = current.Item2.Member;
                    separator = current.Item2.Separator;
                    format = current.Item2.Format;
                    var proceed = type.TryGetProperty(member, out var property);
                    if (proceed)
                    {
                        if (member != this.PropertyName)
                        {
                            yield return new ComposefOfGroup(member, separator, format);
                        }
                        else
                        {
                            var formats = property.GetCustomAttributes<FormatAttribute>();
                            var composedOf = property.GetCustomAttribute<ComposedOfAttribute>();
                            if (composedOf != null)
                            {
                                var members = composedOf.Members;
                                for (int i = 0; i < members.Length; i++)
                                {
                                    var item = members[i];
                                    format = formats.FirstOrDefault(x => x.PropertyName == item);
                                    stack.Push(Tuple.Create(composedOf.Type, new ComposefOfGroup(item, composedOf.Separator, format)));
                                }
                            }
                        }
                    }
                }
            }

            private IEnumerable<string> TryGetAllOrderingNames(string member)
            {
                var type = this.Type;
                var stack = new Stack<Tuple<Type, string>>();
                stack.Push(Tuple.Create(type, member));
                while (stack.Count > XLib.MagicNumber.IntZero)
                {
                    var current = stack.Pop();
                    type = current.Item1;
                    member = current.Item2;
                    var proceed = type.TryGetProperty(member, out var property);
                    if (proceed)
                    {
                        if (property.CanWrite || property.GetCustomAttribute<ReadOnlyOrderingKeyAttribute>() != null)
                        {
                            yield return member;
                        }
                        else
                        {
                            var attribute = property.GetCustomAttribute<ComposedOfAttribute>();
                            if (attribute != null)
                            {
                                var members = attribute.Members;
                                for (int i = 0; i < members.Length; i++)
                                {
                                    stack.Push(Tuple.Create(attribute.Type, members[i]));
                                }
                            }
                        }
                    }
                }
            }

            #region Internal class
            public sealed class ComposefOfGroup
            {
                public string Member { get; }
                public string Separator { get; } = XLib.Text.Space;
                public FormatAttribute Format { get; }

                internal ComposefOfGroup(string member, string separator, FormatAttribute format)
                {
                    this.Member = member;
                    this.Separator = separator;
                    this.Format = format;
                }
            }
            #endregion

            //private IEnumerable<string> TryGetAllBaseNames(ComposedOf member)
            //{
            //    var type = this.Type;
            //    PropertyInfo property = null;
            //    var stack = new Stack<ComposedOf>();
            //    stack.Push(member);
            //    while (stack.Count > XLib.Number.IntZero)
            //    {
            //        member = stack.Pop();
            //        var name = member.ToString();
            //        var proceed = type.TryGetProperty(name, out property);
            //        if (proceed)
            //        {
            //            if (property.CanWrite)
            //            {
            //                yield return name;
            //            }
            //            else
            //            {
            //                var attribute = property.GetCustomAttribute<ComposedOfAttribute>();
            //                if (attribute != null)
            //                {
            //                    var members = attribute.Members;
            //                    for (int i = 0; i < members.Length; i++)
            //                    {
            //                        stack.Push(members[i]);
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            #endregion
        }
        #endregion
    }
}
