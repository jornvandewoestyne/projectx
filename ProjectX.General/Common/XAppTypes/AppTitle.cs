﻿using System;
using System.Configuration;

//TODO: summary
//TODO: constants or readonly
//TODO: use of Lazy???? -> Keep?

namespace ProjectX.General
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class AppTitle : BaseClass
    {
        #region Fields
        private const string _DEFAULTNAME = "ProjectX";
        private const string _DEFAULTVERSION = "1.0";
        private static readonly Lazy<AppTitle> _lazyInstance = new Lazy<AppTitle>(() => new AppTitle());
        #endregion

        #region Properties
        private string Name { get; set; }
        private string Version { get; set; }
        #endregion

        #region Static Properties
        public static AppTitle Instance => _lazyInstance.Value;
        public static string FolderName => Instance.Name;
        public static string FullName => Instance.ToString();
        #endregion

        #region Constructors
        private AppTitle()
        {
            this.Initialize();
        }
        #endregion

        #region Methods
        private void Initialize()
        {
            //TODO: fix hard-coded strings
            this.Name = ConfigurationManager.AppSettings["ApplicationName"];
            this.Version = ConfigurationManager.AppSettings["ApplicationVersion"];
            if (this.Name.IsEmptyString())
            {
                this.Name = _DEFAULTNAME;
            }
            if (this.Version.IsEmptyString())
            {
                this.Version = _DEFAULTVERSION;
            }
        }

        public override string ToString()
        {
            if (this != null)
            {
                return StringHelper.Sentence(this.Name, this.Version);
            }
            return String.Empty;
        }
        #endregion
    }
}
