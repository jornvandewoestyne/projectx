﻿using System.Drawing;

//TODO: summary

namespace ProjectX.General
{
    /// <summary>
    /// ...
    /// </summary>
    public static class AppColor
    {
        #region Properties
        public static Color DefaultColor { get; set; } = Color.White;

        public static Color ActiveColor { get; set; } = Color.Lavender;

        public static Color DefaultSystemColor { get; set; } = SystemColors.Control;

        public static Color DefaultWindowControlColor => SystemColors.Window;
        #endregion
    }
}
