﻿using System.Drawing;

using XLib;

namespace ProjectX.General
{
    public sealed class ColorPreference : BaseClass
    {
        #region Properties
        public Color BorderColor { get; }
        public Color BackgroundColor { get; }
        #endregion

        #region Constructors
        internal ColorPreference(Color backgroundColor, Color borderColor)
        {
            this.BackgroundColor = backgroundColor;
            this.BorderColor = borderColor;
        }
        #endregion

        #region Methods
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="work"></param>
        /// <returns></returns>
        public static ColorPreference GetAccentColor(Work work)
        {
            switch (work)
            {
                case Work.Cancel:
                case Work.Close:
                case Work.Delete:
                    return ColorScheme.AlertAccent;
                default:
                    return ColorScheme.DefaultAccent;
            }
        }

        public sealed override string ToString()
        {
            return this.ToFriendlyString();
        }
        #endregion
    }
}
