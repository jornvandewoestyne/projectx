﻿using ProjectX.General.Attributes;
using System;

namespace ProjectX.General
{
    public class PropertyPreference
    {
        #region Properties
        public int Width { get; internal set; } = 100;
        public int MaximumWidth { get; internal set; } = 200;
        public int MinimumWidth { get; internal set; } = 30;
        public string Format { get; internal set; } = String.Empty;
        public Alignment Alignment { get; internal set; } = Alignment.Left;
        #endregion

        #region Constructors
        public PropertyPreference()
        { }

        public PropertyPreference(int width, int maximumWidth, int minimumWidth)
        {
            this.Initialize(width, maximumWidth, minimumWidth);
        }

        public PropertyPreference(int width, int maximumWidth, int minimumWidth, string format, Alignment alignment)
        {
            this.Initialize(width, maximumWidth, minimumWidth, format, alignment);
        }

        public PropertyPreference(DisplayPreference preference)
        {
            this.Initialize(preference);
        }
        #endregion

        #region Methods
        protected void Initialize(int width, int maximumWidth, int minimumWidth)
        {
            this.Width = width;
            this.MaximumWidth = maximumWidth;
            this.MinimumWidth = minimumWidth;
        }

        protected void Initialize(int width, int maximumWidth, int minimumWidth, string format, Alignment alignment)
        {
            this.Initialize(width, maximumWidth, minimumWidth);
            this.Format = format;
            this.Alignment = alignment;
        }

        protected void Initialize(DisplayPreference preference)
        {
            switch (preference)
            {
                case DisplayPreference.Code:
                    this.Initialize(70, 100, 70, this.Format, Alignment.Right);
                    break;
                case DisplayPreference.Short:
                    this.Initialize(50, 100, 30);
                    break;
                case DisplayPreference.Medium:
                    this.Initialize(100, 200, 30);
                    break;
                case DisplayPreference.Long:
                    this.Initialize(150, 300, 100);
                    break;
                case DisplayPreference.ExtraLong:
                    this.Initialize(250, 400, 100);
                    break;
                case DisplayPreference.Memo:
                    this.Initialize(400, 500, 100);
                    break;
                case DisplayPreference.Boolean:
                    this.Initialize(30, 100, 30, this.Format, Alignment.Center);
                    break;
                case DisplayPreference.Date:
                    this.Initialize(80, 100, 50, "{0:dd/MM/yyyy}", Alignment.Right);
                    break;
                case DisplayPreference.Time:
                    this.Initialize(80, 100, 50, "{0:HH:mm:ss}", Alignment.Right);
                    break;
                case DisplayPreference.DateAndTime:
                    this.Initialize(120, 120, 50, "{0:dd/MM/yyyy HH:mm:ss}", Alignment.Right);
                    break;
                case DisplayPreference.Integer:
                    this.Initialize(60, 100, 50, "{0:N0}", Alignment.Right);
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}
