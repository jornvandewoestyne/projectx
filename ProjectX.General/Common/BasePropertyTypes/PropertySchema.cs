﻿using ProjectX.General.Attributes;
using System;
using System.Reflection;

namespace ProjectX.General
{
    public sealed class PropertySchema : PropertyPreference
    {
        #region Properties
        public Type Type { get; private set; }
        public string Name { get; private set; }
        //public string Format { get; private set; } = String.Empty;
        //public Alignment Alignment { get; private set; } = Alignment.Left;
        //public int Width { get; private set; } = 100;
        //public int MaximumWidth { get; private set; } = 250;
        //public int MinimumWidth { get; private set; } = 50;

        public bool IsVisible { get; private set; } = true;
        public bool IsEditable { get; private set; } = false;
        public bool CheckBoxes { get; private set; } = false;
        public bool TriStateCheckBoxes { get; private set; } = false;
        public bool Hyperlink { get; private set; } = false;
        public bool FillsFreeSpace { get; private set; } = false;
        public int FreeSpaceProportion { get; private set; }

        public bool IsObsolete { get; private set; } = false;
        public bool IsOptional { get; private set; } = false;
        public bool IsSafe => this.GetFlagIsSafe();
        #endregion

        #region Constructors
        public PropertySchema(PropertyInfo propertyInfo)
        {
            this.Initialize(propertyInfo);
        }
        #endregion

        #region Methods
        private bool GetFlagIsSafe()
        {
            return !this.IsObsolete && !this.IsOptional ? true : false;
        }

        private void Initialize(PropertyInfo propertyInfo)
        {
            //TODO: message NullReferenceException
            if (propertyInfo == null)
            {
                throw new NullReferenceException();
            }
            this.Name = propertyInfo.Name;
            this.Type = propertyInfo.PropertyType;
            this.IsObsolete = propertyInfo.HasAttribute<ObsoleteAttribute>();
            this.IsOptional = propertyInfo.HasAttribute<OptionalAttribute>();
            if (this.Type == typeof(bool))
            {
                this.CheckBoxes = true;
                this.TriStateCheckBoxes = true;
                this.IsEditable = false;
                this.Initialize(DisplayPreference.Boolean);
            }
            else if (this.Type == typeof(int))
            {
                this.Initialize(DisplayPreference.Integer);
            }
            else if (this.Type == typeof(DateTime) || this.Type == typeof(Nullable<DateTime>))
            {
                this.Initialize(DisplayPreference.Date);
            }
            //Override some properties when needed:
            if (propertyInfo.HasAttribute<DisplayPreferenceAttribute>())
            {
                var value = propertyInfo.GetCustomAttribute<DisplayPreferenceAttribute>().Preference;
                this.Initialize(value.Width, value.MaximumWidth, value.MinimumWidth, value.Format, value.Alignment);
                //this.Width = attribute.Width;
                //this.MaximumWidth = attribute.MaximumWidth;
                //this.MinimumWidth = attribute.MinimumWidth;
            }
        }
        #endregion
    }
}
