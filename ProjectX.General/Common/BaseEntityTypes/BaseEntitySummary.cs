﻿namespace ProjectX.General
{
    public abstract class BaseEntitySummary : BaseClass
    {
        #region Properties
        //public MenuBloc Action { get; set; }
        public abstract string Title { get; }
        public abstract string SubTitle { get; }
        public abstract string FullName { get; }
        public abstract string SortName { get; }
        //public string Identifier { get; set; }
        #endregion

        //#region Constructors
        //internal EntitySummary(string title, string subTitle, string sortName)
        //{
        //    this.Title = title;
        //    this.SubTitle = subTitle;
        //    this.SortName = sortName;
        //}
        //#endregion

        #region Methods
        public override string ToString()
        {
            return StringHelper.Title(this.Title, this.SubTitle);
        }
        #endregion
    }
}
