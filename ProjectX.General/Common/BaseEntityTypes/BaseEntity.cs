﻿namespace ProjectX.General
{
    public abstract class BaseEntity : IBaseEntity
    {
        public virtual SupportedLanguage CurrentLanguage { get; } = XLib.Preference.CurrentLanguage;

        public object this[string propertyName]
        {
            //TODO: ExceptionHandling
            //System.ArgumentException -> Exemplaareigenschap PartnerID,Code is niet gedefinieerd voor type ProjectX.Domain.Models.CustomerGeneralData
            //get { return (int)(this.GetType().GetProperty(propertyName).GetValue(this, null)); }

            //get { return (this.GetType().GetProperty(propertyName).GetValue(this, null)); }

            get { return PropertyCache.GetCachedProperty(this.GetType(), propertyName).Value(this); }
        }

        public object GetValueOf(string propertyName)
        {
            return this[propertyName];
        }

        //TODO: keep? - performance? - load at start program = once!
        //protected BaseEntity()
        //{
        //    PropertyHelper.GetProperties(this.GetType());
        //}
    }
}
