﻿//TODO: TEST case when multiple primary keys?

namespace ProjectX.General
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class EntityKeyName : BaseClass, IResolvable
    {
        #region Properties
        public string[] Members { get; }
        #endregion

        #region Constructors
        public EntityKeyName() : this(null)
        { }

        internal EntityKeyName(string[] members)
        {
            this.Members = members;
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            var result = this.Members.TryArrayToString<string>();
            if (result.IsEmptyString())
            {
                throw new MissingMethodParameterException(nameof(EntityKeyName));
            }
            return result;
        }
        #endregion
    }
}
