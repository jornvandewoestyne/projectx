﻿//TODO: TEST case when multiple primary keys?

namespace ProjectX.General
{
    public sealed class EntityKey : BaseClass, IResolvable
    {
        #region Properties
        public object[] Keys { get; }
        #endregion

        #region Constructors
        public EntityKey() : this(null)
        { }

        internal EntityKey(object[] keys)
        {
            this.Keys = keys;
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            var result = this.Keys.TryArrayToString<object>();
            if (result.IsEmptyString())
            {
                throw new MissingMethodParameterException(nameof(EntityKey));
            }
            return result;
        }
        #endregion
    }
}
