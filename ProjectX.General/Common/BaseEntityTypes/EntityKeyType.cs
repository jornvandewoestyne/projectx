﻿using System;

//TODO: TEST case when multiple primary keys?

namespace ProjectX.General
{
    public sealed class EntityKeyType : BaseClass, IResolvable
    {
        #region Properties
        public Type[] Types { get; }
        #endregion

        #region Constructors
        public EntityKeyType()
            : this(null)
        { }

        internal EntityKeyType(Type[] types)
        {
            this.Types = types;
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            var result = this.Types.TryArrayToString<object>();
            if (result.IsEmptyString())
            {
                throw new MissingMethodParameterException(nameof(EntityKeyType));
            }
            return result;
        }
        #endregion
    }
}

