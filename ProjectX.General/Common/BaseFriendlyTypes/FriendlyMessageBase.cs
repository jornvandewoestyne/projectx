﻿using System;
using System.ComponentModel.DataAnnotations;

//TODO: summary

namespace ProjectX.General
{
    /// <summary>
    /// ...
    /// </summary>
    public abstract class FriendlyMessageBase : BaseClass
    {
        #region Properties
        public Enum EnumType { get; }

        [Display(GroupName = nameof(KeyWord.Details))]
        public int Number { get; }

        [Display(GroupName = nameof(KeyWord.Details))]
        public bool IsNotification { get; }

        [Display(GroupName = nameof(KeyWord.Details))]
        public string Name { get; }

        [Display(GroupName = nameof(KeyWord.Details))]
        public Type Type { get; }

        [Display(GroupName = nameof(KeyWord.Details))]
        public string Message { get; protected set; }

        [Display(GroupName = nameof(KeyWord.Details))]
        public object[] Arguments { get; }
        #endregion

        #region Constructors
        protected FriendlyMessageBase(Enum enumType, params object[] args)
            : this(enumType, false, args)
        { }

        internal FriendlyMessageBase(Enum enumType, bool isNotification, params object[] args)
        {
            this.EnumType = enumType;
            this.Type = enumType.GetType();
            this.Number = enumType.GetHashCode();
            this.Name = this.Message = enumType.ToString();
            this.IsNotification = isNotification;
            this.Arguments = args;
        }
        #endregion

        #region Methods
        protected virtual void SetMessage(bool translateArguments, object[] args)
        {
            //REMINDER: override this method
            this.Message = this.ToFriendlyString(nameof(Message), nameof(Arguments));
        }

        //protected abstract void SetMessage(object[] args);

        public sealed override string ToString()
        {
            return this.ToFriendlyStringAsOneLine();
        }
        #endregion
    }
}
