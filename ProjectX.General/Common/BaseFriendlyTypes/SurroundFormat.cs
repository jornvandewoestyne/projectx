﻿namespace ProjectX.General
{
    public sealed class SurroundFormat
    {
        #region Properties
        public string Prefix { get; }
        public string Suffix { get; }
        public static SurroundFormat None => null;
        #endregion

        #region Constructors
        public SurroundFormat(string surround)
            : this(surround, surround)
        { }

        public SurroundFormat(string prefix, string suffix)
        {
            this.Prefix = prefix;
            this.Suffix = suffix;
        }
        #endregion
    }
}
