﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ProjectX.General
{
    public sealed class FriendlyMethodInfo : BaseClass
    {
        #region Properties
        public MethodInfo MethodInfo { get; private set; }
        public object[] Arguments { get; private set; }

        [Display(GroupName = nameof(KeyWord.Details))]
        public string DeclaringType => this.MethodInfo?.DeclaringType?.ToString();

        [Display(GroupName = nameof(KeyWord.Details))]
        public string Method => this.MethodInfo?.ToString();

        [Display(GroupName = nameof(KeyWord.Details))]
        public string ParameterValues => GetParameterValues().ToString();
        #endregion

        #region Constructors
        public FriendlyMethodInfo(MethodInfo methodInfo, object[] arguments)
        {
            this.MethodInfo = methodInfo;
            this.Arguments = arguments;
        }
        #endregion

        #region Methods
        public sealed override string ToString()
        {
            return this.ToFriendlyStringAsOneLine();
        }
        #endregion

        #region Methods: Helpers
        private StringBuilder GetParameterValues()
        {
            //TODO: hard coded strings?
            var stringBuilder = new StringBuilder();
            if (this.MethodInfo != null)
            {
                var parameters = this.MethodInfo.GetParameters().ToList();
                var values = this.Arguments;
                if (parameters.Count > XLib.MagicNumber.IntZero) { stringBuilder.Append("("); }
                for (int i = 0; i < parameters.Count; i++)
                {
                    var parameter = parameters[i].Name;
                    var value = XLib.Text.NotSpecified;
                    if (!(values == null || (values != null && values.Length <= i)))
                    {
                        value = values[i].TryArrayToString<object>();
                    }
                    if (i > XLib.MagicNumber.IntZero) { stringBuilder.Append(XLib.Text.HorizontalEnumSeparator); }
                    stringBuilder.AppendFormat("{0}={1}", parameter, value);
                }
                if (parameters.Count > XLib.MagicNumber.IntZero) { stringBuilder.Append(")"); }
            }
            return stringBuilder;
        }
        #endregion
    }
}
