﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectX.General
{
    public sealed class FriendlyExceptionInfo : BaseClass
    {
        #region Fields
        private Exception _exception;
        #endregion

        #region Properties
        [Display(GroupName = nameof(KeyWord.Details))]
        public string Application => AppTitle.FullName;

        [Display(GroupName = nameof(KeyWord.Details))]
        public LogLevel LogLevel { get; }

        [Display(GroupName = nameof(KeyWord.Details))]
        public string HResult => this.Exception.HResult.ToString();

        [Display(GroupName = nameof(KeyWord.Details))]
        public Type Type => this.Exception.GetType();

        [Display(GroupName = nameof(KeyWord.Details))]
        public string Message => this.Exception.Message.RemoveLineBreaks();

        public string StackTrace
        {
            get
            {
                var exception = this.Exception.ToString().ReplaceLineBreaks(XLib.Text.Space);
                return StringHelper.Concat(XLib.Text.DefaultInfo, XLib.Text.VerticalEnumSeparator, exception);
            }
        }

        public Exception Exception
        {
            get => this._exception;
            private set => this._exception = value ?? new LogNullException();
        }
        #endregion

        #region Constructors
        public FriendlyExceptionInfo(LogLevel logLevel, Exception exception)
        {
            this.LogLevel = logLevel;
            this.Exception = exception;
        }
        #endregion

        #region Methods
        public sealed override string ToString()
        {
            return this.ToFriendlyStringAsOneLine();
        }
        #endregion
    }
}
