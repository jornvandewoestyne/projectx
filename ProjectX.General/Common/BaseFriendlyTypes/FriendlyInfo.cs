﻿//TODO: implement ToString()

namespace ProjectX.General
{
    public sealed class FriendlyInfo
    {
        #region Properties
        public string MainMessage { get; set; }
        public string ExtraMessage { get; set; }
        public string Advice { get; set; }
        #endregion

        #region Constructors
        public FriendlyInfo(string mainMessage = null, string extraMessage = null, string advice = null)
        {
            this.MainMessage = mainMessage;
            this.ExtraMessage = extraMessage;
            this.Advice = advice;
        }
        #endregion
    }
}
