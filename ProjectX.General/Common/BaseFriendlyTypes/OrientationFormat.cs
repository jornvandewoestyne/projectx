﻿namespace ProjectX.General
{
    public sealed class OrientationFormat
    {
        #region Properties
        public string Suffix { get; }
        public string Separator { get; }
        #endregion

        #region Constructor
        public OrientationFormat(string suffix, string separator)
        {
            this.Suffix = suffix;
            this.Separator = separator;
        }
        #endregion
    }
}
