﻿namespace ProjectX.General
{
    public sealed class Paging : BaseClass, IResolvable
    {
        #region Statics
        public static readonly Paging None = new Paging()
        {
            Take = XLib.Limit.MaxItemsToBeReturned,
            TakeAll = true
        };
        #endregion

        #region Properties
        public int Page { get; } = 1;
        public int Take { get; private set; } = XLib.Limit.MaxItemsPerPage;
        public int Skip => this.Take * (this.Page - 1);
        public bool SkipAll { get; }
        public bool TakeAll { get; set; }
        #endregion

        #region Constructor
        public Paging()
        { }

        public Paging(bool skipAll)
        {
            this.SkipAll = skipAll;
        }

        public Paging(int page, int itemsPerPage)
        {
            //TODO: Check for values > 0!!!
            this.Page = page;
            this.Take = itemsPerPage;
            this.SkipAll = false;
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return this.ToFriendlyStringAsOneLine();
        }
        #endregion
    }
}
