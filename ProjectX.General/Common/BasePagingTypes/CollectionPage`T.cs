﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

//https://www.chevtek.io/how-to-properly-return-a-paged-result-set-from-your-repository/

namespace ProjectX.General
{
    /// <summary>
    /// Holds relevant information related to a page of a collection of information.
    /// </summary>
    public sealed class CollectionPage<TEntity> : ICollectionPage<TEntity> where TEntity : BaseEntity
    {
        #region Fields
        IList<Notification> _notifications;
        #endregion

        #region Properties
        public IList<object> Items { get; private set; }
        public IEnumerable<TEntity> EntityItems { get; private set; }
        public int Count { get; private set; }
        public int TotalItems { get; private set; }
        public int ItemsPerPage { get; private set; }
        public int TotalPages { get; private set; }
        public IList<PropertySchema> EntityPropertySchemas => ReflectionExtensions.GetCollectionPagePropertySchemas<TEntity>(true).ToList();

        public IList<Notification> Notifications
        {
            get => this._notifications.ResolveNull();
            private set => this._notifications = value;
        }
        #endregion

        #region Constructor
        private CollectionPage()
        { }
        #endregion

        #region Methods
        private async Task<ICollectionPage<TEntity>> InitializeAsync(IEnumerable<TEntity> items, Paging paging, int totalItems, IList<Notification> notifications, CancellationToken cancellationToken)
        {
            if (items != null)
            {
                //Materialize the given items to a generic list:
                this.EntityItems = items;
                //TODO: check SelectDynamic()
                //TODO: is this async???
                //TODO: do also .Select at the Repository level!!!! -> = Including
                this.Items = await items.SelectDynamic<TEntity>(true).AsToListAsync<object>(cancellationToken);
                this.Count = this.Items.Count;
                if (this.Count == XLib.MagicNumber.IntZero)
                {
                    totalItems = this.Count;
                }
            }
            this.ItemsPerPage = paging.ResolveNull().Take;
            this.TotalItems = totalItems;
            this.TotalPages = this.GetTotalPages();
            this.Notifications = this.GetNotifications(notifications, paging);
            return this;
        }

        /// <summary>
        /// ...
        /// </summary>
        /// <param name="items"></param>
        /// <param name="paging"></param>
        /// <param name="totalItems"></param>
        /// <param name="notifications"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async Task<ICollectionPage<TEntity>> CreateAsync(IEnumerable<TEntity> items, Paging paging, int totalItems, IList<Notification> notifications, CancellationToken cancellationToken)
        {
            var instance = new CollectionPage<TEntity>();
            return await instance.InitializeAsync(items, paging, totalItems, notifications, cancellationToken);
        }
        #endregion

        #region Helpers
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="notifications"></param>
        /// <param name="paging"></param>
        /// <returns></returns>
        private IList<Notification> GetNotifications(IList<Notification> notifications, Paging paging)
        {
            if (paging != null && paging.SkipAll)
            {
                notifications.ResolveNull().Add(new Notification(FriendlyMessageType.LoadDataAdvice, false));
            }
            return notifications;
        }

        /// <summary>
        /// The total number of pages.
        /// </summary>
        private int GetTotalPages()
        {
            return (int)(Math.Ceiling((Decimal)this.TotalItems / (Decimal)this.ItemsPerPage));
        }
        #endregion

        //TODO
        public void Dispose()
        {
            this.Items?.Clear();
            this.Notifications?.Clear();
            this.EntityPropertySchemas?.Clear();
            this.Items = null;
            this.EntityItems = null;
            this.Notifications = null;
        }
    }
}
