﻿using System;
using System.Linq.Expressions;

namespace ProjectX.General
{
    public sealed class OrderingSetting<TEntity> : BaseClass where TEntity : BaseEntity
    {
        #region Properties
        public OrderingSetting OrderBySetting { get; }
        #endregion

        #region Constructors
        public OrderingSetting(Expression<Func<TEntity, object>> expression, OrderingDirection direction = OrderingDirection.Ascending)
        {
            var property = new IncludingProperty<TEntity>(expression);
            this.OrderBySetting = new OrderingSetting(property.PropertyInfo.Name, direction);
        }
        #endregion

        #region Methods
        public sealed override string ToString()
        {
            return this.OrderBySetting.ToString();
        }
        #endregion
    }
}
