﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

//TODO: summary
//TODO: set warning text....? -> use IWarning (with properties) instead of HasNotification?

namespace ProjectX.General
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class OrderingSetting : BaseOrdering
    {
        #region Properties
        public override Type Type { get; internal set; }
        public override bool IsValid { get; internal set; }
        public override bool HasNotification { get; internal set; }
        //public override Notification Notification { get; internal set; }
        public string PropertyName { get; private set; }
        public OrderingDirection Direction { get; private set; }
        public List<OrderingSettingMember> Members { get; private set; }
        #endregion

        #region Constructors
        public OrderingSetting(string propertyName, OrderingDirection direction = OrderingDirection.Ascending)
        {
            this.PropertyName = propertyName.TryTrim();
            this.Direction = direction;
        }
        #endregion

        #region Methods
        internal sealed override void Validate(Type type)
        {
            this.Type = type;
            var proceed = type.TryGetProperty(this.PropertyName, out var property);
            if (proceed)
            {
                if (this.Members == null)
                {
                    this.Members = new List<OrderingSettingMember>();
                }
                var attribute = property.GetCustomAttribute<BusinessRule.ComposedOfAttribute>();
                if (attribute != null)
                {
                    var names = attribute.OrderingNames;
                    if (names != null && names.Length > XLib.MagicNumber.IntZero)
                    {
                        var length = names.Length;
                        for (int i = 0; i < length; i++)
                        {
                            var member = new OrderingSettingMember(type, names[i]);
                            this.Members.Add(member);
                        }
                    }
                }
                else
                {
                    var member = new OrderingSettingMember(type, property.Name);
                    this.Members.Add(member);
                }
            }
            //Set flags:
            this.SetFlags();
        }

        internal sealed override void SetFlags()
        {
            this.HasNotification = true;
            if (this.Members != null)
            {
                if (this.Members.Where(x => x.IsValid).Count() > XLib.MagicNumber.IntZero)
                {
                    this.IsValid = true;
                }
                if (this.Members.Where(x => x.HasNotification).Count() == XLib.MagicNumber.IntZero)
                {
                    this.HasNotification = false;
                }
            }
        }

        public sealed override string ToString()
        {
            string result = null;
            if (this.Members != null)
            {
                var collection = new List<string>();
                foreach (var member in this.Members)
                {
                    if (member.IsValid)
                    {
                        collection.Add(member.ToString());
                    }
                }
                if (collection.Count > XLib.MagicNumber.IntZero)
                {
                    var members = StringHelper.PlusStyle(collection.ToArray());
                    result = StringHelper.Sentence(members, this.Direction.ToString().ToUpper());
                }
            }
            return result.ResolveNull();
        }
        #endregion

        #region Nested Class
        public sealed class OrderingSettingMember : BaseOrdering
        {
            #region Properties
            public override Type Type { get; internal set; }
            public override bool IsValid { get; internal set; }
            public override bool HasNotification { get; internal set; }
            //public override Notification Notification { get; internal set; }
            public string PropertyName { get; private set; }
            public Notification Notification { get; private set; }
            #endregion

            #region Constructor
            public OrderingSettingMember(Type type, string propertyName)
            {
                this.Type = type;
                this.PropertyName = propertyName;
                this.Validate(this.Type);
            }
            #endregion

            #region Methods
            internal sealed override void Validate(Type type)
            {
                this.HasNotification = true;
                var proceed = type.TryGetProperty(this.PropertyName, out var property);
                if (proceed)
                {
                    if (property.CanWrite)
                    {
                        this.SetFlags();
                    }
                    else
                    {
                        //TODO: check
                        this.Notification = new Notification(FriendlyMessageType.PropertyNotSupportedAlert, true, this.PropertyName, "Delete", "Belgium");
                    }
                }
                else
                {
                    //TODO: check
                    this.Notification = new Notification(FriendlyMessageType.PropertyNotFoundAlert, true, this.PropertyName, "Delete", "Belgium");
                }
            }

            internal sealed override void SetFlags()
            {
                this.IsValid = true;
                this.HasNotification = false;
            }

            public sealed override string ToString()
            {
                if (this != null)
                {
                    return this.PropertyName;
                }
                return String.Empty;
            }
            #endregion
        }
        #endregion
    }
}
