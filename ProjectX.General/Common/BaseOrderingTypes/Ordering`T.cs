﻿using System;
using System.Linq.Expressions;

namespace ProjectX.General
{
    public sealed class Ordering<TEntity> : BaseClass where TEntity : BaseEntity
    {
        #region Properties
        public Ordering OrderBy { get; private set; }
        #endregion

        #region Constructors
        /// <summary>
        /// ...
        /// <para>DefaultSettings are used (if available) when no settings are given.</para>
        /// </summary>
        public Ordering()
        {
            this.Initialize();
        }

        /// <summary>
        /// ...
        /// <para>DefaultSettings are used (if available) when no settings are given.</para>
        /// </summary>
        /// <param name="orderby"></param>
        public Ordering(params OrderingSetting[] orderby)
        {
            this.Initialize(orderby);
        }

        /// <summary>
        /// ...
        /// <para>DefaultSettings are used (if available) when no settings are given.</para>
        /// </summary>
        /// <param name="orderby"></param>
        public Ordering(params string[] orderby)
        {
            if (orderby != null && orderby.Length > XLib.MagicNumber.IntZero)
            {
                var settings = new OrderingSetting[orderby.Length];
                for (int i = 0; i < orderby.Length; i++)
                {
                    settings[i] = new OrderingSetting(orderby[i]);
                }
                this.Initialize(settings);
            }
            else
            {
                this.Initialize();
            }
        }

        /// <summary>
        /// ...
        /// <para>DefaultSettings are used (if available) when no settings are given.</para>
        /// </summary>
        /// <param name="orderby"></param>
        public Ordering(params Expression<Func<TEntity, object>>[] orderby)
        {
            if (orderby != null && orderby.Length > XLib.MagicNumber.IntZero)
            {
                var settings = new OrderingSetting[orderby.Length];
                for (int i = 0; i < orderby.Length; i++)
                {
                    settings[i] = new OrderingSetting<TEntity>(orderby[i]).OrderBySetting;
                }
                this.Initialize(settings);
            }
            else
            {
                this.Initialize();
            }
        }

        /// <summary>
        /// ...
        /// <para>DefaultSettings are used (if available) when no settings are given.</para>
        /// </summary>
        /// <param name="orderby"></param>
        public Ordering(params OrderingSetting<TEntity>[] orderby)
        {
            if (orderby != null && orderby.Length > XLib.MagicNumber.IntZero)
            {
                var settings = new OrderingSetting[orderby.Length];
                for (int i = 0; i < orderby.Length; i++)
                {
                    settings[i] = orderby[i].OrderBySetting;
                }
                this.Initialize(settings);
            }
            else
            {
                this.Initialize();
            }
        }
        #endregion

        #region Methods
        private void Initialize(params OrderingSetting[] settings)
        {
            var type = typeof(TEntity);
            this.OrderBy = new Ordering(type, settings);
        }

        public override string ToString()
        {
            return this.OrderBy.ToString();
        }
        #endregion
    }
}
