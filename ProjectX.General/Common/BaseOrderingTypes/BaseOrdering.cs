﻿using System;

//TODO: summary

namespace ProjectX.General
{
    /// <summary>
    /// ...
    /// </summary>
    public abstract class BaseOrdering : BaseClass
    {
        #region Properties
        public abstract Type Type { get; internal set; }
        public abstract bool IsValid { get; internal set; }
        public abstract bool HasNotification { get; internal set; }
        //public abstract Notification Notification { get; internal set; }
        #endregion

        #region Methods
        internal abstract void Validate(Type type);
        internal abstract void SetFlags();
        public abstract override string ToString();
        #endregion
    }
}
