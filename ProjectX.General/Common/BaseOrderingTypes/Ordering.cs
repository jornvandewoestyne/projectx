﻿using System;
using System.Collections.Generic;
using System.Linq;

//TODO: summary
//TODO: set warning text....? -> use IWarning (with properties) instead of HasWarning?
//TODO: NotificationsToString()

namespace ProjectX.General
{
    /// <summary>
    /// ...
    /// </summary>
    public sealed class Ordering : BaseOrdering //, IWarning
    {
        #region Statics
        /// <summary>
        /// Explicitly set the ordering to 'None'.
        /// As a result, unsorted raw data is returned.
        /// <para>When not set like this, DefaultSettings are used (if available) when no settings are given.</para>
        /// </summary>
        public static readonly Ordering None = new Ordering(null) { IsValid = true };

        //private static readonly Lazy<Ordering> _none = new Lazy<Ordering>(() => new Ordering(null) { IsValid = true });

        ///// <summary>
        ///// Explicitly set the ordering to 'None'.
        ///// As a result, unsorted raw data is returned.
        ///// <para>When not set like this, DefaultSettings are used (if available) when no settings are given.</para>
        ///// </summary>
        //public static Ordering None => _none.Value;

        //private Ordering()
        //{
        //    _none = new Ordering(null) { IsValid = true };
        //}
        #endregion

        #region Properties
        public override Type Type { get; internal set; }
        public override bool IsValid { get; internal set; }
        public override bool HasNotification { get; internal set; }
        public bool HasDefaultSettings { get; internal set; } = true;
        //public override Notification Notification { get; internal set; }
        public bool UseDefaultSettings { get; private set; }
        public bool IsSafe => this.GetFlagIsSafe();
        public List<OrderingSetting> Settings { get; private set; }
        public List<OrderingSetting> DefaultSettings { get; private set; }
        #endregion

        #region Constructors
        /// <summary>
        /// ...
        /// <para>DefaultSettings are used (if available) when no settings are given.</para>
        /// </summary>
        /// <param name="type"></param>
        /// <param name="settings"></param>
        internal Ordering(Type type, params OrderingSetting[] settings)
        {
            this.Initialize(type, settings);
        }
        #endregion

        #region Methods
        //TODO: IWarning
        //public IWarning GetWarning()
        //{

        //}

        internal override void Validate(Type type)
        {
            this.ValidateSettings(type);
            this.ValidateDefaultSettings(type);
        }

        internal override void SetFlags()
        {
            if (!this.IsValid)
            {
                if (this.Settings != null && this.DefaultSettings == null)
                {
                    if (this.Settings.Any(x => x.IsValid))
                    {
                        this.IsValid = true;
                    }
                    if (!this.Settings.Any(x => x.HasNotification))
                    {
                        this.HasNotification = false;
                    }
                }
                else if (this.DefaultSettings != null)
                {
                    if (this.DefaultSettings.Any(x => x.IsValid))
                    {
                        this.UseDefaultSettings = true;
                        //When incorrect settings are given but defaultsettings are valid:
                        if (this.Settings == null)
                        {
                            this.IsValid = true;
                        }
                    }
                }
            }
        }

        public override string ToString()
        {
            string result = null;
            var settings = this.Settings;
            if (this.UseDefaultSettings)
            {
                settings = this.DefaultSettings;
            }
            if (settings != null)
            {
                var collection = new List<string>();
                foreach (var setting in settings)
                {
                    if (setting.IsValid)
                    {
                        collection.Add(setting.ToString());
                    }
                }
                if (collection.Count > XLib.MagicNumber.IntZero)
                {
                    result = StringHelper.Enumeration(collection.ToArray());
                }
            }
            return result.ResolveNull();
        }
        #endregion

        #region Methods: Helpers
        private void Initialize(Type type, params OrderingSetting[] settings)
        {
            this.HasNotification = true;
            if (type != null)
            {
                this.Type = type;
                if (settings != null && settings.Length > XLib.MagicNumber.IntZero)
                {
                    this.Settings = settings.ToList();
                }
                this.Validate(type);
            }
        }

        private bool GetFlagIsSafe()
        {
            return !this.ToString().IsEmptyString() && (this.IsValid || this.UseDefaultSettings || this.HasDefaultSettings);
        }

        private void ValidateSettings(Type type)
        {
            if (this.Settings != null)
            {
                foreach (var setting in this.Settings)
                {
                    setting.Validate(type);
                }
            }
            //Set flags:
            this.SetFlags();
        }

        private void ValidateDefaultSettings(Type type)
        {
            //At least try to get the default settings when no valid settings are given:
            if (!this.IsValid)
            {
                var names = type.GetDefaultOrderingPropertyNames();
                if (names != null && names.Length > XLib.MagicNumber.IntZero)
                {
                    if (this.DefaultSettings == null)
                    {
                        this.DefaultSettings = new List<OrderingSetting>();
                    }
                    var length = names.Length;
                    for (int i = 0; i < length; i++)
                    {
                        var setting = new OrderingSetting(names[i]);
                        setting.Validate(type);
                        this.DefaultSettings.Add(setting);
                    }
                }
                else
                {
                    //when no default ordering is available at the level of the entity:
                    this.HasDefaultSettings = false;
                    if (this.Settings == null)
                    {
                        this.IsValid = true;
                    }
                }
            }
            //Set flags:
            this.SetFlags();
        }
        #endregion
    }
}
