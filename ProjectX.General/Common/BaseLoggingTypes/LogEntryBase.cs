﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectX.General
{
    public class LogEntryBase : BaseClass
    {
        #region Properties
        [Display(GroupName = nameof(KeyWord.Details))]
        public DateTime Date => DateTime.Now;

        [Display(GroupName = nameof(KeyWord.Details), Name = nameof(KeyWord.Exception))]
        public FriendlyExceptionInfo FriendlyExceptionInfo { get; }

        [Display(GroupName = nameof(KeyWord.Details), Name = XLib.Constant.EmptyName)]
        public LogInformationBase Information { get; }

        [Display(GroupName = nameof(KeyWord.Details))]
        public string StackTrace => this.FriendlyExceptionInfo.StackTrace.ReplaceLineBreaks("#");
        #endregion

        #region Constructor
        public LogEntryBase(LogLevel logLevel, LogInformationBase information, Exception exception)
        {
            this.Information = information;
            this.FriendlyExceptionInfo = new FriendlyExceptionInfo(logLevel, exception);
        }
        #endregion

        #region Methods
        public sealed override string ToString()
        {
            //TODO!!!
            return StringHelper.Join(Environment.NewLine,
                this.Date,
                this.FriendlyExceptionInfo.ToFriendlyStringAsOneLine(true),
                this.Information.ToFriendlyStringAsOneLine(true),
                this.StackTrace);
            return this.ToFriendlyString();
        }
        #endregion
    }
}
