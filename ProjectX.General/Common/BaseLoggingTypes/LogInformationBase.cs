﻿using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace ProjectX.General
{
    public class LogInformationBase : BaseClass
    {
        #region Properties
        public Assembly Assembly { get; }

        [Display(GroupName = nameof(KeyWord.Details))]
        public object Interceptor { get; }

        [Display(GroupName = nameof(KeyWord.Details))]
        public FriendlyMessageBase FriendlyMessage { get; }

        [Display(GroupName = nameof(KeyWord.Details))]
        public string ErrorInformation { get; }

        [Display(GroupName = nameof(KeyWord.Details))]
        public virtual string ConnectionString => null;

        [Display(GroupName = nameof(KeyWord.Details), Name = nameof(KeyWord.Assembly))]
        public string AssemblyQualifiedName { get; }

        [Display(GroupName = nameof(KeyWord.Details), Name = nameof(KeyWord.MethodInfo))]
        public FriendlyMethodInfo FriendlyMethodInfo { get; }
        #endregion

        #region Constructors
        internal LogInformationBase(object interceptor, Assembly assembly, MethodInfo methodInfo, string errorInformation)
            : this(interceptor, assembly, assembly.FullName, methodInfo, null, errorInformation.RemoveLineBreaks(), null)
        { }

        public LogInformationBase(LogInformationBase information, string errorInformation, FriendlyMessageBase friendlyMessage)
            : this(information.Interceptor, information.Assembly, information.AssemblyQualifiedName, information.FriendlyMethodInfo.MethodInfo, information.FriendlyMethodInfo.Arguments, errorInformation, friendlyMessage)
        { }

        public LogInformationBase(object interceptor, Assembly assembly, string assemblyQualifiedName, MethodInfo methodInfo, object[] arguments, string errorInformation = null, FriendlyMessageBase friendlyMessage = null)
        {
            this.Interceptor = interceptor;
            this.Assembly = assembly;
            this.AssemblyQualifiedName = assemblyQualifiedName;
            this.ErrorInformation = errorInformation;
            this.FriendlyMessage = friendlyMessage;
            this.FriendlyMethodInfo = new FriendlyMethodInfo(methodInfo, arguments);
        }
        #endregion

        #region Methods
        public sealed override string ToString()
        {
            return this.ToFriendlyString();
        }
        #endregion
    }
}