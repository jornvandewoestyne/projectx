﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

//https://entityframework.net/knowledge-base/51114332/adding-include-option-in-repository-pattern-s-get-method
//https://stackoverflow.com/questions/37580842/add-include-to-repository

namespace ProjectX.General
{
    /// <summary>
    /// Only includes one level and valid navigation properties, otherwise skipped!
    /// When no includes are given, all navigation properties are included!
    /// Except when explicitly set to 'None', which is equivalent to an empty including.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public sealed class Including<TEntity> : BaseClass where TEntity : BaseEntity
    {
        #region Statics
        //private static readonly Lazy<Including<TEntity>> _none = new Lazy<Including<TEntity>>(() => new Including<TEntity>());

        ///// <summary>
        ///// Explicitly set the including to 'None'.
        ///// </summary>
        //public static Including<TEntity> None => _none.Value;

        /// <summary>
        /// Explicitly set the including to 'None'.
        /// </summary>
        public static readonly Including<TEntity> None = new Including<TEntity>();
        #endregion

        #region Properties
        public bool IsSafe => this.GetFlagIsSafe();
        //public List<IncludingProperty<TEntity>> DataProperties { get; } = new List<IncludingProperty<TEntity>>();
        public List<IncludingProperty<TEntity>> ValidProperties { get; } = new List<IncludingProperty<TEntity>>();
        public List<PropertyInfo> NavigationProperties { get; set; } = new List<PropertyInfo>();
        #endregion

        #region Constructors
        private Including() { }

        public Including(IEnumerable<PropertyInfo> navigationProperties, params Expression<Func<TEntity, object>>[] includes)
        {
            this.Initialize(navigationProperties, includes);
        }
        #endregion

        #region Methods
        private void Initialize(IEnumerable<PropertyInfo> navigationProperties, params Expression<Func<TEntity, object>>[] includes)
        {
            if (navigationProperties == null)
            {
                throw new MissingMethodParameterException(nameof(navigationProperties));
            }
            this.NavigationProperties = navigationProperties.ToList();
            if (includes != null && includes.Length > XLib.MagicNumber.IntZero)
            {
                for (int i = 0; i < includes.Length; i++)
                {
                    var property = new IncludingProperty<TEntity>(includes[i]);
                    if (this.NavigationProperties.Contains(property.PropertyInfo))
                    {
                        this.ValidProperties.Add(property);
                    }
                }
            }
            else
            {
                foreach (var item in this.NavigationProperties)
                {
                    var property = new IncludingProperty<TEntity>(item);
                    this.ValidProperties.Add(property);
                }
            }
        }

        private bool GetFlagIsSafe()
        {
            return this.ValidProperties != null && this.NavigationProperties != null ? true : false;
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return this.ToFriendlyStringAsOneLine();
        }
        #endregion
    }
}
