﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace ProjectX.General
{
    public sealed class IncludingProperty<TEntity> : BaseClass where TEntity : BaseEntity
    {
        #region Properties
        public Expression<Func<TEntity, object>> Expression { get; private set; }
        public MemberExpression MemberExpression { get; private set; }
        public PropertyInfo PropertyInfo { get; private set; }
        public bool IsCollection => this.IsCollectionType(this.PropertyInfo?.PropertyType);
        #endregion

        #region Constructors
        internal IncludingProperty(PropertyInfo propertyInfo)
        {
            this.Initialize(propertyInfo);
        }

        internal IncludingProperty(Expression<Func<TEntity, object>> expression)
        {
            this.Initialize(expression);
        }
        #endregion

        #region Methods
        private void Initialize(PropertyInfo propertyInfo)
        {
            if (propertyInfo != null)
            {
                this.PropertyInfo = propertyInfo;
            }
        }

        private void Initialize(Expression<Func<TEntity, object>> expression)
        {
            //TODO: Make lambda helper in General layer
            if (expression != null)
            {
                this.Expression = expression;
                var lambda = (LambdaExpression)expression;
                if (lambda.Body.NodeType != ExpressionType.MemberAccess)
                {
                    //TODO: string + customException
                    throw new InvalidOperationException("Expression must be a MemberExpression");
                }
                if (lambda.Body is UnaryExpression unaryExpression)
                {
                    this.MemberExpression = (MemberExpression)(unaryExpression.Operand);
                }
                else
                {
                    this.MemberExpression = (MemberExpression)(lambda.Body);
                }
                this.PropertyInfo = (PropertyInfo)this.MemberExpression.Member;
            }
        }

        //TODO: IsCollection > find a better way?
        private bool IsCollectionType(Type type)
        {
            if (type != null)
            {
                return type.IsGenericType && type.Name.Contains(typeof(ICollection<>).Name);
            }
            return false;
        }
        #endregion

        #region Methods Overrides
        public override string ToString()
        {
            return this.PropertyInfo?.Name;
        }
        #endregion
    }
}
