﻿namespace XString
{
    internal sealed class QualifiedName : XString.Base
    {
        #region Constructors
        /// <summary>
        /// Concatenates the elements of an object array, using a DOT separator between each element.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// </summary>
        /// <param name="args"></param>
        public QualifiedName(params object[] args)
            : base(XLib.Text.Dot, args)
        { }
        #endregion
    }
}
