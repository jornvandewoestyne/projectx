﻿namespace XString
{
    internal sealed class Join : XString.Base
    {
        #region Constructors
        /// <summary>
        /// Concatenates the elements of an object array, using the given separator between each element.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// </summary>
        /// <param name="args"></param>
        public Join(string separator, params object[] args)
            : base(separator, args)
        { }
        #endregion
    }
}
