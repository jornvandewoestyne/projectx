﻿namespace XString
{
    internal sealed class ArrowStyle : XString.Base
    {
        #region Constructors
        /// <summary>
        /// Concatenates the elements of an object array, using a ARROW separator between each element.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// </summary>
        /// <param name="args"></param>
        public ArrowStyle(params object[] args)
            : base(XLib.Text.ArrowSeparator, args)
        { }
        #endregion
    }
}
