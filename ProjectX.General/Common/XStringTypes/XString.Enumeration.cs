﻿namespace XString
{
    internal sealed class Enumeration : XString.Base
    {
        #region Constructors
        /// <summary>
        /// Concatenates the elements of an object array, using a HORIZONTAL separator between each element.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// </summary>
        /// <param name="args"></param>
        public Enumeration(params object[] args)
            : base(XLib.Text.HorizontalEnumSeparator, args)
        { }
        #endregion
    }
}
