﻿using ProjectX.General;
using System;

namespace XString
{
    internal sealed class Concat : XString.Base
    {
        #region Constructors
        /// <summary>
        /// Concatenates the elements of an object array.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// <para>NewLine arguments are accepted!</para>
        /// </summary>
        /// <param name="args"></param>
        public Concat(params object[] args)
            : base(SurroundFormat.None, args)
        { }
        /// <summary>
        /// Concatenates the elements of an object array, surrounding the result with the given surround prefix and/or suffix.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// <para>NewLine arguments are accepted!</para>
        /// </summary>
        /// <param name="args"></param>
        public Concat(SurroundFormat surround, params object[] args)
            : base(surround, args)
        { }
        #endregion

        #region Methods
        public override string ToString()
        {
            //TODO: return Empty when instance is null or own Xlib.???
            string result = null;
            if (this != null)
            {
                var collection = this.Arguments.TryGetValidArguments<object>(true);
                result = String.Concat(collection).TryTrim();
                if (this.SurroundFormat != null && !result.IsEmptyString())
                {
                    result = String.Concat(this.SurroundFormat.Prefix, result, this.SurroundFormat.Suffix);
                }
            }
            return result.ResolveNull();
        }
        #endregion
    }
}
