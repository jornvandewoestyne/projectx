﻿namespace XString
{
    internal sealed class PlusStyle : XString.Base
    {
        #region Constructors
        /// <summary>
        /// Concatenates the elements of an object array, using a PLUS separator between each element.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// </summary>
        /// <param name="args"></param>
        public PlusStyle(params object[] args)
            : base(XLib.Text.JoinPlusOperator, args)
        { }
        #endregion
    }
}
