﻿using ProjectX.General;
using System;

namespace XString
{
    internal abstract class Base : BaseClass
    {
        #region Properties
        public string Separator { get; }
        public SurroundFormat SurroundFormat { get; }
        public object[] Arguments { get; private set; }
        #endregion

        #region Constructors
        internal Base(string separator, params object[] args)
        {
            this.Separator = separator;
            this.SetArguments(args);
        }

        internal Base(SurroundFormat surround, params object[] args)
        {
            this.SurroundFormat = surround;
            this.SetArguments(args);
        }
        #endregion

        #region Methods
        private void SetArguments(params object[] args)
        {
            this.Arguments = args.TryConvertToArray();
        }

        public override string ToString()
        {
            if (this != null)
            {
                var collection = this.Arguments.TryGetValidArguments<object>();
                return String.Join(this.Separator, collection).TryTrim();
            }
            return String.Empty;
        }
        #endregion
    }
}
