﻿namespace XString
{
    internal sealed class Sentence : XString.Base
    {
        #region Constructors
        /// <summary>
        /// Concatenates the elements of an object array, using a SPACE separator between each element.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// </summary>
        /// <param name="args"></param>
        public Sentence(params object[] args)
            : base(XLib.Text.Space, args)
        { }
        #endregion
    }
}
