﻿namespace XString
{
    internal sealed class Title : XString.Base
    {
        #region Constructors
        /// <summary>
        /// Concatenates the elements of an object array, using a TITLE separator between each element.
        /// <para>Null or WhiteSpace arguments are discarded!</para>
        /// </summary>
        /// <param name="args"></param>
        public Title(params object[] args)
            : base(XLib.Text.TitleSeparator, args)
        { }
        #endregion
    }
}
