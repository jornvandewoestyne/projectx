﻿using System;

namespace ProjectX.General
{
    public abstract class DisposableItem : IDisposable
    {
        #region Properties
        protected bool IsDisposed { get; set; } = false;
        #endregion

        #region Finalizer
        ~DisposableItem() => Dispose(false);
        #endregion

        #region Methods
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.IsDisposed)
            {
                return;
            }
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
                this.Finish();
                this.Unsubscribe();
                this.Unbind();
            }
            // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
            // TODO: set large fields to null.
            this.IsDisposed = true;
        }

        /// <summary>
        /// Finish class specific actions.
        /// </summary>
        protected abstract void Finish();

        /// <summary>
        /// Unsubscribe possible events.
        /// </summary>
        protected abstract void Unsubscribe();

        /// <summary>
        /// Set large fields to null.
        /// </summary>
        protected abstract void Unbind();
        #endregion
    }
}
