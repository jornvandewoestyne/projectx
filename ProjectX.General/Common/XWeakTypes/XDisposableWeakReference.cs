﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace ProjectX.General
{
    [SecurityPermissionAttribute(SecurityAction.InheritanceDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
    public class XDisposableWeakReference : IDisposable, ISerializable
    {
        private WeakReference handle;

        public XDisposableWeakReference(object target)
        {
            handle = new WeakReference(target);
        }

        public XDisposableWeakReference(object target, bool trackResurrection)
        {
            handle = new WeakReference(target, trackResurrection);
        }
        #region ISerializable

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            handle.GetObjectData(info, context);
        }

        #endregion

        #region Properties

        public Object Target
        {
            get { return this.handle.Target; }
            set { this.handle.Target = value; }
        }

        public bool IsAlive
        {
            get { return this.handle.IsAlive; }
        }

        public bool TrackResurrection
        {
            get { return this.handle.TrackResurrection; }
        }

        #endregion

        #region DisposePattern

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.handle = null;
            }

            GC.SuppressFinalize(this);
        }

        public void Dispose()
        {
            this.Dispose(true);
        }

        public void Close()
        {
            this.Dispose(true);
        }

        ~XDisposableWeakReference()
        {
            this.Dispose(false);
        }
        #endregion
    }
}