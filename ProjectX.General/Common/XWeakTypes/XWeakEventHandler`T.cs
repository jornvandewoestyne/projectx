﻿using System;
using System.Diagnostics;
using System.Reflection;

#region Source:
//https://michaelscodingspot.com/5-techniques-to-avoid-memory-leaks-by-events-in-c-net-you-should-know/
//https://paulstovell.com/weakevents/
#endregion

namespace ProjectX.General
{
    /// <summary>
    /// ...
    /// </summary>
    /// <typeparam name="TEventArgs"></typeparam>
    [DebuggerNonUserCode]
    public sealed class WeakEventHandler<TEventArgs> where TEventArgs : EventArgs
    {
        #region Properties
        private WeakReference Reference { get; }
        private MethodInfo Method { get; }
        #endregion

        #region Constructor
        public WeakEventHandler(EventHandler<TEventArgs> callback)
        {
            this.Method = callback.Method;
            this.Reference = new WeakReference(callback.Target, true);
        }
        #endregion

        #region Methods
        [DebuggerNonUserCode]
        public void Event(object sender, TEventArgs e)
        {
            var target = this.Reference.Target;
            if (target != null)
            {
                var action = Delegate
                    .CreateDelegate(typeof(Action<object, TEventArgs>), target, this.Method, true) as Action<object, TEventArgs>;
                action?.Invoke(sender, e);
            }
        }

        public string GetQualifiedName(string controlName)
        {
            var targetName = this.Reference?.Target?.GetType()?.Name;
            var methodName = this.Method?.Name;
            return StringHelper.QualifiedName(targetName, controlName, methodName);
        }
        #endregion
    }
}