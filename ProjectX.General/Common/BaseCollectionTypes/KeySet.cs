﻿using System.Collections.Generic;

namespace ProjectX.General
{
    public sealed class KeySet<T> : IKeySet<T> where T : struct
    {
        #region Fields
        HashSet<T> _items;
        IList<Notification> _notifications;
        #endregion

        #region Properties
        public HashSet<T> Items
        {
            get => this._items.ResolveNull();
            private set => this._items = value;
        }

        public IList<Notification> Notifications
        {
            get => this._notifications.ResolveNull();
            private set => this._notifications = value;
        }

        public bool HasNotifications => this.Notifications.ResolveNull().Count > XLib.MagicNumber.IntZero;
        public bool HasItems => this.Items.ResolveNull().Count > XLib.MagicNumber.IntZero;
        #endregion

        #region Constructor
        internal KeySet(IEnumerable<T> items, IList<Notification> notifications)
        {
            this.Items = new HashSet<T>(items.ResolveNull());
            this.Notifications = notifications.ResolveNull();
        }
        #endregion
    }
}
