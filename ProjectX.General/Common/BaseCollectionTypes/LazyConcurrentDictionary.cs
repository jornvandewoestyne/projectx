﻿using System;
using System.Collections.Concurrent;
using System.Threading;

#region Source
//https://endjin.com/blog/2015/10/using-lazy-and-concurrentdictionary-to-ensure-a-thread-safe-run-once-lazy-loaded-collection
#endregion

namespace ProjectX.General
{
    public sealed class LazyConcurrentDictionary<TKey, TValue>
    {
        private readonly ConcurrentDictionary<TKey, Lazy<TValue>> Dictionary;

        public LazyConcurrentDictionary()
        {
            this.Dictionary = new ConcurrentDictionary<TKey, Lazy<TValue>>();
        }

        //public bool TryGetValue(TKey key, out TValue value)
        //{
        //    var result = this.Dictionary.TryGetValue(key, out var lazyResult);
        //    value = lazyResult.Value;
        //    return result;
        //}

        //public bool TryAdd(TKey key, TValue value)
        //{
        //    //TODO: check!
        //    return this.Dictionary.TryAdd(key, new Lazy<TValue>(() => value));
        //}

        public TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory)
        {
            var lazyResult = this.Dictionary.GetOrAdd(
                key, k => new Lazy<TValue>(() => valueFactory(k), LazyThreadSafetyMode.ExecutionAndPublication));
            return lazyResult.Value;
        }

        //public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        //{
        //    //Node[] buckets = m_tables.m_buckets;

        //    //foreach (var item in concurrentDictionary)
        //    //{

        //    //}

        //    for (int i = 0; i < this.Dictionary.Count; i++)
        //    {
        //        // The Volatile.Read ensures that the load of the fields of 'current'
        //        // doesn't move before the load from buckets[i].
        //        //var current = Volatile.Read<Node>(ref buckets[i]);

        //        //while (current != null)
        //        //{
        //            yield return new KeyValuePair<TKey, TValue>(current.m_key, current.m_value);
        //            current = current.m_next;
        //        }
        //    }
        //}
    }
}
