﻿using System;

namespace ProjectX.General.Localization
{
    public sealed class Translation : BaseClass
    {
        #region Properties
        public string Key { get; private set; }
        public string Value { get; private set; }
        public bool IsFound { get; private set; }
        public SupportedLanguage Language { get; private set; }
        #endregion

        #region Constructors
        internal Translation(Enum enumType)
            : this(enumType.ToString(), XLib.Preference.CurrentLanguage, false)
        { }

        internal Translation(Enum enumType, SupportedLanguage language)
            : this(enumType.ToString(), language, false)
        { }

        internal Translation(Enum enumType, SupportedLanguage language, bool translateArguments, params object[] args)
            : this(enumType.ToString(), language, translateArguments, args)
        { }

        internal Translation(string key)
            : this(key, XLib.Preference.CurrentLanguage, false)
        { }

        internal Translation(string key, SupportedLanguage language)
            : this(key, language, false)
        { }

        internal Translation(string key, SupportedLanguage language, bool translateArguments, params object[] args)
        {
            this.Language = language;
            this.Value = this.Key = key.TryTrim();
            var translator = AppLibrary.Translator;
            if (translator != null)
            {
                var translation = translator.TryTanslate(this.Key, this.Language, translateArguments, args);
                this.Initialize(translation.Key, translation.Value, translation.Language, translation.IsFound);
            }
        }

        public Translation(string key, string value, SupportedLanguage language, bool isFound)
        {
            this.Initialize(key, value, language, isFound);
        }
        #endregion

        #region Methods
        private void Initialize(string key, string value, SupportedLanguage language, bool isFound)
        {
            this.Key = key;
            this.Value = value;
            this.Language = language;
            this.IsFound = isFound;
        }

        public override string ToString()
        {
            return this.Value;
        }
        #endregion
    }
}
